---
author: Jeff
title: Environmental Evils of Electricity
slug: evils-of-electricity
id: 1997
date: '2012-08-17 08:51:15'
layout: draft
categories:
  - Opinions
---

I was reading an article on Forbes entitled [The Naked Cost of Energy -- Stripping Away Financing and Subsidies](http://www.forbes.com/sites/jamesconca/2012/06/15/the-naked-cost-of-energy-stripping-away-financing-and-subsidies/), in which the author describes the actual cost of electrical production and which form is most cost-effective. I was not surprised that solar power is one of the most expensive forms of electricity there is, the equipment is difficult to manufacture, requires weird and wonderful materials, and is quick to break-down. However, I was _very_ surprised to learn that Gas is the second most expensive form of electricity. Having lived most of my life in Alberta, Gas was a regular component of people's home energy strategies. People had gas stoves, gas furnaces, even gas driers and refrigerators and automobiles. Many people switched from electric to gas because of the cost savings. While the appliances were always more expensive, they were also significantly more cost effective over time, and many a person bragged of a great deal they received on a gas appliance. So if gas is cheaper than electricity, how can gas be so expensive according to this study? Well, the study is looking at the cost of producing electricity.