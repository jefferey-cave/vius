---
id: 1043
title: Snow Management
date: 2011-02-05T19:00:30+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1043
permalink: /2011/02/05/snow-management/
categories:
  - Living
---

<figure>
 <img src="/media/2011/02/its_a_long_way.jpg" width="300" height="225" />
 <figcaption>It's a long way to the house from the barn</figcaption>
</figure>

A couple of days ago, we had a snow storm. The snow is waste deep; it's going
to rain tonight; and some
[dough-head](/media/2011/01/ChristmasSuspenders.20101225.jpeg) put all the
firewood in the barn. I just spent the entire afternoon digging out a trail
from the barn to the basement. Suddenly, the reality of managing this much
snow becomes crystal clear.

This isn't the first time I've had to dig out this trail of mine, and the
novelty of it is starting to wear a little thin. After having spent the entire
afternoon moving the snow, I've learned a few things about moving snow.

# Get the right Shovel

When we moved in, the prior owner had left a snow shovel in the barn. This
was great! I had a shovel for moving the snow.

The problem with Snow Shovels is they are designed for plowing the snow.
Plowing is the process of <q>pushing</q> the snow to a better place.

<figure>
 <img src="/media/2011/02/snow_piles.jpg" alt="Huge Piles of Snow" width="300" height="225" />
 <figcaption>Some of the piles of snow are bigger than I am.</figcaption>
</figure>

Uh&hellip; no&hellip;

Instead, I went to the hardware store and got a Grain Scoop. This is a great
tool for lifting and moving large amounts of snow. I can actually **dig** with
this tool. It's made out of light aluminium, which is nice when I have to lift
it the thousandth time in a day. Be careful with ice, aluminium is not the
right tool for chipping; for that I have a rusty old steel shovel I found in
the barn. While I don't have any, I'm thinking chemical de-icer is going to be
my new best-friend.

# Shovels are wider than people. Wheelbarrows are wider than shovels.

The first time I dug the trail, I dug it one shovel's width: no point in
doing more work than necessary. Done with that chore, I went back, grabbed
the wheel-barrow and instantly hit snow. The stupid legs hit the snow on the
sides of the trail. Back to the shovel.

In order to get the wheelbarrow through, the path must be about three shovel
widths wide. As I was digging, and getting more tired, <q>wide enough</q>
started to get narrower and narrower, until I was down to only one shovel's
width. The key was to find some _handy_ measuring tool to keep me honest.

<figure>
 <img src="/media/2011/02/shovel.jpg" alt="The handle of my shovel" width="300" height="225" />
 <figcaption>
The handle of my shovel is about the right width for my snow trails
 </figcaption>
</figure>

# Snow is Heavy.

When the snow is three feet deep, you aren't lifting it all in one go. You
need to break the job down. I really helps to <q>cut</q> chunks of snow out to
be dug. Basically, chisel out the area you want to remove, cut any big chunks
into little ones, and start shovelling the resulting debris. This also results
in nice straight tidy walls (The Wife is rolling her eyes again).

<figure>
 <img title="Chiselling Snow" src="/media/2011/02/digging_snow2.jpg" alt="Cutting off managable amounts of snow" width="300" height="225" />
 <figcaption>
Rather than trying to move all the snow at once, I flip the shovel over and
slice of a managable amount
 </figcaption>
</figure>

# Wet Snow is Heavier.

Just before the blizzard, I had dug the path out&hellip; again, only to have
the blizzard fill all my trails. I had had enough. I wasn't going to move more
snow, only to have it filled in again by Bestla. I checked the weather report,
and sure enough there was more snow on the way, so I waited, and waited
&hellip; it was getting warmer.

This morning the snow-plough guy came to finish the driveway and mentioned the
upcoming rain. Freezing rain. Hmmm&hellip; Maybe I should move the three feet
of snow before it is turned into three feet of ice. I get out there with my
shovel and og to lift the first shovel full&hellip;. of wet melted snow. It
has been sitting there for three days with the sun beating down on it and now
it weighs a ton. To top it off, when you throw the wet snow, half of it sticks
to the shovel, which means you have to lift the weight, but you haven't got
rid of the snow. You have to lift it again, and again, and again&hellip;.

I hurt everywhere this evening.

In the future, I can't put it off. It needs to be done immediately.

# The Shortest Distance between two points

When the trail is completely buried, and I have to dig a new one. I will walk
out to the barn, watching the door, before I even dig my first shovel full.
This way, when I've got my head down digging, I can just follow my foot prints
through the snow, and not have to look up to see if I am digging the most
direct route to the barn. The short walk has saved me a lot of work.<a href="#f-1">1</a>

Having said that: The shortest distance between to points&hellip; is moving
the two points closer to one another.

I have begun to strategize about different ways to reduce the amount of snow
that needs clearing. Basically, I'm determining which parts of the property I
can write off for the winter.For starters, I am not putting the wood on the
other side of the property. That's just dumb. There is no point in trucking
the stuff all that distance every couple of days.

Next year, I plan on putting as much of the wood in the basement as possible.
I have a spot all picked out that it will store nicely in, stay dry, and be
very convenient. That spot won't fit an entire year's worth of wood, so I
imagine the rest will go in the garage.

I've already determined that the chickens are going to go into the shed next
to the garage. There is a door between the two, which means I don't have to
walk through miles of waste deep snow just to get to them. Even if I do, I
don't need to dig a trail.

Maybe I should look into snow shoes as well.

# Hang your doors correctly

The hardest part of digging the trail is clearing the doorways to the barn and
garage. There is always so much snow that has crashed off the roof of these
two that the snow is twice as deep as anywhere else. This snow has to be
completely cleared before the doors will even move.

After the first time I moved all that snow, I started to envision huge open
porches in front of each of the doors, with metal roofs. The snow can come
crashing down and will be diverted to another place (somewhere, not the door).
I started describing the venture to The Wife, whose eyes got a little wide
and worried. We had guests over that night and I described it to them: big
heavy beams, strategically placed roofs, nice deep workspaces, &hellip;

<q>Why don't you just hang your doors so they open inwards?</q>

Well&hellip;

Uh&hellip;

What&hellip;

Oh&hellip;

I'll tell you why! Because it is so blindingly obvious that I should have
already thought of it!

Come spring, I imagine I'll be rehanging the garage door.

In my defence, the barn door is a sliding door, so I can't rehang it. That
door has just been inappropriately positioned.


<figure>
 <img src="/media/2011/02/barn_doors.jpg" alt="The Barn" width="300" height="225" />
 <figcaption>
INCORRECT: The barn doors are positioned so that snow falls from the roof and
buries the door.
 </figcaption>
</figure>

<figure>
 <img src="http://www.barnsbarnsbarns.com/maplecreek.jpg" width="341" height="227" />
 <figcaption>CORRECT: the snow slides off and does not cover the door.</figcaption>
</figure>


# Footnotes

<aside>
 <ol>
  <li id="f-1">
   <p>
the snow that has fallen since my first trail, has covered an embarrassingly
zig-zaggy trail. I probably moved twice as much trail as necessary, and
following the path with a full wheel barrow is interesting
<a href="#rf1-1043" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
   </p>
  </li>
 </ol>
</aside>
