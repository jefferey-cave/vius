---
id: 401
title: Relaxing Day
date: 2010-06-23T23:29:25+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=401
permalink: /2010/06/23/relaxing-day/
categories:
  - Moving
---
Today has been really nice, it has been our first full day where we have not had to be somewhere. We have booked our current room for 3 nights, so we are able to just kick back and relax for a couple of days.

It started off with a fox hunt (with a camera). This morning when I went out for my first cigarette, I noticed something moving near the cliffs. It turned out to be a fox. I ran back to grab  The Wife and we ended up walking along the top of the cliffs following it while it tried to get away along the beach. It was really neat to be so close.

We went around to a couple of the local farms today. There was a sheep farm and a goat farm: both specialising in wool. Christina at the goat farm was very friendly and nice. Turns out she has done what we are looking to do. As soon as she found out we are looking to move in and start a farm she told us we needed to move into the Tatamagouche/Pictou area. Both her and her husband took the leap to rural living four years ago, leaving behind professional jobs in the city. She says it was a good move, and that's good to hear. We spent a long time talking to her about some of the problems they have experienced and ways they have solved them. We also spent a bunch of time petting the pig, donkey, and watching the gander get in a scrap with one of the ducks<sup id="rf1-401"><a href="#fn1-401" title="The duck was fine, he was just trying to get it on with the gander&rsquo;s lady and the gander had to let the duck know it wasn&rsquo;t how the system works" rel="footnote">1</a></sup>.

Later  The Wife and I went for a walk down by the beach during low tide. We poked at snails and dead crabs, and even came across a couple of Jelly Fish on the beach. I picked up a stick and started poking at one, then did a really dumb thing, I touched the end of the stick that I had been poking at the Jelly Fish with.

Sure enough the venom made it to the stick, from the stick to my hands, from my hands to my eyes. I didn't want to fess up and for the next hour walked around the beach sniffling and feeling my eyes burn beneath my sunglasses. By the time we got back to the cottage, my eyes were very red.

Chimutisk has been passed out all day. We took him for a walk this morning, but the wind scared him and we had to come back. He hasn't ventured out of his cage since.

<div id='gallery-5' class='gallery galleryid-401 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon portrait'>
    <a href='/media/2010/06/IMG_0066.jpg'><img width="113" height="150" src="/media/2010/06/IMG_0066.jpg" class="attachment-thumbnail size-thumbnail" alt="Chimutisk&#039;s First Day at the Beach" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0069.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0069.jpg" class="attachment-thumbnail size-thumbnail" alt="Fox By the Sea" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0070.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0070.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0068.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0068.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0071.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0071.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0072.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0072.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0073.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0073.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0074.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0074.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0077.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0077.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0079.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0079.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0080.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0080.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0081.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0081.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0082.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0082.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0083.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0083.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0076.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0076.jpg" class="attachment-thumbnail size-thumbnail" alt="Coast at Emily&#039;s Cottages" aria-describedby="gallery-5-443" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-5-443'> Coasst at Emily's Cottages </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='/media/2010/06/IMG_0078.jpg'><img width="150" height="113" src="/media/2010/06/IMG_0078.jpg" class="attachment-thumbnail size-thumbnail" alt=" The Wife pointing out to sea" /></a>
  </div></figure>
</div>

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-401">
    <p>
      The duck was fine, he was just trying to get it on with the gander's lady and the gander had to let the duck know it wasn't how the system works&nbsp;<a href="#rf1-401" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>