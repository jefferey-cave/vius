---
id: 1807
title: Firefighter School
date: 2012-02-29T16:27:22+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1807
permalink: /2012/02/29/firefighter-school/
image: /files/2012/03/IMG_00451.jpg
categories:
  - Living
---
I just got back from my first weekend at firefighter school. What a blast.

<!--more-->

This is going to take a long time: every other weekend for 6 months. This is hard work, a lot of valuable time, and (most importantly) takes me away from The Wife. I really hope the people around town appreciate what we go through.

I did pass all of my tests. This weekend we were doing Personal Protective Equipment (PPE) and Self Contained Breathing Apparatus (SCBA). The two practical tests were getting from street clothes to PPE in 60 seconds, then PPE to full SCBA in 60 seconds. That is kind of tricky, but got both.<figure id="attachment_1810" style="max-width: 640px" class="wp-caption alignright">

<a href="http://vius.ca/2012/02/firefighter-school/img_0045-2/" rel="attachment wp-att-1810"><img class="size-full wp-image-1810" title="FirefighterSchool" src="/media/2012/03/IMG_00451.jpg" alt="Firefighter School - first class" width="640" height="480" /></a><figcaption class="wp-caption-text">Left to right: Kavius, Zach, Gary, Wes</figcaption></figure>