---
id: 1801
title: Maple Syrup
date: 2012-03-02T15:36:17+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1801
permalink: /2012/03/02/maple-syrup/
categories:
  - Ag Notes
---
The sap started flowing on Saturday. We tapped two trees behind the barn. The big one fills two pop-bottles twice a day, the other did not seem to produce anything. No other trees seem to be doing anything in the area.

<!--more-->

Since none of the other trees were doing much, we went up our back road to the edge of the field. Sure enough there were three maple bunches just off the road along the back of the field. We put 12 spiles in three bunches, each of these trees was dripping sap before I could even get the spiles into the drilled holes. Hopefully, these trees will be as prolific as the big one behind the barn.

According to <a title="Cornell Maple FAQ" href="http://maple.dnr.cornell.edu/FAQ.htm" target="_blank">one site</a>, you can figure about 1 litre of syrup per tap (that's about 40 litres of sap per tap). With ~15 taps in place, that's about 15 litres of syrup, divide by two because I have no idea what I'm doing, and we should able to get 7 litres of syrup out of it. Not enough to sell, but definitely a nice haul for the first year.

We shall see.