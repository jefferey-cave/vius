---
id: 399
title: First Day in Nova Scotia
date: 2010-06-21T23:13:05+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=399
permalink: /2010/06/21/first-day-in-nova-scotia/
categories:
  - Moving
---

First up&hellip; I'm a little annoyed with the motel (Hillcrest Motel,
Pugwash, Nova Scotia). The place is very new, still smells of new carpet,
however this means that there is no internet yet (internet was advertised),
and no phone in the room. I believe the owner has been spending too much time
watching the Home & Garden channel as well; the room has a lot of very nice
furniture, but its layout means that the room is unusable as a motel room: not
enough counter space for toothbrushes and suitcases. It's unfortunate, but at
this point I'm too tired to worried about it.

Today we made two touristy stops: we saw the longest covered bridge in the world, and we stopped at the Magnetic Hill. Magnetic Hill was lame, about the only cool part about it was the fact that I got my trailer turned around at the bottom, that was an awesome feat of driving: single lane road, car with a long wheel base. The covered bridge was interesting, but it was  The Wife's detour, so I'll let her tell of it.

Driving in Nova Scotia is interesting, narrow road, no shoulder, 90 k and hour&hellip; On the highway, you are expected to slow down for construction workers&hellip; to 90. Alberta driving laws are for wusses!

<figure>
 <img width="150" height="113" src="/media/2010/06/IMG_0061.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/IMG_0062.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/IMG_0063.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/IMG_0065.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/IMG_0067.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/IMG_0059.jpg" />
 <figcaption>Entrance sign for province of Nova Scotia </figcaption>
</figure>
