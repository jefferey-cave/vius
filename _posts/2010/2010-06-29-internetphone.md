---
id: 466
title: Internet/Phone
date: 2010-06-29T18:29:44+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=466
permalink: /2010/06/29/internetphone/
categories:
  - Uncategorized
---
We just stopped at the local Bell Store. We should have Internet and a house phone on July 8th. Until then, you may not hear much from us&hellip;

Kavius has also changed his cell phone number, if you want the number, feel free to send an email or use the contact form. Obviously, I'm not sending it out over the web.