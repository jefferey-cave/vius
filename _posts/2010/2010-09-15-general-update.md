---
id: 624
title: General Update
date: 2010-09-15T05:57:44+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=624
permalink: /2010/09/15/general-update/
categories:
  - Updates
---
Well, its official, we have the keys to our new place and have been out to visit it for a weekend. We haven't yet spent a night, but instead drove stuff out each day.

We have really enjoyed the 7 apple trees (some have tasty apples, others not so much) and all the black berry bushes. Everytime we go out, we eat an apple or two; unfortunately, it appears the deer got to the berries before us on the last trip.

On Saturday afternoon, we went out to visit with some new friends we have made in the area. They gave us a tour of their property, and introduced us to their chickens. We even got a look at the falcon that has been hanging out on their property, eating their chickens (I was more impressed than AJ who first saw it while it was gnawing on a chicken right in front of him).

 The Wife has been acquiring furniture like mad. She even picked me up a rocking chair! We really do have a couple of beautiful pieces, I'm just not sure how we are going to move it all.

Pumpkin is coming to visit us on for a week and a half starting Saturday. I've missed her, and am looking forward to seeing her.