---
id: 172
title: Our Life in Boxes
date: 2010-06-05T23:56:41+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=172
permalink: /2010/06/05/our-life-in-boxes/
categories:
  - Moving
  - TheWife
---

We started packing. I know, it's getting late, but up until this point we have been staring at the 24+ boxes of books we had packed earlier wondering where to start. (Did you know you can stack 24  paper boxes, cover them in a quilt and it looks just like a bed? Thank you [Rosemary](http://www.vius.ca/testimonials/ron-clappison/ "Ron Clappison Testimonial") for help with staging our house).

Deciding what to keep  is a challenge. What is worth the cost of moving and storing it? I find myself being brutal and discarding all sorts of useful stuff, then coming across my old Post Office Savings book and wanting to keep it (the last time it was updated was before decimalization of the currency in Britain). I guess you can't explain the attachment you have to some things.

We have decided not to take much with us. Our daughter has laid claim to most of the furniture and knick-knacks, and what she doesn't take is going to be hauled to our local Goodwill Depot.

The movers will arrive on Thursday and once our stuff ((everything we have left)) has gone, we will be living out of our camping gear until our stuff arrives in Halifax, in a few weeks time. It is somewhat difficult to know what we'll need from now until the beginning of July; Kavius still has to find out the dress code for his new job.

Tomorrow I will do the last bit of baking for our trip, then the kitchen will be packed away. We will live on leftovers and crackers and cheese for the next week&hellip; Kavius will be as happy as a clam.

~  The Wife
