---
id: 152
title: 'Last Mortgage Payment&hellip; EVER!'
date: 2010-06-02T03:51:31+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=152
permalink: /2010/06/02/last-mortgage-payment-ever/
categories:
  - Opinions
  - Homesteading
---

 The Wife and I live a very simple lifestyle. That is part of what brought us together<sup id="rf1-152"><a href="#fn1-152" title="really its a similar attitude toward life in general, of which living a simple life is a symptom" rel="footnote">1</a></sup>. Today, all the scrimping and saving paid off a little. From this day forward, we should never have another mortgage payment for our home.<sup id="rf2-152"><a href="#fn2-152" title="Just as our interest rate went up too!" rel="footnote">2</a></sup>

This is huge. Prior to this, nearly $1000 every month was required to go to the bank to pay for our home. From this day forward, that money stays in our pockets to go towards other things. I have to confess, we did cheat a little bit. As of this payment we still have $7270.12 outstanding<sup id="rf3-152"><a href="#fn3-152" title="~1 year, something I&rsquo;m still proud of, even with the cheat" rel="footnote">3</a></sup>, but with the sale of our house that goes away.

My mother has always been worried that  The Wife and I are impoverished because she has never seen our wealth. I feel differently, I feel pretty wealthy knowing that the things I own, I own outright. From now on, almost<sup id="rf4-152"><a href="#fn4-152" title="I say almost because I can&rsquo;t figure out a way to not pay taxes" rel="footnote">4</a></sup> everything I earn, I get to keep.

Simplicity makes for a good life.

<aside>
<ol class="footnotes">
  <li id="fn1-152">
    <p>
      really its a similar attitude toward life in general, of which living a simple life is a symptom&nbsp;<a href="#rf1-152" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn2-152">
    <p>
      Just as our interest rate went up too!&nbsp;<a href="#rf2-152" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn3-152">
    <p>
      ~1 year, something I'm still proud of, even with the cheat&nbsp;<a href="#rf3-152" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn4-152">
    <p>
      I say almost because I can't figure out a way to not pay taxes&nbsp;<a href="#rf4-152" class="backlink" title="Jump back to footnote 4 in the text.">&#8617;</a>
    </p>
  </li>
</ol>
</aside>
