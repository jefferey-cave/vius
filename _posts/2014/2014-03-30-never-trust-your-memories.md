---
id: 2149
title: Never Trust Your Memories
date: 2014-03-30T21:12:36+00:00
author: Kavius
layout: post
guid: http://vius.plaidsheep.ca/?p=2149
permalink: /2014/03/30/never-trust-your-memories/
categories:
  - Musings
---
I just had the most fascinating experience of falling down the stairs today.

Ice storms have ripped through my city, and as I stepped out the front door to have a smoke, my feet shot out from under me and down the stairs I went. While no bones cracked, they certainly creaked, and I have spent the rest of the day sleeping and healing up, leaving me with a lot of time to think about my experience.

I remember it vividly:

  1. Opened the door, noticed ice.
  2. Felt the deck and confirmed it was slippery
  3. Brought my second foot out and onto the deck.
  4. As soon as my second foot met my first, the momentum of my feet just carried them down the steps.
  5. I watched my feet go down the steps
  6. I reached out for something to grab onto (nothing but air)
  7. I felt my back meet the last step (with a thud, something other than air)
  8. I watched my heels dig two ruts into the sod at the bottom of the stairs.
  9. I remember not being able to breath, and getting up in the hopes it would allow me to breath easier (it didn't)
 10. The wife opening the door and peering out to see what the sound was. (I distinctly remember waving her back inside, as I couldn't get enough air in my lungs to yell to her)

Very scary, very stretched out, and very vivid; but also not real.

Note steps 6 and 10, they contradict one another. I remember stepping out, and reaching for something to hold onto; I even have scrapes on my hands where they finally slammed into the decking; but if the door had been open (it opens outward), and I had been standing in the doorway, I would have had something to hold onto. Also, I was totally cognisant of The Wife opening the door, and me panicking that she would get injured coming out to find me. She knows she had to open the door, also she would have heard more clearly if the door had been opened.

I **must** have closed the door before slipping.

I must have closed the door, but I distinctly remember **not** closing it.

The Wife and I have discussed false memories in the past, and this experience serves to remind me that we cannot trust even our own experiences and memories. It reminds me that we must always test, verify, and approach life with a healthy dose of scepticism, because the easiest person to fool is yourself.

It also reminds me to salt the steps **prior** to an ice storm, and to keep an adequate supply of Ibuprofen in the house.

<a title="False Memories - Wikipedia" href="http://en.wikipedia.org/wiki/False_memory" target="_blank">http://en.wikipedia.org/wiki/False_memory</a>

&nbsp;