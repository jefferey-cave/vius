---
id: 1167
title: This is My Last Post
date: 2011-02-25T00:00:06+00:00
author: Kavius
excerpt: |
  Many years ago, I was having a discussion with some guy at a party on some issue or another. It was a really interesting debate with fascinating points flying back and forth between the two of us. We had accumulated quite a crowd around us. Eventually, as the discussion grew more and more heated, he stated, “I don’t think we are in the same ball-park on this one”.
  
  One of the guys in the crowd burst out laughing: “Kavius isn’t even playing ball. He’s off in the forest communing with the wolves.”
layout: post
guid: http://vius.ca/?p=1167
permalink: /2011/02/25/this-is-my-last-post/
enclosure:
  - |
    http://vius.ca/wp-content/uploads/Kleskivania.mp4
    17327309
    video/mp4
    
  - |
    http://vius.ca/wp-content/uploads/Kleskivania.mp4
    17327309
    video/mp4
    
categories:
  - Uncategorized
---
I have decided to retire Vius.

Many years ago, I was having a discussion with some guy at a party on some issue or another. It was a really interesting debate with fascinating points flying back and forth between the two of us. We had accumulated quite a crowd around us. Eventually, as the discussion grew more and more heated, he stated, <q>I don't think we are in the same ball-park on this one</q>.

One of the guys in the crowd burst out laughing: <q>Kavius isn't even playing ball. He's off in the forest communing with the wolves.</q>
  
<!--more-->


  
I don't accept the status-quo: the status quo sucks. Moving to rural Nova Scotia is about seeking out a better way of living, a way that others may not have considered. Vius became about questioning the things we take for granted and looking into, not only the causes of the effects, but also the cause of the cause of the cause of the effect.

I think Vius has ended up in the woods, communing with the wolves.

Based on some feedback from people who have read my posts, I am concerned that people will begin to misinterpret my writing, and therefore begin to misinterpret who I am. For this reason, I have decided to discontinue any further posting on Vius.

I would like to thank those who have read these <q>rantings of a madman</q>, and have taken the opportunity to challenge my views, or to ask questions to better understand them. That is everything I could hope for in essays such as these: open, civil, and interesting dialogue between disagreeing parties.

Vius will likely find a new use at some time in the future but for now, I will allow these essays to remain and collect dust until I can find a more appropriate home for them. For those of you just <q>blog stalking</q> us, that would like to keep up with the development of our homestead, please head on over to The Wife's blog: <q><a title="Quilting The Farm" href="http://quiltingthefarm.vius.ca" target="_self">Quilting the Farm</a><q>. She is doing an excellent job of keeping a journal of general goings-on.

[flowplayer src=</q>http://vius.ca/wp-content/uploads/Kleskivania.mp4</q>]

<span id="Update">

<h2>
  Update
</h2></span> 

> The honest man tries to speak to the mind of his listeners, not to their ears, because he is confident in the inherent strength of his ideas. He will even accept unflattering names for his position, and grant flattering names to his opponent's position, if that will but put an end to the distracting word games and allow the true debate to begin.
> 
> &#8212; <a title="Overt and Covert Socialism" href="http://mises.org/daily/5080/Overt-and-Covert-Socialisms" target="_blank">Daniel James Sanchez</a>
> 
> &nbsp;