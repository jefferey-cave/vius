---
id: 2131
title: The more things change
date: 2013-07-30T01:19:41+00:00
author: Kavius
layout: post
guid: http://vius.plaidsheep.ca/?p=2131
permalink: /2013/07/30/the-more-things-change/
categories:
  - Musings
---


> &hellips; and they shall beat their swords into plowshares, and their spears into pruning hooks: nation shall not lift up sword against nation, neither shall they learn war any more.
> 
> &mdash; Isaiah 2:3-4

---

> The War That Will End War
> 
> &mdash; HG Wells (1914)

---

> This war, like the next war, is a war to end war
> 
> &mdash; David Lloyd George
