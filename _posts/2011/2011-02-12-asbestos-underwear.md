---
id: 1121
title: Asbestos Underwear
date: 2011-02-12T00:00:57+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1121
permalink: /2011/02/12/asbestos-underwear/
categories:
  - Living
---
Thursday was our weekly run into town to pick up things we didn't realize we needed until we didn't have them. Today's grand purchase, that I am very excited about, was Asbestos Underwear! Ok, so they were fire resistant gloves, I just like the sound of the word Asbestos.

Since December, I have been tending a wood fired furnace to keep our house warm, and not all of the wood is small. This means I either have to chop it up or (if the fire is hot enough) carefully manoeuvre giant logs through a tiny door to gently place the log in just the right place without smashing it through the brick work.

<!--more-->In doing so I have learned two very important things:

  1. Metal doors heat up when used to contain a fire for the entire day
  2. There is a direct relationship between my touching metal doors on furnaces, and a strong soap-like taste in my mouth.<figure id="attachment_1124" style="max-width: 210px" class="wp-caption alignright">

[<img class="size-full wp-image-1124 " title="FireGloves" src="/media/2011/02/FireGloves.jpg" alt="Steven Raichlen SR8038 Extra Long Suede Gloves, Pair" width="210" height="210" />](http://www.amazon.ca/gp/product/B0007ZGURU?ie=UTF8&tag=vius-20&linkCode=as2&camp=15121&creative=390961&creativeASIN=B0007ZGURU)<figcaption class="wp-caption-text">Heat & Fire Hand-Protectors</figcaption></figure> 

So, in defence of my dainty little fingers, I purchased a pair of fire resistant gauntlets. I thoroughly expect this investment to pay for itself quickly through reduced usage of burn salve and soap.

I am very excited to have got the last pair from the local hardware store and am itching to put them on and test them out. Having said that,  I am a starting to have a doubt. I just looked online and found a pair for $100&hellip;

I paid $13.