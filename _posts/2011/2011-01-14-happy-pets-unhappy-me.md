---
id: 748
title: Happy Pets = Unhappy Me
date: 2011-01-14T00:00:42+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=748
permalink: /2011/01/14/happy-pets-unhappy-me/
categories:
  - Living
---

<blockquote style="border: 3px solid #dbad2b;background-color: #ffffe0">
 For those interested, I recently added profiles for our two pets:
 <a href='http://www.vius.ca/about/chimutisk/'>Chimutisk</a>
 and
 <a href='http://www.vius.ca/about/rabbit-stew/'>Stew</a>.
</blockquote>

Before we left Calgary, I wrote about being grateful for having healthy pets.
After watching the animals for the last couple of weeks, The Wife and I were
musing that its nice that they are happy too.

After we purchased our
[Hundred Acre Wood](http://www.vius.ca/2010/08/we-have-our-new-home/ "Our Hundred Acre Wood"),
we spent a couple of weeks moving stuff in with the intent of getting enough
furniture for us to spend whole weekends at the property. This meant locking
up Stew in his cage, and packing up Chimutisk into his kennel for the two hour
drive to and from the property. The first time we did this it was a total
fiasco.

For Stew, the idea of spending _any_ daytime hours in his cage is abhorrent.
Putting him in his cage, during daylight hours is a tricky.<a href='#f-1'>1</a>
It involves trying to catch him while he hides behind very piece of furniture
you have. Pull the furniture away, and he bolts for the next hiding spot.
Eventually, you just give up, wave a carrot in his face, and throw the carrot
in the cage; when he goes for it, you slam the cage shut. It is best to do
this just before you leave so you don't have to listen to him rattling the
cage door.

While driving we discovered Chimutisk can't <q>hold it</q> as long as he used
to. We needed to ensure we stopped every hour for bathroom breaks, or he wets
his kennel. We discovered this the hard way on that first trip; twice.

Was it worth the pain? That depends on who you ask.

While we were travelling back and forth, getting Chimutisk in his carrier was
was simple enough; he was terrified he was going to be left behind. Every
Saturday morning we would start packing, and he would be in a panic if his
carrier was not on the floor with the door open so he could wait inside. If I
took some things to the car, he would panic that he wasn't included in the
stuff taken to the car.  The Wife would have to pick him up and hold him,
whimpering, until I came back. He would arrive at the house, his tail would
[Bottle Brush](http://www.everythingferret.com/ferret_behavior.htm "Ferret Behaviour"),
resulting in him passing out from the excitement.

Now that we live here, every morning Chimutisk wakes up and goes to the
kitchen for breakfast. As soon as he has had his breakfast,  his tail
instantly goes
[Bottle Brush](http://www.everythingferret.com/ferret_behavior.htm "Ferret Behaviour"),
he runs (actually walks **really fast**&hellip; he's old) to the bathroom and
spends the next 5 minutes rolling around on the bathroom rug, scratching his
back. This leaves him totally exhausted so he goes back to his cage and passes
out (tail still bottle brushed). In the evenings, you will hear him wake up,
and start exploring the house&hellip; tail completely bottle brushed again.
One minute of bottle brushed tail later, he is in his couch grooming himself
and settling in for the evening.

Seeing Chimutisk so excited, _every day_ definitely shows how wonderful this
place is for him.

Stew is another kettle of fish. He wakes up in the morning and starts doing
[Binkies](http://www.fuzzy-rabbit.com/behaviourfaq.htm) immediately. Generally
there is a race between the fridge (where his breakfast is kept) and his cage
(where he is given breakfast); he runs from one to the other waiting for the
food to be delivered. Once the food is delivered, its over to the living room
(no he doesn't bother eating, its too exciting) to start doing the morning
Binkies. After 15 minutes of that, he starts his morning rounds ensuring that
the curtains, couches, and ferret are exactly where he left them. If something
has changed, he will generally spend extra time investigating whether it is
something he can eat, climb, or move. After a couple of hours of this, he is
generally settled enough to sit down for the afternoon, and returns to his
cage; only to discover that _somebody put food in there_ (the breakfast he
never got around to eating), resulting in another 15 minute round of Binkies!

Seeing Stew so excited, _every day_ definitely shows how wonderful this place
is for him.

All of this excitement starts around 7am, by about 8am  The Wife and I are
exhausted. Chimutisk's bottle brushed rolling around on his scratch mat
generally atracts the attention of Stew, who wonders what the noise is. Stew
owns the bathroom, and decides to cuff Chimutisk to get him out, we have to
run in and separate the two of them. While we are carrying Stew to a different
room, Chimutisk discovers  The Wife's good fuzzy bath mat, and decides that
this is the greatest thing in the world to dig in: put Stew down, and go get
Chimutisk. While you are cleaning the bits of rug Chimutisk dug up, Stew comes
to investigate what you are working so diligently at. The smell of ferret
sends him on a marking spree where he has to rub his chin on everything in
**his** bathroom; leading him to mark the baseboard&hellip; which smells
really good&hellip; so good its worth a nibble. <q>STEW! Stop that!</q>,
sending him bolting for the door (generally with two circles around you on
the way), ears flopping away as he runs, just daring you to catch him.

Things have finally settled down, so you decide to go to the basement to start the furnace, only to have both animals follow you to the door to investigate what is down there. After shooing them both away and squeezing yourself and not them through the door, you end up coming back up to both of them laying in front of the door so you can't open it.

Sitting down at my desk to get some work done becomes an exercise in multi-tasking: typing, while either fighting Stew for my slippers or listening for him heading up the stairs, climbing the furniture, eating books, or getting bored enough to go into the ferret cage to pick a fight with the sleeping ferret.

I hate my critters:

* Stew trying to steal my slippers, while I'm wearing them.
* Chimutisk nesting in your clothes, just as you come out of the shower.
* Stew discovering the stairs can be climbed (and the tops of the furniture
  near by).
* Stew picking fights with Chimutisk.
* Chimutisk daring Stew to pick fights with him.
* Stew joining you on the sofa.
* Chimutisk wanting to be picked up, then put down, then up, down, up, down,
  up down updownup&hellip;
* Stew eating books, computer cables, and wires.
* Stew re-arranging curtains (with his teeth), and digging hardwood floors
  (tickity-tickity, tickity-tickity, tickity-tickity, tickity-tickity,
  &hellip;).
* Chimutisk digging in rugs (the more expensive, the better).
* Stew eating computer cables and wires.
* Stew stealing food, while you are eating it.
* Chimutisk hiding his food dishes.
* Stew freaking out that Chimutisk is being fed and he didn't get anything.
* Stew <q>helping</q> with repairs around the house'
* Chimutisk <q>helping</q> with chores around the house.
* Stew using **anything** you put down as a ladder to somewhere he shouldn't
  be (see: eating books).
* Chimutisk tipping waste baskets.
* Stew closing his cage door, then freaking out that his cage is closed
  (hasn't figured out how to open doors, just close).
* Either of them being quiet.

Some days, I wish we had <q>misplaced</q> them along the way.

# Gallery

<figure>
 <img width="113" height="150" src="/media/2011/01/FeedingTime.20110112.1.jpeg" />
 <figcaption>Chimutisk (ferret) and Stew (rabbit) eating together</figcaption>
</figure>
<figure>
 <img width="113" height="150" src="/media/2011/01/FeedingTime.20110112.2.jpeg" />
 <img width="150" height="113" src="/media/2011/01/FeedingTime.20110112.3.jpeg" />
 <figcaption>Stew and Chimutisk. Stew has lost interest in his food.</figcaption>
</figure>
<figure>
 <img width="150" height="113" src="/media/2011/01/FeedingTime.20110112.4.jpeg" />
 <img width="150" height="113" src="/media/2011/01/FeedingTime.20110112.5.jpeg" />
 <figcaption>Stew wondering if Chimutisk’s food is better</figcaption>
</figure>


# Footnotes

<aside>
<ol>
<li id='f-1'>
we actually have a ritual at night that has him eager to be locked up at night
</li>
</ol>
</aside>