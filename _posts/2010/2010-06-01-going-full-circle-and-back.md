---
id: 124
title: 'Going Full Circle&hellip;and back'
date: 2010-06-01T02:06:02+00:00
author: The Wife
layout: post
guid: http://www.vius.ca/?p=124
tags: [TheWife]
categories:
  - Moving
  - Homesteading
  - TheWife

---

We have decided to drive my car to Nova Scotia. We have gone through many different ways to get there but all have been thwart with problems in one way or another. So now we come full circle back to my car.

Option 1. Take my car with utility trailer in tow

Option 2. (this one was exciting at first) Buy a camperized van

Option 3. Take Kavius's car

Option 4. Buy a van conversion

Option 5. Buy a VW Westfalia

Option 6. Fly with WestJet

Option 7. Via Rail

Option 8.  **_Take my car with utility trailer in tow_**

I was really excited about **option 2**. We could travel in comfort, cook our own meals, stay at camp-grounds or even on the side of the road. This one certainly had possibilities. We went shopping. We actually found van sellers a strange bunch. One guy absolutely refused to let us have it mechanically inspected, another eventually said if we took it somewhere close by and paid for his gas money and let him drive it then it would be okay (he added he would throw in his time for free! I kid you not). Yet another advertised his van as being in great condition with NO RUST. We actually thought we were looking at the wrong van when I realised I could almost crawl through the holes in the bottom! Then there was the homemade one&hellip; All in all the vans were way to big, we would have problems with parking and they were expensive on gas.

And thus our van shopping came to a disappointing end.

I'll skip over option 3 quickly as Kavius's car isn't big enough, doesn't have air conditioning and doesn't have a hitch.

Option 4. A van conversion. Not as big as a camperized van, a comfortable ride with AC and other luxuries like a DVD player and a trailer hitch. For some reason I didn't feel comfortable; a little claustrophobic, and I had motion sickness before we had even started the engine! I put the kibosh on conversion vans.

Now to option 5. A Westy! I never actually wanted a hippy van&hellip;ever, but now this looked like the most promising option. A Westfalia conversion is small; much smaller than the vans we looked at and cheaper on gas. We went shopping. Westies are expensive, even the ancient ones, but we were willing to spend a little more for something we could use in the future. The only Westfalia we went to see needed work, okay, we could deal with a few things. The guy was selling it for a friend, well may be not a friend but an acquaintance, well may be not an acquaintance but a guy who left it in his driveway 3 years ago. There were red flags. How can you sell what isn't yours? How can a vehicle registered in Ontario 3 years ago be insured to test drive in Alberta and why did the guy try, with all his might, to _**not**_ sell us the van? We walked away. Perhaps that was best as we have since discovered VW Vanagons won't pull our tiny trailer.

Option 6 wasn't something we really wanted to do. Here we had a chance to drive this vast country and take a vacation. If we flew, that would mean selling a car, renting a car to tide us over, renting a car in Nova Scotia and paying for lots of extra baggage. It wasn't worth it.

Via Rail from Edmonton to Halifax. A beautiful trip, not too expensive and with the feel of a vacation. There was one major flaw in this option. Kavius's a smoker. The train stops only 5 times between Edmonton and Toronto! A trip, in close quarters with a smoker who can't have a smoke, isn't my idea of a fun filled vacation. If Kavius hadn't put the kibosh on this, I would have!

Now we are back to&hellip; Option 8.  _**Take my car with utility trailer in tow.**_ We have come full circle, and the <q>back</q> part, I'm still looking for a van!

~ The Wife
