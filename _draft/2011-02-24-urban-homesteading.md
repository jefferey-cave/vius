---
author: Jeff
title: Urban Homesteading
slug: urban-homesteading
id: 1158
date: '2011-02-24 07:27:39'
layout: draft
categories:
  - Uncategorized
---

As a software developer, copyright has always played an important role in my life. It may take me years to produce a piece of software, years during which I am not paid and recieve no compensation. Having poured your life into a product only to have it copied and used without payment can be disheartening, even worse is when you do all the work and someone else makes money selling it (yes, it has happened to me. Someone out there is rich, I'm not). In fact, one of the reasons I have moved to an agricultural setting, is my inability to continue to make a living writing software: I want a physical product that I can control the distribution of. Even after getting out of the industry, my younger brother will soon be graduating from University in the same field, and will have to face the same issues I have in the past. For these reasons I have traditionally been very supportive of Intellectual Property laws. However, as with all matters involving the use of force, I have been coming to realize that Intellectual Property is being used as an aggressive legal tool, rather than the defensive legal tool it was originally conceived as.

## Devraes Institute

The most recent [http://urbanhomestead.org/journal/2011/02/16/fyi-urban-homestead-trademark-matter/](http://urbanhomestead.org/journal/2011/02/16/fyi-urban-homestead-trademark-matter/)

## Conclusion

While I have always been a fan, in the past I have never agreed with Jeffery Tucker's position on Intellectual Property. He believes that Intellectual Property should be done away with. However, the example of "Urban Homesteading" has me asking the same question he does:

> So consider a world without trademark, copyright, or patents. It would still be a world with innovation — perhaps far more of it. And yes, there would still be profits due to those who are entrepreneurial. Perhaps there would be a bit less profit for litigators and IP lawyers — but is this a bad thing? [http://mises.org/daily/2632](http://mises.org/daily/2632)

As for the Devraes, and their trademarks; while they may be able to use intimidation to force us to not use certain terms, they still cannot force us to visit their website, or purchase their products. I would urge others to avoid (as much as possible) willingly handing money over (by purchasing products or services) to any organization that uses the threat of violence to force money from others.