---
id: 83
title: Renting in Halifax
date: 2010-05-25T00:00:18+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=83
tags: ['rent']
categories:
  - Opinions

---

So, I have a 6 month contract lined up in Halifax. I won't mention where until
I finish all the paper work but I will say that I start July 5. This is going
to make our transition to Nova Scotia a whole lot easier as now we will have
an income while we search for the perfect property. This reduces a bit of the
stresses around that.

Unfortunately, it causes a whole lot of different ones.

# Rent is Expensive

It's been a long time since either  The Wife, or I, have rented. We have both
owned our own homes for at least 5 years. I must say that rent rates caught us
a little off guard. One thing I found particularly interesting is that Calgary
is actually cheaper than Halifax.

Rental rates in Calgary and Halifax are equivalent, as best I can tell
(actually Halifax looks a little more expensive), but wages are signifigantly
lower (also income tax is higher). This means that my cost of living stays the
same, but my income goes down. Relatively speaking, this means that it is more
expensive for me to rent in Halifax than it is for me to rent in Calgary.

It actually works out to almost cheaper not to take the job and just move to
the country and rent there.

Is there really such a shortage of housing, in Halifax, that landlords can ask
that much more?

# One Year Leases

We have contacted about a half-dozen places about renting their properties,
and all of them have refused to even discuss anything shorter than a one year
lease. In one year I plan on having finished 6 months of work fixing up the
inside of my new home, and having started on the outside. After asking around
a little bit, things aren't any different in Calgary.

Is there really such a shortage of housing, in Halifax, that landlords can
afford to be that fussy?

<span style="font-size:large">Where are we going to live?</span>