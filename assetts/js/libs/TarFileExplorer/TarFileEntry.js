import ByteHelper from 'ByteHelper.js';

class TarFileEntry{
	constructor(header, dataAsBytes)
	{
		this.header = header;
		this.dataAsBytes = dataAsBytes;
	}

	// static methods

	static directoryNew(directoryName)
	{
		let header = TarFileEntryHeader.directoryNew(directoryName);

		let entry = new TarFileEntry(header, []);

		return entry;
	}

	static fileNew(fileName, fileContentsAsBytes)
	{
		let header = TarFileEntryHeader.fileNew(fileName, fileContentsAsBytes);

		let entry = new TarFileEntry(header, fileContentsAsBytes);

		return entry;
	}

	static fromBytes(chunkAsBytes, reader)
	{
		let chunkSize = TarFile.ChunkSize;
		let header = TarFileEntryHeader.fromBytes(chunkAsBytes);
		let sizeOfDataEntryInBytesUnpadded = header.fileSizeInBytes;
		let numberOfChunksOccupiedByDataEntry = Math.ceil(sizeOfDataEntryInBytesUnpadded / chunkSize);

		let sizeOfDataEntryInBytesPadded = numberOfChunksOccupiedByDataEntry * chunkSize;
		let dataAsBytes = reader
			.readBytes(sizeOfDataEntryInBytesPadded)
			.slice(0, sizeOfDataEntryInBytesUnpadded)
			;

		let entry = new TarFileEntry(header, dataAsBytes);
		return entry;
	}

	static manyFromByteArrays(fileNamePrefix, fileNameSuffix, entriesAsByteArrays){
		let returnValues = [];

		for (let i = 0; i < entriesAsByteArrays.length; i++){
			let entryAsBytes = entriesAsByteArrays[i];
			let entry = TarFileEntry.fileNew(
					fileNamePrefix + i + fileNameSuffix,
					entryAsBytes
				);

			returnValues.push(entry);
		}

		return returnValues;
	}

	// instance methods
	download(event)
	{
		FileHelper.saveBytesAsFile(this.dataAsBytes,this.header.fileName);
	}

	isNormalFile()
	{
		return (this.header.typeFlag == TarFileTypeFlag.Instances.Normal);
	}

	toBytes(){
		let entryAsBytes = [];
		let chunkSize = TarFile.ChunkSize;

		let headerAsBytes = this.header.toBytes();
		entryAsBytes = entryAsBytes.concat(headerAsBytes);

		entryAsBytes = entryAsBytes.concat(this.dataAsBytes);

		let sizeOfDataEntryInBytesUnpadded = this.header.fileSizeInBytes;

		let numberOfChunksOccupiedByDataEntry = Math.ceil
		(
			sizeOfDataEntryInBytesUnpadded / chunkSize
		)

		let sizeOfDataEntryInBytesPadded =
			numberOfChunksOccupiedByDataEntry
			* chunkSize;

		let numberOfBytesOfPadding =
			sizeOfDataEntryInBytesPadded - sizeOfDataEntryInBytesUnpadded;

		for (let i = 0; i < numberOfBytesOfPadding; i++)
		{
			entryAsBytes.push(0);
		}

		return entryAsBytes;
	}

	// strings

	toString(){
		let newline = "\n";

		let headerAsString = this.header.toString();

		let dataAsHexadecimalString = ByteHelper.bytesToStringHexadecimal(this.dataAsBytes);

		let returnValue =
			"[TarFileEntry]" + newline
			+ headerAsString
			+ "[Data]"
			+ dataAsHexadecimalString
			+ "[/Data]" + newline
			+ "[/TarFileEntry]"
			+ newline;

		return returnValue
	}

}
