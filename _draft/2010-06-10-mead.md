---
author: Jeff
title: Mead
slug: mead
id: 231
date: '2010-06-10 01:23:29'
layout: draft
categories:
  - Recipes
---

When I was a bachelor, and started keeping a budget to work out where all my money was going, and why I was broke all the time, I noticed one place where a lot of savings could be had: booze. You see, I drank a lot. I went out and partied with the boys all the time. Unfortunately, that meant a lot of money spent on alcohol. All of us had to come up with strategies for saving money on alcohol: one guy started by scotch, another cheaper beer, I started making my own. Cheaper beer is a reasonable solution. Unfortunately, I love my taste buds too much. Hard alcohol is definitely cheaper per alcohol molecule, but then there is all the mix, and I find it doesn't take long before you are mixing you drinks a little stronger, and a little stronger, and a little stronger, and straight up. Instead, I opted for making my own. It costs me about 35&cent; per bottle, it tastes good (usually), and the day I take making it is time I am having fun, and not spending money.

## Recipe

### Ingredients

12 Kilos Honey 12 bags tea 1 kilo lemons (~12 lemons) 1 kilo limes (~12 limes)

### Procedure

steps