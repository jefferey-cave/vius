---
id: 706
title: Stupid Little Necessities
date: 2011-01-02T01:41:29+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=706
permalink: /2011/01/02/stupid-little-necessities/
categories:
  - Living
---

The devil is in the details.

As with any new undertaking, you find there are those stupid little details that nobody bothers to mention because they are so blindingly obvious to someone who is in the know. Living in the country is no different. We have been living in the country for exactly one week and there are a thousand little things that I am finding would make life far more comfortable but would never make anyone's list of necessities.<!--more-->

# Fly-Swatter

Without fail&hellip; we have just fired up the television for the night, settled in to watch the next exciting installment of our current television show, and you hear it&hellip; the buzzing of a fly. If they are feeling particularly cheeky they land right on the televisions screen. We have been using chemicals sprays and sticky traps in our on-going war with the flies, but there are sometimes where you need the immediateness , precision, and definitiveness of just splattering the little devil.

The Wife hooked me up with a great house warming gift: a leather fly swatter. Oh you may think this is over-kill, but with the number of flies we've had to kill, I would have worn a plastic one out already.

# Rubber Boots

Grab something from the car, get a tool from the garage, dump the ashes from the fireplace. With all of the work that needs to be done outside, and with the outside really just being an extension of the inside (and with being an outdoor smoker), it would be really handy to have something I could just slip on my feet when I go tromping through the snow for something quick.

This is one I already knew about, but since moving, I haven't been able to find mine. I assume that someone from the moving company has a new pair of rubber boots&hellip; I hope he finds out they have a leak the hard way.

# Slippers

[<img style="float: right" src="https://images-na.ssl-images-amazon.com/images/I/41uK703lYfL._SL160_.jpg" border="0" alt="" />](http://www.amazon.ca/gp/product/B0047M69MU?ie=UTF8&tag=vius-20&linkCode=as2&camp=15121&creative=330641&creativeASIN=B0047M69MU)<img style="border: none !important;margin: 0px !important" src="http://www.assoc-amazon.ca/e/ir?t=vius-20&l=as2&o=15&a=B0047M69MU" border="0" alt="" width="1" height="1" />I always swore I would never wear slippers. I used to work at a nursing home, and slippers are for the little old ladies living there. My pride will just not allow me to wear such a contrivance. First thing in the morning, when the wood furnace has died down and I have to get out of bed, cross the wood floors in the house, go to the basement with the concrete floor, and build the fire; suddenly my pride counts for squat.

Fortunately, The Wife doesn't worry about my pride, and bought me slippers for Christmas. They haven't left my feet since.

<span id="Pee-Mat_The_Pee-Mat_is_the_mat_that_goes_immediately_in_front_of_the_toilet_to_catch_any_drips_that_may_occur.">

# Pee-Mat

<sup id="rf1-706"><a href="#fn1-706" title="The Pee-Mat is the mat that goes immediately in front of the toilet, to catch any drips that may occur." rel="footnote">1</a></sup>


[<img style='float:right' src="http://ecx.images-amazon.com/images/I/51PcgjVgSbL._SL160_.jpg" />](http://www.amazon.ca/gp/product/B00444T0V8?ie=UTF8&tag=vius-20&linkCode=as2&camp=15121&creative=390961&creativeASIN=B00444T0V8)<img style="border: none !important;margin: 0px !important" src="http://www.assoc-amazon.ca/e/ir?t=vius-20&l=as2&o=15&a=B00444T0V8" border="0" alt="" width="1" height="1" /> The Wife and I disagree on the necessity of this item; S thinks they are a disgusting concept. Let's face it, when you are sitting on the toilet, the last thing you want to be worrying about is cold feet. No, wearing slippers is not an option: one's time communing with nature should be done as free of false encumbrances as possible (alright, so my feet just keep slipping out).

The Wife is just going to have to accept that sometimes function comes before
form; or accept that her bath mats are going to magically migrate themselves
closer to the toilet.

<figure>
 <img src="/media/2011/01/ChristmasSuspenders.20101225.jpeg" width="300" height="225" />
 <figcaption>My New Christmas Suspenders! Another little necessity.</figcaption>
</figure>

<aside>
 <ol>
  <li id="f-1">
   <p>
The Pee-Mat is the mat that goes immediately in front of the toilet, to catch any drips that may occur.
<a href="#rf1-706" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
   </p>
  </li>
 </ol>
</aside>
