---
id: 1592
title: Still No Job
date: 2011-10-12T08:14:52+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1592
permalink: /2011/10/12/still-no-job/
categories:
  - Uncategorized
---
I have really been looking forward to a job interview that I have had scheduled for the last week. It was for an office building cleaning job. Basically, I would go in and vacuum, wash windows, and wipe down various surfaces for some of the local businesses after hours and weekends.

<!--more-->

As I said, I was excited, until I got this email this morning:

> I know this is short notice, but I have filled the position in Annapolis Royal so I am cancelling all of my Wednesday interviews. I do keep all resumes for one full year, so if this position again becomes available I will contact you to re-schedule an interview. Again, I apologize for the short notice, but I didn't want to waste any ones time interviewing for a position that is no longer available

This is very disappointing. After not getting a call back from Tim Horton's I'm starting to wonder what the hell is going on.

The Wife was laughing/crying the other day. I got offered a job 3.5 hours away, working for Hewlett Packard, making $100K+ a year, heading up a new system testing team (which of course would include 70 hour weeks, around the clock support, and a high stress load)&hellip; but I can't get a job at Tim Horton's serving coffee, washing dishes at the local restaurant, punching tickets at the local fun park, or washing floors in the local office buildings.

WTF?

Update 2011-10-13: Have a Job

I just got hired onto an Apple Picking crew. At least one Orchard in the valley is hiring local labour to pick their apples. Hard work, but its work.