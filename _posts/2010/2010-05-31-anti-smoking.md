---
id: 115
title: Anti-Smoking
date: 2010-05-31T14:05:00+00:00
author: Kavius
layout: post
categories:
  - Opinions
  - Essays
---

Today is
[World No Tobacco Day 2010](http://www.who.int/tobacco/wntd/2010/announcement/en/),
and as an unapologetic smoker it seems like a pretty good time to say something.

<img class="size-full wp-image-175 alignright" title="Smoking Section" src="/media/2010/06/smoking-sign.png" alt="" width="116" height="116" />

First I would like to distinguish between non-smokers (like The Wife), and
anti-smokers. I have the utmost respect for those people in the world who have
chosen not to smoke: they were faced with a choice, and they chose; well done.
Anti-smokers are those who feel that they need to force others not to smoke.
This usually takes on the form of either a campaign of shame or through threat
of violence. I have no time for anti-smokers, don't care for your opinion, and
in most cases consider you little better than petty thugs.

I myself am what I would call a _considerate smoker_. While I make no
apologise for my choice to smoke, I do recognize that not everyone wants to be
around the smoking. This means that I will be smoking, but I will excuse
myself from the group, and find a place out of the way in which to do so. I do
not force my smoking on anyone else.

What I have found most interesting is anti-smoker attitude towards this. I was
once having a conversation with a guy I worked with on this matter and his
statement was that he cannot enjoy a space that I am smoking in. This is fair,
but by the same token (I pointed out), I cannot enjoy (smoke) the same space
he is occupying as a non-smoker. Who has the right to use the space? Just
handing the space over to the non-smoker because they are a non-smoker is
fairly arbitrary.

Not only is it arbitrary, but one thing that I have noticed about anti-smokers
is they have no urge to occupy a given space, **until someone is smoking in
it**. The minute a smoker lights up, the anti-smoker feels they are being
restrained from entering that space. As I put it to the guy I was working
with: <q>My issue isn't that smokers are occupying space a non-smoker wants to
occupy, its that within minutes of a smoker choosing the middle of a dung pile
half a kilometre from the building, an anti-smoker would suddenly feel
compelled to enjoy the sweet essence of natural recycling</q>. Yes, that was
sarcasm you were picking up on.

So the real question becomes, at what point does the non-smoker's right to be
free of tobacco smoke, trump the right of the smoker to light up?

I can already hear the comment on <q>smoking is not a right</q>;. Before
posting this comment, re-read my comment on that being _arbitrary_.

Even the Non-Smoker's Rights Association has reservations regarding total
bans. Instead they recommend setting aside segregated areas where people smoke:

> NSRA believes that every hospital in Canada has a duty to:
>
> * Ban smoking indoors;
> * Provide smoke-free buffer zones (at least 7 m) around doorways, operable
>   windows and air intakes;
> * Limit smoking on hospital property to outdoor designated smoking areas
>   (DSAs).
>
> <footer><cite><a href="http://www.nsra-adnf.ca/cms/file/pdf/SF_hospital_properties_2008.pdf" target="_blank">Smoke-free Hospital Properties</a></cite></footer>

This from a group of militant anti-smokers.

While they take the line that non-smoker rights trump smoker rights, they do
recognise that total bans can cause a number of complications. Not only may
people avoid treatment because they are not allowed to smoke; smokers tend to
socialise, and by not having designated smoking areas, staff and customers
will tend to find the same areas for smoking causing inappropriate social
situations (the nurse that needs a cigarette after dealing with a frustrated
family, is placed near the family that needs a cigarette after dealing with a
frustrating nurse).

That the NSRA are forced to take such a soft line on hospital smoking bans (in
a nation where most hospitals have already put a smoking ban in place), must
make them cringe. For this, I would like to give a thumbs up to the NSRA, for
stating an uncomfortable truth, rather than a comfortable but inflammatory
one. I do not agree with the conclusions of the report, but the content is far
more fair, and well reasoned, than I have come to expect from anti-smokers.

The most disturbing aspect of the modern anti-smoking attitude, is the social
acceptance of the attitudes. Rarely in history has telling a peer how to live
their life resulted in anything more than a <q>f*@k off</q>. Unfortunately, the
smoker is one of the first causalities in a trend toward _helping people_
(whether they want it or not).

That hospitals across Canada (with the few exceptions of hospitals that have
reversed their decision) have banned smoking from their properties is a very
thin veneer of concern for other people's well-being over an obvious attempt
to control the lives of staff, patients, and visitors; enforcing an ideals of
a small group on a larger mass.

For patients this can mean a serious issue, in order to smoke, they must take
hospital equipment (gowns, IV rods, monitors) off the hospital property. If
this is disallowed, the patient may be forced to choose between leaving the
life saving equipment behind or having a cigarette. Hospitals are large, this
means that patients must travel a long distance from the people attempting to
keep them healthy; if they have a problem, they are a long way from help.

Staff are faced with issues as well. In order for them to have a cigarette,
they must also leave the property. This turns a 10 minute break into a 20,
maybe 30, minute break. The individual who was a helpful productive employee
is now forced to choose between doing their job and smoking. Really the only
options left in a large facility is to quit, or lose the job. Employees who
try be good employees and still smoke may try to sneak a smoke in order to
reduce their time away from the job; bad idea, there are
[fines and imprisonment](http://www.tegh.on.ca/bins/content_page.asp?cid=2-27-29#smo)
associated with that.

I am very disappointed in every anti-smoker out there. What gives you the
right to force me into a course of action by threatening my job, threatening
to take money from me by force, or threatening me with other forms of
violence. I have not done anything to harm you; you have no right to harm me.

To use the words of the IWK Health Centre in Halifax, on their
[page regarding policy](http://www.iwk.nshealth.ca/index.cfm?objectid=553AF2DE-EFA3-1D4B-52FCC9BB1BF75354):
<q>Your co-operation and consideration of others is appreciated</q>.
