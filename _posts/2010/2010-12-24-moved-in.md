---
id: 783
title: Moved In
date: 2010-12-24T00:00:23+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=783
permalink: /2010/12/24/moved-in/
categories:
  - Updates
---
We are now moved in Full-Time. We have no further ties to the city.

Just a quick photo tour of our new home. These pictures were taken a couple of days ago.

<div id='gallery-10' class='gallery galleryid-783 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-8/'><img width="150" height="113" src="/media/2011/01/Property.20101205.8.jpeg" class="attachment-thumbnail size-thumbnail" alt="Driveway" aria-describedby="gallery-10-799" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-799'> Driveway and Pond from sunroom </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-7/'><img width="150" height="113" src="/media/2011/01/Property.20101205.7.jpeg" class="attachment-thumbnail size-thumbnail" alt="Apple Trees" aria-describedby="gallery-10-798" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-798'> Apple Trees behind Garage </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-5/'><img width="150" height="113" src="/media/2011/01/Property.20101205.5.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-4/'><img width="150" height="113" src="/media/2011/01/Property.20101205.4.jpeg" class="attachment-thumbnail size-thumbnail" alt="Pond" aria-describedby="gallery-10-795" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-795'> Pond from sunroom </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-3/'><img width="150" height="113" src="/media/2011/01/Property.20101205.3.jpeg" class="attachment-thumbnail size-thumbnail" alt="Courtyard" aria-describedby="gallery-10-794" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-794'> Courtyard from the upstairs landing </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-2/'><img width="150" height="113" src="/media/2011/01/Property.20101205.2.jpeg" class="attachment-thumbnail size-thumbnail" alt="Trees Behind Garage" aria-describedby="gallery-10-793" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-793'> Trees Behind the Garage </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-1/'><img width="150" height="113" src="/media/2011/01/Property.20101205.1.jpeg" class="attachment-thumbnail size-thumbnail" alt="Driveway" aria-describedby="gallery-10-792" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-792'> Driveway from Sunroom </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/property-20101205-6/'><img width="150" height="113" src="/media/2011/01/Property.20101205.6.jpeg" class="attachment-thumbnail size-thumbnail" alt="Mudroom" aria-describedby="gallery-10-797" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-797'> Mudroom </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-7/'><img width="150" height="113" src="/media/2011/01/House.20101205.7.jpeg" class="attachment-thumbnail size-thumbnail" alt="Bedroom" aria-describedby="gallery-10-791" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-791'> Bedroom </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-6/'><img width="150" height="113" src="/media/2011/01/House.20101205.6.jpeg" class="attachment-thumbnail size-thumbnail" alt="Upstairs Bathroom" aria-describedby="gallery-10-790" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-790'> Upstairs Bathroom </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-5/'><img width="150" height="113" src="/media/2011/01/House.20101205.5.jpeg" class="attachment-thumbnail size-thumbnail" alt="Dining Area and Kitchen" aria-describedby="gallery-10-789" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-789'> Dining Area and Kitchen </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-4/'><img width="150" height="113" src="/media/2011/01/House.20101205.4.jpeg" class="attachment-thumbnail size-thumbnail" alt="Sun Room" aria-describedby="gallery-10-788" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-788'> Sun Room </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-3/'><img width="150" height="113" src="/media/2011/01/House.20101205.3.jpeg" class="attachment-thumbnail size-thumbnail" alt="Office" aria-describedby="gallery-10-787" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-787'> The Office </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-2/'><img width="150" height="113" src="/media/2011/01/House.20101205.2.jpeg" class="attachment-thumbnail size-thumbnail" alt="Sitting Area" aria-describedby="gallery-10-786" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-786'> Sitting Area (from the bookshelf) </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/24/moved-in/house-20101205-1/'><img width="150" height="113" src="/media/2011/01/House.20101205.1.jpeg" class="attachment-thumbnail size-thumbnail" alt="Living Room" aria-describedby="gallery-10-785" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-10-785'> Living Room from the bottom of the stairs </figcaption></figure>
</div>