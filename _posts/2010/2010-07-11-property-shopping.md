---
id: 472
title: Property Shopping
date: 2010-07-11T21:51:43+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=472
permalink: /2010/07/11/property-shopping/
categories:
  - Moving
---
Over the last two weeks, we have done a lot of house shopping. We have seen several properties along the Northumberland Strait, Annapolis Valley, and one in a town called Springhill.

Unfortunately, at this point, the Strait seems to be a write off. None of the properties were geared for agricultural use at all. I know this can't be right, as we met a couple out there that are doing exactly what we want to be doing, and they have a great property. At this point, I believe our Real Estate Agent misunderstood what we are looking for, she kept taking us into properties and showing us how beautifully landscaped the properties are. Even after explaining our interest in raising goats, she tried to impress me with a beautiful house with a 2 acre rose garden and lawn, and not a single out-building in sight.  The Wife and I left very disheartened.

Near to the Annapolis Valley we saw several properties that look very promising. One thing I noticed was that if I was not seeing a property with a barn, I was not impressed. Several properties we looked at had very nice barns (and I will be putting the elevators in the offer), and I liked all of them. I think my positive reaction to the Valley was due, in part, to the Real Estate agent we are working with there. She has faced the giant spider webs in basements to help me check the wiring and heating. On top of that, she has offered interesting insight into wells and septic systems<sup id="rf1-472"><a href="#fn1-472" title="she has some sort of certificate in Well Water Management, or some such" rel="footnote">1</a></sup>. Most interestingly she has given a little bit of insight into the communities we were driving through.<sup id="rf2-472"><a href="#fn2-472" title="Off the topic a little: I think she has an awesome view of animals as well. She had me chasing a turtle around the highway trying to get it to safety; she spent a good 5 minutes scratching a cow behind the ear (the cow cried when we left); and we had a discussion about her personal experience about how good pet goats taste after they have eaten your garden (a good curry is the key) " rel="footnote">2</a></sup>

Looking back on past articles, I realize how stupid this sounds. When we first passed through, we hated the <a href="http://www.vius.ca/2010/06/annapolis-royal/" target="_blank">Annapolis Valley and loved the Strait</a>; now that we are looking at properties, we love the Valley and hate the Strait. It just goes to show how fickle we are being with our shopping. That's alright, you are allowed to be fickle early on. You don't have to be objective until it comes time to actually make a choice; then you better have documented, written down, reasons for why you make the choice you do.

For the record, there are some very specific things we are looking for (some are deal breakers, others are high priority):

  * Surface water (stream, brook)
  * Good Well
  * Treed Section
  * 2-3 acres cleared
  * Barn
  * Fruit Trees
  * Century Home (not falling over)
  * Wood/Oil furnace
  * at least 10 acres (I've already been talked down from 100)

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-472">
    <p>
      she has some sort of certificate in Well Water Management, or some such&nbsp;<a href="#rf1-472" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-472">
    <p>
      Off the topic a little: I think she has an awesome view of animals as well. She had me chasing a turtle around the highway trying to get it to safety; she spent a good 5 minutes scratching a cow behind the ear (the cow cried when we left); and we had a discussion about her personal experience about how good pet goats taste after they have eaten your garden (a good curry is the key) &nbsp;<a href="#rf2-472" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
</ol>