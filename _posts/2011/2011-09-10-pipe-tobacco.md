---
id: 1449
title: Pipe Tobacco
date: 2011-09-10T17:12:40+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1449
permalink: /2011/09/10/pipe-tobacco/
categories:
  - Uncategorized
---
I have quit smoking.

I once swore I would never quit smoking. When The Wife and I started dating, I had a discusion with her about hte fact that she better be alright with me smoking because I wasn't going to quit for her. I think smoking is a fine past-time that everyone should never be interfered with!

I can't afford it.

With our recent move, and lack of employment, we have been cutting luxury, after luxury out of our lives. Unfortunately, I only had one luxury left: smoking. So now it is gone.

Still,  The Wife and I discussed not dropping it completely.

I have decided to switch to pipe smoking. This is not a new idea, I have been aquiring pipes for 2 years with the intent to switch, but have just never got around to making the move. My current situation serves as an impetous to make that move. Rather than having a quick cigarette at almost regular intervals, I should be switching to taking a little meditative time, once a day, to slowly enjoy a pipe. Hopefully this will reduce expenses.

So, I have now been a full week without a cigarette, and have also ordered my first lot of pipe tobacco from <q>4noggins.com</q>. I have ordered one of their sampler sets, and am excitedly checking the mail box everyday.

It's likely going to be a while before it arrives, but that will just force me to go without tobacco a little longer, and see the pipe tobacco as that much more of a treat when I get it.

PS. I'm hoping to grow my own tobacco, but that's for the future. Seed to pipe takes two to three years (from what I've read), so it looks like I'll be buying it for a little while yet.