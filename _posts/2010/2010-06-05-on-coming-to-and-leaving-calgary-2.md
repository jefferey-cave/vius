---
id: 188
title: On Coming to, and Leaving Calgary
date: 2010-06-05T23:22:02+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=188
permalink: /2010/06/05/on-coming-to-and-leaving-calgary-2/
categories:
  - Musings
  - TheWife
---

Calgary has been my home for many years now. Originally from England; I made my space here in my early twenties.

I have survived, although not thrived, many winters of temperatures it’s hard to imagine until you live them. I have promised myself time and time again to enjoy the short, short summer, only to realize, too late, that winter had reared its ugly head once again.

Since I stepped off of that plane one cold February afternoon I have learned many new things:

  * Driving in the snow and ice. (no matter what they say, ‘winter’ tires are a necessity)
  * Politely asking someone to stop by for coffee; even though you know they never will
  * Going outside in the winter with wet hair is a mistake (I know, it should have been obvious)
  * It’s not a dry cold…it’s just damn cold!
  * Visiting a friend or relative who lives 400 km away is called a ‘day trip’
  * Snow can and does happen during any month.
  * The Rocky Mountains are incredible
  * It’s much harder to ski than it looks!
  * You have to plug in your car in the winter or the chances of it starting are between zero and none.

Now I’m heading east, not to England this time but to Nova Scotia. Back to the ocean, back to feel the cool coastal breezes on my face, the taste of salt on my lips and, most certainly, the oppressive humidity that comes with it. Having lived in a climate where moisturiser is a girl’s best friend, I’m not sure how I’ll cope. As an acquaintance told me many years ago, the lower cost of living in Calgary is offset by the need to purchase so much skin lotion!

I’m excited about Nova Scotia; it’s Canada with a large dollop of Britain mixed in. It is a place where you can buy a home that’s more than 40 years old and still live in it. I hope it’s the place for me; a perfect place for us.

~  The Wife
