---
title: Books
date: 2018-10-30T21:57:37+00:00
author: Kavius
layout: page
---

So I've always loved books, and always fanticised about having my own digital
library... I called it *Alexandria*. Then one day I discoverd Project
Gutenberg, and that all went out the window.

There are only two flaws that I see with Project Gutenberg:

1. it is constrained to books whose copyright has expired
2. it is contrainted to books that are legal in any given country

Having said that, they do a pretty good job.

This is my most recent project to build an online project gutenberg reader.
