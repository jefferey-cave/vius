---
id: 573
title: Curb Shopping and Yard Sales
date: 2010-08-31T05:04:02+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=573
permalink: /2010/08/31/curb-shopping-and-yard-sales/
categories:
  - Moving
---
When we left Calgary, we left all our furniture behind. Our daughter claimed anything she could fit in her apartment, and the remainder was given away to anyone that was willing to come pick it up. Now that we are in Halifax and need to purchase new (to us) furniture, we rediscover _yard sales_ and newly discover _Curbside Shopping_.

People in Calgary have yard sales and garage sales all the time, this is nothing new. What is new is that Nova Scotians are selling off their antique, solid wood furniture in favour of new <q>IKEA</q> stuff. We left behind our IKEA stuff and are looking for well made furniture that lasts. This is almost a match made in heaven. So far I have purchased a beautiful four drawer dresser ($40), a small solid wood bookcase ($20) and 3 heavy antique kitchen chairs ($30).

This Saturday was my first experience getting something from the <q>curbside</q>. While yard sales are nothing new, curbside shopping is. In Calgary, if you don't won't something, you either sell it, give it away, or haul it to the dump and pay the landfill fee. Here in Halifax and, I believe, most of Nova Scotia, you can just haul it to your curb and anybody driving by can just load it up and take it away. If it's not gone by garbage pick up day, the city just picks it up with the rest of the garbage and takes it off your hands. My curbside find was an old vanity/dresser. It does need a little work, but what can you expect for nothing, that's correct, nothing, zilch, zippo, for an antique waterfall dresser! I can see I'm going to have fun here.