'use strict';

import vius from '/assetts/js/vius/util.js';

(function(){

class gutSearchBox extends HTMLFormElement{
	constructor(){
		super();
		customElements
			.whenDefined('lib-list')
			.then(()=>{
				this.listing = this.getAttribute('for');
				this.listing = document.querySelector(this.listing);
			})
			;
		this.onsubmit = ()=>{return false;};
		this.oninput = ()=>{
			this.search();
		};
	}

	get value(){
		let val = vius.readOptions(this);
		return val;
	}

	search(){
		let val = this.value;
		let index = 'title';

		if(val.subjects) index = 'subjects';
		if(val.creators) index = 'creators';

		let	searchVal = val[index] || '';

		index = ['library',index].join('/');
		if(index === 'library/creators'){
			index = 'library/authors';
		}
		this.listing.view = index;

		this.listing.opts = !searchVal ? {} : {
			startkey:[searchVal],
			endkey:[searchVal+'\uffff'],
		};
	}
}
customElements.define('gut-search-box', gutSearchBox, {extends:'form'});


class LibList extends HTMLElement{
	constructor(){
		super();

		this.db = this.getAttribute('db');

		let renderer = this.render.bind(this);
		this.render = vius.throttle(150,renderer);

		this.shadow = this.attachShadow({mode: 'open'});
		this.tmpl = this.innerHTML;
		this.shadow.appendChild(document.createElement('ul'));

		this._view = 'library/title';
		this._opts = {
			live:true,
			include_docs:true,
		};

		this._limit = 6;
		this._offset = 0;
		this.results = {total_rows:0,rows:[]};

		this._isUpdating = false;
		this._isDirty = true;

		this.db
			.changes({
				since: 'now',
				live: true,
				include_docs: false,
			})
			.on('change', (change)=>{
				this.isDirty = true;
			})
			;
		this.updateResults();
	}

	set db(value){
		if(typeof value === 'string'){
			value = new PouchDB(value);
		}
		if(!(value instanceof PouchDB)){
			return;
		}
		this._db = value;
		this.isDirty = true;
	}
	get db(){
		return this._db;
	}

	set view(value){
		if(typeof value !== 'string') throw new Error('View must be a string');
		this._view = value;
		this.isDirty = true;
	}

	get view(){
		return this._view;
	}

	set opts(value){
		if(!(value instanceof Object)) throw new Error('View must be an Object');
		this._opts = value;
		this.isDirty = true;
	}

	get opts(){
		return this._opts;
	}

	set offset(val){
		if(val < 0){
			if(this.results.total_rows === 0){
				val = 0;
			}
			else{
				val = val % this.results.total_rows;
				val = this.results.total_rows + val;
			}
		}
		if(this._offset !== val){
			this.isDirty = true;
		}
		let event = new CustomEvent('offset',{oldValue:this._limit,newValue:val});
		this._offset = val;
		this.dispatchEvent(event);
	}
	get offset(){
		return this._offset;
	}

	set size(value){
		value = +value;
		if(value < 1){
			value = 1;
		}
		if(value > 100){
			value = 100;
		}
		if(value === this._limit) return;

		let event = new CustomEvent('size',{oldValue:this._limit,newValue:value});
		this._limit = value;
		this.dispatchEvent(event);
		this.isDirty = true;
	}
	get size(){
		return this._limit;
	}

	set isDirty(val){
		if(val === true){
			this.updateResults();
		}
		this._isDirty = val;
	}

	get isDirty(){
		return this._isDirty;
	}

	async updateResults(wait = 50){
		if(this.isUpdating) return;
		this.isUpdating = true;

		wait = parseInt(wait,10);
		if(Number.isNaN(wait)) wait = 50;
		if(wait < 50) wait = 50;
		if(wait > 600000) wait = 600000;

		let sched = vius.schedule(wait,async ()=>{
			let opts = JSON.merge(
				{
					include_docs: true,
					limit:this.size,
					skip:this.offset,
					startkey:'',
					endkey:{},
				},
				this.opts,
				{
					stale: 'update_after',
					include_docs: true
				}
			);
			try{
				this.isDirty = false;
				let results = await this.db.query(this.view, opts);
				this.offset = results.offset;
				this.results.total_rows = results.total_rows;
				this.results.rows = results.rows;
				this.render();
			}
			catch(err){
				this.isDirty = true;
				if(err.status === 404){
					this.isUpdating = false;
					if(wait < 1000){
						wait = 1000;
					}
					return this.updateResults(wait*2);
				}
				console.error(err);
			}
			finally{
				this.isUpdating = false;
				if(this.isDirty){
					return this.updateResults();
				}
			}
			return true;
		});
		return sched;
	}

	render(){
		let style = this.shadow.querySelector(':host > style');
		if(!style){
			style = document.createElement('style');
			style.textContent = `
				h1 {
					font-size:inherit;
					margin:0;
				}
				:host > ul {
					list-style-type: none;
					padding: 0;
					padding-left:0.5em;
					margin: 0;
				}
				:host > ul > li {
					border: 1px solid black;
					margin-bottom: 1em;
					padding: 1em;
					padding-bottom: 0;
					border-radius: var(--corners);
				}
				ul[name='creators']{
					padding-left:1.5em;
				}
				ul[name='subjects']{
					margin:0;
					margin-top:1em;
					padding:0;
					font-size:50%;
				}
				ul[name='subjects'] > li{
					display:inline-block;
					background-color:var(--main-color);
					color:var(--main-color-contrast);
					border-radius:1em;
					padding:0.2em;
					margin:0.1em;
					/*
					max-width:5em;
					height:1.5em;
					overflow: hidden;
					*/
					transition:font-size 1s;
				}
				ul[name='subjects'] > li:hover{
					font-size:200%;
					/*
					max-width:none;
					*/
					transition:font-size 1s;
				}
				ul[name='isAllowedForFree']{
					padding:0;
					text-align:right;
				}
				ul[name='isAllowedForFree'] > li{
					display:inline-block;
					padding:0;
					margin:0;

				}
			`;
			this.shadow.appendChild(style);
		}

		let ul = this.shadow.querySelector(':host > ul');
		ul.innerHTML = '';

		let items = Array.from(ul.querySelectorAll(':host > ul > li'));
		while(items.length > this.results.rows.length){
			let li = items.pop();
			li.parent.remove(li);
		}
		while(items.length < this.results.rows.length){
			let li = document.createElement('li');
			ul.appendChild(li);
			items.push(li);
		}
		this.results.rows.forEach((rec,i)=>{
			let li = items[i];
			this.RenderItem(li,this.tmpl,rec.doc);
		});
	}

	RenderItem(container, tmpl, rec){
		container.innerHTML = tmpl;
		//let list = Array.from(container.querySelectorAll(':host > ul["name"],:host > ol["name"]'));
		let lists = Array.from(container.childNodes);
		lists
			.filter((list)=>{
				let keep = ['UL','OL'].includes(list.nodeName);
				return keep;
			})
			.forEach((list)=>{
				let field = list.getAttribute('name');
				let r = rec[field];
				if(r){
					let tmpl = list.querySelector('li');
					if(tmpl){
						tmpl = tmpl.innerHTML;
						list.innerHTML = '';
						r.forEach((r)=>{
							let li = document.createElement('li');
							list.append(li);
							this.RenderItem(li,tmpl, r);
						});
					}
				}
			});
		let outputs = Array.from(container.querySelectorAll('output[name]'));
		outputs = outputs.filter((o)=>{
			let keep = (!o.value);
			return keep;
		});
		outputs.forEach((output)=>{
			let field = output.getAttribute('name');
			let val = rec;
			if(field !== '.'){
				val = val[field];
			}
			if(val){
				output.value = val;
			}
		});
	}
}
customElements.define('lib-list', LibList);



class PageCtl extends HTMLElement{
	constructor() {
		super();

		this._size = 6;
		customElements
			.whenDefined('lib-list')
			.then(()=>{
				this._listing = this.getAttribute('for');
				this._listing = document.querySelector(this._listing);
				let self = this;
				this._listing.addEventListener('size',(e)=>{
					self.size = e.target.size;
				});
			})
			;

		let shadow = this.attachShadow({mode:'open'});

		let btn = document.createElement('button');
		btn.innerHTML = '&lt;';
		btn.addEventListener('click',this.prev.bind(this));
		shadow.appendChild(btn);

		let select = document.createElement('select');
		select.innerHTML = this.innerHTML;
		select.addEventListener('change',()=>{
			if(!this._listing) return;
			this._listing.size = +select.value;

		});
		select.value = this.size;
		shadow.appendChild(select);

		btn = document.createElement('button');
		btn.innerHTML = '&gt;';
		btn.addEventListener('click',this.next.bind(this));
		shadow.appendChild(btn);
	}

	get size(){
		return this._size;
	}

	set size(value){
		value = +value;
		if(this._listing.size === value) return;
		var event = new CustomEvent('size', { oldValue: this._listing.limit, newValue:value });
		this._listing.size = value;
		this.dispatchEvent(event);
	}

	get listing(){
		return this._listing;
	}

	set listing(value){
		if(this._listing === value) return;
		if(!(this._listing instanceof LibList)) return;
		this._listing = value;
	}

	next(){
		this.change(1);
	}

	prev(){
		this.change(-1);
	}

	change(qty=1){
		if(!this._listing) return;
		var event = new CustomEvent('offset');
		this._listing.offset += qty;
		this.dispatchEvent(event);
	}
}
customElements.define('page-ctl', PageCtl);



})();
