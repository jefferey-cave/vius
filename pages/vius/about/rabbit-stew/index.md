---
id: 724
title: Rabbit Stew
date: 2011-01-13T19:59:35+00:00
author: Kavius
layout: page
---
July, 2003 &mdash; April, 2015

![Rabbit "Stew"](/media/2011/01/Stew.20110110.jpeg "Pet Rabbit Stew")

Stew is our pet rabbit. At <del>7&half;</del> <del>11</del> 12&frac34; years 
old, is still happy and healthy.

He is a holy terror and runs about the house looking for people to eat and 
furniture to climb on. Beware not paying attention to him when he wants it 
&hellip; he will bite you. Yelling at him helps, causing him to run away with 
his ears bouncing in glee. Unfortunately, this usually results in him coming 
back to do it all over again.

Stew's favourite past-time is hunting [Chimutisk](http://vius.ca/chimutisk/), our pet ferret. Over the years, both  The Wife and I have developed a reflexive dash to rescue the ferret when we hear Stew's little claws clattering to a run, or the distinctive cry of a terrified ferret. When we first introduced them, Stew drop-kicked Chimutisk in the head, thus setting the tone for their relationship ever since. Stew hates Chimutisk, and wouldn't be caught dead showing anything other than disdain for him&hellip; unless he doesn't think anyone is watching. We have come home from work to find Stew sleeping next to Chimutisk's cage; have noticed he is distressed and checking the cage regularily when Chimutisk has spent the night at the vet; and on a couple of occasions, watched him snuggle up to a ferret (frozen in terror) for a nice cuddle.

Always intrepid, Stew is always fascinated by anything new, especially new rooms (moving a piece of furniture is worth at least three days of exploration). It means the chance to explore new places and things, and basically get in the way while you are doing stuff. This generally leads to him having an eye for <q>up</q>. He is always looking for something to climb. It is not uncommon to find him on top of boxes during a move; and the distinctive crash of pots is a clear indicator that somebody left something too close to the kitchen counter, offering him a way up. Generally, the climbing is just a search for his favourite foods: cardboard, chocolate, novel, and plastic.

When Stew isn't trying to cut your Achilles, beat the tar out of [Chimutisk](http://www.vius.ca/chimutisk/ "The Ferret"), or sitting on your keyboard, he can often be seen sitting with Wets. Wets is Stew's friend that lives in the mirrors around the house. Stew can be seen staring at Wets for hours, just visiting and sniffing. His time with Wets, and his time asleep,  are about the only peaceful times we get.

Some days, I swear <q>Stew</q> is going to be more than just a name.

* * *

On 2015-04-10, Stew died. Having had a stroke a couple of days earlier, he came to the edge of his kennel, and nestled in The Wife's arms, where he passed a couple of minutes later. 

<q>The Annoying One</q> will be missed.

* * *

See also:

[Stew **almost** dies](http://vius.plaidsheep.ca/stuff/experience-with-wry-neck-in-a-pet-rabbit/ "Treatment of Wry Neck in a Pet Rabbit")