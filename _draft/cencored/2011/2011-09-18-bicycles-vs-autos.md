---
id: 1489
title: Bicycles vs Autos
date: 2011-09-18T07:35:28+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1489
permalink: /2011/09/18/bicycles-vs-autos/
categories:
  - Opinions
---
I just read an article regarding the <a title="The Stranger: If Safer Streets Mean War, We’re Ready for Combat" href="http://dc.streetsblog.org/2011/09/16/the-stranger-if-safer-streets-mean-war-were-ready-for-combat/" target="_blank">antagonism between Cyclists, and Auto Drivers</a>. The author views the subsidies towards drivers to be the proble, and proposes increased subsidies to bicycle paths, public transit, et cetera.

> This antagonism [between car driver and nondriver] traces directly to the creation of the modern car driver, a privileged individual who, as noted, is the beneficiary of a long course of subsidies, tax incentives, and wars for cheap oil. But the same subsidies that created this creature (who now rages about the roads while simultaneously screaming of being a victim in some war) can—and must, beginning now—be used to build bike lanes, sidewalks, light rail, and other benefits to the nondriving classes.

I kind of agree. I agree the issue is with subsidies, but TPTB have demonstrated they cannot allocate subsidies in an effective manner therefore the answer is not more subsidies. The solution would be to **stop** <a title="Energy Policy of Canada" href="http://en.wikipedia.org/wiki/Energy_policy_of_Canada#Non-conventional_oil" target="_blank">subsidising oil production</a>, and to **stop** subsidising roads.

Once subsidies on oil are removed, fuel costs normally hidden in taxes, would increase encouraging alternate energy sources to be used, and making them more financially competitive. People would look at the expense of purchasing a tank of fuel and start looking more closely at other options. ((Gas Taxes are just plain stupid. I have a tractor for food production and property maintenance. It is not registered for the road, and cannot be legally driven on the road. Why in all hells am I paying to finance the roads every-time I fill up the tank?))

Roads could be direct billed rather than hidden in taxes: people who register their car would pay a significantly higher fee but those that do not use the roads would not pay anything at all. The increased visibility of the costs associated with road usage would also encourage people to seek alternate modes of transportation (public, bicycle, walk).

The answer is not more subsidies, but to **_remove all subsidies_**. Stop charging people for services they don't want, and don't use. Instead start direct billing for the services people do.