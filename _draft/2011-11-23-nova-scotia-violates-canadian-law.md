---
author: Jeff
title: Nova Scotia violates Canadian Law
slug: nova-scotia-violates-canadian-law
id: 1674
date: '2011-11-23 18:45:15'
layout: draft
categories:
  - Uncategorized
---

Both the milk and wine cartel were implemented to increase profits for the select milk farmers. Taking this action is a clear violation of Canadian law. [http://laws-lois.justice.gc.ca/eng/acts/C-34/page-26.html#h-19](http://laws-lois.justice.gc.ca/eng/acts/C-34/page-26.html#h-19)