---
id: 329
title: Portage La Prairie to Ignace
date: 2010-06-15T19:10:07+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/2010/06/portage-la-prairie-to-ignace/
permalink: /2010/06/15/portage-la-prairie-to-ignace/
categories:
  - Uncategorized
---

We crossed into Ontario this afternoon (Tuesday). We decided to have an easier day and have made it to about 2 hours west of Thunder Bay.

Northern Ontario is very similar to BC; lots of forest, rocks and lakes. We
stopped in the very pretty town of Kenora around noon. Kenora sits on a huge
lake and everyone there seems to own a boat.

Chimutisk suddenly had a huge burst of energy this evening. He ran, or rather
bounced, through the long grass, he was excited about something&hellip; we
don't know what. It is wonderful to see him spry again.

Tonight we are staying at the Westwood Motel in Ignace, Ontario. Our motel
room is not that great, I wouldn't recommend staying here. There is no coffee
pot or wireless internet (although they advertise they do) the towels are
threadbare, everything is old, worn and grungy looking. Just as well we'll be
moving on tomorrow. I'm typing this hoping that we can find an internet
connection somewhere tomorrow morning so I can post.

Tomorrow we should hit the Great Lakes. Kavius has never seen the lakes and
his first sight will be that of Lake Superior, the largest lake. I think he'll
be in awe.

<figure>
 <img width="150" height="113" src="/media/2010/06/CarTrailer1.jpg" />
 <img width="150" height="113" src="/media/2010/06/CarTrailer2.jpg" />
 <figcaption>Our Vehicle for the Cross-Country Trip </figcaption>
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/IgnaceParkLoitering.jpg" alt="Ignace Park Sign" />
 <figcaption>Isn't loitering the point to a park?</figcaption>
</figure>
