---
id: 1612
title: Corporatism and Fascism
date: 2011-10-15T00:00:53+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1612
permalink: /2011/10/15/corporatism-and-fascism/
categories:
  - "1984"
  - Opinions
---
Some years ago, I was humming and hawing over whether I should purchase a first edition copy of <q>Facism</q> by Benito Musolini. I spent two weeks trying to decide if I should drop the cash for what was really a boring book. I would regularirly return to the book store, take the book off the shelf and read a couple of pages. I flipped through the book and felt that for the most part, it failed to define Facism effectively, and therefore wasn't worth purchasing&hellip; except for the introduction.

I read the entire introduction and found it rivetting.

Written by one of Musollini's (and Facism's) political admirers, Winston Churchill, it gave clear expression to the the fact that there was no clear definition of Facism at the time (it was getting made up as they went along) but did offer a clear perspective of what it was shaping into. To this day, Churchill has offered the clearest explanation of what Facism is, that I have ever seen.

<!--more-->

Unfortunately, I did not purchase the book — something I kick myself in the ass for regularly — and can no longer find that introduction. I kick myself because so many people forget that most of the Occident viewed Fascist nations with envious eyes, and held them up as models of progress, something to aspire to, and an introduction praising Fascism by Winston Churchill is priceless when people forget that. Secondly, most people define Fascism as a totalitarian government, I can't remember the exact definition Churchill offered, but it was such a simple and elegant definition and focussed more on the economic aspects of what Fascism was about.

Recently I found <a title="Mercantilism, Fascism, Corporatism, And Capitalism" href="http://www.thelibertypapers.org/2007/05/23/mercantilism-fascism-corporatism-and-capitalism/" target="_blank">this article</a> dating from 2007, which defines Corporatism and Fascism as related entities:

> Corporatism and Fascism
> 
> I place these two together because there are many similarities. They’re both political systems based on widespread government intervention and planning in the economy. They’re both seen as a bit of a third-way between capitalism and socialism, with corporatism _seemingly_ nearer to capitalism, and fascism _seemingly_ nearer to socialism. There are a few main differences though, most notably that fascism is a complete political system, whereas corporatism mainly deals with economic matters. But the biggest difference is who is pulling the levers of central planning, and for what purpose.
> 
> _Fascism_ is a political system where individual interests are subservient to those of the state. In fascism, this occurs in all spheres of life, but this post deals purely with the economic. As I mentioned, fascism involves extensive central planning. It doesn’t abolish private property, but it drastically curtails the scope of property rights. Property can be used by owners for all “approved” state purposes, and only for those purposes. Venezuela, for example, would be more of a fascist state than a socialist state. But the key point of fascism is that it is an authoritarian state where the needs of the individual or corporation are subservient to those of the state. It is the politicians who are pulling the levers, and they’re doing it for national honor.
> 
> _Corporatism_, on the other hand, is a political system dominated by corporate interests with the stated goal of improving the economy. Individual rights are a little more widespread, but economic liberty is curtailed to ensure smooth and planned economic growth. While many would consider eminent domain cases like Kelo to be “fascist”, it’s more accurate to describe it as corporatist, as it involves economic actors pushing government into violating individual rights to promote business interests. America would be an example of a true corporatist state, where high-dollar business interests get politicians to write regulations friendly to their interest and punishing their competitors, under the false front of “protecting the consumer”. The businessmen pull the levers, for their own interests.
> 
> Corporatism and fascism have similarities, in that both involve widespread government intervention into an economy, but the former involves businesses controlling politicians for business interests, and the latter involves the politicians controlling business to further state interests.

This definition seems to be similar to the one presented in <q>Fascism</q>, and is reasonably complete, and does highlight the economic principles. Very interesting definition&hellip;

The author of this article also draws a distinction between Corporatism and Fascism, not something I have traditionally done (I have used these terms synonymously in the past). While he draws a distinction, he does also group them together as closely related. Clearly, the author views them as so closely related as to be nearly the same thing, but does not explicitly define how they are so similar.

While the author states that the difference is who is pressuring whom, the key point of the definition offered by Churchill was _co-operation between Company and State_. If Corporatism is business putting pressure on government, and Fascism is government putting pressure on business, they only represent starting points along a progression towards _complete collusion between the two groups_. Corporatism versus Fascism becomes a <q>chicken and egg</q> style question: both offer a positive feedback for the other. In fact, it is questionable as to whether the two can exist independently. Overtime, politicians and businessmen who are successful and have learned to co-operate will continue to assist one-another until they are in complete co-operation and there is little distinction between the two entities (revolving door between business and government).

The nations of Europe have fought their bloody battles, and reformed their governments, around the issue of separating Church and State. This is one of the most pervasive ideals held up in modern occidental nations. Similarily, we need to recognize that there needs to be a separation between Corporation and State as well. The idea of a Nation's Wealth is a false ideal rooted in the Nationalistic tendancies of the past. As societies begin to move beyond identifying themselves and others as members of a race or nation, and more as individuals, it is important to recognize that the wealth of the Nation is a meaningless aggregate of the wealth of the individuals that make up that Nation. Whether that starts as Merchantalism, Fascism, Corporatism, or Communism, once people grasp this distinction between individuals and nation, the wealth of the Nation is meaningless, and all forms of centrally planned economies become abhorrent.