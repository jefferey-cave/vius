---
id: 88
title: The Here and Now
date: 2010-05-26T02:38:56+00:00
author: The Wife
layout: post
guid: http://www.vius.ca/?p=88
permalink: /2010/05/26/the-here-and-now/
categories:
  - HomeSteading

---

Today I sit here and realize I’ve contributed nothing to this blog yet.

Today I look around and realize that we haven’t packed a single thing.

Today I mull over our options and realize we have nowhere to go.

Today I realize that my husband types really fast.

Today I smile; I’ve made my first blog entry!

~The Wife