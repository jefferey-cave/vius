---
id: 1696
title: Water Storage
date: 2011-12-19T11:24:11+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1696
---
Living in a rural setting means that we are not dependent on anyone else for our water supply. This has the benefit of not having to worry about the town having some failure, long supply chains (water mains) freezing and bursting; unfortunately, as with so many things, this means we are dependent on ourselves to ensure everything is working and our water is coming through. In the event of a pump failure, we can quickly run out of water . This is a common occurrence as we are dependant on the town for electricity to run our well pump on and when the winter winds blow, power lines go down.

Given short term disruptions are common, we need a source of drinking water readily available on standby for when our well fails us. Currently, we are storing a seven gallon water jug in the house. This water is cycled every month to ensure it remains fresh and drinkable. Bleach is added to it to ensure it does not start growing things over the years.

Seven gallons is known to not be enough for a long term outage (multiple days without power, well pump failure, well failure). But in the event of a long term outage, alternate solutions can be sought out. At the bottom of the  hill we live on (4 km away) there is spring that is often used by the locals for fresh clean water; water can be collected from this source, though it is inconvenient. Depending on the nature of the disruption, it is also possible to rely on neighbors to refill our in house water supply.

<span id="Drinking_Water">

<h1>
  Drinking Water
</h1></span> 

The objective in bleaching water is to add enough bleach to the water to ensure no microbial growth in the water while not adding so much that it is poisionous to the people that need to drink it: three parts per million (3ppm) is considered the ideal number. Over time bleach will evaporate, so one should strive to get it up to 3ppm and replenish it when the level goes down to 1ppm. Using Clorox brand bleach, 3ppm aproximately equates to ½ a teaspoon per 5 gallons.

In our case we are using a 7 gallon container, and cycling the water every month. This reduces the cost of bleach, and ensures it stays fresh. It also forces us to check periodically. Ideally, we would have at least three of these containers so that in the event of a longer disruption we would have sufficient water on hand, and would be able to maintain some water in the house while refilling containers.

<span id="Grey_Water">

<h1>
  Grey Water
</h1></span> 

We maintain two 5 gallon buckets of water in the basement for using in flushing of toilets. This water does not have bleach added to it, as it is not intended for human consumption. This water is not cycled either, as again, we are not concerned about microbial growth. This water can be collected from local streams, rain, or (most conveniently) from the outdoor taps.

<span id="Monthly_Routine">

<h1>
  Monthly Routine
</h1></span> 

  1. Empty drinking water
  2. Inspect for cracks and leaks
  3. Refill from household supply
  4. Add 1/2 teaspoon of Clorox bleach

&nbsp;

&nbsp;