---
id: 1734
title: Is Alberta Leaving Your Personal Information Insecure?
date: 2012-02-14T17:53:07+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1734
permalink: /2012/02/14/is-alberta-leaving-your-personal-information-insecure/
categories:
  - Uncategorized
---
The Government of Alberta, Alberta Health Services (AHS) does not take your privacy or security seriously.

Oh, they say they do:

> At AHS, everyone, including our staff, physicians, students, volunteers and contracted affiliates, is responsible for protecting the confidential health and personal information of our patients, co-workers, the public and AHS business information.
> 
> &#8211; [Information & Privacy and IT Security & Compliance](http://www.albertahealthservices.ca/3934.asp)

But the truth is, they can't really be bothered.

<!--more-->

For the past 3 years, I have been doing some web based work for Alberta Health Services (AHS). Every year, I am horrified when it comes up that the website needs to continue to support Internet Explorer 6.

Its not usually a big deal (especially since I charge double time for IE6 support), often it is just a matter of a partner sending a logo as a PNG and the logo not appearing on the website. Every year, I am reminded that all computers within their offices are running IE6. Every year I spend 6-24 hours fixing the problem and explaining to them that it is not my system's fault they are using an antiquated piece of garbage. This year I was told that <q>they are the government</q> and I am not in a position to question them.

As an IT professional with 15 years experience in various industries, including Government Health Care, I think I am in a very good position to question their decision not to upgrade to a more recent browser. I am not suggesting the upgrade mearly because most of the graphical elements they want are a devil to debug in unsupported browsers, but because it poses a security threat to their clientelle: the people of Alberta.

IE6 has not been supported by Microsoft for a very long time, new features are not being rolled out, and security flaws are not being corrected. In early 2010, Google was hit in a targeted attack that used IE6 to gain control of computers inside their network.

<http://www.guardian.co.uk/technology/blog/2010/jan/18/google-china-ie6-zeroday-vulnerability>

That a major IT firm could be hit and found vulnerable demonstrates that the threat is very real, and dangerous. That the Alberta Government ignores this threat is disturbing. Health records contain your name, address, embarrassing medical conditions you have, embarrassing procedures you have had, family members. The most intimate information there is, and they are hiding it behind a piece of software that has known exploits.

That Google has copies of IE6 around makes sense, they need to support companies like Alberta Health Services. That Alberta Health Services uses IE6 is horrifying, they are supposed to be protecting your personal information.

&nbsp;