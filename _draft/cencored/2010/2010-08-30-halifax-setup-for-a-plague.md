---
id: 555
title: Halifax Setup for a Plague
date: 2010-08-30T04:13:22+00:00
author: Jeff
layout: post
guid: http://www.vius.ca/?p=555
permalink: /2010/08/30/halifax-setup-for-a-plague/
categories:
  - Opinions
---
Whenever I go to another place, I suffer a little culture shock. When we first arrived in Nova Scotia, the culture shock arose with their <q>[Green Carts](http://www.halifax.ca/wrms/greencart.html)<q>.

Green Carts are part of Halifax's (and Nova Scotia's in general) effort to reduce waste being placed in their land fills. Waste is to be separated into four groups: paper, other recyclables, organics, and true waste. What shocked  The Wife and I was the _organics_. The first organics bin (Green Cart) we came across was near a convenience store in a small town. As we left the store with our slice of pizza and looking about for a place to sit and eat it, all we were left with was a spot next to a stinky bucket of rotting waste. When we arrived in Halifax, we were informed that our apartment building had a Green Cart, and that we are legal obligated to use it. Every day,  The Wife or I takes our organic waste out to the communal Green Cart for our apartment. It is always overflowing with wet, smelly, garbage, with <a href="http://www.youtube.com/watch?v=qjLBXb1kgMo" target="_blank">flies swarming about it</a>. Inevitably, when the lid is lifted, there is something sticky on the handle (I don't even want to think about what is on my hands at that point). All in all, I dislike the things.

I mentioned my culture shock to one of my co-workers the other day. I explained that I was having a really hard time with the Green Carts, every time I see them I cringe. The smell coming from them, the flies, and the really filthy appearance of their outsides, all make me really uncomfortable. She pounced: compost is safe, lots of people compost, and its good for gardens. I was forced to back down really quickly, my intent was not to attack Halifax's policy of having Green Carts, but only to express (and laugh at) my discomfort with them.

I took the bus home, with this little nugget bothering me. It bothered me that I was bothered by Green Carts. I was left feeling guilty for questioning the _ecologically friendly_ practice of composting household waste. When I mentioned this conversation to  The Wife, she pointed out that she dislikes the Green Carts too. She pointed out that while we never had a problem composting at home, the practice of loading these bins has a lot of problems. That's when it clicked for me: my co-worker was defending composting, I am alright with composting, I like composting, I do compost; my problem is the the Green Carts themselves, and the two things are totally unrelated. I shouldn't feel guilty for attacking composting (I wasn't), I should feel guilty for not attacking the Green Carts vehemently enough.

<span id="Pointless">

<h2>
  Pointless
</h2></span> 

When we were leaving Calgary, we found ourselves making a lot of trips to the garbage dump. There were a lot of items we needed to get rid of that were either left from previous owners, we could not find a good home for, or we had used well past its life-span.<sup id="rf1-555"><a href="#fn1-555" title="One item we got rid of was an office chair that we had fished out of a dumpster three years before. It was broken when we found it, but was serviceable, so we patched it and used it for years. When we threw it out, some young guy commented that it was nicer than the chair he had. I think he was trying to shame me for being so wasteful (while he threw out what appeared to be a nice desk, bed, and nightstand). I told him that this was where I got it from, this was where it was going&hellip; Ashes to ashes, Dump to dump." rel="footnote">1</a></sup> Before our first run to the dump, we had to do a bit of research into what was acceptable to be thrown out, what needed special treatment, and what was just disallowed. Now the dump charges you a minimum of $12 for a load of garbage, unless you have the one free item: _clean fill_. Clean fill is plain old dirt and rocks and is a necessary component to the waste management, they need to cut the entire thing with a neutral substrate. If you are dropping off clean fill, you are saving the dump money; they no longer have to go out and get something to mix into their mix. Frankly, the more clean fill, the healthier the mix.

If we consider organic waste healthy material to be putting in our gardens, surely it can be counted as _clean fill_ .By outlawing the disposal of organic waste in the regular garbage, the Halifax Regional Municipality has cut itself off from from a free and easy source of clean fill. Their waste will no longer be diluted, but instead will be toxic concentrate; the land (once buried) will no longer be fit for any use, and will truly be a <q>waste land</q>.

<span id="Disease">

<h2>
  Disease
</h2></span> 

The responsibility of municipalities to ensure proper waste disposal was still in contention in France up to the mid-1800s. Many land owners did not want the expense of creating the infrastructure to deal with household waste. Regardless of the expense involved, municipal leaders, the world over, have recognized the need for effective disposal of waste in high density areas. This need has been recognized since the great plagues of Europe.

Waste material has several serious health concerns associated with it: toxicity, biologic contamination, and pest control. The toxic nature of the materials that make up items such as paint and light bulbs is readily recognized, and not part of any organic disposal.

Biological hazards, include diseases that can be spread from the fluids humans excrete. These can contain everything from the common cold to more dangerous conditions. This is why hospitals incinerate so much of their waste, it is considered to be the only safe way to destroy the material. For the most part, this is recognized as not something that is to be placed in compost<sup id="rf2-555"><a href="#fn2-555" title="I am convinced that much of this material can be safely composted, but the dividing line is very dependant on the individuals involved and is certainly not feasible to determine on a municipal level. For example, it is likely safe for me to throw the 100% organic t-shirt I used to bandage my sliced up arm in the compost; however, an individual who is HIV positive should take greater care in disposing of such material. Somewhere between me (who recently had a battery of blood tests) and a known HIV positive individual is a grey area" rel="footnote">2</a></sup>, but I am confident that not everybody knows where to draw the line.

The most disconcerting part of the Green Carts is that they sit out in the sun, with no air circulation, acting as a breeding pool for disease, and a pest attractant. Halifax does have rats (only natural given that it has been a port city for hundreds of years), but controlling the rodent population should be an important consideration. It was years before it was determined what caused the <a href="http://en.wikipedia.org/wiki/Black_Death" target="_blank">Black Plague</a> outbreaks, and more recently it was years before it was determined what caused the [Hantavirus](http://earthobservatory.nasa.gov/Features/Hanta/) outbreaks, but in both cases it was determined to be rodents (rats and mice respectively). While controlling pest infestations is a continuous struggle for humans, the first step is always the same: control the food supply. Whether your pests are <a href="http://www.youtube.com/watch?v=iEl4L149dEA" target="_blank">ants</a>, [mice](http://www.youtube.com/watch?v=6A-Opqo_hDY), <a href="http://www.youtube.com/watch?v=63QOBq08Fxk&feature=fvw" target="_blank">rats</a>, <a href="http://cnettv.cnet.com/coyote-gets-head-stuck-jar/9742-1_53-50050364.html" target="_blank">coyotes</a>, <a href="http://www.youtube.com/watch?v=4JkQjOebMK0" target="_blank">grizzly</a>, or <a href="http://www.polarbearalley.com/polar-bears-of-churchill-cinnamon.html" target="_blank">polar bears</a>; the first step is to ensure there is no free food sitting around that would encourage them to move into the area.

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-555">
    <p>
      One item we got rid of was an office chair that we had fished out of a dumpster three years before. It was broken when we found it, but was serviceable, so we patched it and used it for years. When we threw it out, some young guy commented that it was nicer than the chair he had. I think he was trying to shame me for being so wasteful (while he threw out what appeared to be a nice desk, bed, and nightstand). I told him that this was where I got it from, this was where it was going&hellip; Ashes to ashes, Dump to dump.&nbsp;<a href="#rf1-555" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-555">
    <p>
      I am convinced that much of this material can be safely composted, but the dividing line is very dependant on the individuals involved and is certainly not feasible to determine on a municipal level. For example, it is likely safe for me to throw the 100% organic t-shirt I used to bandage my sliced up arm in the compost; however, an individual who is HIV positive should take greater care in disposing of such material. Somewhere between me (who recently had a battery of blood tests) and a known HIV positive individual is a grey area&nbsp;<a href="#rf2-555" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
</ol>