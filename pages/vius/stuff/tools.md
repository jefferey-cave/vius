---
id: 1607
title: Tools
date: 2011-10-14T13:54:38+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1607
---
This is a list of must-have tools. Some of them I have, some of them I still need

<http://readynutrition.com/resources/50-simple-tools-used-to-rebuild-haiti_29092011/>

Round point and square nose shovels, preferably heavy-duty variety with extra long blade socket.
  
Pick axe
  
Pulaski axe
  
Rig builder’s hatchet
  
Axe
  
Bow saw
  
24-oz. framing hammer
  
Sledge hammer
  
Digging bars, preferably both pointed and chisel tip varieties; crow bars.
  
Leather or synthetic work gloves
  
Protective eye wear
  
Hard hats
  
Dust masks
  
Contractor-grade wheel barrows
  
Bolt cutters
  
Large-diameter heavy-duty weatherproof rope; small-diameter light-duty line
  
Rope hoist/pulley, minimum 250-lb. capacity
  
Folding knife

&nbsp;

8-point crosscut saw
  
2. Carpenter’s pencil
  
3. Carpenter’s square
  
4. Framing hammers and carpenter’s hammers—smaller sizes for various family members, in addition to the 24-ounce tool above.
  
5. 25-foot Metric/English tape rule
  
6. Bit Brace and a set of solid-center auger bits, ¼ inch through 1 inch
  
7. Utility knife, spare blades
  
8. High-tension hacksaw and selection of spare blades

&nbsp;

1/2 inch exterior-grade plywood, which has the structural stability to help frame out a building’s wall.
  
2. 2 x 4 x 8 lumber by the pallet
  
3. 8-d common nails
  
4. 12-d and 16-d common nails
  
5. blue tarps in various sizes (5 x 7, 10 x 10, 12 x 20)
  
6. 6-mil plastic sheet, roll
  
7. 5-gallon plastic buckets
  
8. Self-stick roll roofing

&nbsp;