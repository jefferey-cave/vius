---
id: 1872
title: Sam Harris is a Scary Man
date: 2012-05-09T12:47:55+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1872
permalink: /2012/05/09/sam-harris-is-a-scary-man/
image: /files/2012/05/India-Nudity.jpg
categories:
  - Opinions
---
[Sam Harris](http://en.wikipedia.org/wiki/Sam_Harris_(author) "Wikipedia: Sam Harris") is a Neuroscientist who actively advocates for a scientific approach to Morality.

I recently watched a presentation by Mr. Harris that he gave at <a title="TED: Ideas worth sharing" href="http://www.TED.com" target="_blank">TED </a>entitled <q>Science Can Answer Moral Questions</q>. While I agree with his overall point, I am somewhat disturbed by the techniques and examples used to sell his ideologie to the audience.

Mr. Harris is one of those individuals to whom the end justifies the means, or perhaps truth is not allowed to get in the way of The Truth (aka His Truth).

<span id="Contradictions_and_Bias">

<h1>
  Contradictions and Bias
</h1></span>

<img class="alignright size-full wp-image-1892" title="India Nudity" alt="" src="/media/2012/05/India-Nudity.jpg" />

Immediately, Mr. Harris failed to ask some fundamental questions and demonstrated serious bias. At about 10:29 he discusses covering women's bodies, I was listening, not watching and the first thing that went through my head was a bikini. Once I realized he was speaking of the Middle East, I felt that he fails to recognize that the society he lives in has the same rules he is chastising. Only to have him question nudity at 12:14. So he suggests that women <q>should be allowed to wear whatever they want</q> unless it disagrees with his sensibilities (nudity). Unfortunately, he fails to reconcile his moral judgement with <a title="Men Aren’t Hard Wired To Find Breasts Attractive" href="http://broadblogs.com/2010/11/04/men-aren%E2%80%99t-hard-wired-to-find-breasts-attractive/" target="_blank">modern Oceanic Cultures' lack of sexual or moral implications</a> (as noted by scientists in the field of morality).

<img class=" wp-image-1893 aligncenter" title="Oceana nudity" alt="" src="/media/2012/05/Oceana-nudity.jpg" width="576" height="324" />

At about 16:00, he posits a contrast between the Dali Llama and Ted Bundy. His claim is that the Dali Llama is an expert on morality, simply because the Dali Llama's sense of morality tends to agree with his. He is seeking experts whose credentials are <q>they agree with me</q>. I happen to like his choice, but that is luck, not careful reasoning.

This is as dangerous (perhaps more) than listening to religious leaders who claim to have received their guidance from <q>a voice in a whirlwind</q>. At least the religious leaders have had an evolutionary pressure applied to their ethics. He is really making contradictory statements when he denigrates religious leaders but then chooses an expert based on the fact that the agree with his preconceptions of morality.

# Expert Testimony

Mr. Harris attempts to justify his rational through an analogy in the realm of physics. He demonstrates that if he (Mr. Harris) were to show up at a physics conference and tell Edward Whitten (an expert physicist) that he doesn't <q>like</q> String Theory, he would be ignored, and Mr. Whitten would be listened to, because Mr. Whitten is the expert. This argument is backwards. Mr. Harris is correct, he should be ignored if he comes in stating that he doesn't <q>like</q> String Theory, not because he isn't an expert, but because we don't care. It is not important that we like or dislike String Theory, only whether it is accurate or not accurate. If Mr. Harris arrives at the conference and states he doesn't like String Theory _and offers a compelling argument_, we would begin to accept Mr Harris _as an expert._

Mr Harris' argument is backwards. We do not accept the arguments of people we consider experts, we consider people experts when we are forced to accept their arguments. It is the act of studying a matter, developing an opinion, and (most importantly) testing or defending it that makes people experts. Testing and defending through debate and discussion is very important because _experts don't always agree_, and nor should they. It is the act of disagreement that allows us to root out false ideas, and proves people to be experts.

His point is that we must exclude non-expert opinions in the moral sphere because they come from non-experts. This puts our moral leaders in a position to become moral authoritarians. He is correct, we must exclude non-expert opinions, but he fails to identify what makes them non-expert. It is not important whose mouths the ideas come from, but that the ideas themselves are correct or incorrect. We listen to experts because they are more likely to have correct ideas, but <q>more likely</q> does not mean we get to exclude alternate ideas. Each idea must be tested independently. In the case of non-expert moral ideas, we exclude their ideas not because they come from non-experts, but because the ideas themselves have already been tested and found lacking.

It is self-evident that at some point experts were non-experts. Every new an great idea needed to be brought forward tested against the ideas of experts of that time. Eventually, the good ideas are accepted and the person that brought them forward is considered an expert, because (after testing) their idea was demonstrated to be right. We must not exclude new ideas, only old ideas that have been proved wrong.

# Group Identity

> Does the Taliban have a point of view, on physics, that is worth considering?

No, but then again neither does MIT.

Groups can't have opinions, only individuals within those groups. In fact, groups can often be defined by their leadership. Those leaders being selected based on their expertise, or in their ability to select experts to advise them.

It is not the group itself that carries an opinion on a given subject but a critical mass (that mass is set by the influentialness of certain members of the group) of individuals subscribe to a set of views. It is very possible that that there are expert physicists within the Taliban, and that their ideas are well worth considering. By this token, the Taliban may in fact have as valid ideas on physics as MIT does.

> How is their ignorance any less obvious on the subject of human well-being.

It is alright for us to state that we _believe_ their experts are ignorant on the subject of morality, but we cannot state that it is _obvious_ without an unbiased refutation of the ideas.

# Conclusion

He is not unique in the belief that ethics is a scientific topic that should be approached with cold unbiased reasoning. Several existing sciences are defined by this very subject, with very notable experts in their fields:

  * Pythagorus (~570BC)
  * Socrates (~399BC)
  * Aristotle (~384BC)
  * St. Thomas Aquinas, [Summa Theologica](href="http://www.ccel.org/ccel/aquinas/summa.toc.html) (1265)
  * Immanual Kant, <a title="Fundamental Principles of the Metaphysic of Morals" href="http://www.gutenberg.org/ebooks/5682" target="_blank">Fundamental Principles of the Metaphysics of Morals</a> (1785)
  * Jeremy Bentham, <a title="An Introductino to the Princples of Morals and Legislation" href="http://www.econlib.org/library/Bentham/bnthPML.html" target="_blank">An Introduction to the Principles of Morals and Legislation</a> (1789)
  * Fredrick Bastiat, <a title="The Law" href="http://bastiat.org/en/the_law.html" target="_blank">The Law</a> (1850)
  * David Hume, <a title="An Enquiry Concerning the Principles of Morals" href="http://www.anselm.edu/homepage/dbanach/Hume-Enquiry%20Concerning%20Morals.htm" target="_blank">An Enquiry Concerning the Principle of Morals</a> (1898)
  * Ludwig von Mises, <a title="Human Action" href="http://mises.org/document/3250" target="_blank">Human Action</a> (1949)
  * Murray Rothbard, <a title="The Ethics of Liberty" href="http://mises.org/rothbard/ethics/ethics.asp" target="_blank">The Ethics of Liberty</a> (1982)
  * Hans Herman Hoppe, <a title="Economics and Ethics of Private Property" href="http://mises.org/document/860/Economics-and-Ethics-of-Private-Property-Studies-in-Political-Economy-and-Philosophy-The" target="_blank">Economics and Ethics of Private Property</a> (1993)

Some of their ideas have been refuted, some of them still stand; some are accepted without question, some are still being debated. This is the nature of ideas and expertise. Unfortunately, Mr. Harris, through his own false arguments, and lack of understanding of the work already done in this area of study, demonstrates that he himself is not an expert on the matter.

> It is possible for individuals and even for whole cultures to care about the wrong things. Which is to say it is possible to have beliefs and desires that reliably lead to needless human suffering

