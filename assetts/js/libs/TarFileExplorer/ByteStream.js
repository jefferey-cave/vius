
class ByteStream{

	constructor(bytes){
		this.bytes = bytes;
		this.byteIndexCurrent = 0;
	}


	// constants
	static get BitsPerByte(){
		return 8;
	}

	static get BitsPerByteTimesTwo(){
		return ByteStream.BitsPerByte * 2;
	}

	static get BitsPerByteTimesThree(){
		return ByteStream.BitsPerByte * 3;
	}

	// instance methods
	hasMoreBytes(){
		return (this.byteIndexCurrent < this.bytes.length);
	}

	readBytes(numberOfBytesToRead){
		let returnValue = [];
		for (let b = 0; b < numberOfBytesToRead; b++){
			returnValue[b] = this.readByte();
		}
		return returnValue;
	}

	readByte(){
		let returnValue = this.bytes[this.byteIndexCurrent];
		this.byteIndexCurrent++;
		return returnValue;
	}

	readString(lengthOfString){
		let returnValue = "";
		for (let i = 0; i < lengthOfString; i++){
			let byte = this.readByte();
			if (byte != 0){
				let byteAsChar = String.fromCharCode(byte);
				returnValue += byteAsChar;
			}
		}
		return returnValue;
	}

	writeBytes(bytesToWrite){
		for (let b = 0; b < bytesToWrite.length; b++){
			this.bytes.push(bytesToWrite[b]);
		}
		this.byteIndexCurrent = this.bytes.length;
	}

	writeByte(byteToWrite){
		this.bytes.push(byteToWrite);
		this.byteIndexCurrent++;
	}

	writeString(stringToWrite, lengthPadded){
		for (let i = 0; i < stringToWrite.length; i++){
			let charAsByte = stringToWrite.charCodeAt(i);
			this.writeByte(charAsByte);
		}

		let numberOfPaddingChars = lengthPadded - stringToWrite.length;
		for (let i = 0; i < numberOfPaddingChars; i++){
			this.writeByte(0);
		}
	}
}
