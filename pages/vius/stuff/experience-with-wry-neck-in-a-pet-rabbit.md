---
id: 1737
title: Treatment of Wry Neck in a Pet Rabbit
date: 2012-02-18T20:04:03+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1737
---
<div class='toc toc'>
  <h2>
    Contents
  </h2>
  
  <ol class='toc-odd level-1'>
    <li>
      <a href="#The_Short_Version">The Short Version</a>
    </li>
    <li>
      <a href="#The_Full_Story">The Full Story</a> <ol class='toc-even level-2'>
        <li>
          <a href="#Recurrence">Recurrence</a>
        </li>
        <li>
          <a href="#Veterinarian_Response">Veterinarian Response</a>
        </li>
      </ol>
      
      <li>
        <a href="#Hindsights_2020">Hindsight's 20/20</a> <ol class='toc-even level-2'>
          <li>
            <a href="#Symptoms">Symptoms</a> <ol class='toc-odd level-3'>
              <li>
                <a href="#General_Symptoms">General Symptoms</a>
              </li>
              <li>
                <a href="#Specific_Symptom">Specific Symptom</a>
              </li>
            </ol>
            
            <li>
              <a href="#Disease_Vectors">Disease Vectors</a> <ol class='toc-odd level-3'>
                <li>
                  <a href="#Outdoor_Vegetables">Outdoor Vegetables</a>
                </li>
                <li>
                  <a href="#Mice">Mice</a>
                </li>
              </ol>
              
              <li>
                <a href="#Drugs_Involved">Drugs Involved</a> <ol class='toc-odd level-3'>
                  <li>
                    <a href="#Selamectin">Selamectin</a>
                  </li>
                  <li>
                    <a href="#Invermectin_Panomec">Invermectin (Panomec)</a>
                  </li>
                </ol></ol> 
                
                <li>
                  <a href="#UPDATE:_Stew_is_not_dead_yet_--_2012-10-01">UPDATE: Stew is not dead yet -- 2012-10-01</a>
                </li>
                <li>
                  <a href="#UPDATE:_Stew_is_not_dead_yet_--_2013-10-01">UPDATE: Stew is not dead yet -- 2013-10-01</a>
                </li>
                <li>
                  <a href="#UPDATE:_Stew_is_not_dead_yet_--_2014-10-01">UPDATE: Stew is not dead yet -- 2014-10-01</a>
                </li>
                <li>
                  <a href="#UPDATE:_Stew_is_dead_--_2015-04-10">UPDATE: Stew is dead -- 2015-04-10</a>
                </li></ol> </div> 
                <div class='toc-end'>
                  &nbsp;
                </div> Recently (October 2011), our pet rabbit suffered from an illness that nearly killed him. It is my hope that this story can act to assist others in effective diagnosis and treatment of health conditions in rabbits. Comments and questions are very welcome.</p> 
                
                <p>
                  We love our pet rabbit, <a title="Stew's Profile" href="http://vius.ca/about/rabbit-stew/" target="_blank">Stew</a>. He has been a constant source of annoyance and pleasure for 8¾ years. As an older rabbit, we have to accept that he won't be with us much longer, but we are left with the satisfaction of knowing that he has led a good and healthy life<sup id="rf1-1737"><a href="#fn1-1737" title="In the end, he lived another 3&frac12; years" rel="footnote">1</a></sup>. He has free range of the house, has an excellent diet, and veterinarians have pointed out that he has the physique of a rabbit half his age.
                </p>
                
                <span id="The_Short_Version">
                
                <h1>
                  The Short Version
                </h1></span> <figure id="attachment_808" style="max-width: 300px" class="wp-caption alignleft">
                
                <a href="/media/2011/01/Stew.20110110.jpeg"><img class="size-medium wp-image-808" title="Rabbit Stew" src="/media/2011/01/Stew.20110110.jpeg" alt="Pet Rabbit &quot;Stew&quot;" width="300" height="225" /></a><figcaption class="wp-caption-text">Stew sleeping under a desk</figcaption></figure> <p>
                  Recently, Stew began suffering some serious health issues, at first we wrote them off to being depression (he had recently lost his <a title="Chimutisk" href="http://vius.ca/about/chimutisk/" target="_blank">cage mate</a>) and old age. We began to worry that he was suffering from something more specific when he began running in circles, unable to stop. The uncontrollable running, combined with his cataracts, had him running into furniture. Concerned, we decided to take him to the veterinarian first thing the next morning, and (being worried) began reading online.
                </p>
                
                <p>
                  The veterinarian diagnosed Stew as having had a stroke (consistent with the symptoms) and based on his age and condition, recommended euthanasing him. Based on <a title="Barbi Brown's Bunnies" href="http://www.barbibrownsbunnies.com/ecuniculi.htm" target="_blank">online research</a> we pressed for treatment with an anti-parasitic instead. Believing he would die shortly anyway, she allowed us to proceed with antibiotics and a dewormer. Stew made a full recovery in three weeks, in fact recovering much of his health loss we had initially attributed to depression and old age.
                </p>
                
                <span id="The_Full_Story">
                
                <h1>
                  The Full Story
                </h1></span> <figure id="attachment_818" style="max-width: 225px" class="wp-caption alignright">
                
                <a href="/media/2011/01/FeedingTime.20110112.1.jpeg"><img class="size-medium wp-image-818 " title="FeedingTime.20110112.1" src="/media/2011/01/FeedingTime.20110112.1.jpeg" alt="Supper time for the pets" width="225" height="300" /></a><figcaption class="wp-caption-text">Chimutisk (ferret) and Stew (rabbit) eating together</figcaption></figure> <p>
                  After the death of this mortal enemy (and best friend) <a title="Chimutisk" href="http://vius.ca/about/chimutisk/" target="_blank">Chimutisk the Ferret</a> we were concerned that Stew would begin to slow down. Since the day they were introduced Stew set out with a vengeance to ensure his dominance in the household. This primarily involved chinning (territorial marking) anything Chimutisk touched, was suspected of touching, showed an interest in, or could possibly show an interest in at some point in the future; as well as smacking the ferret around periodically for good measure. This became constant source of amusement for us when we realized that this was sibling rivalry between the two. When seperated for any length of time they desperately missed one another, and could be seen searching one another's sleeping areas. On several occasions, Stew could be found sleeping pressed up to Chimutisk through the bars of Chimutisk's cage.<sup id="rf2-1737"><a href="#fn2-1737" title="While both of our pets had free range of the house while we were home, Chimutisk needed to be locked in his cage while we were away, for his own protection. He managed to get into enough trouble while we were watching, let alone while we were at work or asleep." rel="footnote">2</a></sup> In hindsight, the competition between the two of them gave meaning to one another's lives: Chimutisk avoiding the rabbit at all cost, and Stew taking every opportunity to let the ferret know who was boss.
                </p>
                
                <p>
                  After Chimutisk's death, Stew was left without the constant challenge and stimulation of having to protect all of his belongings from the ferret. Like a human whose husband has died, we expected Stew to shortly follow. Sure enough, within a few months we started to see him spending more time sitting quietly in corners and under chairs, rather than patrolling the house and marking his belongings.
                </p>
                
                <p>
                  One day we noticed that he seemed to be running in circles. At first we laughed, thinking this was some new game of his. Eventually we started to notice that he was doing this uncontrollably. We began treating him for basic illness, believing he was dizzy from some sort of flu or cold. As it continued, we resigned ourselves to the fact that he was old, possibly senile, and was simply slipping away. We began the process of grieving and preparing for the loss.
                </p>
                
                <p>
                  Over the course of several days, we watched his condition worsen: the circles got tighter, eventually he could not walk in a line at all, his hind legs began to show weakness, and eventually his eyes and head would <q>track</q> to the left (his eyes would go all the way to the left, his head would follow, he would jerk his head back straight, then start over &#8211; about 1 second per repetition). The tracking is what changed our attitude, and called us to action.
                </p>
                
                <p>
                  It is one thing to loose an elderly pet to <q>old-criteritis</q>, it is totally another to watch him suffering from some illness. The Wife and I suddenly got a little bit of fight back. Even assuming he was dying from some old age illness, at the very least we can treat the immediate symptoms and make him more comfortable. We resolved to take him to the vet at the next available opportunity: two days away (it was Saturday night when we got our fight back, the vet would not be available until Monday morning 8am). While we were resolved to get something done, we were left with a <strong>full</strong> day to sit around and worry.
                </p>
                
                <p>
                  On Sunday, while The Wife was out working an evening shift, I sat in front of the computer looking up possible causes and treatments for Stew's condition. When she came home I could hardly contain myself, my reading had turned up a possible cause, and more importantly cure. The running in circles and tracking in his eyes seemed to be consistent with Wry-Neck, a debilitating and lethal condition. I was fortunate to come across a reference to someone who has had success in treating the condition:
                </p>
                
                <p>
                  <a title="Barbi Brown's Bunnies" href="http://www.barbibrownsbunnies.com/ecuniculi.htm" target="_blank">Barbi Brown's Bunnies: Wry Neck Page</a>
                </p>
                
                <p>
                  Ms. Brown suggests that, contrary to conventional wisdom, the condition is caused by a parasite that infects the brain. The symptoms we were seeing would be caused by the parasite attacking brain tissue, causing the rabbit to loose control of his own body. Fortunately, this leads directly into a treatment: Ivermectin, a livestock de-wormer. Fortunately, I live in a rural area, and the Country Store is a mere 15 minutes up the road. Left with a choice (run to the Farm Store, or veterinarian), we decided to proceed with the Veterinarian, but to make sure she was aware of this contrary diagnosis/treatment.
                </p>
                
                <p>
                  Monday morning, The Wife was waiting in the car at the veterinarian offices waiting for them to unlock the doors. Upon examination, the veterinarian's diagnosis was simple: Stew was over 8 years old, he will be inactive and won't have much quality of life, especially being confined to a cage the way rabbits are. This was shocking to The Wife, the description was completely at odds with <a title="Stew's Profile" href="http://vius.ca/about/rabbit-stew/" target="_blank">our experience</a> with our rabbit.<sup id="rf3-1737"><a href="#fn3-1737" title="Please take a moment to read Stew&rsquo;s profile if you haven&rsquo;t yet" rel="footnote">3</a></sup> She tried to explain that he had been seen chasing a mouse only a few months earlier, that his slow down was very recent, and that he had full run of the house (only confined to a cage at night). The vet was dubious, in her mind, the symptoms indicated a stroke or brain tumour. She gently (but firmly) suggested that it was time to put Stew down; that as his owners, we had a responsibility to do what was best for him, not us. The wife adamantly refused: if it was his time, so be it, but if he was suffering from an infection, euthanasia was totally uncalled for. We <strong>would</strong> attempt to treat the infection <strong>before</strong> we wrote him off.
                </p>
                
                <p>
                  The vet was not impressed. She believed Stew had suffered a stroke that had caused significant brain damage. She more adamantly suggested terminating Stew, The Wife more adamantly insisted that we would not entertain termination until we had attempted to treat potential simple causes. In her words: <q>I am not giving up on him if this is a simple fix</q>. The Wife came home with antibiotics, and anti-parasitics, and a brochures on <q>doing the right thing</q> for your pet. We weren't impressed.
                </p>
                
                <p>
                  We began medication immediately, with few to no results. On Friday, we phoned the vet and stated that he was getting better (maybe 75%), but later we had to confess to one another that it was hard to say, and perhaps we were just wanting him to get better.
                </p>
                
                <p>
                  We cried ourselves to sleep.
                </p>
                
                <p>
                  The next morning, we were awakened by a crash! And then another, and another, and &hellip; it couldn't be. It sounded like Stew was attempting to push the litter box out of his cage. This is a difficult proposition for him because he has to get it (~5 pounds) over the sill of the cage door (~2 inches). That's a big lift for a little bunny who only weighs about 4 pounds, and was made all the more difficult by the fact that the door was latched shut.<sup id="rf4-1737"><a href="#fn4-1737" title="I have never accused that Rabbit of having an over abundance of brains" rel="footnote">4</a></sup> The banging was him lifting the litter box and dropping it repeatedly. When that did not get the results he had hoped for, he got into the litter box and began flinging litter out the door. This was Stew's old means of letting us know he wanted to get out of bed. He was back to himself&hellip; 100% recovery.
                </p>
                
                <p>
                  Over the course of the rest of the day, we kept an eye on him. He was back to his old self. Oh he wasn't climbing the furniture any more, and he was still sleeping a lot, but he was not having any difficulty with paralysis of his hind legs, and was not tracking at all. He did some exploration of the house, and then slept for the rest of the day. We had our bunny back.
                </p>
                
                <p>
                  Interestingly, as the week progressed, he continued to get better. By the end of the week we were wishing for him to get sick again. He had climbed onto the coffee table to steal and eat bills, he had decided to start climbing the stairs in the house (something he had never done before), there was a new game that involved running a gauntlet of furniture at full sprint with his eyes closed (he's totally blind), and <strong>not</strong> petting him was firmly reprimanded with boxing and nipping. This was a 125% recovery, he was back to his health prior to Chimutisk's death. We had attributed much of his decline to depression and lack of activity due to his cage mate's death. We were suddenly forced to acknowledge that he may have been sick for a significantly longer time.
                </p>
                
                <p>
                  We felt vindicated and relieved. The vet had been wrong, and not just wrong, but mortally wrong. The proof was before us though, the rabbit was terrorizing the house, for 8 hours a day, barely letting up. He was living a very active life, better than what she believed rabbits could have in full health.
                </p>
                
                <span id="Recurrence">
                
                <h2>
                  Recurrence
                </h2></span> 
                
                <p>
                  Months later (Feb 2012), I noticed Stew seemed to be trapped in a corner behind his cage. He had gone behind his cage to investigate a box I had put there (the box was to prevent Stew from investigating the mouse trap inside the box), but his eyes were tracking to the left and he kept missing the exit when he approached it. Thinking back, he had been quiet the last day or two, sitting in corners and not interacting as much as usual.
                </p>
                
                <p>
                  Concerned about dealing with the veterinarian we decided to treat Stew ourselves this time. I drove to the Farm Store, to pick up the Ivomec. I must state that the gentleman at the store was very helpful. I explained what I was attempting to treat, and he suggested I purchase a different brand. The smallest Ivomec was $60, but was designed for treating herds of sheep, he had another brand of Ivermectin that was marketed towards horses and was designed for treating a single horse for $12. It was the same medication, but less of it (still plenty of it though, 600kg of horse vs 2kg of bunny). He and I even stood at the counter for ½ an hour together doing the math to determine how much to dilute the medication by and the corresponding dosage volumes.
                </p>
                
                <p>
                  At this point<sup id="rf5-1737"><a href="#fn5-1737" title="2012-02-19" rel="footnote">5</a></sup> we have given Stew his first dose, and are hopeful. In the mornings he is still very active, but by evening he is exhibiting severe symptoms (though only mildly): tracking eyes, weakness in the hind quarters. We will wait and see.
                </p>
                
                <p>
                  If this current treatment is successful, we will likely regularly treat Stew at 3 month intervals. He likely has an underlying issue, but given his age, I don't want to put him through extensive testing. We will just treat this condition and give regular boosters.
                </p>
                
                <span id="Veterinarian_Response">
                
                <h2>
                  Veterinarian Response
                </h2></span> 
                
                <p>
                  Would our veterinarian humour us this time? We aren't sure. This is an ongoing problem we have with our vets. Unfortunately, vets only ever see animals when they are sick. They do not see the rest of the time when the animal is healthy.
                </p>
                
                <p>
                  Rather than help us, we are concerned that the vet will take the opposite track. Because she has not seen the intervening time that the rabbit was healthy and active, if we show up she will likely perceive that the rabbit never got better: he showed up with stroke symptoms, and 6 months later is still suffering from those symptoms. She will believe that the real problem is owners that will allow the animal to suffer because they can't let go. She will not give us new medication to treat a disease she does not believe the animal is suffering from. Further, there is a risk that she will then escalate the issue to animal welfare groups; while I'm not convinced they could do anything, this is a small town, gossip runs fast, and people will ostracise you.
                </p>
                
                <p>
                  I have dealt with 4 veterinarians with two pets over the years, and (with the exception of one who I would like as my personal physician) I have learned not to trust their judgement.
                </p>
                
                <span id="Hindsight8217s_2020">
                
                <h1>
                  Hindsight's 20/20
                </h1></span> 
                
                <p>
                  In hindsight, I think the story as told is somewhat incorrect and incomplete. When we first lived through it we were attempting to learn what the problem was, and even learn that there was a problem. Looking back on it, there were clues as to what was happening.
                </p>
                
                <span id="Symptoms">
                
                <h2>
                  Symptoms
                </h2></span> 
                
                <p>
                  There were several symptoms that were present that we had either overlooked or assumed were not as significant as they were. Some of the symptoms were vague, some clear indicators of a problem, and some symptoms were clear indicators of <strong>this</strong> problem.
                </p>
                
                <span id="General_Symptoms">
                
                <h3>
                  General Symptoms
                </h3></span> 
                
                <ul>
                  <li>
                    <em>Huddling</em> in corner was evident in hindsight. There were several reasons this was particularly difficult to spot. Because the rabbit has the free range of the house, it is not uncommon for him to hide somewhere for a nap. Identifying it as <q>huddling in a corner</q> vs <q>cozy place to sleep</q> was not easy. This was interpreted more as a symptom of general unwellness.
                  </li>
                  <li>
                    <em>Loss of appetite</em> was one of the major signs that there was a problem. Every Friday, Stew gets two Doritos. When he was sick he showed no interest in his weekly treat. As the condition worsened we were force feeding him a liquid diet via syringe.
                  </li>
                  <li>
                    <em>Cæcotrophs</em> were found everywhere. Either he was unwilling (due to loss of appetite) or unable (due to dizziness/paralysis) to eat them. This is a general sign of unwellness in our rabbit, often related to constipation.
                  </li>
                </ul>
                
                <span id="Specific_Symptom">
                
                <h3>
                  Specific Symptom
                </h3></span> 
                
                <ul>
                  <li>
                    <em>Nystygmus (darting eye)</em> was the creepiest of symptoms and the one that made me feel the worst for him. I believe this symptom causes several of the other symptoms. Basically this results in the animal following its line of sight, getting dizzy, walking in circles, and laying down to relieve the dizziness. Watching the eyes track and dart was very disturbing. It looked very unnatural. While it was very obvious once noted, looking the rabbit in the eyes was the trick. Normally he is 1&#8243; off the ground and I am 5.5&#8242; off the ground.
                  </li>
                  <li>
                    <em>Walking in circles</em> was not a fun new game. Never having seen the behaviour, we assumed it was harmless and silly for about a day. When he couldn't stop and began running into furniture we began to worry.
                  </li>
                  <li>
                    <em>Lying close to the floor with head down</em> is often a sign of contentment in our rabbit. When he is utterly relaxed his head tends to drift to the floor. The difference was that he was doing it with his eyes wide open. When he is relaxed, he falls asleep and his head lowers; when he was sick, he puts his head down because he is dizzy.
                  </li>
                  <li>
                    <em>Paralysis of the hind quarter</em> was not evident right away. This presented in gradients starting with <q>Staggering gait</q>
                  </li>
                  <li>
                    <em>Staggering gait</em> was definitely present, though initially this was attributed to age and hardwood floors. On carpet, Stew was fine, but when he hit hardwood floor he would walk more carefully.
                  </li>
                  <li>
                    <em>Bed wetting</em> was prevalent. While my rabbit enjoys urinating everywhere in his cage, he is quite fastidious. As his illness became worse he began laying in his own urine. We would pick him up and find his entire belly and chest soaked with urine. This was a particularly alarming symptom for us. This may have been a side effect of the paralysis.
                  </li>
                  <li>
                    <em>Stargazing</em> may have been present though being blind, it is hard to tell. His eyes are unable to focus on anything anyway.
                  </li>
                </ul>
                
                <span id="Disease_Vectors">
                
                <h2>
                  Disease Vectors
                </h2></span> 
                
                <span id="Outdoor_Vegetables">
                
                <h3>
                  Outdoor Vegetables
                </h3></span> 
                
                <p>
                  Stew has lived on a diet primarily made up of leafy greens for most of his life. This is expensive when purchased at the grocery store, so The Wife and I have long had a policy of walking up and down alley ways picking dandelions for the rabbit. Since moving to a rural area, we have more than enough greens simply in our own yard. Every day we spend a little time picking dandelions, clover, and wild grass for his meals.
                </p>
                
                <p>
                  One thing I have since learned is that Raccoons and mice are potential vectors for this disease. I have seen both raccoons and mice around our house attempting to raid chicken feed supplies. There is a possibility that an infected wild animal infected grass or greens that we then picked and fed to Stew.
                </p>
                
                <span id="Mice">
                
                <h3>
                  Mice
                </h3></span> 
                
                <p>
                  Prior to purchasing our new rural home, it had remained unoccupied for a few months. As country readers know, keeping unwanted pests out of your home requires constant vigilance. With everyone away, the mice came out to play. We spent at least two months intensively trapping mice, prior to moving in.
                </p>
                
                <p>
                  Chimutisk was a ferret. Ferrets have a distinct predator odour. As soon as we introduced him to the house, the mice left. He was no longer an effective predator, but the smell was enough to warn the mice off. Mice are a known vector for this parasite.
                </p>
                
                <p>
                  Shortly after Chimutisk's death we started to note mouse sign in the house and went through a period of heavy trapping to get the problem under control. Stew actually played the role of mouser for a period of time. If he caught sound or smell of a mouse, he would chase and try to kill them during this period. Did I mention he is active? This was about time he started to display his initial symptoms.
                </p>
                
                <p>
                  We had not seen another mouse until winter and some home renovations. The combination of winter enticing mice into the house, and renovations changing their movement through the house, reduced the effectiveness of the trapping, and we had a single mouse move in. This mouse regularly visited Stews cage for about a week (the time it took for us find where the mouse was getting in and out, and deal with the new movement) prior to Stew getting sick again.
                </p>
                
                <p>
                  Each illness event does seem to correspond to a mouse getting into our home. Prior to moving in we trapped a mouse a week. After moving in, we did not see another mouse until we did some renovations which opened a path into the home for a single mouse. The recurrence occurred after performing renovations again, and a single mouse (actually a vole) finding a new way into the house. In both cases, it took about a week to identify the mouse's patterns and lay <em>effective</em> trapping.<sup id="rf6-1737"><a href="#fn6-1737" title="While we have traps around the perimeter of the basement that need to be checked regularly, the two mice that have actually made it into the house proper have posed a fascinating challenge to trap. I generally throw 3 traps down immediately and then spend a couple of days watching for the invader. Invariably, after 4 or 5 days of watching, I realize that the standard trapping places are totally ineffective and I lay another trap in a strategic location specifically for this single invader. In both cases, I have had the little devil within 2 days of laying the specific trap. Its amazing how challenging they can be." rel="footnote">6</a></sup>
                </p>
                
                <span id="Drugs_Involved">
                
                <h2>
                  Drugs Involved
                </h2></span> 
                
                <span id="Selamectin">
                
                <h3>
                  Selamectin
                </h3></span> 
                
                <p>
                  Prior to moving to the country, and given Chimutisk's illness, our veterinarian suggested a preventative course of flea medication. Neither ferrets, nor rabbits, can harbour fleas, but the prior occupants were known to have had a dog. Given Chimutisk's failing health there was a concern that while fleas require a dog to reproduce, they would attack Chimutisk until they died off, and we were concerned that he wouldn't survive it. A 6 month course of <a title="Revolution for Pets" href="http://www.revolutionpet.com/revolutionpet.aspx" target="_blank">Revolution </a>(selamectin) was prescribed for both Stew and Chimutisk as a preventative.
                </p>
                
                <p>
                  Stew was still receiving his monthly dosages until about a month before he got sick. Revolution may have been acting as an effective treatment against E. Cuniculi.
                </p>
                
                <span id="Invermectin_Panomec">
                
                <h3>
                  Invermectin (Panomec)
                </h3></span> 
                
                <p>
                  According to <a href="http://www.barbibrownsbunnies.com/ecuniculi.htm">Barby Brown's Bunnies</a>, Invermectin (a common cattle dewormer) is highly effective. Living in a rural community, and deciding to treat Stew myself, we go to the local Feed&Seed to get some Invermectin. Unfortunately, the exact brand recommended only came in a $70 bottle (suitable for treating your entire heard for a year). Not needing that much, and explaining the situation to the owner, he suggested an oral version for horses. Medication for 600 kg of horse seemed more manageable than 100 head of cattle.
                </p>
                
                <p>
                  The brand name is <a href="http://ca.merial.com/horses/panomec-paste.asp">Panomec</a>, and it contains 120mg of ivermectin, is $20, and apple flavoured.
                </p>
                
                <p>
                  The syringe contains enough medication for a 600kg animal, Stew is 2kg. Some adjustment was required: (please check my math&hellip; to this day, I kind of suspect I overdosed him)
                </p>
                
                <ol>
                  <li>
                    Dispense 100kg worth (clearly marked on the side of the syringe)
                  </li>
                  <li>
                    Mix with water to bring to 50ml
                  </li>
                  <li>
                    Administer 1ml (1cc) orally
                  </li>
                </ol>
                
                <p>
                  The apple flavouring did not help.
                </p>
                
                <span id="UPDATE:_Stew_is_not_dead_yet_8212_2012-10-01">
                
                <h1>
                  UPDATE: Stew is not dead yet &#8212; 2012-10-01
                </h1></span> 
                
                <p>
                  Though he may be if keeps biting me for attention.
                </p>
                
                <p>
                  This marks the one year anniversary of when the vet would have put him down.
                </p>
                
                <span id="UPDATE:_Stew_is_not_dead_yet_8212_2013-10-01">
                
                <h1>
                  UPDATE: Stew is not dead yet &#8212; 2013-10-01
                </h1></span> 
                
                <p>
                  Though he may be if he continues to steal the paper work off the coffee table.
                </p>
                
                <p>
                  This marks the two year anniversary of when the vet would have put him down.
                </p>
                
                <span id="UPDATE:_Stew_is_not_dead_yet_8212_2014-10-01">
                
                <h1>
                  UPDATE: Stew is not dead yet &#8212; 2014-10-01
                </h1></span> 
                
                <p>
                  Though he may be if he continues to noisily throw his litter around at 2 in the morning.
                </p>
                
                <p>
                  This marks the three year anniversary of when the vet would have put him down.
                </p>
                
                <span id="UPDATE:_Stew_is_dead_8212_2015-04-10">
                
                <h1>
                  UPDATE: Stew is dead &#8212; 2015-04-10
                </h1></span> 
                
                <p>
                  He came over to The Wife this morning and said good-bye, just before finally breathing his last. 12 years and 10 months&hellip; not a bad run for a bunny.
                </p>
                
                <p>
                  <span>Stew, You missed your last Dorito by hours</span>
                </p>
                
                <p>
                  &nbsp;
                </p>
                
                <hr class="footnotes" />
                
                <ol class="footnotes">
                  <li id="fn1-1737">
                    <p>
                      In the end, he lived another 3&frac12; years&nbsp;<a href="#rf1-1737" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
                    </p>
                  </li>
                  
                  <li id="fn2-1737">
                    <p>
                      While both of our pets had free range of the house while we were home, Chimutisk needed to be locked in his cage while we were away, for his own protection. He managed to get into enough trouble while we were watching, let alone while we were at work or asleep.&nbsp;<a href="#rf2-1737" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
                    </p>
                  </li>
                  
                  <li id="fn3-1737">
                    <p>
                      Please take a moment to read <a title="Stew" href="http://vius.ca/about/rabbit-stew/" target="_blank">Stew's profile</a> if you haven't yet&nbsp;<a href="#rf3-1737" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
                    </p>
                  </li>
                  
                  <li id="fn4-1737">
                    <p>
                      I have never accused that Rabbit of having an over abundance of brains&nbsp;<a href="#rf4-1737" class="backlink" title="Jump back to footnote 4 in the text.">&#8617;</a>
                    </p>
                  </li>
                  
                  <li id="fn5-1737">
                    <p>
                      2012-02-19&nbsp;<a href="#rf5-1737" class="backlink" title="Jump back to footnote 5 in the text.">&#8617;</a>
                    </p>
                  </li>
                  
                  <li id="fn6-1737">
                    <p>
                      While we have traps around the perimeter of the basement that need to be checked regularly, the two mice that have actually made it into the house proper have posed a fascinating challenge to trap. I generally throw 3 traps down immediately and then spend a couple of days watching for the invader. Invariably, after 4 or 5 days of watching, I realize that the standard trapping places are totally ineffective and I lay another trap in a strategic location specifically for this single invader. In both cases, I have had the little devil within 2 days of laying the specific trap. Its amazing how challenging they can be.&nbsp;<a href="#rf6-1737" class="backlink" title="Jump back to footnote 6 in the text.">&#8617;</a>
                    </p>
                  </li>
                </ol>