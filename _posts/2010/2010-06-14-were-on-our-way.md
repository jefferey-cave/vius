---
id: 310
title: We're on our way!
date: 2010-06-14T03:15:34+00:00
author: The Wife
layout: post
guid: http://www.vius.ca/?p=310
tags: ['MoveNS']
categories:
  - HomeSteading
---

We have eventually hit the road, albeit a day later than expected.

At 9 am, The Husband stood in the driveway shaking his head, he said, <q>not
everything is going to fit</q>. 3 hours later we had the puzzle together and
started our journey.

We are in Swift Current, Saskatchewan tonight, it's sunny and hot and we are
tired.
