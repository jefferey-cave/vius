---
id: 1828
title: 'Global Warming: And Nothing Happened'
date: 2012-03-10T19:48:05+00:00
author: Kavius
layout: post
categories:
  - Opinions
  - Analysis
  - Essays
---

I've been reading a lot of Global Warming articles over the last few days, and suddenly a few events over the last few years coalesced for me:

  * Woman goes hysterical at a [town hall meeting](/2011/10/councillor-mcwade/). The meeting is regarding what regulations the town should enact around Windmills. The woman get's up and literally says, <q>We are all going to drown, but that's no reason to stop trying</q>.
  * I read an <a title="An Unsettling Week for Settled Science" href="http://www.forbes.com/sites/patrickmichaels/2012/02/10/an-unsettling-week-for-global-warmings-settled-science/" target="_blank">article</a><sup id="rf1-1828"><a href="#fn1-1828" title="I thought it was Kaviusrey Tucker, but I can&rsquo;t find the reference anymore. I swear it was about two weeks ago, and it discussed the feedback mechanisms involved. This one references the same data." rel="footnote">1</a></sup> pointing to the inaccuracy of the Global Warming climate models that people are panicking about
  * I recently moved to a coastal region, I began to sarcastically wonder if I would ever get an ocean view
  * The Government of Nova Scotia recently [announced more programs](http://www.gov.ns.ca/news/details.asp?id=20120305001) to determine how its going to adapt to rising waters.

This got me thinking. Surely someone has used Google Earth to plot this out. It would be a simple matter of activating terrain, and putting a blue surface at whatever the future sea level was predicted to be. Sure enough someone has.

<!--more-->

<a title="http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=7" href="http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=7" target="_blank">http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=7</a>

Woo Hoo! I get an ocean view!

But wait. I haven't actually established that the guy that did this knows what he's talking about. So I looked up the anticipated rise in sea level on <a title="Sea Level Projections" href="http://en.wikipedia.org/wiki/Current_sea_level_rise#Projections" target="_blank">Wikipedia</a>. That's when things start to get interesting.

So the woman that had her little freak out at the town council meeting was hysterical because she believed the waters were going to rise and swallow us all. Looking at the map supplied, we can see she is out to lunch, but I can also see that the entire town of Annapolis Royal will be underwater. However, look a little closer and you get some idea of the misinformation that goes on.

The **worst** case scenario presented by the <a title="Wikipedia: Sea Levels" href="http://en.wikipedia.org/wiki/Current_sea_level_rise#Projections" target="_blank">US National Research Council</a> is a two metres rise in water over the next **90 years** (To the hysterical lady: I think we will have time to get out of the way). But, look at the map again its set to 7 metres by default, that's more than 3 times the worst case scenario!

Fortunately, we can adjust the settings on the map:

<a title="http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=20" href="http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=20" target="_blank">http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=20</a>

Nice! The Wife gets her ocean view, and the airforce pilots stationed at Greenwood Airforce Base become naval pilots really quick (and quit using my house as a landmark for aerial maneuvers)&hellip;

Oh, wrong way. This is what the actual water levels will look like.

<a title="http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=2" href="http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=2" target="_blank">http://flood.firetree.net/?ll=44.7466,-65.5018&zoom=14&type=hybrid&m=2</a>

So basically, the town of Annapolis Royal doesn't change. The only places that get wet, is the marsh that The Wife uses for her walks. No ocean view for me. In fact, no ocean view for the Annapolis Royal Fire Department just around the corner from the water.

As ever, I am forced to ask that we steer clear of the rhetoric and hysterics and look at the actual consequences of what is going to happen (and how fast it is expected to happen).

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1828">
    <p>
      I thought it was Kaviusrey Tucker, but I can't find the reference anymore. I swear it was about two weeks ago, and it discussed the feedback mechanisms involved. <a title="Forbes: An Unsettling Week For Global Warming's &quot;Settled Science&quot;" href="http://www.forbes.com/sites/patrickmichaels/2012/02/10/an-unsettling-week-for-global-warmings-settled-science/" target="_blank">This one references the same data</a>.&nbsp;<a href="#rf1-1828" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>