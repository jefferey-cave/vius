
---
id: 
title: Don't Destroy the UK (Gaurdian Comment)
author: pinkie123
date: 2020-04-11 11:56
layout: post
categories:
tags:
  - "1984"
  - "opinion"
---

> This article was originally a 
> [comment on a Guardian article](https://www.theguardian.com/commentisfree/2020/apr/11/coronavirus-death-uk-restaurant-industry-rent-government#comment-139700412) 
> regarding the current state of the UK during the COVID-19 Crisis.
> 
> I have placed it here because it is a valuable statement itself, and 
> will be lost as a comment.

For the past week I've been trying to get three things straight in my head: 
the true purpose of the lockdown, what the majority of the public imagine, 
or have been led to believe by the government and media to be the purpose 
of the lockdown, and whether the two align.

Conclusion: this is the mother of all comms disasters for which there 
will one day be the mother of all enquiries.

As far as I can gather, the true purpose of the lockdown is to delay and 
flatten the Coronavirus spread. This ensures the NHS is not overwhelmed, 
allowing it time to prepare to manage the impact – hence the Nightingale 
Hospitals being built all over the country – and protect its staff. By 
social distancing, the public is engaged in a collective effort to ensure 
the NHS has the capacity to treat everyone. Absolutely 100% fine with 
this bit.

However, many clearly believe the purpose of the lockdown is also to reduce 
the overall rate of infection, give themselves ongoing 100% protection from 
infection, or even eradicate the disease entirely. None are possible. A government 
decree cannot eradicate a virus. The overall rate of infection can only 
be spread out, not reduced. Once the lockdown is lifted (which it will 
have to be at some point in the near future), we will all be once again 
susceptible to infection.

Media panic in symbiosis with ceaseless rhetoric of 'beating', 'defeating' 
and 'overcoming' the virus, has entrenched the false hope that this pandemic 
will come to a clean end. Only a vaccine could guarantee sudden mass immunity. 
However, a vaccine is not a cast iron certainty. Scientists have been working 
on vaccines for other Coronaviruses unsuccessfully for years now. And 
even if there were a vaccine, the virus may mutate, becoming an endemic, 
seasonal illness.

Dark, dangerous perceptions have been allowed or even encouraged to germinate 
during the hysteria. Yesterday, somebody made this comment on CIF: "Every 
person who defies the lockdown endangers others'. What does s/he mean by 
that? That the flouter would endanger others by increasing strain on NHS 
services during the lockdown period? Okay, I guess. Or do they mean that 
one should be held morally accountable for causing another person’s death 
by transmitting disease? Please reflect on what a seriously dangerous, 
DANGEROUS notion that is.

These people who fume in terror at sunbathers, recoil in fear if you step 
with two metres of them in the supermarket, monitor the comings and goings 
and neighbours from windows – are they supposed to suddenly readjust to 
coexisting with others in close proximity when the lockdown ends? How 
will they cope? Have they been led to believe that the lockdown will only 
end when the disease has been eradicated? If so, they have been lied to – 
by politicians terrified of the power of tabloids and a callow liberal 
media.

During the Black Death, religious zealots known as Flagellants wandered 
Europe vigorously whipping themselves in public displays of penance. The 
current exultation in atonement and condemnation of sinners is a comparable 
display of magical thinking and moral hysterics.

If I was PM, this is what I would have said:

> Ladies and gentlemen of the public. With the Coronavirus we face a collective 
> threat. In my capacity as Prime Minister I will do all I can to ensure 
> our NHS services are equipped to cope with those of us unfortunate enough 
> to be seriously affected by this illness. For this reason, I am ordering 
> a lockdown with immediate effect. How long the lockdown will last is 
> impossible to say, but it is essential that our NHS is not overwhelmed, 
> and the lives of its staff not put at risk. By flattening the curve 
> of infection, this pandemic should become manageable enough for some 
> measure of normal lfe to resume in due course.
> 
> It is, I think, my duty to be as honest with you as possible. I cannot 
> pledge you that you or your family will be unaffected by this illness. 
> The restrictions about to be imposed are temporary and cannot eradicate 
> the virus, only delay its spread. A vaccine may one day become available, 
> but, alas, I can offer no guarantee. As with all viruses, immunity across 
> the population will increase as infection spreads. In this way, we can 
> all be assured that the pandemic will one day draw to a close.
> 
> However, I would urge people not to panic and end on a note of hope. 
> While potentially deadly, the vast majority of individuals make a full 
> recovery from this illness – even those who are older and more vulnerable 
> to complications. In many cases, symptoms are mild or even non-existent. 
> Tragically, younger people are occasionally struck down, but such cases 
> are highly rare.
> 
> Fear, I would remind you, is the enemy of reason. A sense of perspective 
> is crucial. To get through this, we need to be vigilant but to be calm 
> and look out for one another.

Any PM going to say something like that these days, with the Sun, Mail 
and millions of rabid death cult Facebookers breathing down their neck? 

Nah.
