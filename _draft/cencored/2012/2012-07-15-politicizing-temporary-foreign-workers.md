---
id: 1952
title: Politicizing Temporary Foreign Workers
date: 2012-07-15T14:19:58+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1952
permalink: /2012/07/15/politicizing-temporary-foreign-workers/
categories:
  - Analysis
---
> <table>
>   <tr>
>     <td width="50%">
>       The government cannot in good conscience continue to admit temporary foreign workers to work in businesses in sectors where there are reasonable grounds to suspect a risk of sexual exploitation
>     </td>
>     
>     <td width="50%">
>       They’re destroying the industry by creating a labor shortage
>     </td>
>   </tr>
>   
>   <tr>
>     <td>
>       &#8211; Minister of Citizenship, Immigration and Multiculturalism
>     </td>
>     
>     <td>
>       &#8211; Adult Entertainment Association of Canada
>     </td>
>   </tr>
> </table>
> 
> [Canada Bans Foreign Strippers](http://www.newser.com/story/149581/canada-bans-foreign-strippers.html)

&hellip; and I'm fairly certain neither of them are giving a straight answer.

<!--more-->

# The Government

Like them or not, the Government of Canada does not forbid the development of Strip Clubs. While culturally they tend to differ across Canada, the clubs are not forbidden. This action appears to be the Government of Canada attempting to make new law by using the administrative process to bypass the legal process. It seems very clear to me that the Government cannot pass a law forbidding strip clubs, but it can make the administrative process so difficult to navigate, operating a club will not be feasible.

The saddest part of this is that while they state they are doing this protect <q>women and children</q>, instead they increase the very risks they state they are attemtping to eliminate. Currently, women who work in these industries are protected by laws forbidding their exploitation, which allow them to seek police and legal assistance if a situation gets out of control. Removing the ability to legally find work will only create a situation where they will continue to do the work, but without legal protection. As for <q>children</q>, the industry is the <q>Adult</q> industry, no work visas would be issued to a child to begin with.

If this is not about the stated reasons, the question becomes what are the real motivations?

We can see signs of the real intent hidden in the stated reasons for this effort: <q>sexual exploitation</q> is already illegal. If these industries are engaging in _sexual exploitation_, then there are current laws to deal with the problem, then any businesses that engage in legally inappropriate behaviour need to be shut down. Strip Clubs take this **very** seriously. The security is heavy, and the men hired to maintain control and civility are very professional. The ladies performing are not to be bothered in any way, and you **will** be escorted out if you do not respect that rule.

If these businesses are not being shut down, they must not be engaging in illegal activity, including the <q>sexual exploitation</q> of women. What actually appears to be happening is someone in the Federal government finds these activities morally distasteful, and since they do not have the power to legislate the actions of Canadians, they are instead going to use their administrative powers to make it increasingly difficult to operate a legitimate business. They can't forbid the action, but they can make it so difficult as to make it not worth while.

They are making rules, but skipping the legal process. That the Federal Government is doing this, should scare Canadians.

This is not a defense of the lobbying efforts of the Adult Entertainment Association of Canada (AEAC).

<span id="The_Lobbyists">

<h1>
  The Lobbyists
</h1></span> 

Lobbyists are representatives of a group of people who want to ensure that the government takes into account their particular side of the story before making a decision. From the group, and the lobbyist's perspective, that means getting results. These lobbyists are paid a lot of money to ensure that governments do not interfere (and maybe even help) with the pornography industry.

Talking about jobs, and exploited women, creates an emotional response that can gain them support, regardless of the truth of the matter.

Firstly, they refer to the industry's moniker <q>Exotic Dancing</q>, indicating that patrons want to see foreign performers. The implication is that performers must be brought from overseas, to meet the demand for <q>exotic</q> racial types. Failure to do so will result in human trafficing from these foreign areas. This seems highly unlikely for two reasons.

First, every performer I have ever seen has been Caucasian. There is no exception to this. Most performers, and the most sought after performers are Caucasian. While importing a couple of very specialized performers may occur, it is very possible to find the desired racial type right in Canada. Having said that, it wouldn't matter what the racial type being sought was, Canada is a very multi-racial country, bringing us to the second reasons: regardless of the racial type being sought, they are available in Canada. The AEAC is blowing smoke if they are talking about having to import foreign workers to meet demand for racial body types.

The AEAC goes on to state that if the industry is not able to import foreign workers, they will be forced to go to foreign students to meet the demand. They say this as though it is a bad thing. In fact, this begs the question of why they are not offering the opportunities to local women anyway? If there are jobs to be filled, why are students, trying to earn extra money to get through University being passed over for imported workers. The foreign worker's act indicates that the employer must demonstrate that there is no one locally willing to fill the job, that there are students being passed over indicates to me that the employers are simply not willing to pay full price. They are looking for cheap foreign labour, rather than paying local labour prices.