---
id: 900
title: Our Oceanfront Property in Alberta
date: 2011-01-30T12:59:21+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=900
permalink: /2011/01/30/our-oceanfront-property-in-alberta/
categories:
  - Opinions
---
 The Wife and I sold most of our investments years ago.

We had both entered a phase of our lives where we had saved enough money to think about investing and not just day to day expenses. First we did our homework on what investing was all about, where best to allocate our resources, and what to expect from markets. We then approached our bank's investment departments and began talking with their <q>Investment Advisors</q>, only to find they were selling us things we didn't think were good investments (based on our own analysis). More research indicated to us that there is a conflict of interest: the banks don't make money on giving us sound advice, they make money selling us advice. It doesn't matter if we make money, they make money either way.
  
<!--more-->


  
Basically, if we were going to do it, we had to become self-reliant and learn to do the analysis ourselves. It didn't take long for us to realize that the time we were spending studying companies did not match the meagre returns we could expect on our meagre savings. If you aren't talking hundreds of thousands of dollars, a part-time job at the corner store pays better.

We concluded that while investing in the stock market is a good way to make money, very few people have the resources (mostly time) available to make effective market analysis decisions.

I was reading an article this morning (<a href="http://www.kitco.com/reports/KitcoNews201101024DeC_coins.html" target="_blank">Silver Coin Sales, ETF Outflows Show Divergence In Market</a>) that demonstrated this to me perfectly. Proof positive that I don't want anything to do with Bay Street Markets anymore. Basically the article is discussing if there is any relationship between Physical Silver and Paper Receipts for Silver.

NO RELATIONSHIP? WHAT THE F@$%?<sup id="rf1-900"><a href="#fn1-900" title="This is one of those events in life where only swear can really express what is going through my head. This makes so little sense that I can&rsquo;t actually come up with a polite thought on the matter. Trying to is actually causing my face to twitch" rel="footnote">1</a></sup>

If the bigger players are questioning a relationship, then they have accepted that the supply of paper silver exceeds physical availability; in other words, nobody actually has any.

Wait a minute&hellip; scrap everything I just said.

I would like to announce an exciting new product being offered by Vius: Silver Deposit Receipts. They sell at spot, and a physical receipt will be issued to you by mail. Please note the small print: they may never be redeemed for the actual silver they may, or may not, actually represent.

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-900">
    <p>
      This is one of those events in life where only swear can really express what is going through my head. This makes so little sense that I can't actually come up with a polite thought on the matter. Trying to is actually causing my face to twitch&nbsp;<a href="#rf1-900" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>