---
id: 469
title: 'Halifax: Lovely Place to Visit (but I live here)'
date: 2010-07-02T14:08:05+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/2010/07/halifax-lovely-place-to-visit/
permalink: /2010/07/02/halifax-lovely-place-to-visit/
categories:
  - Moving
---
We have been in Halifax for three days now, and so far, all we are experiencing is a general malaise and depression. At first we thought it was Halifax itself, but yesterday we went on a tour of Halifax and noticed that it is a beautiful city. So the question becomes, what is wrong?

At this point we have realized that our objective was to get out of the city, move to a large piece of land, where we have control over our lives. In order to get there, we have made a stop, in a small 500 sq foot apartment, that we rent, that is pet free, that we aren't allowed to work on, in the busy suburbs of a University city, with no furniture. Pretty much the exact opposite of what we are working towards.

Suddenly, a lot of this part of the move is starting to look like a bad idea. Originally, we had questioned the wisdom of taking a city job, even temporarily, but had felt that the income would ease the transition and help with some of the moving expenses. Now we are questioning the wisdom of this again. We have sacrificed a lot of our independence (something very dear to us), for a little security. If we stay in this position, we are concerned that the transition to an independent lifestyle will be even more difficult. Here we have to live week to week, rather than year to year; we are concerned that we will lose the ability (or drive) to repair our own toilets, dig our own gardens, or maintain food supplies.

Added to this are concerns as to whether or not, I will even be able to start this job in the end. I am still waiting on my police background check to be mailed here, and have just learned that they would like to see Proof of Eduction, something that is buried in a mountain of boxes in a move we haven't even caught up with yet. I do not think I will be to get this paper work in order in time to start work on Monday. Add to this the fact that while we were driving I finally received the offer of employment in writing, and the term of the contract is different than discussed over the phone. The primary benefit of this position was the term of the contract, and I am not interested in the term on the letter. The question becomes, do we skip Halifax all together?

A friend of mine (Glen) once told me a true story about watching for omens. In his story, someone was moving and it was a really bad idea, the morning of the move, the water company had dug a 20 ft trench through his drive-way. This is an omen that you are not supposed to move. There have been several omens for us.

  * Our first full day, an old lady hit our car in a parking lot. No damage, for us (she hit the hitch on our car), and the cops were already there talking to her about hitting and running, but definitely not a good sign.
  * While moving in, we met the building manager, and noticed that she likes to run a little fiefdom. She flat out told us to let her know when we were moving in so she could watch what was being brought in, and has also informed us that there are repairs that she will need to enter our apartment to make.
  * The morning we arrived, the worst rain Halifax has seen in weeks began. We were soaked to the bone by the time we had the essentials in (and then gave up).
  * My job does not seem to be lining up well all of a sudden.
  * Parking for two cars, turned into parking for one car upon arrival, and we also own a trailer.
  * Our toilet won't flush.

At this point if a Canada Day Parade went past the apartment and took a break, and all the people holding letters went into the parking lot to have a cigarette and as they milled about the letters formed the phrase, <q> The Wife & Kavius r dUMB &#8216;n sHOUld mve OUT OF the cItY</q>,<sup id="rf1-469"><a href="#fn1-469" title="basically your hand of God type situation" rel="footnote">1</a></sup> we would not be surprised, or shaken.

Obviously, we can't move out tomorrow, but we can begin taking measures to leave immediately. It is possible to move back to the original plan at this point. That plan stated that we rent somewhere, for as short a time as possible, while doing intensive house shopping. Halifax, was not the original place to do that from, however it is central to all of Nova Scotia, and would therefore be a good home base for this.

At this point, I will continue on with attempting to start my new job, but if it falls through, I think it will be more of a relief than anything else. If the position falls through,  The Wife and I will begin an exciting time of intensive property shopping that could possibly break a realtor's mind, body, and soul.

Real-Estate Agents of Halifax, beware.

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-469">
    <p>
      basically your hand of God type situation&nbsp;<a href="#rf1-469" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>