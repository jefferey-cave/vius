---
id: 576
title: A Day at the Beach
date: 2010-08-30T05:17:04+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=576
permalink: /2010/08/30/a-day-at-the-beach/
categories:
  - Living
---

They say, if you don't like the weather just wait 5 minutes. They say this in Alberta, they say this in BC and they say this in Nova Scotia. But I've been waiting, I've been really patient, I've crossed my fingers and toes, but still nothing but hot, humid days here in Halifax. We had rain on the first day we arrived (not good) and we had rain the day of our first home inspection (good &#8211; it showed a very leaky roof) but the rest of the time we spend trying to beat the heat.

Beating the heat on the prairies usually means standing under a lawn sprinkler in your backyard. Beating the heat in Halifax means heading to the beach! We left Halifax around 10:30 and headed off down the coast about 30 minutes to the very small town of Hubbards. After taking a quick detour to purchase chairs at a yard sale, we arrived to an already busy beach. We chose one of the smaller ones (possibly because it was the only one that still had parking) and staked our space. The water sparkled like thousands of crystals under a beautiful blue sky, the water was warm and inviting, and we felt as if we were in the Tropics and not on some Northerly piece of land stuck in the cold Atlantic Ocean.

We stayed and played for a couple of hours before heading back to the city (with 3 dining room chairs wedged into the back of the car) to tend to the side effects of the day out&hellip;**sunburn**!

<figure>
 <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-64.01528477668764%2C44.6416853652909%2C-64.01117563247682%2C44.64376551884521&amp;layer=mapnik&amp;marker=44.64272545139117%2C-64.01323020458221" style="border: 1px solid black"></iframe>
 <figcaption>
  <a href="https://www.openstreetmap.org/?mlat=44.64273&mlon=-64.01323#map=18/44.64273/-64.01323">
   View Larger Map
  </a>
 </figcaption>
</figure>

