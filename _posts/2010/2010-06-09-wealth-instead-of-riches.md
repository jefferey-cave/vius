---
id: 184
title: Wealth Instead of Riches
date: 2010-06-09T15:01:01+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=184
permalink: /2010/06/09/wealth-instead-of-riches/
categories:
  - Opinions
---

One of the things I've been doing over the course of the last few articles, was defining a common vernacular. As is often the case, when people are debating an issue, the use of words actually gets in the way because two individuals are using the same term to mean two different things.

 The Wife and I debate and discuss a lot of things, and over time we have realized that we have had to specifically define common terms in uncommon ways (in particular, very specific ways). Often, people use these terms interchangeably, but for us we need to distinguish between a subtle difference that others have often not considered. We have needed to do this because we are seeking happiness in a world where most people do not achieve it, and we feel that people are not achieving it because they are shooting at the wrong target.

 The Wife and I are unable to just discuss dreams for the future; instead we first have to determine if they are _dreams_<sup id="rf1-184"><a href="#fn1-184" title="we are still looking for a better term for this" rel="footnote">1</a></sup>, or if they are _fantasies_. We are unable to discuss problems in life without first determining if we are _living with intent_ or _living by default_.

I am hoping that as this journal continues, and if others actually ever read it<sup id="rf2-184"><a href="#fn2-184" title="I actually heard that my mother-in-law is reading it, and this horrifies me. It was nice to be able to hide on the other side of the ocean and have her think really nicely of me. Now she&rsquo;s actually going to find out what an opinionated-jerk-outcast-nutbar her&nbsp; daughter is actually married too. I am sorry, I was hoping to let you live in blissful ignorance for much longer than this." rel="footnote">2</a></sup>, that others actually understand what we are talking about. In the vein of drawing distinctions between simply living, and living well, I need to define the difference between _wealth_ and _riches_.

# Being Rich

Riches are what most people strive for in life: they want more money, a nicer car, a bigger house, a corner office. Unfortunately, being rich does not solve your problems<sup id="rf3-184"><a href="#fn3-184" title="I&rsquo;m not saying being rich won&rsquo;t help. In the words of Weird Al Yankovich, &ldquo;If money can&rsquo;t by happiness, I guess I&rsquo;ll have to rent it&rdquo;" rel="footnote">3</a></sup>. There are <a title="Lottery Hell" href="http://www.milwaukeemagazine.com/currentIssue/full_feature_story.asp?NewMessageID=13120" target="_blank">countless stories</a> of people winning the lottery, making them rich, but resulting in significant problems for them and their family.

Further, not only can money cause problems, more importantly, it (in and of itself) does not create happiness. For example, if my <a title="The Godfather" href="http://www.imdb.com/media/rm4098529280/tt0068646" target="_blank">Fairy Godfather</a> came down tomorrow and offered me a [Pagani Zonda](http://www.paganiautomobili.it/english.htm "Pagani Zonda"), I would be very disappointed: I don't like sports cars. All of the sports cars in the world won't help me be happy as I do not have a taste for them. Surely, all my friends would be really impressed, but that doesn't help **me**. I don't care what **they think**; I care what **I feel**.

For  The Wife's and my purpose, being rich is <q>having a lot of money</q>.

Of itself, money doesn't do me any good; it is only a means to an end, not the end itself. Money serves a single purpose: to be traded for stuff. The only real question is, what am I going to trade my money for.

# Being Wealthy


Wealth is very distinct from riches. Wealth, to us, means having everything we want; or, more specifically, not wanting for anything. By definition, I can be extremely poor, but have everything I desire, and therefore be very wealthy. To an extent, this is impossible: I have infinite desires, and only limited resources. Perfect wealth is an impossibility, but that doesn't mean we can't strive for being as wealthy as possible.

I really like example given by Predrag Rajsic, in his article <a title="Measuring the Immeasurable by Predrag Rajsic" href="http://mises.org/daily/4361" target="_blank"><em>Measuring the Immeasurable</em></a>:

> For example, if Jim's income is $4,000 and Janis's income is $1,000, does this mean that Jim's wants and needs are satisfied better than Janis's? We don't know. Likewise, if Jim tells you that, on a scale from one to five, his level of happiness is three, and Janis tells you that her level of happiness is four, this does not tell you that Jim is less happy than Janis — because Jim's level three and Janis's level three are not the same subjective state of mind.

This example clearly identifies the problem: Jim and Janis are incomparable. If Janis is able to meet her wants on her income, but Jim is not, then Janis is wealthier than Jim, even though Jim is richer. There are a few conclusions that can be drawn from this distinction:<figure id="attachment_225" style="max-width: 260px" class="wp-caption alignright">

[<img class="size-medium wp-image-225 " style="float: right" title="Man with a Shotgun" src="http://www.vius.ca/wp-content/uploads/lans-man-with-shotgun-260x300.jpg" alt="" width="260" height="300" />](http://www.stoningtongalleries.com/lorraine-lans-studio/)<figcaption class="wp-caption-text">Kavius's dreams of wealth</figcaption></figure>

  * wealth is not dependant on income
  * lowering expectations, increases wealth.
  * the objective should be wealth, not riches

 The Wife and I consider ourselves very wealthy. While we do not have a lot of stuff, or huge money making jobs, we have the things we most desire.

That's the key, did you catch it?

_The things we **most** desire._

Through a lot of hard work, and attention to detail,  The Wife and I have determined what things will make **us most happy**, rated those things against our ability to attain them, and then strove for the items that would give us the largest gain in happiness<sup id="rf4-184"><a href="#fn4-184" title="this is a value scale, very important concept" rel="footnote">4</a></sup>. Money is not even on the list. Money is a means to aquire the things we want. If we can find a different means, we no longer require the money.

If I want food in my belly; I can buy it, or grow it.

If I want clothes on my back; I can buy it, or weave it.

If I want good sensible advice; I can buy articles at the news-stand, or write them&hellip; Oh wait&hellip;

Two things that  The Wife and I want, that rate highly on our list of things we value, are time and independence: time to spend with one another, and the independence to know we do not depend on anyone else for our own well being. Working for someone else at a desk job sacrifices time and independence (things that rank highly on our value scale), for money (something that ranks low on our value scale). Does that make sense? Trading something of lesser value for something of greater value? Buy high, sell low?

<div id="_mcePaste" style="width: 1px;height: 1px;overflow: hidden">
  me me
</div>

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-184">
    <p>
      we are still looking for a better term for this&nbsp;<a href="#rf1-184" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn2-184">
    <p>
      I actually heard that my mother-in-law is reading it, and this horrifies me. It was nice to be able to hide on the other side of the ocean and have her think really nicely of me. Now she's actually going to find out what an opinionated-jerk-outcast-nutbar her  daughter is actually married too. I am sorry, I was hoping to let you live in blissful ignorance for much longer than this.&nbsp;<a href="#rf2-184" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn3-184">
    <p>
      I'm not saying being rich won't help. In the words of Weird Al Yankovich, <q>If money can't by happiness, I guess I'll have to rent it</q>&nbsp;<a href="#rf3-184" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn4-184">
    <p>
      this is a <em>value scale</em>, very important concept&nbsp;<a href="#rf4-184" class="backlink" title="Jump back to footnote 4 in the text.">&#8617;</a>
    </p>
  </li>
</ol>