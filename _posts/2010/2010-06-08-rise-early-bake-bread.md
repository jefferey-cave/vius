---
id: 216
title: 'Rise Early &#8211; Bake Bread'
date: 2010-06-08T14:30:07+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=216
tags: [' The Wife']
categories:
  - Recipes
  - TheWife
---

While trying to use up my food staples I came across this recipe. [http://www.eatingwell.com/recipes/honey\_oat\_quick_bread.html](http://www.eatingwell.com/recipes/honey_oat_quick_bread.html)

I made this bread a few weeks ago; it is delicious. Its texture and taste are a cross between a yeast bread and a quick bread. It's great with butter, toasted or just sliced and eaten as is.

You will need:

  * 1 cup (plus a little extra for sprinkling on top) rolled or quick-cooking oats
  * 2 1/3 cup all-purpose flour
  * 2 1/4 teaspoons baking powder
  * 1/4 teaspoon baking soda
  * 1 1/4 teaspoons salt
  * 1 cup yogurt ( I used 2/3 cup milk with  tsp lemon juice instead)
  * 1 large egg
  * 1/4 cup canola oil
  * 1/4 cup honey
  * 3/4 cup nonfat or low-fat milk
<span id="Preparation">

# Preparation

  1. preheat to 375°F. Generously grease  a  loaf pan. Sprinkle 1 tablespoon oats in the pan. Tip the pan back and forth to coat the sides and bottom with oats.
  2. Thoroughly stir together flour, baking powder, baking soda and salt in a large bowl. Using a fork, beat the remaining 1 cup oats, yogurt (or soured milk), egg, oil and honey in a medium bowl until well blended. Stir in milk. Gently stir the yogurt mixture into the flour mixture just until thoroughly incorporated but not overmixed. Put the batter into the pan, spreading evenly to the edges. Sprinkle the remaining 1 tablespoon oats over the top.
  3. Bake the loaf until well browned on top and a toothpick inserted in the center comes out clean, 40 to 50 minutes. (It’s normal for the top to crack.) Let stand in the pan on a wire rack for 15 minutes. Run a knife around the loaf to loosen it and turn it out onto the rack. Let cool . Enjoy warm, naked or with butter.

I actually got up early last Thursday and baked 2 loaves before work.

~  The Wife
