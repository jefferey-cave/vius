---
id: 378
title: Only One Province Away
date: 2010-06-21T00:34:12+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=378
permalink: /2010/06/21/only-one-province-away/
categories:
  - Moving
---
We arrived in New Brunswick late this afternoon. We are now on the home stretch.

The drivers in Quebec are in a class of their own. Driving through Montreal had Kavius's adrenalin pumping so much that he didn't need his morning coffee! Kavius will post about this experience later.

It rained most of the day; sometimes so hard I couldn't see the road. The cooler weather brings a little relief, but it is still quite warm tonight;  Chimutisk is stretched out on the bathroom floor.

I can't believe we have been driving for 8 days and I'm still not sick of Kavius: that was until this afternoon when he   started singing Barry Manilow show tunes!

We hope to arrive in Nova Scotia tomorrow afternoon. We have decided to stay on the North Shore for a couple of days then on to the Annapolis Valley before we hit Halifax. Our rental apartment won't be ready for us until the end of June so we might as well use this opportunity to get to know the area better.

Now I'm going to read a little a enjoy my evening. So it's _bonne nuit_ and goodnight from New Brunswick.

<div id='gallery-3' class='gallery galleryid-378 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0044/'><img width="150" height="113" src="/media/2010/06/IMG_0044.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0045/'><img width="150" height="113" src="/media/2010/06/IMG_0045.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0046/'><img width="150" height="113" src="/media/2010/06/IMG_0046.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0047/'><img width="150" height="113" src="/media/2010/06/IMG_0047.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0048/'><img width="150" height="113" src="/media/2010/06/IMG_0048.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0049/'><img width="150" height="113" src="/media/2010/06/IMG_0049.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0050/'><img width="150" height="113" src="/media/2010/06/IMG_0050.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0052/'><img width="150" height="113" src="/media/2010/06/IMG_0052.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0054/'><img width="150" height="113" src="/media/2010/06/IMG_0054.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0057/'><img width="150" height="113" src="/media/2010/06/IMG_0057.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon portrait'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0051/'><img width="113" height="150" src="/media/2010/06/IMG_0051.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0056/'><img width="150" height="113" src="/media/2010/06/IMG_0056.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0055/'><img width="150" height="113" src="/media/2010/06/IMG_0055.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/06/21/only-one-province-away/img_0053/'><img width="150" height="113" src="/media/2010/06/IMG_0053.jpg" class="attachment-thumbnail size-thumbnail" alt="Hartland Bridge Plaque" /></a>
  </div></figure>
</div>