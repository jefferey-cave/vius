import ByteStream from 'ByteStream.js';


class TarFile{

	constructor(fileName, entries=[]){
		this.fileName = fileName;
		this.entries = entries;
	}

	// constants
	static ChunkSize = 512;

	// static methods

	static fromBytes(fileName, bytes){
		let reader = new ByteStream(bytes);
		let entries = [];
		let chunkSize = TarFile.ChunkSize;
		let numberOfConsecutiveZeroChunks = 0;

		while(reader.hasMoreBytes())
		{
			let chunkAsBytes = reader.readBytes(chunkSize);

			let areAllBytesInChunkZeroes = true;
			for (let b = 0; b < chunkAsBytes.length; b++){
				if (chunkAsBytes[b] != 0){
					areAllBytesInChunkZeroes = false;
					break;
				}
			}

			if (areAllBytesInChunkZeroes){
				numberOfConsecutiveZeroChunks++;
				if (numberOfConsecutiveZeroChunks === 2){
					break;
				}
			}
			else{
				numberOfConsecutiveZeroChunks = 0;
				let entry = TarFileEntry.fromBytes(chunkAsBytes, reader);
				entries.push(entry);
			}
		}

		let returnValue = new TarFile(fileName,entries);

		return returnValue;
	}

	// instance methods

	downloadAs(fileNameToSaveAs)
	{
		this.entriesAllChecksumsCalculate();

		FileHelper.saveBytesAsFile
		(
			this.toBytes(),
			fileNameToSaveAs
		)
	}

	TarFile.prototype.entriesAllChecksumsCalculate = function()
	{
		for (let i = 0; i < this.entries.length; i++)
		{
			let entry = this.entries[i];
			entry.header.checksumCalculate();
		}
	}

	TarFile.prototype.entriesForDirectories = function()
	{
		let returnValues = [];

		for (let i = 0; i < this.entries.length; i++)
		{
			let entry = this.entries[i];
			if (entry.header.typeFlag.name == "Directory")
			{
				returnValues.push(entry);
			}
		}

		return returnValues;
	}

	TarFile.prototype.toBytes = function()
	{
		let fileAsBytes = [];

		// hack - For easier debugging.
		let entriesAsByteArrays = [];

		for (let i = 0; i < this.entries.length; i++)
		{
			let entry = this.entries[i];
			let entryAsBytes = entry.toBytes();
			entriesAsByteArrays.push(entryAsBytes);
		}

		for (let i = 0; i < entriesAsByteArrays.length; i++)
		{
			let entryAsBytes = entriesAsByteArrays[i];
			fileAsBytes = fileAsBytes.concat(entryAsBytes);
		}

		let chunkSize = TarFile.ChunkSize;

		let numberOfZeroChunksToWrite = 2;

		for (let i = 0; i < numberOfZeroChunksToWrite; i++)
		{
			for (let b = 0; b < chunkSize; b++)
			{
				fileAsBytes.push(0);
			}
		}

		return fileAsBytes;
	}

	// strings

	TarFile.prototype.toString = function()
	{
		let newline = "\n";

		let returnValue = "[TarFile]" + newline;

		for (let i = 0; i < this.entries.length; i++)
		{
			let entry = this.entries[i];
			let entryAsString = entry.toString();
			returnValue += entryAsString;
		}

		returnValue += "[/TarFile]" + newline;

		return returnValue;
	}
}
