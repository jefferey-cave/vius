---
id: 1776
title: Response to Bill C-10 Video
date: 2012-02-21T08:39:11+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1776
permalink: /2012/02/21/response-to-bill-c-10-video/
categories:
  - Opinions
---
A friend of mine recently posted this video on Facebook. As I am currently boycotting Facebook, I have to bitch about it here:

I'm no fan of minimum sentencing, I think it is a terrible idea. But, that video had some pretty seriously flawed arguments. If people are basing their decision, that Bill C-10 is a bad idea, on the consumption of media such as this video; I'm thinking its time to start looking for a nice place in the mountains to hide until the madness blows over.

<!--more-->

<span id="Flaws">

<h1>
  Flaws
</h1></span> 

Comparing cost per capita of school children to cost per capita of prisoners is not even a remotely sensible comparison? Even a cursory bit of thinking would suggest why: we have a lot more students than prisoners; if we have 10 times as many students as prisoners, these costs are actually equivalent. We have a lot more than that. In 2001, Canada had 32,000 people incarcerated,<sup id="rf1-1776"><a href="#fn1-1776" title="Wikipedia" rel="footnote">1</a></sup> I 3000 students in my High School, and at least 10 High Schools in my city.

While schools do have much in common with the prison system, the cost of education must be less than the cost of incarceration. Care of inmates is 24/7, while parents (rightfully so) pick up the tab for much of the care of students. Parents feed and clothe, as well as supply a home for their children. Prisoners require feeding and clothing, activities to keep them fit, and from going insane. All of this does not even take into account the security involved in keeping prisoners in, compared to the security of keeping students in. Of course maintaining prisoners is going to be more expensive.

Further, the video does not take into account the fact that the Federal government is not responsible for education. In Canada, each province is responsible for its own Medical, Educational, and Transportation infrastructure. With this in mind, it is evident that most Federal funding of schools would not be labelled as such. Most funds given to the provinces are given without a specified purpose. It is up to the provinces to determine the best use for those funds, they may allocate it to Medicince, Transportion, and even education. That means that the Federal government is not <q>not funding schools</q>, it means they are keeping their noses out of the provinces business. They are indirectly funding education because that is their role. On the flip side, Federal Prisons are a direct responsibility of the Federal government. Of course they are going to spend more on prisons&hellip; on paper.

The key is that we know exactly how much the Federal government spends on prisons because that is one of their primary functions. We can't know how much the Federal government spends on schools, because most of it is undocumented, because they aren't supposed to be spending any.

<span id="What_Does_it_Mean">

<h1>
  What Does it Mean
</h1></span> 

While I was writing this, The Wife went and looked up some numbers.

In 2007, the provinces spent, on average, $10,000 per year, per student, to get them from Kindergarten through to Graduation. According to the video, the Federal Government spends approximately, $90,000 per year, per prisoner; as well as $8000 per year per student.

Prisoners spend 24 hours, seven days a week behind bars; Given students spend about six hours a day in class, as well as the fact that they do not spend seven days a week in classes we will need to make adjustments to account for the actual contact time during which students and prisoners spend in contact with their facilities.

<table>
  <tr>
    <th>
    </th>
    
    <th>
      Students
    </th>
    
    <th>
      Prisoners
    </th>
  </tr>
  
  <tr>
    <th style="text-align: right">
      Federal
    </th>
    
    <td style="text-align: right">
      $8,000
    </td>
    
    <td style="text-align: right">
      $90,000
    </td>
  </tr>
  
  <tr>
    <th style="text-align: right">
      Provincial
    </th>
    
    <td style="text-align: right">
      $10,000
    </td>
    
    <td style="text-align: right">
      $0
    </td>
  </tr>
  
  <tr>
    <th style="text-align: right">
      Contact <sup>Hours</sup>/<sub>Day</sub>
    </th>
    
    <td style="text-align: right">
      6
    </td>
    
    <td style="text-align: right">
      24
    </td>
  </tr>
  
  <tr>
    <th style="text-align: right">
      Contact <sup>Days</sup>/<sub>Year</sub>
    </th>
    
    <td style="text-align: right">
      190<sup id="rf2-1776"><a href="#fn2-1776" title="excluding Quebec, which was 180 hours. Quebec is always different for some reason or another." rel="footnote">2</a></sup>
    </td>
    
    <td style="text-align: right">
      365
    </td>
  </tr>
  
  <tr>
    <th style="text-align: right">
      Contact <sup>Hours</sup>/<sub>Year</sub>
    </th>
    
    <td style="text-align: right">
      1140
    </td>
    
    <td style="text-align: right">
      8760
    </td>
  </tr>
  
  <tr>
    <th style="text-align: right">
      <sup>Cost</sup>/<sub>Hour</sub>
    </th>
    
    <td style="text-align: right">
      $15.79
    </td>
    
    <td style="text-align: right">
      $10.28
    </td>
  </tr>
</table>

Some of my numbers for students are from 2007 (the most recent we could find), I am fairly confident that more (not less) is spent at this time. But this was some quick bar napkin math, please feel free to check my numbers. I still think the point is valid. This is not just a simple question as to whether the Federal Government is has its priorities backwards: based on this assessment, it appears that priority **is** given to students over prisoners.

<span id="Division_of_Labour">

<h1>
  Division of Labour
</h1></span> 

In fact, if one looks at it as a matter of departments, one could ask why the Canadian Government is involved in education at all? It could be argued that the Federal Government is wasting resources by duplicating infrastructure that already exists at the provincial level.

The Provinces are in charge of Medicine, Education, and Transport (the old MET from my school days), they have developed an extensive administration and infrastructure to meet these needs. Having the Federal government involved in education is equivalent to a company having an accounting department undertaking care of marketing, it is a waste of resources. Leave marketting to the marketting department and have the accountants focus on accounting. In this case, the Fed should stay out of education and focus on defense, international relations, and incarceration; its area of expertise.

While this is a bit of a silly argument, it is no sillier than the initial argument in the video.

<span id="Who_Cares">

<h1>
  Who Cares
</h1></span> 

I recently commented on a <a title="This Thing We Call Science" href="http://vius.ca/2011/10/this-thing-we-call-science/" target="_blank">Climate Change Editorial</a> for much the same reason. I am concerned with current political debate. It has nothing to do with reality, and everything to do with ideology. Every decision made at the political level is a decision on how to use violence. With that in mind, it is very important that the decisions be the correct decisions.

While I do not agree with minimum sentencing, and have several problems with Bill C-10, I do not agree with Straw Man arguments either. It leads to poor decision making, based on ideology rather than calm, factual, analysis. Decisions based on ideology make for inappropriate use of government, and therefore taxpayer, resources.

In the end, ideology makes for poor policy.

<span id="Update:_2012-02-28">

<h1>
   Update: 2012-02-28
</h1></span> 

Given the debate that followed (with a friend of mine), and the conclusion I ended with, I thought this video that I came across to-day was interesting.

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1776">
    <p>
      Wikipedia&nbsp;<a href="#rf1-1776" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-1776">
    <p>
      excluding Quebec, which was 180 hours. Quebec is always different for some reason or another.&nbsp;<a href="#rf2-1776" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
</ol>