---
id: 2013
title: Check Your Charity
date: 2012-11-01T00:00:58+00:00
author: Kavius
layout: post
categories:
  - Uncategorized
---

Media outlets have managed to politicise another disaster.

Recently, Mitt Romney has come under fire for encouraging people to donate food to relief efforts in Sandy stricken regions. Unfortunately, several media outlets have pointed out that The Red Cross no longer accepts donations of tangible goods.

As I don't deal with the logistics of getting aid to those in devistated areas, I don't have a complete understanding of why they no longer do. From the Red Cross website:

> Local purchases of food and clothing are more culturally appropriate and effective.<sup id="rf1-2013"><a href="#fn1-2013" title="Red Cross FAQ" rel="footnote">1</a></sup>

While relief work is among the most important work there is, in my mind. I am not a large scale relief administrator, I have decided to donate my time to _local_ relief efforts instead. So when I read that statement by the Red Cross, I kind of accepted it&hellip; Then I come across this:

> <q>No one left behind</q>&hellip; Really apparently the residents of Coney Island and Staten Island don't count in the presidents assessment. FEMA just like all other big government services has FAILED these residents. Who is helping out??? Individuals, charities, faith based groups, philanthropic groups have been there. Forget what the media is telling you send food. Send what you can, do what you can do.<sup id="rf2-2013"><a href="#fn2-2013" title="G+: Michael Weaver" rel="footnote">2</a></sup>

As a volunteer firefighter, this statement rings very true to me: do what **you** can! This got me thinking about the Red Cross' stance a little more; I am not convinced as to how _effective_ local purchases are. In fact, the more I think about it, the more I believe that local purchases are actually an _impossibility,_ and the whole statement is designed deceive by playing to current cultural trends.

<span id="Marketing_Speak">

<h1>
  Marketing Speak
</h1></span>

The statement is very carefully crafted to use words that will control people's emotions. <q>Local purchases</q>, and <q>culturally appropriate</q> bring up images of respecting the rights of <q>noble savages</q>; while <q>local purchases</q> combined with <q>effective</q> generate images of buying food from local farmers who have been hard hit and need the help too. Everyone goes home feeling good about how <q>responsible</q> the Red Cross is being, nobody digs deeper.

When organizations use a lot of language designed to divert attention, it is important to ask what they are diverting attention from.

<span id="Effective_or_Impossible">

<h1>
  Effective or Impossible?
</h1></span>

Disasters such as Sandy destroy resources that people have built or stored. People had a roof over their head, food in the pantry, and blankets on the bed; now the roof has fallen in destroying the blankets and taking the pantry with it. When this happens to an _individual_, aid can be given by their local community: family and friends can offer them a place to stay until they get back on their feet. Unfortunately, in this case, the neighbors, friends, and family are all in the same position, everybody has had the goods destroyed.

So if the point to outside aid is that the resources locally have been destroyed, where does the Red Cross plan on getting these local resources. The whole point to requiring Red Cross aid is that there are insufficient local resources. On the flip side, if the resources are available locally, what need is there for the Red Cross. If local charities have access to resources, then neighbors can help neighbors, and the Red Cross is not necessary.

Since it is not possible for the Red Cross to be getting these resources locally, the real question becomes where are they getting the resources from.

<span id="Follow_the_Money">

<h1>
  Follow the Money
</h1></span>

While I was looking up the Red Cross' stance on their FAQ, I noticed their promotional advertisement at the top of the screen. It has a young lady, obviously staying in an emergency shelter, huddling in a blanket.

<a href="http://vius.ca/2012/11/check-your-charity/redcrossblankets-2/" rel="attachment wp-att-2019"><img class=" wp-image-2019" title="RedCrossBlankets" src="/media/2012/11/RedCrossBlankets1.png" alt="" width="559" height="212" /></a>

One of the primary resources that every disaster needs are blankets. Blankets not only offer warmth to people, they offer a critical psychological comfort that people need. I remember as a child folding up a <q>gently used</q> blanket to be donated to victims of some disaster or another. I remember feeling good, that someone in need was going to get some comfort from the blanket that had brought me so much comfort.

Look closely at the blanket she is wearing; it could not have been a locally acquired resource; it is covered in <q>American Red Cross</q> logos. This blanket was obviously transported in from another location, why could the blanket I wanted to donate not be transported in its place.

It is obvious to me that this was a bulk purchase of blankets, specially ordered by the Red Cross, months or years, in advance. They have plastered their logo all over it, to identify their resources (and maybe get a little marketing in while their at it). They likely got a contract to purchase thousands of them, thereby getting them at a reduced cost. They likely conform to a specifications tailored to the anticipated types of disasters, climate, and transport requirements. But unless the Red Cross was able to predict the disaster years in advance, **they were not purchased locally**!

So why lie about it? I just gave three very valid reasons for asking for money instead of blankets, that would all be valid explanations as to it being responsible use of resources. Instead of coming up with very reasonable explanations, they flat out lied about it. Why?

Maybe this is part of the reason

> According to United Press International, Gail McGovern took over as CEO of the American Red Cross in 2008 at an annual salary of $500,000<sup id="rf3-2013"><a href="#fn3-2013" title="About.com, Urban Legends: Salaries of Charities" rel="footnote">3</a></sup>

Not the biggest paycheck I've seen for CEOs, but certainly not a modest one. What I suspect more, is that the contracts go to <a title="Invisible Children Criticism" href="http://www.washingtonpost.com/blogs/blogpost/post/invisible-childrens-stop-kony-campaign/2012/03/07/gIQA7B31wR_blog.html" target="_blank">friends</a>, and companies that managers are heavily invested in.

<span id="Take_Action">

<h1>
  Take Action
</h1></span>

I like the Red Cross, I like to think they are a private organization that does a necessary job. I like to think that the request for cash is about efficiency (e.g. bulk purchases, technical specifications), but I don't know why they are being so deceptive about it.

In the end, what is really needed is for individuals to take action. What is needed is for **you** to take action. Do what **you** can. Maybe that means sending <a title="City Meals on Wheels" href="http://www.citymeals.org/support-us" target="_blank">cash</a> <a title="United Way" href="https://donate.unitedway.ca/index.php?WID=UWC-CC" target="_blank">to</a> <a title="Catholic Charities" href="http://www.catholiccharitiesusa.org/sslpage.aspx?pid=1304" target="_blank">the</a> <a title="Red Cross" href="https://secure.e2rm.com/registrant/donate.aspx?eventid=89776&gclid=CPP5n4WhtbMCFexFMgodggcAIg" target="_blank">Red Cross</a>; maybe that means sending <a title="United Way" href="http://www.uwsect.org/news/wake-sandy-how-you-can-help-donations-food-water-blankets-needed" target="_blank">food</a>, <a title="Mercury One" href="http://www.mercuryone.org/" target="_blank">blankets</a>, and tools; maybe that means getting in a <a title="Tools for Rebuilding" href="http://www.popularmechanics.com/outdoors/survival/gear/4346954" target="_blank">truck and driving out</a>, maybe that means <a title="Don’t help with Sandy unless you’re unionized" href="http://news.yahoo.com/jersey-town-ala-volunteer-utility-crew-don-t-053500307.html" target="_blank">staying out of the way of people actually trying to help</a>.

**Aside:**

This discussion doesn't even include the <a title="Without Electricity, New Yorkers on Food Stamps Can’t Pay for Food" href="http://colorlines.com/archives/2012/11/without_electricity_new_yorkers_on_food_stamps_cant_pay_for_food.html" target="_blank">complete failure that Food Stamps</a> are under these conditions. No, I don't think there are any technical solutions to this problem, only trucks full of food.

&nbsp;

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-2013">
    <p>
      <a title="Red Cross FAQ" href="http://www.redcross.ca/article.asp?id=002653&#q25" target="_blank">Red Cross FAQ</a>&nbsp;<a href="#rf1-2013" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn2-2013">
    <p>
      <a title="Michael Weaver" href="https://plus.google.com/105528442272691468186/posts/6758capnDoA?utm_source=chrome_ntp_icon&utm_medium=chrome_app&utm_campaign=chrome" target="_blank">G+: Michael Weaver</a>&nbsp;<a href="#rf2-2013" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn3-2013">
    <p>
      <a title="Salaries of Charity CEOs Compared" href="http://urbanlegends.about.com/library/bl_charities_salaries.htm" target="_blank">About.com, Urban Legends: Salaries of Charities</a>&nbsp;<a href="#rf3-2013" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
</ol>