---
id: 580
title: We have our new Home!
date: 2010-08-30T05:09:20+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=580
permalink: /2010/08/30/we-have-our-new-home/
categories:
  - Moving
---
We have purchased our dream. A house, over 100 acres of land, ponds and several barns in the Annapolis Valley. We now have our very own &#8216;Hundred Acre Wood'&hellip;

<div id='gallery-6' class='gallery galleryid-580 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/08/30/we-have-our-new-home/trail/'><img width="150" height="111" src="/media/2010/08/trail.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/08/30/we-have-our-new-home/garage/'><img width="150" height="112" src="/media/2010/08/garage.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/08/30/we-have-our-new-home/house/'><img width="150" height="112" src="/media/2010/08/house.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/08/30/we-have-our-new-home/hundred-acre-wood/'><img width="150" height="113" src="/media/2010/08/Hundred-Acre-Wood.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/08/30/we-have-our-new-home/pond/'><img width="150" height="112" src="/media/2010/08/pond.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure>
</div>