---
id: 176
title: Surprise! We meant it
date: 2010-06-05T17:26:18+00:00
author: Kavius
layout: post
categories:
  - Musings
---

On Thursday (two days ago), I stopped by the offices of a company I used to work for (two years and a half years ago). I had gone for lunch with an friend and we stopped by the office for a hand-shake/good-bye to a few of the people I worked with.

Every one of them was shocked I was actually doing it.

This should not have come as a surprise to many of these people: I had begun to develop this idea at the time I worked there; I had run into them at a Christmas party and explained the new plan. Still, they thought I was joking.  The Wife has had the same experience. A friend of her's has expressed surprise, and envy, that we are doing what we are doing.

I am surprised, that these people are surprised. We said we were going to do it, where is the shocker?

 The Wife has explained it to me.

It's important to have dreams, but for most people they never take their dream beyond the stage of fantasy. A fantasy is an unattainable dream, or at least one you have no intention of actually pursuing. For example, I have this fantasy that I am a great king over a small kingdom and rule with a kind, but iron-fist. It's not happening<sup id="rf1-176"><a href="#fn1-176" title="though at one time I did investigate the possibility of pursuing this dream, it entailed too much risk for the proposed payout, as well as a really long investment time. In the end I abandoned it as possible, but not worth the risk/effort" rel="footnote">1</a></sup>.  The Wife, and I, do not have a term for the other type of dream. The one you for which you create a plan; the one that you work year after year for. These are attainable, realistic<sup id="rf2-176"><a href="#fn2-176" title="yes, my king dream is realistic, just difficult. Feel free to contact me for aspects of the plan that I still remember. Do not attempt to implement this dream unless you between the ages of 19 and 23, and have shit for brains" rel="footnote">2</a></sup>, planned for. Where I have stumbled over the years is to never have recognized that this is what I was doing. I have never considered the possibility of Fantasies, only the other kind of dream.

I knew a woman who had fantasies of retiring to Mexico, so went for a two week holiday in Mexico, twice a year. She was 50 years old, had just purchased a $400,000 home (which was fully mortgaged for 25 years), and had no savings (having spent all her money on Mexican vacations), and had never considered what it took to immigrate to Mexico. For the life of me, I can't figure out how this woman is going to retire, let alone to Mexico.

Most people (I am learning) never consider how they are going to attain their dreams, and therefore have only fantasies. Thus, when  The Wife and I announce we are going to go live our dream, they are totally shocked.

# Thanks to a Friend

I would like to thank the woman I went for lunch with<sup id="rf3-176"><a href="#fn3-176" title="I&rsquo;m keeping her name off the list just because I&rsquo;m not sure if she would appreciate it being published" rel="footnote">3</a></sup>  (the one I went to go see a couple of days ago), for being one of my inspirations in all of this. While I was still figuring out what my dream looked like, she was making it a reality for herself. We had a long chat about how she thinks  The Wife and I are really brave, but frankly, part of what made it possible for us was her example. She has done an amazing amount of work toward realizing her dream. For her it is not a fantasy, it is an objective.

While she feels we are going farther than her, and she is a little jealous. I would like to point out that we have not made it even as far as she has yet, we are still working to achieve some of the things she has been doing for years. I would also like to point out the the purchase of the land was a final stage. We had purchased our current house with the intent to upgrade at sometime in the future, we have just been pushed to do it a little sooner than anticipated.

So, thank-you for pushing us and giving us some good inspiration over the last couple of years.

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-176">
    <p>
      though at one time I did investigate the possibility of pursuing this dream, it entailed too much risk for the proposed payout, as well as a really long investment time. In the end I abandoned it as possible, but not worth the risk/effort&nbsp;<a href="#rf1-176" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-176">
    <p>
      yes, my king dream is realistic, just difficult. Feel free to contact me for aspects of the plan that I still remember. Do not attempt to implement this dream unless you between the ages of 19 and 23, and have shit for brains&nbsp;<a href="#rf2-176" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn3-176">
    <p>
      I'm keeping her name off the list just because I'm not sure if she would appreciate it being published&nbsp;<a href="#rf3-176" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
</ol>