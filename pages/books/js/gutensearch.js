'use strict';

/*
global DOMParser
global moment
global PouchDB
*/

import '/assetts/js/libs/rdflib.min.js';
import '//cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js';
import '/assetts/js/libs/pouchdb.upsert.min.js';

import './lib-list.js';

import vius from '/assetts/js/vius/util.js';


(function(){


const state = {
	config:{
		catalogue:{
			baseUrl: 'https://vius-jeffereycave.c9users.io',
			location: '/datasets/gutenberg/rdf-files.tar.gz',
			ttl: 1000 * 60 * 60 * 4, // 4 hours
			//ttl: 1000 * 60, // seconds
		},
		remote:'http://localhost:5984/books'
	}
};


/**
 * Downloads and synchronises the catalogue with the local DB instance
 */
async function syncCatalogue(force = false){
	if(state.CatalogueSynchronizer){
		console.warn('Request for sync ignored. Already running.');
		return false;
	}

	state.system = state.system || {};
	if(!force){
		try{
			state.system = await state.db.get('system');
		}
		catch(e){
		}
		let now = Date.now();
		if(now < state.system.catalogueTTL){
			console.log('Catalogue is up to date. Have schedule a check ' + moment(state.system.catalogueTTL).fromNow());
			let wait = (state.system.catalogueTTL - now) + 24;
			setTimeout(syncCatalogue, wait);
			return false;
			//console.warn('Running anyway... DEBUG stuff.');
		}
	}

	let awaits = [];
	state.CatalogueSynchronizer = new Worker('js/gutindex.js');
	let fetcher = state.CatalogueSynchronizer;
	let db = state.db;
	//if(state.config.remote){
	//	db = new PouchDB(state.config.remote);
	//}
	fetcher.onmessage = (e)=>{
		switch(e.data.type){
			case 'taskadd':
				state.elements.progress.max += e.data.value;
				//console.log('Task Add: ' + e.data.value);
				break;
			case 'taskdone':
				state.elements.progress.value += e.data.value;
				//console.log('Task Done: ' + e.data.value);
				break;
			case 'xml':
				saveCatalogue(e.data.value.xml).then((records)=>{
					state.elements.progress.max += records.length;
					records.map(b=>{
						return db.upsert(b._id,(old)=>{
							b = JSON.merge(old,b);
							if(b.inLanguage !== 'en'){
								return false;
							}
							if(isPouchEquivalent(b,old)){
								return false;
							}
							return b;
						})
						.finally(()=>{
							state.elements.progress.value+=1;
						})
						;
					})
					.forEach(save=>{
						awaits.push(save);
					})
					;
				});
				if(awaits.length > 1000){
					fetcher.postMessage({cmd:'pause'});
					let proc = awaits.slice(0);
					awaits = [];
					Promise.all(proc).then(()=>{
						if(!fetcher.continuing){
							fetcher.continuing = true;
							vius.schedule(100,()=>{
								fetcher.continuing = false;
								fetcher.postMessage({cmd:'continue'});
							});
						}
					});
				}
				break;
			case 'complete':
				awaits.push(db.upsert('system',rec=>{
					rec.catalogueTTL = Date.now() + state.config.catalogue.ttl;
					rec.catalogueTTL = Math.floor(rec.catalogueTTL / 60000) * 60000;
					return rec;
				}));
				Promise.all(awaits).then(()=>{
					state.CatalogueSynchronizer = null;
					setTimeout(syncCatalogue,60000);
					console.log(' - rescheduled ');
				});
				break;
			default:
				console.error("Unknown event type from 'gutindex.js' ("+e.data.type+")");
				break;
		}
	};
	fetcher.postMessage({cmd:'start',config:state.config});
}

function parsePerson(person){
	let rtn = {
		"name":'name',
		//"familyName":'',
		//"givenNames":'',
		//"additionalName":'',
		"birthDate":'birthdate',
		"deathDate":'deathdate',
		"alternateName":'alias',
		'sameAs':'webpage',
	};

	for(let key in rtn){
		let selector = rtn[key];
		rtn[key] = Array.from(person.querySelectorAll(selector));
		if(rtn[key].length === 0){
			delete rtn[key];
		}
		else if(rtn[key].length === 1){
			rtn[key] = rtn[key][0].textContent;
		}
		else{
			rtn[key] = rtn[key].map(d=>{
				return d.textContent;
			});
		}
		if(Array.isArray(rtn[key])){
			let val = rtn[key];
			delete rtn[key];
			key += 's';
			rtn[key] = val;
		}
	}
	rtn["@type"] = "Person";

	let author = rtn.name.split(',');
	if(author[0]){
		rtn.familyName = author[0].trim();
	}
	if(author[1]){
		let givenNames = author[1].trim().split('(').pop().replace(')','').split(' ');
		if(givenNames[0][1] === '.'){
			givenNames = [givenNames.join(' ')];
		}
		rtn.givenName = givenNames[0].trim();
		if(givenNames[1]){
			rtn.additionalName = givenNames[1].trim();
		}
	}



	return rtn;
}

function parseCatalogue(catalogue){
	let elem = null;
	let schema = {
		"@context":"http://schema.org",
		"@type":"book",
	};

	schema._id = catalogue
		.attributes['rdf:about']
		.nodeValue
		.split('/')[1]
		;
	schema._id = 'text.pg' + schema._id;
	elem = catalogue.querySelector('title');
	if(elem){
		schema.name = elem.textContent;
	}
	else{
		schema.name = '';
	}
	//console.debug(' - Saving Book: ('+schema._id+') ' + schema.name);
	//if(schema._id === 'text.pg37765'){
	//	debugger;
	//}

	schema.creators = Array.from(catalogue.querySelectorAll('creator agent'))
		.map(creator=>{
			creator = parsePerson(creator);
			return creator;
		});
	schema.publisher = {name:catalogue.querySelector('publisher').textContent};
	schema.subjects = Array.from(catalogue.querySelectorAll('subject Description'))
		.reduce((a,d)=>{
			let type = d.querySelector('memberOf');
			type = type
				.attributes['rdf:resource']
				.nodeValue.split('/')
				.pop()
				;
			let val = d.querySelector('value');
			val = val.textContent;
			a[type] = a[type] || [];
			a[type].push(val);
			return a;
		},{})
		;
	schema.categories = schema.subjects.LCC;
	schema.subjects = schema.subjects.LCSH;
	schema.accessMode = 'textual';
	schema.contributors = Array.from(catalogue.querySelectorAll('trl agent'))
		.map(t=>{
			let person = parsePerson(t);
			person.jobTitle = "Translator";
			return person;
		});
	schema.encodingFormat = 'application/epub+zip';
	elem = catalogue.querySelector('language value');
	if(elem){
		schema.inLanguage = elem.textContent;
	}
	schema.license = catalogue.querySelector('rights');
	if(schema.license){
		schema.license = schema.license.textContent;
		schema.isAccessibleForFree = true;
	}
	else{
		schema.isAccessibleForFree = false;
	}
	schema.sdDatePublished = catalogue.querySelector('issued').textContent;
	schema.sdPublisher = {"@type":"Person", "@id":"http://vius.ca"};
	schema.bookFormat = {"@type":"EBook"};
	//schema.copyrightYear = null;
	//schema.datePublished = null;

	return schema;
}

function parseEPub(catalogue){

}

async function saveCatalogueDOM(catalogue){
	let parser = new DOMParser();
	let doc = parser.parseFromString(catalogue, "application/xml");
	let records = Array.from(doc.querySelectorAll('ebook'))
		.map(b=>{
			let parsed = parseCatalogue(b);
			return parsed;
		})
		;
	return records;
}


async function saveCatalogueRDF(catalogue){
/*

	const types = {
		RDF : $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
		RDFS : $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#"),
		FOAF : $rdf.Namespace("http://xmlns.com/foaf/0.1/"),
		XSD : $rdf.Namespace("http://www.w3.org/2001/XMLSchema#"),
		SCHEMA: $rdf.Namespace("http://schema.org/"),
		PG : $rdf.Namespace('http://www.gutenberg.org/rdfterms#'),
		PGb : $rdf.Namespace('http://www.gutenberg.org/rdfterms/'),
	};
	//types.PG = {
	//	etext: types.PG('etext'),
	//};
	function resolveNode(node){
		switch(node.termType){
			case "Literal":
				break;
			case "NamedNode":
				break;
			case "BlankNode":
				let matches = store.match(node);
				matches = matches.sort((a,b)=>{
					if(a.predicate.value !== b.predicate.value){
						if(a.predicate.value === types.RDF('type').value){
							return -1;
						}
						if(b.predicate.value === types.RDF('type').value){
							return 1;
						}
						if(a.predicate.value === types.RDF('value').value){
							return -1;
						}
						if(b.predicate.value === types.RDF('value').value){
							return 1;
						}
					}
					return 0;
				});
				node = matches.reduce((a,n)=>{
					n.object = resolveNode(n.object);
					if(n.predicate.value === types.RDF('type').value){
						a.datatype = n.object;
						if(a.datatype.value === types.RDF('Bag').value){
							a.value = [];
						}
					}
					else if(n.predicate.value === types.RDF('value').value){
						a = JSON.merge(a,n.object);
					}
					else{
						if(a.datatype.value === types.RDF('Bag').value){
							a.value.push(n.object);
						}
						else{
							a = JSON.merge(a,n.object);
						}
					}
					return a;
				},{});
				break;
			default:
				node.termType = 'Literal';
				node = resolveNode(node);
				break;
		}
		return node;
	}

	let store = $rdf.graph();

	let baseurl = state.config.catalogue.baseUrl + state.config.catalogue.location;
	$rdf.parse(catalogue,store,baseurl,'application/rdf+xml');

	// do something with the data in the store (see below)
	let books = store.match(undefined, types.RDF('type'), types.PGb('etext'))
		.map(t=>t.subject)
		.map(async (b)=>{
			b._id = b.value.split('#').pop().replace('etext','pg');
			//console.log($rdf.serialize(b,store,baseurl,'application/ld+json'));
			console.debug("Book: " + b._id);
			let props = store.match(b);
			let schema = props.map(trip=>{
					console.debug(trip.predicate.value + '\n - ' + JSON.stringify(trip.object,null,4));
					trip.object = resolveNode(trip.object);
					return trip;
				})
				.reduce((a,d)=>{
					if(d.object.datatype && d.object.datatype.value === types.RDF('Bag').value){
						d = d.object.value.map((o)=>{
							return{
								"subject": d.subject,
								"predicate": d.predicate,
								"why": d.why,
								"object": o
							};
						});
					}
					else{
						d = [d];
					}
					a = a.concat(d);
					return a;
				},[])
				.map(async (d)=>{
					let o = d.object;
					o._id = JSON.stringify(o);
					o._id = await GenHash(o._id);
					return d;

				})
				;
			schema = await Promise.all(schema);

			let nodes = schema.map(d=>d.object);
			let rel = schema.map(async d=>{
				let rel = {
					"type": d.predicate.value,
					"value": 0
				};
				rel.nodes = [
					b._id,
					d.object._id
				];
				rel.source = d.why;
				rel._id = await GenHash(rel.nodes.join(''));
				rel._id = 'link.'+ rel._id;

				return rel;
			});
			rel = await Promise.all(rel);


			schema = nodes.concat(rel).map(b=>{
				return state.db.upsert(b._id,(old)=>{
					if(isPouchEquivalent(b,old)){
						return false;
					}
					return b;
				});
			});
			schema = Promise.all(schema);
			return schema;
		});

	let success = await Promise.all(books);
	success = success.every(d=>{
		return d.every(result=>{
			return result;
		});
	});
	return success;
*/
}


const saveCatalogue = saveCatalogueDOM;
//const saveCatalogue = saveCatalogueRDF;


function isPouchEquivalent(a,b){
	let items = [a,b].map((d)=>{
		d = JSON.clone(d);
		Object.keys(d)
			.filter(key =>{
				return key.startsWith('_');
			})
			.forEach(key=>{
				delete d[key];
			})
			;
		d = JSON.stringify(d);
		return d;
	});

	let isSame = (items[0] === items[1]);
	return isSame;
}



function Reset(){
	state.elements.reset.removeEventListener('click',Reset);

	return;
	let DeleteAll = new Promise((resolve)=>{
		function deleter(){
			state.db
				//.allDocs({limit:5,include_docs:true})
				.allDocs({include_docs:true})
				.then(records=>{
					let dels = records.rows
						.filter((rec)=>{
							return rec.doc._id[0] !== '_';
						})
						.map((rec)=>{
							return {
								"_id":rec.doc._id,
								"_rev":rec.doc._rev,
								"_deleted":true,
							};
						});
					dels = state.db.bulkDocs(dels);
					//if(records.rows.length === 0){
						dels = dels.then(resolve);
					//}
					//else{
					//	dels = dels.then(deleter);
					//}
				})
				;
		}
		deleter();
	});
	DeleteAll = Promise.resolve();

	DeleteAll
		.then(()=>{
			return state.db.replicate.to(state.config.remote);
		})
		.then(()=>{
			return state.db.destroy();
		})
		.then(Initialize)
		;
}


function Initialize(){
	state.db = new PouchDB('gutindex');
	console.debug('Syncing with remote db');
	let lastpending = 0;
	state.db.replicate
		.from(state.config.remote,{filter: 'library/en',query_params: {"inLanguage":'en'}})
		.on('change',(info)=>{
			if(!lastpending){
				state.elements.progress.max += info.pending;
			}
			state.elements.progress.value += lastpending - info.pending ;
			lastpending = info.pending;
		})
		.on('complete', async ()=>{
			//await state.db.compact();
			lastpending = 0;
			console.debug('Sync complete');
			state.db.sync(state.config.remote,{
					live: true,
					retry: true,
					filter: 'library/en',
					query_params: { "inLanguage": 'en' }
				})
				.on('change',(info)=>{
					if(!lastpending){
						state.elements.progress.max += info.pending;
					}
					state.elements.progress.value += lastpending - info.pending ;
					lastpending = info.pending;
				})
				;
			setTimeout(syncCatalogue,60000);

			state.elements.reload.addEventListener('click',()=>{syncCatalogue(true);});
			state.elements.reload.enabled = true;
		})
		;

	state.system = {};
	state.elements = {
		reload: document.querySelector('#reload'),
	};
	state.elements.reload.enabled =false;
	state.elements.progress = document.querySelector('#progress');

}


window.addEventListener('load',Initialize);


})();

