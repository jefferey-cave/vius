---
id: 1867
title: By the Great Worm Spirit
date: 2012-05-08T19:24:02+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1867
---
Earth Worm Jim is definitely one of my favourite cartoon characters of all time. Being a worm, Jim worships The Great Worm Spirit, and can often be heard dedicating his victories to Him:

  * _By the Great Worm Spirit_, whose mighty bristles strike the hammer-blows of justice&hellip;
  * _By the Great Worm Spirit_, whose whims all creatures must obey&hellip; (Episode 5)
  * _By the Great Worm Spirit_, whose name is an anagram of <q>warm spit or tiger</q> (Episode 10, the secret of the universe)
  * _By the Great Worm Spirit_, whose entrails mortals are not fit to lick&hellip; (09 &#8211; Trout!)
  * _By the Great Worm Spirit_, whose fearsome breath fricassees the legions of evil&hellip; (11 &#8211; Bring Me the Head of Earthworm Jim)
  * _By the Great Worm Spirit_, whose eyeless visage gazes over us all &hellip; (14 &#8211; The origin of Peter puppy)
  * _By the Great Worm Spirit_, whose secret recipe for lobster bisque must never be revealed&hellip; (16 &#8211; Darwin's Nightmare)
  * _By the Great Worm Spirit_, whose mighty burrows dwarf the works of man&hellip; (?? &#8211; ???)

And Peter Puppy (his little fuzz buddy):

> I must not fear. Fear is the mind killer. Fear is the little death that brings total oblivion.

&nbsp;

&nbsp;