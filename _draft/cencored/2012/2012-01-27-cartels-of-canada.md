---
id: 2268
title: Cartels of Canada
date: 2012-01-27T23:22:37+00:00
author: Jeff
layout: post
guid: http://vius.plaidsheep.ca/?p=2268
permalink: /2012/01/27/cartels-of-canada/
categories:
  - Uncategorized
---
Given the rate of unemployment in the world, why do governments think [mandating high food prices](http://www.gov.ns.ca/just/regulations/regs/difdpric.htm) is a good idea? Given its <q>Jobs Here</q> program, why does the Government of Nova Scotia make it illegal to produce food?

As someone new to the agricultural industry, I find it interesting how many things I am not allowed to produce. I have identified several markets that I could enter, on a small scale, that I would allow me to start small and grow into a larger business over time. Unfortunately, most of these markets are illegal for me to enter, not because the product would be considered unsafe for humans to consume (marijuana, tobacco, raw milk) but because the government maintains a monopoly on the production of the products and limits the produced supply in order to inflate prices and increase its own profits.

Really, it is not so much a Government monopoly, as a Government approved cartel of the big players. According to Canadian law, cartels are illegal if they **unduly** reduce supply, inflate prices, or restrain competition.<sup id="rf1-2268"><a href="#fn1-2268" title="Low et al., &ldquo;Cartel Enforcement in Canada&rdquo;, 2004" rel="footnote">1</a></sup> It is the choice of phrase <q>unduly</q> which is unfortunate. Given the government is running the cartel, asking the government to decide whether it is legal or not presents a problem. The agency responsible for deciding if the cartel is unduly restricting competition is the cartel itself. Those responsible for deciding, profit from the cartels existence.

It is my contention that small agriculture in Canada is crippled by unfair competitive practices held by the agriculture monopolies. This is limiting Canada's ability to create new employment, produce food, and be innovative. What is <q>unduly</q> about forbidding small business to even try?

Canadian Agricultural Cartels

  * [Milk](http://www.dfns.ca/history.htm)
  * [Eggs](http://www.gov.ns.ca/just/regulations/regs/didelpow.htm)
  * [Wheat](http://www.cwb.ca/public/en/)

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-2268">
    <p>
      <a href="http://www.mcmillan.ca/Upload/Publication/Halladay_Low%20cartel%20paper%20for%20Seoul%20conference.pdf">Low et al., <q>Cartel Enforcement in Canada</q>, 2004</a>&nbsp;<a href="#rf1-2268" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>