---
id: 1717
title: Mentioning WWII is not Racism
date: 2012-02-07T20:11:20+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1717
permalink: /2012/02/07/mentioning-wwii-is-not-racism/
categories:
  - Uncategorized
---
I just read an article about a Catholic Bishop that compared modern schools in North America to the school system that was attempted under the National Socialist party in Germany. These statements were critisized as <q>raising the spectre of the Holocaust</q> by the ACLU (<a title="American Civil Liberties Union" href="http://en.wikipedia.org/wiki/American_Civil_Liberties_Union" target="_blank">American Civil Liberties Union</a>) and ADL (<a title="Criticism of the ADL" href="http://en.wikipedia.org/wiki/Anti-Defamation_League#Criticism" target="_blank">Anti-Defamation League</a>).<sup id="rf1-1717"><a href="#fn1-1717" title="I have a longstanding dislike for the ADL. I have long believed that it has&nbsp;pursued&nbsp;a course of action intent on highlighting differences and promoting racism." rel="footnote">1</a></sup>

<!--more-->

<http://cnsnews.com/news/article/pa-bishop-does-not-recant-saying-hitler-and-mussolini-would-love-our-public-school>

I must say that the statements made by the ACLU and ADL are completely irresponsible and demonstrate the absolute lack of sense being used by most Social Justice Advocates in modern society. These organizations are no longer working to protect individuals from tyranny, but are themselves working to control and manipulate the public. They have become a tool of control, rather than a tool of protection.

> The Holocaust was a unique experience.<img class="alignright" title="The Christian Martyrs Last Prayer" src="http://upload.wikimedia.org/wikipedia/commons/c/c4/The_Christian_Martyrs_Last_Prayer.jpg" alt="" width="374" height="227" />

Tell that to the <a title="Persecution of Christians in the Roman Empire" href="http://en.wikipedia.org/wiki/Persecution_of_Christians_in_the_Roman_Empire" target="_blank">Christians of Rome</a>, <a title="Armenian Holocaust" href="http://en.wikipedia.org/wiki/Armenian_Genocide" target="_blank">Armenians of Turkey</a>, <a title="History of Jews in Russia" href="http://en.wikipedia.org/wiki/History_of_the_Jews_in_Russia#Stalinist_antisemitic_campaigns" target="_blank">Jews under Stalin</a>, <a title="Cromwellian Settlement" href="http://en.wikipedia.org/wiki/Cromwellian_conquest_of_Ireland#The_Cromwellian_Settlement" target="_blank">Irish under Cromwell</a>, or <a title="Genocide, Slavery and Rape in the Bible" href="http://rarebible.wordpress.com/2009/12/15/genocide-slavery-and-rape-of-the-midianites/" target="_blank">Midianites under Moses</a>.

> inappropriate analogies/reckless comparisons

This wasn't an analogy. It is a direct comparison of Apples to Apples (I just used a metaphor).

> Our role should be to honor those who fought

I seem to remember being told (by a Veteran who came to speak in my elementary school class) that the best way to honour those who fought and died was to remain ever vigilant against tyranny. To ensure that we never allowed another tyrant to come to power in a Western Nation. We were taught the history in order that we could recognize the signs and steer ourselves away from that direction.

> trivialize the history of the Holocaust

Rather than trivializing the Holocaust, the statements made by the ACLU and ADL <q>undermine</q> the objectives of those heroes that fought. If we are to learn from history, we must be allowed to draw comparisons between history and modern society; we must be able to identify those trends that are leading us to the same problems we have already experienced.

There are two very important lessons to be taken from WWII: first that remaining silent for fear of shame or reprisal leads to totalitarianism; second, we learn that those who would threaten you with shame and reprisal for not remaining silent are not to be trusted (Yes, I'm reffering to the ACLU and the ADL)

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1717">
    <p>
      I have a longstanding dislike for the ADL. I have long believed that it has pursued a course of action intent on highlighting differences and promoting racism.&nbsp;<a href="#rf1-1717" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>