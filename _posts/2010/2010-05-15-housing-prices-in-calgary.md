---
id: 51
title: Housing Prices in Canada
date: 2010-05-15T00:00:26+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=51
permalink: /2010/05/15/housing-prices-in-calgary/
categories:
  - Opinions
  - Essays
---

Recently the [Canada Mortgage and Housing Corporation](http://www.cmhc-schl.gc.ca/en/co/moloin/moloin_003.cfm)
changed some of the rules for getting mortgages. They have raised the minimum
down-payment requirements for mortgages they will insure (the down-payment
must be 10% instead of 5%). One of my  The Wife's managers was really bummed
over this. He has been saving to buy a house for a while now, and suddenly he
doesn't have enough money to make a down-payment on a house. What I explained
to him was that this may not be a bad thing for him.

Assuming that people have saved up a particular amount of money for a down
payment on a home the changing of the down payment requirements will not make
houses unaffordable to these people, but instead force the price of the houses
to come down. This favours buyers instead of sellers.

Most people consider the cost of the home and attempt to determine the amount
of down-payment they need to come up with to purchase that home.


| --- | --- |
| T | Total cost of the house |
| D | Down-payment individual can afford to place |
| d | down-payment as a percentage of total, as required by government |

In this case, assuming you wish to purchase a home for $300,000 it is
necessary to come up with a certain amount of money.

<table>
 <tr>
  <td>
T: $300,000<br />
d: 5%
  </td>
  <td>
<math><mi>D</mi><mo>=</mo><mi>T</mi><mi>d</mi></math>
<br />
<math><mi>D</mi><mo>=</mo><mn>$300,000</mn><mo>&#xB7;</mo><mn>5%</mn></math>
<br />
<math><mi>D</mi><mo>=</mo><mn>$15,000</mn></math>
  </td>
 </tr>
</table>


So, an individual that has worked really hard to save $15,000 to put down on
their first home purchase can afford the home with a 5% down-payment
requirement. Once the government changes the requirements down-payment from 5%
to 10%, the initial deposit requirements go up.


<table>
 <tr>
  <td>
T: $300,000<br />
d: 10%
  </td>
  <td>
<math><mi>D</mi><mo>=</mo><mi>T</mi><mi>d</mi></math>
<br />
<math><mi>D</mi><mo>=</mo><mn>$300,000</mn><mo>&#xB7;</mo><mn>10%</mn></math>
<br />
<math><mi>D</mi><mo>=</mo><mn>$30,000</mn></math>
  </td>
 </tr>
</table>

Suddenly, that person that has spent the last couple of years saving, can no
longer afford the house they were so close to having and were likely very
excited about. I can see why people were getting angry and desperate to
purchase (trying to beat the incoming changes). Unfortunately, these buyers
are looking at it from the perspective of the sellers. They need to be looking
at it from the perspective of the aggregate buyers.

What nobody is considering, is that either the buyer can come up with more
money, _or the seller can accept less_. Assuming individuals have saved a
certain amount of money (in this example $15,000) and the requirements have
changed, nobody can afford to pay more for the properties. Therefore, if the
houses are to sell, the seller must accept less money.

To this point we have assumed the buyer has the down-payment dictated by the
cost of the house, however we can look at the flip side of this statement and
say that the seller has the total sale price of the house dictated by
Effectively the total amount to be spent on the house is dictated by the
amount of down payment that can be generated.

Just as the down-payment can be considered a function of the price, the price
can be considered a function of the down-payment.

<p>
<math><mi>D</mi><mo>=</mo><mi>T</mi><mi>d</mi></math>
</p>

<p>
<math><mi>T</mi><mo>=</mo><mfrac><mi>D</mi><mi>d</mi></mfrac></math>
</p>

<table>
 <tr>
  <td valign="top">
D: $15,000$<br />
d: 5% and 10%
  </td>
  <td>
<math><mi>T</mi><mo>=</mo><mfrac><mi>D</mi><mi>d</mi></mfrac></math>
<br />
<math><mi>T</mi><mo>=</mo><mfrac><mn>$15,000</mn><mn>5%</mn></mfrac></math>
<br />
<math mathvariant="bold"><mi>T</mi><mo>=</mo><mn>$300,000</mn></math>
  </td>
  <td>
<math><mi>T</mi><mo>=</mo><mfrac><mi>D</mi><mi>d</mi></mfrac></math>
<br />
<math>
 <mi>T</mi>
 <mo>=</mo>
 <mfrac>
  <mn>$15,000</mn>
  <mn>10%</mn>
 </mfrac>
</math>
<br />
<math mathvariant="bold"><mi>T</mi><mo>=</mo><mn>$150,000</mn></math>
  </td>
 </tr>
</table>

When looked at in this light, it is the seller, rather than the buyer, that
loses out. The house that was worth $300,000, is not only worth $150,000. In
fact, this is especially good for the buyer as it means a more affordable, and
shorter term on their mortgage.

Naturally, these are not all of the variables involved and the likely end
result of the will neither be an immediate collapse of prices, nor a complete
proportional drop. Several other factors will come in to play (some buyers
will just come up with more money, some sellers will just not sell), and
buyers and sellers will find a middle ground. The primary point is that things
are not usually so simple as they initially appear.
