---
id: 1498
title: In Praise of Lawn Farmers
date: 2011-09-23T18:46:38+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1498
permalink: /2011/09/23/in-praise-of-lawn-farmers/
categories:
  - Opinions
---
The back-to-the-landers, small-holders, self-reliance, hobby farmers and homesteaders are a pretty small community, while each of the categories of small farmers tends to have its own reasons for why people started their journey, and widely different ethics and political frameworks, ideas for caring for the land tend to move through these groups very quickly. People tend to belong to more than one of these categories and, even if our reasons for doing it are different, we all have roughly the same objective: make a living from the land.

One thing I have heard from several people is an anger towards what some call <q>Lawn Farmers</q>.<sup id="rf1-1498"><a href="#fn1-1498" title="Lawn farmers are not the same as Grass Farmers. Grass Farming is a popular idea of pasture maintenance and is well liked in &ldquo;ethical agriculture&rdquo; circles" rel="footnote">1</a></sup> Lawn Farmers are those individuals who have urban jobs, have likely made a good living at it, and have decided to purchase an acreage, build a nice home on it, but not become farmers. Typically, these people will have 2-3 acres of lawn surrounding their home. This lawn is what gives them their moniker.

Among the Farmers-to-Be, Lawn Farmers are a group to be ridiculed. Don't they know that we have a responsibility to take care of the land? Don't they know that their lawn does not produce food? They drive up property prices in the area so that poor homesteaders can't afford to purchase land. They are urbanizing rural areas. They should have their land seized and given over to proper stewards of the land.<sup id="rf2-1498"><a href="#fn2-1498" title="This is a paraphrase of something I have heard repeatedly, and is a common sentiment (see No Farms No Food). I believe this to be one of the most horrific ideas present in Nova Scotia. By &ldquo;proper stewards&rdquo;, these people mean people who share their philosophy, unfortunately there is no&nbsp;guarantee&nbsp;that their philosophy is correct. Using force to compel compliance with your pet project is abhorrent: if you get it wrong, you have not only done more harm than good, you have injured someone in the process. We don&rsquo;t need to do&nbsp;something, we need to do the&nbsp;right-thing; what the right thing is, is usually not obvious." rel="footnote">2</a></sup>

I would recommend that people not be so quick to judge. There are advantages to lawn farmers, and I would like to see more in my area.

<span id="Customers">

<h1>
  Customers
</h1></span> 

As a small farmer the greatest challenge we need to overcome is access to markets. Producing the food, managing the land, and finding means to finance the whole operation are serious and persistant problems; but if we are to move beyond the start-up and become stable and sustainable, we need to find people to trade with for those things we don't produce ourselves. While large producers can make contracts to supply large processors, who in turn can meet the supply requirments of large retailers, the small farmer must find their market one customer at a time.

Lawn Farmers are that market.

Large farmers are able to charge less for each item, making their money by selling large volumes. Small farmers, on the other hand, sell a lot less produce, and therefore need to charge more per item to make a reasonable profit. Because of this, every little cost can have a significant impact on price and transport represents a significant one. For most small farmers it is not economical to transport their produce large distances; it just isn't possible to absorb the cost. Further, small farmers often got into farming to fill some **different** style of farming such as organic agriculture, ethical animal raising, or sustainable agricultural; further increasing the cost of produce in exchange for a perceived higher quality. Based on this, small farmer's target market must share many of the same values that the small farmer does, they must also have expendable income, they can't be farmers themselves, and they can't be very far away.

Lawn Farmers are that market.

They have moved to the country for a better life.

First of all, it is expensive to purchase land that does not earn its keep; right there they have demonstrated that they have produced and saved enough to be above subsistance level. They can afford to pay for better quality produce rather than just worrying about fending off starvation.

Next they recognize that there is something wrong with the modern urban life-style, that is why they have moved to the country. Based on this, they share some of the perspective of the small farmer: both groups recognize something is wrong, and want a better life for themselves and their children. This urge to experience the country lifestyle means they will be more willing to go out of their way to purchase produce from local suppliers (rather than the convenient large retail store).

In the past, rail-roads and ships transported goods all over my area. Small farming communities had a buyer present at every rail-road station and every dock, ready to buy produce and take on the responsibility of transport to urban centres, and absorb the risks of retail sales. Now the rail-roads have been pulled up, and the great river has been dammed, transport must be handled by road, and by the individual. That means a lot of travel for the small farmer. Thankfully, rather than staying in an urban centre and requiring the farmer to come to them, Lawn Farmers have moved closer to the producer, absorbing some of the transport, and marketing, costs on that face the small farmer.

Lawn Farmers **are** the small farmer's market.

<span id="Custodians">

<h1>
  Custodians
</h1></span> 

There are four ways land can be held: urban (paved), wilderness, agriculture, and lawn farm. Breaking pavement, and clearing forest, are both resource intensive tasks. I am envious of the small farmers that have purchased land in a state that can easily be recovered for agriculture: the Lawn Farms that only need to be tilled.

Lawns serve an important role in keeping the weeds at bay. Having an agricultural field next to an unkempt property would mean that every weed that has taken root in your neighbors land would be present in yours. A nicely manicured lawn next door ensures you have fewer worries when it comes to weeds choking out your crops, and fewer pesticides to keep them at bay.

Since the dawn of agriculture, farmers have been plagued by a serious problem: erosion. As the land is tilled nothing is left to hold the soil down. Wind and water take the valuable nutrients away, stripping the land of its agricultural value. There are only two known ways to prevent and slow this process: terracing, and cover crops. That's what lawns are: erosion prevention. It keeps the soil in place waiting for a farmer to come along and seize its potential.

The land I live on once was rolling pastures and orchards as far as the eye could see. Hundreds of acres of active agricultural land. Now, 50 years later, its bush. When I say bush, I mean the dense, impenetrable bush like I witnessed in the jungles of Cambodia. All of it thick rose, blackberry, and hawthorn. The kind of stuff you can't cut your way through with a machete.

How I wish someone had taken the time to mow it once a week, just to keep the forest at bay. If I were faced with mowed lawn, I could just hire a tractor, and turn the sod over into fresh fields. Maybe I could walk out and put a fence around the perimeter to contain cattle. As it stands most of my time will be spent clearing forest to make room for me to plant fields, or lay fencing.

Lawn farmers hold the land in wait. Lobby groups spend spend thousands of dollars attempting to get governments to spend millions of tax dollars protecting land from wasting away. Whatever their personal motivations, Lawn Farmers fill that role without our insistance. They save us precious tax dollars, by using their own pocket books, in the preservation of precious agricultural resources.

Lawn Farmers **are** custodians of the land.

<span id="I_Want_More">

<h1>
  I Want More
</h1></span> 

I recently read that Joel Salatin (a famous, small-farm activist), is known for chastising his customers for walking away when it came time to slaughtering chickens.<sup id="rf3-1498"><a href="#fn3-1498" title="http://www.blogger.com/comment.g?blogID=7339411618153269630&postID=2044725397873399987" rel="footnote">3</a></sup> I believe he is dead wrong for doing this. He is the food producer, not them. It is their unwillingness, or inability, to produce food that allows him to stay in business and experiment with novel approaches to food production.

I personally am grateful for the patronage, and the stewardship, the <q>lawn farmers</q> have undertaken, and would like to extend an invitation to anyone who does not produce their own food, and lives in my area, to come stop by and see how the food I go about raising and growing food. If you would like to trade for some of what I produce, all the better.

I will not ask you to handle the animals if you aren't comfortable with it.

I will not ask you to mend a fence if you aren't up to it.

I will not ask you to pull weeds if you don't want to.

You are welcome to join in if you want, I enjoy showing off my home and business, but I don't expect you to do my job. This is a life I have chosen, and am glad to do this work for you; in exchange for the work you do that I have chosen not to.

Mostly, I thank-you for doing your part in conserving our agricultural land.

&nbsp;

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1498">
    <p>
      Lawn farmers are not the same as <a title="Grass Farmers: Seeing the Big Picture" href="http://newfarm.rodaleinstitute.org/features/0604/grassfarmers/index1.shtml" target="_blank">Grass Farmers</a>. Grass Farming is a popular idea of pasture maintenance and is well liked in <q>ethical agriculture</q> circles&nbsp;<a href="#rf1-1498" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-1498">
    <p>
      This is a paraphrase of something I have heard repeatedly, and is a common sentiment (see No Farms No Food). I believe this to be one of the most horrific ideas present in Nova Scotia. By <q>proper stewards</q>, these people mean people who share their philosophy, unfortunately there is no guarantee that their philosophy is correct. Using force to compel compliance with your pet project is abhorrent: if you get it wrong, you have not only done more harm than good, you have injured someone in the process. We don't need to do <em>something</em>, we need to do the <em>right-thing</em>; what the right thing is, is usually not obvious.&nbsp;<a href="#rf2-1498" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn3-1498">
    <p>
      <a href="http://www.blogger.com/comment.g?blogID=7339411618153269630&postID=2044725397873399987">http://www.blogger.com/comment.g?blogID=7339411618153269630&postID=2044725397873399987</a>&nbsp;<a href="#rf3-1498" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
</ol>