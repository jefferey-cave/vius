---
id: 1565
title: Toro Wheelhorse 244-H
date: 2011-10-01T13:29:06+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1565
panels_data:
  - 'a:0:{}'
---
<figure id="attachment_1588" style="max-width: 640px" class="wp-caption alignright">[<img class="wp-image-1588 size-full" title="ToroWheelHorse244H.Profile" src="/media/2011/10/ToroWheelHorse244H.Profile.jpg" alt="Toro Wheel Horse 244-H" width="640" height="480" />](/media/2011/10/ToroWheelHorse244H.Profile.jpg)<figcaption class="wp-caption-text">1992 Toro Wheel Horse 244-H (22-14OE02)</figcaption></figure> 

I currently own a 1992 Toro Wheelhorse 244-H, with a 12 HP engine and 36&#8243; mower deck. I would love some more attachments for this, but it looks like all of the really cool attachments were for the model one up from mine. I'm not sure if it is possible to mount them on the 244-H frame&hellip; that would be cool (there is a snow-thrower, and tiller that I really want).

This little garden tractor has saved me a lot of work. That 7'x7'x7&#8242; was half a year's worth of firewood and hauling all of that wood would have been a real nightmare if it wasn't for being able to hook up a trailer, load it and drive the wood to where I needed to stack it.

I love my little Wheel Horse.<figure id="attachment_1589" style="max-width: 300px" class="wp-caption alignnone">

[<img class="size-medium wp-image-1589 " title="ToroWheelhorse244H.Front" src="/media/2011/10/ToroWheelhorse244H.Front_-300x225.jpg" alt="Toro Wheel Horse 244-H" width="300" height="225" />](/media/2011/10/ToroWheelhorse244H.Front_.jpg)<figcaption class="wp-caption-text">1992 Toro Wheel Horse 244-H (22-14OE02)</figcaption></figure> <span id="Engine">

<h1>
  Engine
</h1></span> 

Model: E140V-N/10964B

Serial: B913775714

<span id="Starter">

<h2>
  Starter
</h2></span> 

I have had to replace the starter solenoid on this engine once.

Model:

<span id="Starter_Solenoid">

<h3>
  Starter Solenoid
</h3></span> 

Model:

<span id="Mower_Deck">

<h1>
  Mower Deck
</h1></span> 

Model: 05-38SS01
  
Serial: 1051367

This is a 38&#8243; recycler mower.