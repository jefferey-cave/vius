---
id: 1435
title: 'Annapolis County's Political Incompetence'
date: 2011-09-06T20:50:54+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1435
permalink: /2011/09/06/annapolis-countys-political-incompetence/
categories:
  - Opinions
---
While at an Annapolis County Advisory Comitee meeting, The Wife and I watched councilors argue vehemently about whether to impose bonds on Wind Farm opperators. Councilors also became enraged with members of the public for daring to question the hard work the councilors have done.

Councilor McWade vehemently chastised the public for daring to question the members of council who have worked on this (including the question of whether to impose a bond) for over 2 years&hellip;

One woman asked how the posting of a bond would work. Council's legal representation said they did not know. Council's spokesman informed her, flat out, that nobody present was qualified to answer her question.

You are telling me that after two years of debating this issue, nobody on council has the slightest clue of what they are talking about?

&nbsp;