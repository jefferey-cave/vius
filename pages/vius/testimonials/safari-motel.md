---
title: Safari Motel
date: 2010-06-15T03:11:56+00:00
author: Kavius
layout: page
---

I wanted to comment on the hotel we stayed at last night. It was great.

The Safari Motel in Swift Current, Saskatchewan, is a great motel. The rate
was decent, they are pet friendly, clean, and have some basic amenities.
Honestly, the place was so well taken care of, I was a little surprised I was
paying so little. I really thought we got good value for money.

[gmap lat=</q>50.2990857393369&#8243; lon=</q>-107.78936505317688&#8243; zoom=</q>17&#8243;]
