#!/bin/bash

cd rdflib.js

npm install
npm run build:browser

mkdir -p  ../../assetts/js/libs/
#rm -rf    ../../assetts/js/libs/rdflib.js
#cp src -r ../../assetts/js/libs/rdflib.js
cp dist/*.js* ../../assetts/js/libs/
