---
id: 1405
title: Councillor McWade of Annapolis County
date: 2011-10-08T00:00:31+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1405
permalink: /2011/10/08/councillor-mcwade/
categories:
  - Opinions
---
[Originally Written 2011-09-07]

A couple of days ago, I went to a town meeting involving the Annapolis County Council. What a horrifying group of people!

We have placed these people in a position of power in the hopes that they will be able to protect our communal interests. It is a clear sign of danger when they use speaking techniques in an attempt to control, rather than guide us.

Especially disturbing was Councillor M<sup>c</sup>Wade. I have never seen a more blatantly deceptive, and power hungry individual in my life.<sup id="rf1-1405"><a href="#fn1-1405" title="I&rsquo;m sure I&rsquo;ve seen worse, but they have at least made an attempt to hide it and appear they are looking out for other&rsquo;s interests" rel="footnote">1</a></sup> Without exception, every statement he made was designed to either intimidate or misdirect his audience.

<span id="Appeal_to_Authority">

<h1>
  Appeal to Authority
</h1></span> 

Early on, Mr. M<sup>c</sup>Wade chastised members of the public for questioning whether this was a good idea or not. His reasoning, Annapolis County has hired a Policy Planner (refered to only as <q>Albert</q>) who according to Mr. M<sup>c</sup>Wade is very qualified, has been the president of the Planners Association of America, has done planning for Chicago, and several other large US cities. This guy is so qualified, that if <q>as a private citizen you wanted to hire him, you couldn't afford him</q>.<sup id="rf2-1405"><a href="#fn2-1405" title="At one point &ldquo;Albert&rdquo; indicated that he would only create policy as requested by the council. I would question whether he is worth having. One of the reasons to hire highly qualified, very expensive, experts is to have them tell you when you are suggesting to do something stupid. You want to draw on their experience, experience you lack. Albert indicated that he would not advise council, it was their job to make decisions, he would&nbsp;merely&nbsp;craft the wording of the policy. What is he a highly paid expert at if he won&rsquo;t offer advice of any kind?" rel="footnote">2</a></sup>

Wow! This guy must really know his stuff, and be really important, and a real authority on the matter. Except, it doesn't preclude him from being wrong.

According to Wikipedia,

> because the argument is inductive (which in this sense implies that the truth of the conclusion cannot be guaranteed by the truth of the premises), it also is fallacious to assert that the conclusion must be true. Such an assertion is a non-sequitur; the inductive argument might have probabilistic or statistical merit, but the conclusion does not follow unconditionally in the sense of being logically necessary

In other words, you can only state that the expert is _likely true_, not _absolutely true_.  Just because you are an authority, doesn't mean you are right.

I don't know what brought on this three minute tirade, during which he angrily glared at the attending public, but what ever the case, he was attempting to intimidate them for questioning the councils authority.

<span id="Democratic_Tyrants">

<h1>
  Democratic Tyrants
</h1></span> 

Councillor M<sup>c</sup>Wade made a point of chastising members of the public regularily throughout the meeting, leading to some real jaw dropping statements comeing from him. My personal favourite tirade was regarding the offence he had taken at members of the public questioning his authority as a council member; after all we live in a democracy!

This was possibly the most ridiculous and silly statement McWade made during the time we stayed. M<sup>c</sup>Wade was pointing out that he was **personally** offended that members of public would question the work done by council. To start with, that anyone would take personal offence to a critique of their work is a clear sign of a poor work ethic. It indicates that the individual is more interested in being right, than in doing a good job. This is doubly true for political figures, that a political figure would be surprised at being critiqued is lunacy, that is the very nature of western politics (as opposed to politics in other parts of the world).

Instead, M<sup>c</sup>Wade suggests that the public is not to question its duly elected officials, that they are to do as the council members instruct. After all, we live in a democracy, and <q>if you don't like it, you can go live somewhere else</q>. I find it interesting that Mr. M<sup>c</sup>Wade's method of advocating democracy<sup id="rf3-1405"><a href="#fn3-1405" title="I do need to point out that McWade is actually flawed on two fronts on his view of democracy. We don&rsquo;t actually live in a democracy. By definition, if we lived in a democracy, every issue would need to be voted on. We would not have councillors that represented us, we would be our own representatives. Not only is McWade incorrect in his assessment of what democracy is, if we did actually live in a democracy, his behaviour would be judged even more harshly&hellip; and he would be out of a job." rel="footnote">3</a></sup> involves informing the public that they have no recourse to criticize their government, a trait often upheld as a benefit of living in an western nation. To then inform people they should go live elsewhere if they don't like it&hellip; perhaps Mr. M<sup>c</sup>Wade could be encouraged to sit through some Junior High Social Studies classes.<sup id="rf4-1405"><a href="#fn4-1405" title="I will state it flat-out for Mr. McWade: living under a duly elected tyrant, typically goes poorly." rel="footnote">4</a></sup>

Aristotle defined democracy as <q>Rule by the People</q> ([Politics](http://classics.mit.edu/Aristotle/politics.html "Politics by Aristotle")), and we can see that McWade does not even have an Aristotelian understanding of democracy. McWade instead prefers to side with Aristotle's Teacher, Socrates, and his preference for tyrants.

<span id="Prestigious_Jargon">

<h1>
  Prestigious Jargon
</h1></span> 

During his arguments, Mr. M<sup>c</sup>Wade made a point of attempting to sound sophisticated. My personal favourite was that he mentioned a <q>Socratian Principle of Debate</q>, inferring that his debating adversary had withheld information.

Socratian Principle? Really?

Nobody talks like that unless they have something to hide. In fact, it is one of the classic techniques of obfuscating (or hiding) information: use language that sounds really impressive and that your audience is likely to not understand (Argument By Prestigious Jargon). While it is sometimes necessary to use jargon (words specific to a particular field) to explain a point, it is generally bad discussion technique to do so. A speaker should attempt to tailor his language to be understood by his audience, unless he is intentionally misleading them, and does not want them to understand what he is saying.

<span id="Please_Remove_Him">

<h1>
  Please Remove Him
</h1></span> 

This article is full of argumentative improprieties, but this is an opinion piece, not a formal discussion.

Mr. M<sup>c</sup>Wade can quote all of the <q>Socratian</q> principles he likes, if he continues his studies, he may be able to bring himself up to speed with some of the more modern principles of debate and discussion that have been published since the Birth of Christ (Kantian, etc). Frankly, its clear he hasn't even moved on to realizing that democracy is <q>rule by the people</q>, something noted by one of Socrates students, joining instead with Socrates' preference for tyrants.

I hope the residents of Mr. M<sup>c</sup>Wade's district realize that the emperor has no clothes. While Mr. M<sup>c</sup>Wade is quick to use obfuscating language, it is not because he is intelligent, rather Mr. M<sup>c</sup>Wade is hiding a lack of understanding of the underlying issues, an inability to focus on the task at hand, and anything resembling critical thinking skills.

Residents of District 6, Councilman M<sup>c</sup>Wade is trying to bamboozle you with fancy talk. The key to ensuring the success and continuity of government is trust, and trust in government can only be gained through transparency. His attempts to hide what he is saying, behind confusing language, makes him dangerous; I would encourage you to remove him from office at the soonest opportunity.

\***

Don Lindsay, a paranormal sceptic, has compiled a good listing of various fallacious argument techniques.

<http://www.don-lindsay-archive.org/skeptic/arguments.html>

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1405">
    <p>
      I'm sure I've seen worse, but they have at least made an attempt to hide it and appear they are looking out for other's interests&nbsp;<a href="#rf1-1405" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-1405">
    <p>
      At one point <q>Albert</q> indicated that he would only create policy as requested by the council. I would question whether he is worth having. One of the reasons to hire highly qualified, very expensive, experts is to have them tell you when you are suggesting to do something stupid. You want to draw on their experience, experience you lack. Albert indicated that he would not advise council, it was their job to make decisions, he would merely craft the wording of the policy. What is he a highly paid expert at if he won't offer advice of any kind?&nbsp;<a href="#rf2-1405" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn3-1405">
    <p>
      I do need to point out that M<sup>c</sup>Wade is actually flawed on two fronts on his view of democracy. We don't actually live in a democracy. By definition, if we lived in a democracy, every issue would need to be voted on. We would not have councillors that represented us, we would be our own representatives. Not only is M<sup>c</sup>Wade incorrect in his assessment of what democracy is, if we did actually live in a democracy, his behaviour would be judged even more harshly&hellip; and he would be out of a job.&nbsp;<a href="#rf3-1405" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn4-1405">
    <p>
      I will state it flat-out for Mr. M<sup>c</sup>Wade: <a title="Dionysius I of Syracuse (elected 432 BC)" href="http://en.wikipedia.org/wiki/Dionysius_I_of_Syracuse" target="_blank">living</a> <a title="Benito Mussolini (elected 1922)" href="http://en.wikipedia.org/wiki/Benito_Mussolini" target="_blank">under</a> <a title="Adolf Hitler (elected 1933)" href="http://en.wikipedia.org/wiki/Adolf_Hitler" target="_blank">a</a> <a title="Premiers of The Soviet Union (various from 1920s-1980s)" href="http://en.wikipedia.org/wiki/Premier_of_the_Soviet_Union" target="_blank">duly</a><a title="Josip Broz Tito (elected 1939)" href="http://en.wikipedia.org/wiki/Josip_Broz_Tito#Historical_criticism" target="_blank"> elected</a> <a title="François Duvalier (elected 1957)" href="http://en.wikipedia.org/wiki/Fran%C3%A7ois_Duvalier" target="_blank">tyrant</a>, <a title="Indira Gandhi (elected 1966)" href="http://en.wikipedia.org/wiki/Indira_Gandhi" target="_blank">typically</a> <a title="Robert Mugabe (elected 1980)" href="http://en.wikipedia.org/wiki/Robert_Mugabe" target="_blank">goes</a> <a title="Hugo Chávez (elected 1998)" href="http://en.wikipedia.org/wiki/Hugo_Ch%C3%A1vez" target="_blank">poorly</a>.&nbsp;<a href="#rf4-1405" class="backlink" title="Jump back to footnote 4 in the text.">&#8617;</a>
    </p>
  </li>
</ol>