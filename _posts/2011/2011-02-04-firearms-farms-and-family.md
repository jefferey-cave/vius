---
id: 852
title: Firearms, Farms, and Family
date: 2011-02-04T00:00:03+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=852
permalink: /2011/02/04/firearms-farms-and-family/
categories:
  - Opinions
---

<figure>
 <img src="/media/2011/02/SwissMilitiaman.jpeg" alt="Man with Rifle in Grocery Store (swiss)" width="225" height="300" />
 <figcaption>
While the laws are similar, I don't think this would be culturally acceptable
in Canada.
 </figcaption>
</figure>

I was recently reading an
[article on firearms in Switzerland](http://www.davekopel.com/2a/Foreign/The-Swiss-and-their-Guns.htm "The Swiss and Their Guns")
and was surprised to learn that their firearms laws are very similar to
Canada's. What was so surprising about it is that the public perception of
firearms in the two countries is widely different.

I enjoy firearms and it drives me batty the way people view them as dark and
dangerous things. When  The Wife and I started dating, I wanted to ensure that
The Wife had a minimal amount of training with them and to ensure that she was
reasonably comfortable around something that is somewhat important to me. So
I took her to a firearms safety course, and we got our firearms licenses.
This was the first time The Wife ever handled anything like a firearm and it
took a lot of the mystery out of it for her. Suddenly, they weren't as scary.

This is the perception of firearms that I want in the world. The more I see
people being scared of firearms, the more I feel the need to correct their
perceptions.


# Guns and Gun Crime

So many people we speak with hear the word <q>firearm</q> and instantly think
gang-land shoot out, just like in the movies. This was made really evident to
me the first time I offered to take Pumpkin to the shooting range:
<q>No Way!</q> In her mind she saw them as a tool of the criminal. In her
mind, firearms and criminal were synonymous.

I always like the quote that, <q>An armed society is a polite society</q>,
implying that private ownership of firearms acts as a deterrent to crime. The
Wife is always quick to correct me by pointing out that there is no
correlation between gun legality and violent gun crime.<a href="#f-1" rel="footnote">1</a>

This is most evident looking at the quantity of guns per capita in nations. If
one takes the top 15 countries, with the highest number of guns per capita
(excluding the USA where the numbers are way out of proportion), you find
about a 50/50 split of crappy places, and happy places. What is most
interesting, is you have some of the crappiest places on earth and some of the
happiest. It is actually the two extremes that are present in the top of
[the list](http://www.smallarmssurvey.org/fileadmin/docs/A-Yearbook/2007/en/Small-Arms-Survey-2007-Chapter-02-annexe-4-EN.pdf) (which I copied from Wikipedia).

<table>

<thead>
 <tr>
  <th>Country</th>
  <th>%</th>
  <th>Type of Place</th>
 </tr>
</thead>

<tbody>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Flag_of_Yemen.svg/22px-Flag_of_Yemen.svg.png" alt="" width="22" height="15" /> Yemen</td>
  <td>54.8</td>
  <td>Crappy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Switzerland.svg/20px-Flag_of_Switzerland.svg.png" alt="" width="20" height="20" /> Switzerland</td>
  <td>45.7</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/22px-Flag_of_Finland.svg.png" alt="" width="22" height="13" /> Finland</td>
  <td>45.3</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Flag_of_Serbia.svg/22px-Flag_of_Serbia.svg.png" alt="" width="22" height="15" /> Serbia</td>
  <td>37.8</td>
  <td>Crappy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Cyprus.svg/22px-Flag_of_Cyprus.svg.png" alt="" width="22" height="13" /> Cyprus</td>
  <td>36.4</td>
  <td>Crappy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Flag_of_Saudi_Arabia.svg/22px-Flag_of_Saudi_Arabia.svg.png" alt="" width="22" height="15" /> Saudi Arabia</td>
  <td>25.0</td>
  <td>Crappy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Flag_of_Iraq.svg/22px-Flag_of_Iraq.svg.png" alt="" width="22" height="15" /> Iraq</td>
  <td>34.2</td>
  <td>Crappy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Uruguay.svg/22px-Flag_of_Uruguay.svg.png" alt="" width="22" height="15" /> Uruguay</td>
  <td>31.8</td>
  <td>?</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Flag_of_Sweden.svg/22px-Flag_of_Sweden.svg.png" alt="" width="22" height="14" /> Sweden</td>
  <td>31.6</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Norway.svg/22px-Flag_of_Norway.svg.png" alt="" width="22" height="16" /> Norway</td>
  <td>31.3</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/22px-Flag_of_France.svg.png" alt="" width="22" height="15" /> France</td>
  <td>31.2</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Flag_of_Canada.svg/22px-Flag_of_Canada.svg.png" alt="" width="22" height="11" /> Canada</td>
  <td>30.8</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/22px-Flag_of_Austria.svg.png" alt="" width="22" height="15" /> Austria</td>
  <td>30.4</td>
  <td>Happy</td>
 </tr>
 <tr>
  <td><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/22px-Flag_of_Germany.svg.png" alt="" width="22" height="13" /> Germany</td>
  <td>30.3</td>
  <td>Happy</td>
 </tr>
</tbody>

</table>

Also interesting is to look at the countries with the lowest guns per capita:

<table>

<thead>
 <tr>
  <th>Country</th>
  <th>%</th>
  <th>Type of Place</th>
 </tr>
</thead>

<tbody>
  <tr>
    <td>
      Japan
    </td>

    <td>
      0.6
    </td>

    <td>
      Happy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Korea, North
    </td>

    <td>
      0.6
    </td>

    <td>
      Happy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Rwanda
    </td>

    <td>
      0.6
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Sierra Leone
    </td>

    <td>
      0.6
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Haiti
    </td>

    <td>
      0.6
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Bangladesh
    </td>

    <td>
      0.6
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Fiji
    </td>

    <td>
      0.5
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Eritrea
    </td>

    <td>
      0.5
    </td>

    <td>
      ?
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Indonesia
    </td>

    <td>
      0.5
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Singapore
    </td>

    <td>
      0.5
    </td>

    <td>
      Happy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Ethiopia
    </td>

    <td>
      0.5
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Ghana
    </td>

    <td>
      0.5
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Solomon Islands
    </td>

    <td>
      0.5
    </td>

    <td>
      ?
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      East Timor
    </td>

    <td>
      0.5
    </td>

    <td>
      Crappy
    </td>

    <td>
    </td>
  </tr>

  <tr>
    <td>
      Tunisia
    </td>
    <td>
      0.1
    </td>
    <td>
      Crappy
    </td>
  </tr>
  </tbody>
</table>

I'm not seeing any correlation between <q>Gun Crime</q> and <q>Guns</q>.

Let me break this down a little. If its not the <q>Guns</q> that cause <q>Gun
Crime</q>, what on earth could it be? I could try to hazard a guess&hellip;
Could it be&hellip; oh&hellip; <q>Crime</q>? Nations with a high incidence of
violent behaviour have a high incidence of violent behaviour with guns. But
this isn't a function of the guns being available, this is a situation where
people will be violent with whatever they can get their hands on.<a href="#f-2" rel="footnote">2</a>

One of my favourite images is of riots taking place in South Africa. Some of
the most violent situations I have ever seen. There are plenty of images of
people running around with guns, and shoot-outs between police and rioters,
but there is one image that stands out in my mind. A band of men who had just
finished beating someone to death showed one of the rioters charging with his
bloody carpenter's hammer in hand.<a href="#f-3" rel="footnote">3</a>

Violent people will be violent; non-violent people will not be.

# Gun-Grabbers Scare Me

This leads to the next problem I often face when I tell people I enjoy
shooting: they assume I am a violent person.

<figure>
 <img title="BabesForBiathalon" src="/media/2011/01/BabesForBiathalon.jpeg" alt="Canadian Women's Biathalon Team" width="300" height="188" />
 <figcaption>
Canadian Women's Biathalon Team. Obviously a violent pack of hoodlums carrying
their firearms openly about the city
 </figcaption>
</figure>

The flaw in logic is highlighted by an ex-colleague of mine. He walked in on a
conversation about shotguns where I was discussing the versatility of the
shotgun. We had just finished discussing <q>Bear Bangers</q><a href="#f-4">5</a>

In my co-worker's mind, guns can **only** be used for violence; I enjoy using
firearms; therefore I **must** be a violent person.

This attitude is very common.

I am often reminded of something said by Aleister Crowley during alcohol
prohibition in the United States: <q>The Prohibitionist must always be a person
of no moral character; for he cannot even conceive of the possibility of a man
capable of resisting temptation</q>.<a href="#f-6">6</a>

I sometimes wonder what kind of people these gun prohibitionists are&hellip;

# Home Defense

While I am not a violent person, I do recognize that sometimes violence is
necessary, that is why we have police and armies. Unfortunately (as the
saying goes) when seconds count, the police are only minutes away. This holds
doubly true living in the country where services are farther away; you do need
to become more self-sufficient. It is very important that I learn to and am
equipped to protect myself, and those who depend on me for their care.

Being out on the farm, there are many individuals I need to protect against,
and many individuals that need protecting. While we don't have any yet, The
Wife and I are hoping to be keeping chickens very shortly, and chickens are
pretty close to the bottom of the food chain around here (I think
[rabbits](/about/rabbit-stew/ "Our Pet Rabbit") may be the only
thing lower than them). By undertaking the care of animals, I am undertaking a
responsibility to <q>take care of them</q>, and that includes keeping them
safe from harm. Coyotes, foxes, falcons, and eagles, are all predators we have
seen in the area and they will attack and kill our animals.

Certainly I will take measures to reduce the problem:
[keeping food sources away](/2010/08/halifax-setup-for-a-plague/ "Halifax Setup for a Plague"),
and regular walks to scare the predators off; but all of that can only reduce
the chance of the predators finding the ready source of food that is
<q>chicken</q>. Fox will steal your chickens when they find them, and are
notoriously apathetic towards the law. At some point I am going to have to
confront these thieves and explain to them that the chickens are my property
and are under my protection.

The problem with explaining ownership to predators is they speak Animaleze,
not English. So, like the Horse Whisperer, I am going to have to learn to
speak like the animals do:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gMldIcYHymM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

So animals communicate using violence&hellip;

<p style="text-align: center">
  <a href="http://bearsathallobay.blogspot.com/2010/06/bear-paw-print-and-simyras-hand.html"><img class="size-medium wp-image-873 aligncenter" title="PawAndHand" src="/media/2011/01/bearpaw.jpeg" alt="Bear Paw and Human Hand" width="300" height="225" /></a>
</p>

I think I'm going to need a translator.

# Sport

In the end, the best justification for firearm ownership, and firearm usage is
that they are **fun**!

Some of my fondest memories from my childhood revolve around camping with my dad. We spent a lot of time in the bush together, and a lot of time setting up targets and testing our marksmanship.

I remember spending a whole afternoon and evening out shooting together. Towards the end, we spent what must have been half an hour working on my technique. I would shoot at a pop can and my dad would call out whether I was shooting high, low, left, or right. We worked on my shooting for a long time, critiquing my stance, trigger pull, and breathing, but I always missed the can. Finally, I got frustrated, grabbed the binoculars out of his hand, and looked for myself. The pop can was a chewed up mess, every shot had hit its mark.

<q>Oh, I thought you were shooting at the one on the right</q>, was all he said&hellip;

Jerk.

<hr />

All tools are inanimate objects that have no sense of right or wrong. In the end, its what the person holding the tool does with it that defines morality. For me, firearms are about afternoons on sunny days playing and joking with family and friends. Somehow I wish I could make those who see them as tools for violence, and only violence, see them the way I do.

All I can think to do is to keep talking, and keep smiling, and hope that eventually people start associating firearms with a really friendly guy. My fear is that they'll start associating my face with the gun they fear.

Maybe innocent young girls will offer a better distraction from the gun than my ugly mug&hellip;

![YouTube video of 7 year-old biathalete](http://www.youtube.com/watch?v=wDKpogxS_Cc "removed with YouTube firearms ban")

# Footnotes

<aside>
 <ol>
  <li id="fn1-852">
    <p>
      I have to call it <q>violent</q> gun crime because places that make possession criminal the stats will be skewed. For this discussion we are only interested in people that have harmful intent.&nbsp;<a href="#rf1-852" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn2-852">
    <p>
      This is not an argument for making guns illegal. Making guns <q>Criminal</q> will not prevent <q>Criminals</q> from obtaining them. That is inherint in the definition of someone that is Criminal by intent. Criminals don't respect the law, that's what makes them criminals, therefore it is less likely they will obey Gun Laws in particular.&nbsp;<a href="#rf2-852" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn3-852">
    <p>
      There was another story in a major newspaper that discussed rioters burning a house with the family inside. Everyone of the mob had rifles, but when one person tried to make a run for it, they threw a rock at him, and physically dragged him back, not a shot was fired.&nbsp;<a href="#rf3-852" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn4-852">
    <p>
      Bear Bangers are basically shotgun shells with a fire-cracker in them. When fired the projectile flies out and after a pre-set time, explode with a really loud <q>BANG</q>. They are used to scare wildlife away from areas where they don't belong; like birds around the airport, or bears near camp-grounds. The funniest story I ever heard was from a Fish and Wildlife Officer who was tasked with scaring a bear away from a camp-ground. He saw the bear, loaded a 150 foot bear banger, and fired at the bear&hellip; which (unfortunately) was only 100 feet away. The banger went 50 feet past the bear and then BANG! Suddenly the Warden had a very scared and angry bear coming directly at him. He now puts two shells in: one banger, followed by something <q>just in case</q>.&nbsp;<a href="#rf4-852" class="backlink" title="Jump back to footnote 4 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn5-852">
    <p>
      Shotguns, due to their shell configuration, lack of rifling, and large bore size, can fire a large number of projectiles. In the end, a shotgun is not so much a weapon as a high speed delivery device. What you deliver is up to you.&nbsp;<a href="#rf5-852" class="backlink" title="Jump back to footnote 5 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn6-852">
    <p>
      I am a little fuzzy on this quote. I remember it from a Biography on Tesla that I own, but that book is in a box.  The Wife looked it up and found it attributed to <q>Absinthe: The Green Goddess</q> by Crowley, which I have read. I still stand by Tesla on this one.&nbsp;<a href="#rf6-852" class="backlink" title="Jump back to footnote 6 in the text.">&#8617;</a>
    </p>
  </li>
</ol>
</aside>
