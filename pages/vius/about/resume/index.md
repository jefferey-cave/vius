---
id: 2078
title: Resume
date: 2013-01-07T17:55:46+00:00
author: Kavius
layout: page
guid: http://vius.plaidsheep.ca/?page_id=2078
---
[<img alt="View Kavius Cave's profile on LinkedIn" src="http://www.linkedin.com/img/webpromo/btn_viewmy_160x33.png" width="160" height="33" border="0" />](https://www.linkedin.com/in/Kaviusereycave)

If you somehow came across this page, and wish to see my resume, please view my LinkedIn profile. It is my most up to date version of my resume.

I do keep a printable copy of my resume here for my own use. It may, or may not be up to date: <a href="http://vius.plaidsheep.ca/resume/resume-r/" rel="attachment wp-att-2079">Resume</a>

&nbsp;

&nbsp;