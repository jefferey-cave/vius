---
id: 337
title: First Day in the Lakes
date: 2010-06-19T02:18:47+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=337
permalink: /2010/06/19/first-day-in-the-lakes/
categories:
  - Moving
---

# June 17, 2010

Chimutisk was having a rough night, and started to fuss in his cage. It was almost time to get up so  The Wife pulled him up on the bed. He settled right down and curled up between us on the bed. It was pretty cute. All we can think of is that he has been thinking we are going to leave him behind so he freaks out. Putting him on the bed let him know we are still around, and still looking out for him.

When we left the room, we heard an odd bird song. Has anyone noticed that there is a bird in Canada that sings the opening lines to _Oh Canada_? I swear I heard a bird singing <q>Oh Canada</q>,  The Wife can back me up on this. I haven't figured out the type of bird, but I'm looking.

It was a really foggy morning when we left, being near a large body of water means more fog. The fog hugged the road for most of the morning and made for a nerve racking drive. It wasn't bad but it was on strange roads, narrow roads, and pulling a trailer. In the end, it made for a beautiful drive.

While we were driving along the coast, we came across the Ontario provincial parks. We wanted to pull off to take  a bit of a break. Did you know Ontario requires you to have a pass to stop in the Provincial Parks? I've only ever seen that with National Parks, every Provincial Park I have ever been to from BC through Manitoba has been free to use. It was weird and definitely caught me off guard.

## Swimming in Lake Superior

Today we chose to have an easier day. We both needed a bit of a break from driving. First we stopped at Pancake Bay to take a walk on the beach.  The Wife's Mom told us about Pancake bay, so we had to stop and see why she loves it so. Pancake Bay is like a tropical paradise; you can almost mistake the birch for palm trees. As there is only a campground at the Bay, we drove to the next Bay; Batchawana Bay and rented a small cabin. We spent part of the afternoon swimming in Lake Superior. The weather was hot and the lake clear and cold. By the time we went back to the cabin we were refreshed and happily tired (and possible a little sunburned).

While we were swimming a loon swam by with her babies. It was hilarious to watch them speed up to catch up with her, and then climb up on her back. She would have none of it though and dove down.  The Wife has never seen a loon before, so to see a mother and babies, really close up, was pretty cool.

We went to bed in our cabin only to be waken by a thunderstorm overnight. It was pretty wild, but for the most part it boomed and we went back to bed.

# June 18, 2010

I miss having breakfast out with  The Wife, we used to do it every Sunday. We had breakfast this morning at the Voyageur Lodge. We ordered the what amounted to our traditional Sunday breakfast and were shocked that we had to use both hands to hold the plates: there was a lot of food. It was really good too.

We are very surprised by how sparsely populated Ontario is. For a place that is so influential to Canada (politically and economically), and a place that is so heavily populated, it must all be in one place<sup id="rf1-337"><a href="#fn1-337" title="The comparison I made today was that if Ontario is a stomach, everyone seems to live in the apendix" rel="footnote">1</a></sup>. I'm used to driving around Alberta, where no matter where you go, there are plenty of houses and roads. Ontario doesn't have many people. The towns we do come across are all towns with a single gas station, how can such a major province be so sparsely populated.

Perhaps it has something to do with it being God's country. I know this must be God's country since he paid special attention to Ontario. I know this because Ontario is completely made of rock, and I think I know why. When God was creating the world, there was a wobble so he put a big rock in the north (what is now Ontario) to balance it out.  The Wife figures that this is why the earth is slightly squished from North to South, God squished it a little while squishing the rock in. Sure there is a little wobble still, but good enough.

Today's drive was, by far, the most stressful so far this trip (although I'm sure it will come second to driving in Montreal!). When we checked into our motel tonight, they kindly reminded us that it was  Friday;  no wonder there was lots of traffic. It's easy to lose track of time when you're on he road.

Tonight's stay is at the Lincoln Motel in Sturgeon Falls. It's right on the highway, it's clean and it's quite nice. Sturgeon falls is a small town about 50 km west of North Bay. Tomorrow we hope to get to Ottawa and then through Montreal and Quebec City on Sunday.

<aside>
<ol class="footnotes">
  <li id="fn1-337">
    <p>
      The comparison I made today was that if Ontario is a stomach, everyone seems to live in the apendix <a href="#rf1-337" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>
</aside>
