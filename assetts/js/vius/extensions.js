(function(){

'use strict';

/**
 * Create a 'cloning' utility
 *
 * Based on JSON stringify and parse
 */
if(!JSON.clone){
	JSON.clone = (obj)=>{
		return JSON.parse(JSON.stringify(obj));
	};
}


/**
 * Create a 'merge' utility
 *
 * Based on JSON stringify and parse
 */
if(!JSON.merge){
	let merger = (...objs)=>{
		return objs.reduce((a,o)=>{
			o = JSON.clone(o);
			Object.entries(o).forEach(pair=>{
				a[pair[0]] = pair[1];
				if(typeof a[pair[0]] === 'undefined'){
					delete a[pair[0]];
				}
			});
			return a;
		},{});
	};
	JSON.merge = merger;
}


})();
