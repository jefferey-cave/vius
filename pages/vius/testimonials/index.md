---
id: 10
title: Testimonials
date: 2010-05-11T01:01:23+00:00
author: Kavius
layout: page
guid: http://www.vius.ca/?page_id=10
---
We have received a lot of bad service over the the years. A lot of really bad service.

Really, I don't think we would get away with telling you who they are (some are high priced lawyers), but after some thought, that's not what we should be trying to do. What we should be trying to do is reward those who do really good work.

Markets are pretty ruthless. Incompetent people are not going to get repeat business, competent people will. This equates to succeeding or failing in the market place. This site is all about rewarding those that we feel are extremely good at what they do. We hope these companies will succeed and strip all the business from the bad companies.

None of the people listed in these pages have paid us a dime to get in here. We have gladly paid the regular price for their services. We have not been compensated for these testimonials in any way except to have received exceptional service.

We encourage the companies that have received testimonials from us to publish the testimonials on their own websites, and to link to these pages as proof of what we have said.

  * <a href="http://www.vius.ca/?page_id=12" target="_self">Ron Clappison (Realtor)</a>
  * <a href="http://www.vius.ca/?page_id=18" target="_self">DRM Small Haul</a>