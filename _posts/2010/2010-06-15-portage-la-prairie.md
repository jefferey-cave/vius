---
id: 318
title: Portage La Prairie
date: 2010-06-15T03:11:56+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=318
permalink: /2010/06/15/portage-la-prairie/
categories:
  - Uncategorized
---
<span id="Safari_Motel">

<h2>
  Safari Motel
</h2></span> 

I wanted to comment on the hotel we stayed at last night. It was great.

The Safari Motel in Swift Current, Saskatchewan, is a great motel. The rate was decent, they are pet friendly, clean, and have some basic amenities. Honestly, the place was so well taken care of, I was a little surprised I was paying so little. I really thought we got good value for money.

[gmap lat=</q>50.2990857393369&#8243; lon=</q>-107.78936505317688&#8243; zoom=</q>17&#8243;]

<span id="Chimutisk_Wastes_Time">

<h2>
  Chimutisk Wastes Time
</h2></span> 

We had a long day of driving today. Our delay this morning made it a little worse.

This morning we ran out (or were close to running out) of Chimutisk's medication. We packed up, but then had to wait for an hour for the veterinarian in Calgary to open. It turns out that she was able to fax our prescription to the local Shoppers Drug Mart. Unfortunately, that was an extra hour of waiting.

We finally hit the road about 10am.

<span id="On_the_Road">

<h2>
  On the Road
</h2></span> 

There were two vehicles that were stalking us on the trip. One was a trailer full of pigs, we passed it about 3 times. The other was a sports car that with the license plate <q>BYE BYE</q>, he passed us 5 times, but we never figured out where he was coming from or going to. Weird!

Chimutisk travelled well. We made a point of stopping for a pee break every two hours (alright, so I needed the smoke), but this wore Chimutisk right out. He was so worn out by the second stop that he flat out refused to come out of the kennel. We make him come out, but he sure is well behaved in the motel.

<span id="Westgate_Motel">

<h2>
  Westgate Motel
</h2></span> 

Now we are spending the night at the Westgate Motel in Portage La Prairie, Manitoba.

[gmap lat=</q>49.97281889093094&#8243; lon=</q>-98.27581536763319&#8243; zoom=</q>17&#8243;]

Well, we are bagged. We've decided we drove to far today. The amount of driving was alright but the two hours wasted waiting for meds was something we can do without.