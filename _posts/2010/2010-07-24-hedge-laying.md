---
id: 511
title: Hedge Laying
date: 2010-07-24T21:36:16+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=511
permalink: /2010/07/24/hedge-laying/
categories:
  - Living
  - Uncategorized
---

I have always thought hedges are dumb. They serve little purpose, look like
crap because people don't take care of them, and get trashed when kids cut
through them. Apparently that's only because I live in North America.

About two years ago I read a book, by an Englishman, about self-sufficiency.
In this book he made a very brief reference to hedge maintenance, and I saw
something that made me take notice: he was cutting through the branches and
pushing them over. By doing this he was placing the branches close together,
so that as they continued to grow, they would interweave themselves, making
an impenetrable wall. I had a vision, of [gooseberry bushes](http://topveg.com/2008/06/when-to-pick-gooseberries/thorns-gooseberry-invicta/),
along the top of the retaining wall. I could just see the local kids trying to
hop the thorny hedge&hellip; exactly once. After their friends got a good look
at how messed up they are, I wouldn't have to worry about kids jumping my
fence.

I never have been able to find the reference since.

For two years, I have been trying to find instructions on how to undertake this process. The idea of creating an impenetrable fence, made from living and fruit bearing material, fascinates me. That the fence actually regenerates over time is part of that philosophy of being a caretaker of nature, that I so like. The fence is a living part of the property, it acts as a barrier (as all fences do), but is part of the living part of your land.

Apparently the process is called <a href="http://en.wikipedia.org/wiki/Hedge_laying" target="_blank">Hedge Laying</a>.

Apparently, this is very common in the UK, but not so much in North America<sup id="rf1-511"><a href="#fn1-511" title="it almost became a lost skill in England after the 1950&rsquo;s, but it is being practised there again" rel="footnote">1</a></sup>. I'm not even sure that anyone remembers that this was ever done in North America. Some of the properties we have looked at have the appearance of once having a hedge, but the hedges have just been allowed to grow wild. It makes sense that the original English settlers in Nova Scotia (1600's) would have planted hedges, at the time they were an integral part of live stock management (in the past, you would plant your fence, not build it). Having said that, I have never seen a layed hedge, and I don't think there is even the knowledge that such a thing could be done here.<sup id="rf2-511"><a href="#fn2-511" title="It&rsquo;s driving me nuts, looking at a 150 year old house, with a bunch of wild scrub around its perimeter, but all of the &ldquo;wild scrub&rdquo; is in a perfectly straight line and of the same species. It&rsquo;s obvious that 150 years ago, someone planted a line of trees with the intent of them being a fence, but over the years people have stopped laying the hedge, so it just goes feral." rel="footnote">2</a></sup>

When I get to our new property, I think I will try my hand at laying. Optimally, I would <a href="http://www.hedgelaying.org.uk/training.htm" target="_blank">take a class</a>, but since this is lost knowledge on this side of the world, and I'm not going all the way to England just for that, I will have to try to learn the hard way. Maybe I can talk one of the experts in the UK into helping me by critiquing my work from picture and video. Hmmm&hellip;

I'll start with some land that needs clearing, and try cutting some of the smaller trees in the appropriate way for Laying. It doesn't matter if I mess up, I am just going to pull the tree anyway. As I get better, I will start walking the perimeter of the property and begin laying sections of the scrub that is currently there. Over time, I hope to be able to show a well-layed, goat-proof<sup id="rf3-511"><a href="#fn3-511" title="Yes, goat proof. I&rsquo;ve seen sheep proof, and bull proof, styles, but I haven&rsquo;t seen a style that is specialised for goats. Should be interesting." rel="footnote">3</a></sup>, hedge.

Well&hellip; I have my book, my axe, and my saw&hellip; now I just need the
property.


A hedge before laying
<figure>
 <a href="http://www.shropshirehedgelaying.co.uk/before_after.php">
  <img src="/media/2010/07/hedgelaying_tibberton_before.jpg" alt="A hedge before laying" width="300" height="180" />
 </a>
 <figcaption>A hedge before laying</figcaption>
</figure>
<figure>
 <a href="http://www.shropshirehedgelaying.co.uk/before_after.php">
  <img src="/media/2010/07/hedgelaying_tibberton.jpg" alt="Hedge After being &quot;Layed&quot;" width="300" height="149" />
 </a>
 <figcaption>Hedge After being "Layed"</figcaption>
</figure>

The pictures were <q>borrowed</q>, without permission, from
[Karl Liebscher](http://www.shropshirehedgelaying.co.uk/) a professional Hedge
Layer.

<aside>
 <ol>
  <li id="f-1">
   <p>
it almost became a lost skill in England after the 1950's, but it is being
practised there again
<a href="#rf1-511" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
   </p>
  </li>
  <li id="f-2">
   <p>
It's driving me nuts, looking at a 150 year old house, with a bunch of wild
scrub around its perimeter, but all of the <q>wild scrub</q> is in a perfectly
straight line and of the same species. It's obvious that 150 years ago,
someone planted a line of trees with the intent of them being a fence, but
over the years people have stopped laying the hedge, so it just goes feral.
<a href="#rf2-511" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
   </p>
  </li>
  <li id="f-3">
   <p>
Yes, goat proof. I've seen sheep proof, and bull proof, styles, but I haven't
seen a style that is specialised for goats. Should be interesting.
<a href="#rf3-511" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
   </p>
  </li>
 </ol>
</aside>