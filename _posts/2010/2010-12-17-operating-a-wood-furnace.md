---
id: 685
title: Operating a Wood Furnace
date: 2010-12-17T00:00:25+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=685
permalink: /2010/12/17/operating-a-wood-furnace/
categories:
  - Living
---
Well, its time to start making confessions about how little I know about what I am doing. Unfortunately, I am quickly finding, experienced people don't think to mention important details; those little details that are so obvious that you don't even think about them. Those little details that make you look stupid when you are a neophyte and don't know them.

This past weekend we fired up our wood furnace for the first time ever. After numerous repairs to it, and a quick tutorial from one of the furnace guys, it was operating correctly, but after 3 hours of burning, the house had only climbed to 15°C. After all of the hype about what a wonderful heat wood supplied, this was not what I expected. I could not, for the life of me, figure out what was wrong. The fire was burning, the blower was pushing air into the combustion chamber, but the forced air fan was not coming on. So S finally badgered me into swallowing my pride, and calling our furnace guy to ask what the problem was.

It turns out we weren't producing enough heat. It just goes to show what a neophyte I am with wood.

<span>A solid fuel furnace does not work the same way as a gas furnace. There is a key difference: when operating a wood furnace, you have a couple of stages that work independently of one another.</span>

The first stage is your combustion chamber. This is pretty straight forward, it is where the fire goes. When the temperature in the house drops below a the specified temperature, your thermostat activates the blower, injecting air into the combustion chamber and making the fire burn hotter.

As the fire burns, it heats the heat exchanger. The exchanger is a chamber above the furnace that simply holds air. When the air gets hot enough a fan is activated causing all of that nice hot air to be forced into the house (thus the name <q>Forced Air</q>). The forced air fan is controlled by another thermostat on the side of the chamber itself. This is the part I didn't know about.

When the forced air comes on, it pushes all of the hot air into the house, warming it. Once the house's temperature goes above the amount specified on the thermostat, the thermostat turns to the blower off at the combustion chamber. The fire slowly starves for oxygen, getting cooler, causing the heat exchanger to not heat so quickly, causing its fan to turn off.

In our case, there were two problems working against us: the heat exchange thermostat was set to a ridiculously high temperature, and our wood is wet.

<span>The Heat Exchanger's thermostat was set for 200°C (as high as it goes), I set it for 100°C (as low as it goes) and all of a sudden, the Forced Air was coming on regularly. After asking around, it seems the right temperature is around 150°C, but I actually have it set for about 125°-130° to compensate for the wet wood.</span>

Unfortunately, I can't do anything about the wet wood, except for wait for it to dry. Our furnace guy recommends putting a dehumidifier in the storage shed, saying that you will be able to actually watch cracks form in the ends of the wood. In the future, it is probably better we just not accept wet wood in the first place (unfortunately, we didn't know how to judge dry wood).

As it stands, we won't go cold. We ordered our wood from two different sources (3 cords each), just in case something like this happened. The other batch of wood, burns well and produces enough heat.

Hopefully, some other poor sap will find this quick explanation useful and not have to look as stupid as I did.