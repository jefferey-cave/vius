---
id: 1938
title: Genetic Modification in the News
date: 2012-06-30T10:52:56+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1938
permalink: /2012/06/30/genetic-modification-in-the-news/
categories:
  - Homesteading
---
> Preliminary tests revealed the Tifton 85 grass, which has been here for years, had suddenly started producing cyanide gas, poisoning the cattle.
   
> &#8211; [GM Grass Linked to Texas Cattle Deaths](http://www.cbsnews.com/8301-201_162-57459357/gm-grass-linked-to-texas-cattle-deaths/) 

I am generally a proponent for Genetically Modified produce. I believe that modern technology has been responsible for a boom in population (modern medicine) and technology is our only hope for producing sufficient food to feed those masses. Having said that, there are risks associated with this technology. In this case, a product with 15 years of field usage, suddenly started producing cyanide.

I continue to stand by GM research. This scenario does not require GM to have occured. One of the grasses from which genetics were taken is a known cyanide producer. A careful program of breeding would have produced a product that delivered all of the benefits of a hardy grass without the side effect of cyanide production. GM technology was simply used to jump-start the process. That the plant would revert to its old cyanide producing ways could have happened no matter how the plant was produced.

This is not a problem with GM. This plant would have been produced through selective breeding if GM technology were not available. This appears to be a two-fold problem: an unexpected throw-back to an earlier generation of plant, triggered by inappropriate field rotation.

Please take the time to read a brief abstract describing the production of cyanide (cyanogenesis) in African grasses due to overgrazing: [Cyanogenesis in savanna grasses](http://www.tropical-biology.org/admin/documents/pdf_files/Kenyaabstracts/KENYA-PLANT%20ECOLOGY.pdf)