---
id: 1270
title: Don't Call Us, We'll Call You
date: 2011-08-26T09:31:30+00:00
author: Kavius
layout: post
categories:
  - Musings

---

It has been a while since I have last written, I'm not sure I still have the knack of it, but here we go&hellip;

In my last post, I discussed that some of my writing seemed to have gotten me into trouble with people I had met. People were misunderstanding what I was saying. What I was really saying is I seemed to be getting ostracized for some reason, and all I could think of was the fact that Annapolis Royal is a fairly Anti-Firearms community (in the way that is typical of urban areas) and I had written a Pro-Firearms article. I'm starting to think I was mistaken.

<!--more-->

* * *

A few months ago, The Wife and I were building raised garden beds and required top-soil to be delivered. We contacted **every** supplier listed in the phone book, and a few that were not; not one of them answered their phone. We left messages with everyone of them, and not one of those calls were returned. We did a second round of phone calls/messages; not one call was returned, but we did manage to get a hold of two people. They said they couldn't get around to us right away, and they would call us when they were available.

Don't call us, we'll call you.

This is not the only group of vendors we have heard this from: Chimney Installers, Nuisance Wildlife Operators, Mechanics and Firewood Suppliers have all given us the same run around. I have heard several people complain that its hard to make a go of it here, and that there needs to be more government assistance for the unemployed. Unfortunately, no amount of assistance is going to help businesses get off the ground, if they won't take jobs.

* * *

A month ago, I realized that I didn't have anything to do with my days anymore, so I decided I had better start applying for jobs. It didn't take me long to see that what applies to customers applies to dealing with staff as well.

Years ago, while trying to get my Bachelors in Nursing, I was working as a Practical Nurse at a Nursing home. Being unemployed here, working at a Nursing Home seemed like that may be a good job for me to take on as they are always looking for people willing to work part-time.

I applied with three companies in the area that offer those services, one of them sent me an email asking if I could meet them for an interview.

<q>Certainly</q>, I replied, excited to have had a response. <q>When and where?</q>

<q>Do you have a car?</q>

<q>Yes, I do. When would you like to meet?</q>

That was the last we spoke.

I have since sent two more emails to the individual I was talking to, and neither of them have been acknowledged.

Don't call us, we'll call you.

Is it really any surprise when I hear local businessmen complaining about the poor work ethic, or how hard it is to find good employees. Well of course there aren't any good employees, there is no example of professionalism from the business owners and managers, why would their employees be any different.

* * *

Funnily, I have heard the local business owners complaining about the quality of staff. When we first came, we knew we were going to have to reach out to people and make ourselves known, so we made some acquaintances, and started to attend parties. Everyone was very excited to have a new couple in town: we were invited to various dinner parties, and formal events, and plays, and gallery openings, et cetera. There was plenty to do, but we were finding it really hard to connect with individuals within the crowd. We were meeting plenty of people, but not making any friends; so we started inviting people over to our house for less formal, more personal gatherings.

Nobody came.

The excuses were polite, plentiful, and consistent: they would simply state they were busy and would get back to us at some later date. They never called, and at later dates we would hear that they had been bored all week, and all of their friends had  been out of town, and if only there had been some activity for them to do. Alternately, we would learn they **had** been busy all week: busy visiting the historic centres and theme parks and museums and swimming pools.

Oh&hellip; I get it&hellip;

Don't call us, we'll call you.

* * *

I have come to realise that I never offended anyone with my pro-firearms article. I doubt anyone actually read it before they started to ignore me. Frankly, I don't think they were actually paying attention at all. It wasn't that anyone was offended, its just the Annapolis Valley way&hellip;

Don't call us, we'll call you.

&nbsp;