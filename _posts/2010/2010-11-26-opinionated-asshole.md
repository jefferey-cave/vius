---
id: 639
title: Opinionated Asshole
date: 2010-11-26T08:00:46+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=639
permalink: /2010/11/26/opinionated-asshole/
categories:
  - Opinions
---
It has been a long time since I have made any posts.

<p dir="ltr">
  After a while of maintaining the blog, I realized I did not like the direction the posts have been taking since we stopped travelling. They have been getting angrier in tone. The angry tone itself isn't a problem, but I am concerned that it gives people the wrong impression. I am not an angry, sour, surly person. I'm actually a pretty happy person.
</p>

<p dir="ltr">
  The problem appears to be that I am an opinionated ass-hole. Again, this in itself is not a problem, this means I have plenty of things to say. The problem arises in that I have <strong><em>too much</em></strong> to say. Since I have an opinion on everything, I realize that my posts were falling all over the place, or at least seem to be. Some of the things I was writing about are of interest to others, some of the things aren't. I needed to take a step back and reassess what has worked and what has not.
</p>

<p dir="ltr">
  After much discussion between The Wife and I, we determined that I really need to keep the posts focussed. While all of the posts have actually had a common thread to them, it has not been obvious. Without a common thread, they have been turning into unfocussed rants. Rather than unfocussed rants, from now on I am going to keep the posts focussed. The Wife and I are making this change in our lives in an effort to take control of our lives. To do this, we will need to become more <em>Self-Reliant</em>.
</p>

<p dir="ltr">
  One part of Self-Reliance is to have a long view of objectives. Rather than focussing on the immediate, we need to be coming up with solutions to problems we are going to be experiencing years from now. Not only do we want to work on issues we will be experiencing, we are trying to think generationally, we are trying to leave something for Pumpkin, and Pumpkin's children. So with a focus on the long-term we need to ensure that the land we are working on will be <em>Self-Sustaining</em>.
</p>

<p dir="ltr">
  I am a software developer by trade, in particular I focus on modelling business practices into software. This requires a very detailed view of the world. Remember where I said that I have lots to say. That is another problem: I tend to be long winded. While I think this makes for an entertaining post, it takes time to write that much. Part of being Self-Reliant, is doing the work for yourself; if I am writing, I am not working on the home and property. From now on, I am shortening the articles. I am shooting for 250 words or less, but at the same time I find there isn't enough substance with that little. Let's start with 500 words and go from there. I imagine this will take some practice and work, but hopefully (in the long run) this will allow me to write more often and cover more subjects.
</p>

<p dir="ltr">
  One of my favourite series of books was The Sword of Truth by Terry Goodkind. The hero of the series (Richard Cypher), is always telling people to focus on the solution, not the problem. My personal take is slightly different, to me a once a problem is clearly defined, the solution presents itself. Either you look at it, the end result is the same: seeking solutions to the problems life throws at us is the key difference between being a victim of circumstance, and not; of living with intent, and living by default. From now on, articles will focus on the actions being taken to correct the problem, rather than the problem itself. This will hopefully make this a more useful and informative Journal.
</p>

<p dir="ltr">
  When I produce anything, it is absolutely necessary to keep the audience in mind. This ensures that I refine what I am doing for its practical use. I enjoy producing this journal, I am hopeful that others will find it as enjoyable as I do. From now on all posts will focus on the subjects of Self-Reliant living, Self-Sustaining production, and our experiences with trying to achieve these things. Posts will be shorter and more focussed, this will hopefully allow for more interesting and informative writing in the long run. Finally, I am going to focus on action, not the situations that have caused the action. Please feel free to call me on it if I stray: leave a comment and tell me off, I don't mind. In general, I hope this makes for a more enjoyable experience both for myself, and the reader.
</p>