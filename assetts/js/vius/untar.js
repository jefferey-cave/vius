importScripts('/assetts/js/libs/TarFileExplorer/ByteStream.js');

/*
global CustomEvent
global EventTarget
global TextDecoder
*/


class untar extends EventTarget{
	constructor(){
		super();

		this.buffer = new Uint8Array(untar.ChunkSize);
		this.buffer.pos = 0;
		this.numberOfConsecutiveZeroChunks = 0;
		this.file = null;

		this.pushed = 0;
	}

	static get ChunkSize(){
		return 512;
	}

	get ChunkSize(){
		return untar.ChunkSize;
	}

	finalize(){
		this.isFinalized = true;
		//this.push();
		let event = new CustomEvent('done');
		this.dispatchEvent(event);
	}

	push(bytes=new Uint8Array(0)){
		/*
		for(let b=0; b<bytes.length; b++){
			this.buffer.push(bytes[b]);
		}
		while(this.buffer.length+bytes.length >= untar.ChunkSize || this.isFinalized){
			let chunk = new Uint8Array(untar.ChunkSize);
			this.buffer = chunk.slice(untar.ChunkSize);
			chunk.length = untar.ChunkSize;
			this.processChunk(chunk);
		}
		*/
		// we need to copy all of the data out of the inbound array, but we
		// want to process it as quickly as possible to avoid keeping any
		// in memory.
		//bytes = new Uint8Array(bytes);
		let size = 0;
		//console.log('----- New Byte Chunk - ' + (this.pushed++) + ' -----');
		for(let b=size; b<bytes.length && !this.isFinalized; b+=size){
			//if(this.pushed > 11 && b > 8700){
			//	console.warn('this is the start of trouble');
			//}
			//console.log('<b> of <bytes>'.replace(/<b>/g,b).replace(/<bytes>/g,bytes.length));
			// we are building a chunk of data for processing. Figure out
			// how much space we can copy into.
			let buffRoom = this.buffer.length - this.buffer.pos;
			let inbound = bytes.length - b;
			size = Math.min(buffRoom, inbound);
			let segment = new Uint8Array(bytes.buffer,b,size);
			this.buffer.set(segment, this.buffer.pos);
			this.buffer.pos += size;

			// have we created a full chunk?
			//console.log(this.buffer.pos + ' === ' + this.buffer.length);
			if(this.buffer.pos === this.buffer.length){
				let valid = this.processChunk(this.buffer);
				this.buffer.pos = 0;
				//console.log('file:' + this.file);
				//if(this.file === null){
				//	console.warn("This should not happen");
				//}
				if(valid && 0 >= this.file.header.remaining){
					delete this.file.header.remaining;
					this.file.content = new Blob(this.file.content);
					let event = new CustomEvent('file',{detail:this.file});
					this.dispatchEvent(event);
					this.file = null;
				}
			}
		}
	}

	processChunk(chunk){
		let areAllBytesInChunkZeroes = true;
		for (let b = 0; b < chunk.length; b++){
			if (chunk[b] != 0){
				areAllBytesInChunkZeroes = false;
				break;
			}
		}

		if (areAllBytesInChunkZeroes){
			this.numberOfConsecutiveZeroChunks++;
			if (this.numberOfConsecutiveZeroChunks === 2){
				this.finalize();
			}
			return false;
		}

		this.numberOfConsecutiveZeroChunks = 0;
		if(this.file){
			this.content(chunk);
		}
		else{
			this.file = {
				header: this.header(chunk),
				content:null,
			};
			let event = new CustomEvent('filestart',{file:this.file});
			this.dispatchEvent(event);

			this.file.header.remaining = this.file.header.size;
			//console.log(this.file.header.name);
			this.file.content = [];
		}
		return true;
	}

	header(bytes){
		let reader = new ByteStream(bytes);

		let rtn = {
			name:              reader.readString(100).trim(),
			mode:              reader.readString(8),
			user:              reader.readString(8),
			group:             reader.readString(8),
			size:              reader.readString(12),
			time:              reader.readString(12),
			checksum:          reader.readString(8),
			type:              reader.readString(1),
			nameOfLinkedFile:  reader.readString(100),
			uStarIndicator:    reader.readString(6),
			uStarVersion:      reader.readString(2),
			userNameOfOwner:   reader.readString(32),
			groupNameOfOwner:  reader.readString(32),
			deviceNumberMajor: reader.readString(8),
			deviceNumberMinor: reader.readString(8),
			filenamePrefix:    reader.readString(155)
		};

		rtn.size = Number.parseInt(rtn.size, 8);
		rtn.checksum = Number.parseInt(rtn.checksum, 8);

		rtn.time = Number.parseInt(rtn.time,8) * 1000;
		rtn.time = new Date(rtn.time);

		rtn.type = parseInt(rtn.type,8);
		rtn.type = {
			type: rtn.type,
			name: untar.Types[rtn.type]
		};

		return rtn;
	}

	content(chunk){
		let size = this.ChunkSize;
		if(this.file.header.remaining < size){
			size = this.file.header.remaining;
		}
		this.file.header.remaining -= this.ChunkSize;
		this.file.content.push(chunk.slice(0,size));

	}
}

untar.Types = [
		"Normal",
		"Hard Link",
		"Symbolic Link",
		"Character Special",
		"Block Special",
		"Directory",
		"FIFO",
		"Contiguous File"
	];

untar.Types
	.forEach((type,i,types)=>{
		let key = type.replace(/ /g,'');
		types[key] = i;
	})
	;
