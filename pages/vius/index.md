---
id: 158
title: Vius
date: 2010-06-02T21:57:37+00:00
author: Kavius
layout: page
---

This site has been around for a couple of years (1998), and it has changed 
what it is a couple of times within its life. 

Currently this site is an archive of various things I have written over the 
years. Feel free to comment, I'm not ashamed.

## Updates

Updates are likely not going to occur in the current politicaly climate 
(2015 - 2017). Recent events in the media, as well as statements made by 
employers and collegues, have made it clear that political disention are 
grounds for dismissal. 

# Recent Journal Entries

{% for post in site.posts limit:10 %}
{{ post.title }} {{ post.date | date: "%B %d, %Y" }}{% if post.excerpt %} {{ post.excerpt | remove: '\[ ... \]' | remove: '\( ... \)' | markdownify | strip_html | strip_newlines | escape_once }}{% endif %}
{% endfor %}
