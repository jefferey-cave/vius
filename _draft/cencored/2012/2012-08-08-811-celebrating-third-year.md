---
id: 1991
title: '811 Celebrating Third Year &#8211; of harm'
date: 2012-08-08T16:56:30+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1991
permalink: /2012/08/08/811-celebrating-third-year/
categories:
  - NSHealth
---
> More than 130,000 Nova Scotians received health care advice from registered nurses by dialing 811 this past year.
> 
> As part of Nova Scotia's Better Care Sooner plan, 811 is helping to ease pressure at emergency rooms around the province. Nearly half of all callers received instructions on how to treat their symptoms or illnesses at home and were provided with self-care advice. Around 30 per cent of callers were advised to make an appointment with a primary care provider within 48 hours. 
> 
> &mdash; [Trusted Health Resource 811 Celebrating Third Year](http://novascotia.ca/news/release/?id=20120808005) 

Nova Scotia may be celebrating 811, their dial in health advisory. Unfortunately, I question the **quality** of advice given to the 130,000 people served. My wife and I recently had reason to call the 811 service and were disturbed by the advice given.

A bat had been in our house and woke us. After chasing the bat out, we started doing some casual online reading and were startled to see several US States recommending **immediate** treatment for rabies. Unfortunately, bats carry rabies, and you may not be aware you were bitten.

We called the 811 service to determine if rabies is a problem in Nova Scotia; and if so, how we should proceed to be tested and treated. The 811 nurse's response shocked us: <q>I don't think bats carry rabies</q>; <q>you would know if you were bitten</q>. Upon telling her of resources that stated otherwise she instructed us to <q>not go to the hospital unless you develop symptoms</q>.

One of the things, both my wife and I remember from our days as nursing students is that [rabies is a bad condition](http://lmgtfy.com/?q=rabies). Early treatment is necessary, by the time you develop symptoms, it is too late to save your life. That a medical <q>professional</q> would suggest waiting, in a suspected rabies case, told us to seek our advice elsewhere.

While talking to the nurse, my wife became suspicious that the woman was not even located in Nova Scotia. The nurse kept asking if we were located in Nova Scotia, or to contact the forestry department in <q>Nova Scotia</q>. Why would an employee in NS, offering an NS service, to people in NS, keep mentioning NS? Nor could she offer us contact information for the government departments she was referring us to? In fact she suggest talking to the <q>forestry department</q>, not knowing it is called the Department of Natural Resources.

In the end, the 811 service gave us dangerously inacurate advice, and then directed us to go seek help elsewhere&hellip; not really specify where. Nova Scotians are not being well served by the 811 service. In fact, if our case is any example, the service is putting people in danger.

I suppose dying at home would <q>ease pressure at emergency rooms around the province</q>.
