'use strict';



(function(){

function RenderWordChart(element, stats){

	if('string' === typeof element){
		element = document.querySelector(element);
	}
	let opts = JSON.merge({size:9},JSON.parse(element.dataset.opts));

	let list = element.querySelector('ul');
	if(list === null){
		list = document.createElement('ul');
		element.append(list);
	}

	// get the top 10
	let values = stats.values.slice(0,opts.size);

	while(list.childNodes.length < values.length){
		let item = document.createElement('li');
		item.innerHTML = '<label></label><meter></meter><output></output>';
		list.append(item);
	}

	values.forEach(function(d,i){
		let item = list.childNodes[i];
		let out = item.querySelector('output');
		let label = item.querySelector('label');
		let meter = item.querySelector('meter');

		if(label.innerText === ''){
			label.innerText = d.key;
		}

		if(label.innerText !== d.key){
			// update the pretty refreshes
			item.classList.remove('changed');
			item.classList.add('changing');
			setTimeout(function(e){
				item.classList.add('changed');
				label.innerText = d.key;
			},1000);
		}

		// apply the values
		meter.max = stats.max;
		out.value = d.value;
		meter.value = d.value;

	});

}


window.addEventListener('load',()=>{
	let widgets = Array.from(document.querySelectorAll('div[data-type="barchart"]'));
	widgets = widgets.reduce((a,d)=>{
			let src = a[d.dataset.src];
			if(!Array.isArray(src)){
				src = [];
				a[d.dataset.src] = src;
			}
			src.push(d);
			return a;
		},{});
	Object.entries(widgets).forEach(widget=>{
		let result = {};
		const MIN_LENGTH = 5;
		const ignore = ['which'];

		let container = document.querySelector(widget[0]);
		result.values = container.innerText;
		// sanitize the text
		let header = container.querySelector('h1,h2,h3');
		if(header){
			header = header.innerText.toLowerCase();
			header = new RegExp(header,'g');
		}
		else{
			header = new RegExp(' {2}');
		}
		result.values = result.values
			.toLowerCase()
			.replace(header,' ')
			.replace(/[^a-z\-]/g,' ')
			.split(' ')
			// count the words
			.filter(d=>{
				return d.length >= MIN_LENGTH;
			})
			.filter(function(d){
				let keep = ignore.indexOf(d) < 0;
				return keep;
			})
			.reduce((a,d)=>{
				if(!a[d]) a[d] = 0;
				a[d]++;
				return a;
			},{})
			;

		result.values = Object.entries(result.values)
			.map(d=>{
				let rtn = { key:d[0], value:d[1] };
				return rtn;
			})
			.sort((a,b)=>{
				let order = b.value-a.value;
				if(order === 0){
					order = a.key.localeCompare(b.key);
				}
				return order;
			})
			;
		result.max = result.values[0].value;
		result.min = result.values[result.values.length-1].value;
		result.sum = result.values.reduce((a,d)=>{
			return a+d.value;
		},0);
		result.count = result.values.length;
		result.mean = result.sum / result.count;
		result.meanvar = result.values.reduce((a,d)=>{return a+Math.abs(d.value-result.mean);},0) / result.count;
		result.mode = result.values.reduce((a,d)=>{
				a[d.value] = a[d.value] || 0;
				a[d.value]++;
				return a;
			},{});
		result.mode = Object.entries(result.mode)
			.sort((a,b)=>{return b[1]-a[1];})
			.filter((d,i,a)=>{
				let keep = (d===a[0]);
				return keep;
			})
			;
		result.mode = result.mode.reduce((a,d)=>{return a+Number.parseInt(d[0],10);},0) / result.mode.length;

		widget[1].forEach(widget=>{
			RenderWordChart(widget,result);
		});
	});
});



})();

