---
id: 2286
title: Worm Breeding Pen
date: 2015-07-05T19:37:43+00:00
author: Kavius
layout: post
guid: http://vius.plaidsheep.ca/?p=2286
permalink: /2015/07/05/worm-breeding-pen/
categories:
  - Worms
---
I currently have two worm bins with two very different populations: pure Red Wigglers; and a mixture of European Night-crawlers and Red Wigglers.

<span id="History">

<h1>
  History
</h1></span> 

While I have been pleased with the health of the Red bin, their size is not what I had hoped for when it comes to fishing. This prompted me to purchase a pack of worms from the local fishing store (European Night-crawlers) for a fishing trip. When I was done fishing, I realized that this was the species I really wanted: I prepared a bin and threw the last 12 worms into the bin.

12 worms in a bin isn't a lot, and it was taking a long time for them to get the environment up to speed. Therefore I took a handful of castings from the Red bin and placed it in the Euro bin. This had a **very** positive effect on the environment; within a week, material was starting to break-down very quickly. Since that time, I have seen that the Euros seem to be thriving with many young worms found throughout the bin.

I do believe that adding the Red bedding to the Euro bin, saved the Euros.

<span id="Worm_Breeding_Pen">

<h1>
  Worm Breeding Pen
</h1></span> 

This leads to my current problem: I am not 100% sure which species the juveniles are. I'm sure there were Red Eggs in the material I placed in the new bin, and given Reds reproduce more quickly than Euros, I am concerned that the Reds will out compete the Euros. I'm sure they won't drive Euros totally to nothing, but I suspect they will make up a large proportion of the bio-mass (2/3rds? based on reproductive rates).

Now that my Euro population is self-sustaining, I am looking to create a pure-bred Euro bin. To start this bin, I have decided to create a <q>breeding pen</q>; a smaller container that will allow a small number of worms to find one another and mate.

I set up a large margarine tub with holes. I have filled it with shredded paper for bedding; half a cucumber (partially rotten) was added for food stuff. The paper was sprinkled with water from a plant watering can until saturated. This will be allowed to sit until next week to allow it develop a healthy bacterial culture.

When it comes time to separate the worms, I will need to ensure there are no Red Wigglers accidentally introduced. To avoid introducing Reds to the new bin, it will be necessary to <q>wash</q> the introduced Euros prior to introducing them to the sanitized bin.

Once they have successfully processed the smaller breeding pen, the entire bin can be placed in a new larger compartment. This will allow them to create a pocket of healthy environment. If there are losses due to the stress of the washing, it will be minimized to a single batch, and the process can be repeated using worms from the mixed bin.

<span id="Material">

<h3>
  Material
</h3></span> 

  * Breeding Pen
  * Bathing Jar
  * Clean water. Allow it to have set for a week to allow any chemicals to evaporate.

<span id="Procedure">

<h3>
  Procedure
</h3></span> 

  1. Fill the <q>bath</q> with clean water
  2. Take 5 mature Euros from the mixed bin
  3. Wash each worm in a bath of water 
      * Process each worm individually.
      * Be quick about this, we don't want to drown them
  4. Place the worm in the breeing pen immediately after their bath

Once the breeding pen is fully processed, dump it into the final pure-bred bin.