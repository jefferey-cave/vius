---
id: 1916
title: Knots
date: 2012-05-17T14:18:58+00:00
author: Kavius
---


There are a lot of knots that a Boy Scout can learn, but at some point there are only so many things a person may want to do with knots.

The goal of this catalogue of knots is to give a minimal list of knots a person should know, based on the types of use a person may have. Highly specialized knots ... well probably won't get a mention.

# Fundamental Knots

The basic uses for Knots

* anchoring
* joining
* stopping
* hitching

I am of the opion that learning one knot for each of these should cover all the basic uses the average person would need in their life. This assumes that the knot learned is sufficiently simple, and versatile.

There are specialty knots that serve better under various conditions, but most people aren't exposed to those specialty conditions. If you are in a field that requires specialty knots: **learn them**!

# Specialty Knots

Examples of specialty fields

* Rock Climbing
* Fishing

# Resources

A very good source of learning various knots is [Animated Knots by Grog](https://www.animatedknots.com/)
