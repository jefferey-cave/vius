import ByteStream from 'ByteStream.js';
import TarFileTypeFlag from 'TarFileTypeFlag.js'

class TarFileEntryHeader{
	constructor(
		fileName,
		fileMode,
		userIDOfOwner,
		userIDOfGroup,
		fileSizeInBytes,
		timeModifiedInUnixFormat,
		checksum,
		typeFlag,
		nameOfLinkedFile,
		uStarIndicator,
		uStarVersion,
		userNameOfOwner,
		groupNameOfOwner,
		deviceNumberMajor,
		deviceNumberMinor,
		filenamePrefix
	)
	{
		this.fileName = fileName;
		this.fileMode = fileMode;
		this.userIDOfOwner = userIDOfOwner;
		this.userIDOfGroup = userIDOfGroup;
		this.fileSizeInBytes = fileSizeInBytes;
		this.timeModifiedInUnixFormat = timeModifiedInUnixFormat;
		this.checksum = checksum;
		this.typeFlag = typeFlag;
		this.nameOfLinkedFile = nameOfLinkedFile;
		this.uStarIndicator = uStarIndicator;
		this.uStarVersion = uStarVersion;
		this.userNameOfOwner = userNameOfOwner;
		this.groupNameOfOwner = groupNameOfOwner;
		this.deviceNumberMajor = deviceNumberMajor;
		this.deviceNumberMinor = deviceNumberMinor;
		this.filenamePrefix = filenamePrefix;
	}


	SizeInBytes = 500;

	// static methods

	static default()
	{
		let now = new Date();
		let unixEpoch = new Date(1970, 1, 1);
		let millisecondsSinceUnixEpoch = now - unixEpoch;
		let secondsSinceUnixEpoch = Math.floor(millisecondsSinceUnixEpoch / 1000);
		let secondsSinceUnixEpochAsStringOctal =secondsSinceUnixEpoch.toString(8).padRight(12, " ");
		let timeModifiedInUnixFormat = [];
		for (let i = 0; i < secondsSinceUnixEpochAsStringOctal.length; i++){
			let digitAsASCIICode =
				secondsSinceUnixEpochAsStringOctal.charCodeAt(i);
			timeModifiedInUnixFormat.push(digitAsASCIICode);
		}

		let returnValue = new TarFileEntryHeader
		(
			"".padRight(100, "\0"), // fileName
			"100777 \0", // fileMode
			"0 \0".padLeft(8, " "), // userIDOfOwner
			"0 \0".padLeft(8, " "), // userIDOfGroup
			0, // fileSizeInBytes
			timeModifiedInUnixFormat,
			0, // checksum
			TarFileTypeFlag.Instances.Normal,
			"".padRight(100, "\0"), // nameOfLinkedFile,
			"".padRight(6, "\0"), // uStarIndicator,
			"".padRight(2, "\0"), // uStarVersion,
			"".padRight(32, "\0"), // userNameOfOwner,
			"".padRight(32, "\0"), // groupNameOfOwner,
			"".padRight(8, "\0"), // deviceNumberMajor,
			"".padRight(8, "\0"), // deviceNumberMinor,
			"".padRight(155, "\0") // filenamePrefix
		);

		return returnValue;
	}

	static directoryNew(directoryName)
	{
		let header = TarFileEntryHeader.default();
		header.fileName = directoryName;
		header.typeFlag = TarFileTypeFlag.Instances.Directory;
		header.fileSizeInBytes = 0;
		header.checksumCalculate();

		return header;
	}

	static fileNew(fileName, fileContentsAsBytes)
	{
		let header = TarFileEntryHeader.default();
		header.fileName = fileName;
		header.typeFlag = TarFileTypeFlag.Instances.Normal;
		header.fileSizeInBytes = fileContentsAsBytes.length;
		header.checksumCalculate();

		return header;
	}

	static fromBytes(bytes)
	{
		let reader = new ByteStream(bytes);

		let fileName = reader.readString(100).trim();
		let fileMode = reader.readString(8);
		let userIDOfOwner = reader.readString(8);
		let userIDOfGroup = reader.readString(8);
		let fileSizeInBytesAsStringOctal = reader.readString(12);
		let timeModifiedInUnixFormat = reader.readBytes(12);
		let checksumAsStringOctal = reader.readString(8);
		let typeFlagValue = reader.readString(1);
		let nameOfLinkedFile = reader.readString(100);
		let uStarIndicator = reader.readString(6);
		let uStarVersion = reader.readString(2);
		let userNameOfOwner = reader.readString(32);
		let groupNameOfOwner = reader.readString(32);
		let deviceNumberMajor = reader.readString(8);
		let deviceNumberMinor = reader.readString(8);
		let filenamePrefix = reader.readString(155);
		let reserved = reader.readBytes(12);

		let fileSizeInBytes = parseInt(fileSizeInBytesAsStringOctal.trim(), 8);
		let checksum = parseInt(checksumAsStringOctal, 8);

		let typeFlags = TarFileTypeFlag.Instances._All;
		let typeFlagID = "_" + typeFlagValue;
		let typeFlag = typeFlags[typeFlagID];

		let returnValue = new TarFileEntryHeader(
				fileName,
				fileMode,
				userIDOfOwner,
				userIDOfGroup,
				fileSizeInBytes,
				timeModifiedInUnixFormat,
				checksum,
				typeFlag,
				nameOfLinkedFile,
				uStarIndicator,
				uStarVersion,
				userNameOfOwner,
				groupNameOfOwner,
				deviceNumberMajor,
				deviceNumberMinor,
				filenamePrefix
			);
		return returnValue;
	}

	// instance methods

	checksumCalculate(){
		let thisAsBytes = this.toBytes();

		// The checksum is the sum of all bytes in the header,
		// except we obviously can't include the checksum itself.
		// So it's assumed that all 8 of checksum's bytes are spaces (0x20=32).
		// So we need to set this manually.

		let offsetOfChecksumInBytes = 148;
		let numberOfBytesInChecksum = 8;
		let presumedValueOfEachChecksumByte = " ".charCodeAt(0);
		for (let i = 0; i < numberOfBytesInChecksum; i++){
			let offsetOfByte = offsetOfChecksumInBytes + i;
			thisAsBytes[offsetOfByte] = presumedValueOfEachChecksumByte;
		}

		let checksumSoFar = 0;

		for (let i = 0; i < thisAsBytes.length; i++){
			let byteToAdd = thisAsBytes[i];
			checksumSoFar += byteToAdd;
		}

		this.checksum = checksumSoFar;

		return this.checksum;
	}

	toBytes(){
		let headerAsBytes = [];
		let writer = new ByteStream(headerAsBytes);

		let fileSizeInBytesAsStringOctal = (this.fileSizeInBytes.toString(8) + " ").padLeft(12, " ")
		let checksumAsStringOctal = (this.checksum.toString(8) + " \0").padLeft(8, " ");

		writer.writeString(this.fileName, 100);
		writer.writeString(this.fileMode, 8);
		writer.writeString(this.userIDOfOwner, 8);
		writer.writeString(this.userIDOfGroup, 8);
		writer.writeString(fileSizeInBytesAsStringOctal, 12);
		writer.writeBytes(this.timeModifiedInUnixFormat);
		writer.writeString(checksumAsStringOctal, 8);
		writer.writeString(this.typeFlag.value, 1);
		writer.writeString(this.nameOfLinkedFile, 100);
		writer.writeString(this.uStarIndicator, 6);
		writer.writeString(this.uStarVersion, 2);
		writer.writeString(this.userNameOfOwner, 32);
		writer.writeString(this.groupNameOfOwner, 32);
		writer.writeString(this.deviceNumberMajor, 8);
		writer.writeString(this.deviceNumberMinor, 8);
		writer.writeString(this.filenamePrefix, 155);
		writer.writeString("".padRight(12, "\0")); // reserved

		return headerAsBytes;
	}

	// strings

	toString(){
		let newline = "\n";

		let returnValue =
			"[TarFileEntryHeader "
			+ "fileName='" + this.fileName + "' "
			+ "typeFlag='" + (this.typeFlag == null ? "err" : this.typeFlag.name) + "' "
			+ "fileSizeInBytes='" + this.fileSizeInBytes + "' "
			+ "]"
			+ newline;

		return returnValue;
	}
}
