---
id: 649
title: Storing Wood
date: 2010-12-10T09:51:18+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=649
permalink: /2010/12/10/storing-wood/
categories:
  - Living
---
Our new home has a Wood/Oil combination furnace. This means that we can burn oil on one side, and start a wood fire on the other. This is great for us because we have 100 acres of woodland out behind the house. If I can stay on top of it, we will never lack for home heating. Eventually we are hoping to be able to harvest our own fire wood from our own property. This will significantly reduce the cost of heating our home.

One of the problems with doing this is the length of time wood needs to be dried for. Based on all of my reading, wood should be cut into 16 inch lengths (bucked) and left for at least one year (longer is better), to slowly dry out. As you can imagine this takes up a lot of space: leaving it to dry for a year, means having a stack you are burning, a stack you are drying, and a stack you are building up. So that's three stacks at 6 cords (768 feet<sup>3</sup>) each, for a total of 18 cords (2304 feet<sup>3</sup>), all needing to be stored and sheltered.

That's a lot of wood.

Currently, we are using two buildings for storing wood: the stable portion of the barn, and a shed that is attached to the Garage. I don't mind using the shed, but am very disapointed that we are taking up a large portion of the barn. Further, this only accounts for a single year's worth of wood: what do we do with the other two year's worth.

One technique I have seen for storing that much wood is to stack it in such a way that it shelters itself. Effectively, you build a stack, and then shingle it with some of the wood you have cut and split. This keeps the wood dry, and uses no extra material.

One interesting technique that I came across for doing this is the [Holz Meite](http://www.holzmiete.de/instruction.php) or the Holz Hausen<sup id="rf1-649"><a href="#fn1-649" title="While many North American sites state that this is an ancient German technique, Germans seem to have never heared of it." rel="footnote">1</a></sup> Due to its cylindrical shape, the Holz Meite minimizes the surface area that is exposed to rain, and minimizes the footprint. By stacking the wood in a tight block, as tall as it is wide, you find a balance between stability and minimum footprint (you want short and squat, not tall and fall over).

My idea is to construct a Square Holz Meite. This will give me the nice dense storage of a Holz Meite; the strength<sup id="rf2-649"><a href="#fn2-649" title="I know that round is often stronger than square, however, it is damn near impossible to get perfectly round by hand. Making reasonably straight sides is possible." rel="footnote">2</a></sup> and tidiness<sup id="rf3-649"><a href="#fn3-649" title="S makes fun of me for my insistance on being so fussy about tidiness, but I know (without proof) that there is a correlation between strength and tidiness." rel="footnote">3</a></sup> of nice square joins . Once I have completed the peaked <q>roof</q>, I will lay a tarp over the entire stack, and place another layer of logs over that. This should keep the tarp from flying away.

This leads to an ongoing annual strategy for dealing with wood:

  1. Spring: start obtaining 6 cords of wood. This can be done either through buying or cutting my own.
  2. Summer: make a new stack of wood for long term storage and seasoning(Holz Meite)
  3. Fall: take old stack and move it to the storage shed
  4. Winter: empty the storage shed by heating the house

Supply chains can fall apart and prices can go through the roof; but hopefully, with a hundred acres of forest, we can become self-sufficient in our heating. It will take a lot of work, but with the expense of purchasing fuel for the house, a couple of days in the woods sounds like a small price to pay.

<div id='gallery-9' class='gallery galleryid-649 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'>

  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/10/storing-wood/woodpile4-20101205/'><img width="150" height="113" src="/media/2011/01/WoodPile4.20101205.jpeg" class="attachment-thumbnail size-thumbnail" alt="Tidy Wood Stack 1" aria-describedby="gallery-9-776" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-9-776'> Our first stack of wood </figcaption></figure><figure class='gallery-item'>

  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/10/storing-wood/woodpile5-20101205/'><img width="150" height="113" src="/media/2011/01/WoodPile5.20101205.jpeg" class="attachment-thumbnail size-thumbnail" alt="Tidy Wood Stack 2" aria-describedby="gallery-9-777" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-9-777'> Our first stack of wood </figcaption></figure><figure class='gallery-item'>

  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/10/storing-wood/woodpile6-20101205/'><img width="150" height="113" src="/media/2011/01/WoodPile6.20101205.jpeg" class="attachment-thumbnail size-thumbnail" alt="Tidy Wood Stack 3" aria-describedby="gallery-9-778" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-9-778'> Our first stack of wood </figcaption></figure><figure class='gallery-item'>

  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/10/storing-wood/woodpile7-20101205/'><img width="150" height="113" src="/media/2011/01/WoodPile7.20101205.jpeg" class="attachment-thumbnail size-thumbnail" alt="Tidy Wood Stack 4" aria-describedby="gallery-9-779" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-9-779'> Our first stack of wood </figcaption></figure><figure class='gallery-item'>

  <div class='gallery-icon landscape'>
    <a href='http://vius.plaidsheep.ca/2010/12/10/storing-wood/woodpile8-20101205/'><img width="150" height="113" src="/media/2011/01/WoodPile8.20101205.jpeg" class="attachment-thumbnail size-thumbnail" alt="Tidy Wood Stack 5" aria-describedby="gallery-9-780" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-9-780'> Our first stack of wood </figcaption></figure>
</div>

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-649">
    <p>
      While many North American sites state that this is an ancient German technique, Germans seem to have <a title="see the comments at the bottom" href="http://www.woodheat.org/firewood/holtzhausen.htm">never heared of it</a>.&nbsp;<a href="#rf1-649" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn2-649">
    <p>
      I know that round is often stronger than square, however, it is damn near impossible to get perfectly round by hand. Making reasonably straight sides is possible.&nbsp;<a href="#rf2-649" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>

  <li id="fn3-649">
    <p>
      The Wife makes fun of me for my insistance on being so fussy about tidiness, but I know (without proof) that there is a correlation between strength and tidiness.&nbsp;<a href="#rf3-649" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
</ol>