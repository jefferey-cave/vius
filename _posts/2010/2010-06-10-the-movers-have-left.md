---
id: 305
title: The Movers Have Left!
date: 2010-06-10T17:36:01+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=305
permalink: /2010/06/10/the-movers-have-left/
categories:
  - Moving
  - HomeSteading
---

Everything has been loaded up and is on its way to Halifax, Nova Scotia. We will catch up with it again around July 1st.