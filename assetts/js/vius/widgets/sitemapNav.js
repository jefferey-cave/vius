'use strict';


let privates = new Promise(function(){
	return {path:[],children:[]};
});

export default class SiteMapNav{
	constructor(){
		let currentLoc = UrlToNav(window.location);

		let url = new URL('/sitemap.json', window.location.href);
		privates = fetch(url)
			.then(function(response) {
				return response.json();
			})
			.then(function(locations) {
				locations.pages = locations.pages
					.filter(function(loc){
						return loc;
					})
					.filter((loc)=>{
						let keep = 0 <= ['page','post'].indexOf(loc.type);
						return keep;
					})
					;
				let locs = Array.from(locations.pages)
					.map(function(loc){
						loc.path = loc.url.split('/');
						while(loc.path[0] === ''){
							loc.path.shift();
						}
						//loc.file = loc.path.pop();
						//if(loc.file === ''){
						//	loc.file = 'index.html';
						//}
						while(!loc.file && loc.path.length){
							loc.file = loc.path.pop();
						}
						return loc;
					})
					.filter(function(loc){
						// need to loop through the current path, and the link's path
						// if any of it differ, we reject
						let a = JSON.stringify(loc.path);
						let b = JSON.stringify(currentLoc.path);
						let pathSame = a === b;
						return pathSame;
					})
					.map(function(loc){
						loc.path = RemoveCommon(loc.path,currentLoc.path);
						return loc;
					});
				let rtn = {};
				rtn.children = locs;


				let cwd = '';
				let possibles = locations.pages.filter(function(loc){return loc.file === '';});
				rtn.path = Array.from(currentLoc.path).map(function(path,i){
					// loop through all the locations and find
					cwd = [cwd,path].join('/');
					let match = Array.from(locations.pages).filter(function(loc){
						return loc.url === (cwd+'/');
					});
					if(match.length > 1){
						throw new Error('Should only ever end up with a single match');
					}
					match = match.pop();
					return match;
				})
				.filter(p => p);
				return rtn;

			});


	}

	async getPath(){
		let path = await privates;
		return path.path;
	}

	async getChildren(){
		let children = await privates;
		return children.children;
	}

}

function UrlToNav(url){
	let rtn = {};
	rtn.href = url.href;

	let path = url.pathname.split('/');
	path.reverse();
	path.pop();
	rtn.name = path[0];
	path.reverse();
	path.pop();
	rtn.path = path;

	return rtn;
}

function RemoveCommon(path,base){
	path = JSON.parse(JSON.stringify(path));
	base = JSON.parse(JSON.stringify(base));
	while(path.length > 0 && base.length > 0 && path[0] === base[0]){
		path.shift();
		base.shift();
	}
	return path;
}




