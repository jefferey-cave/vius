---
id: 2030
title: About
date: 2010-05-10T22:12:41+00:00
author: Kavius
layout: page
guid: http://www.vius.ca/?page_id=2
---
 The Wife and Kavius are just two people trying to make a go of it in the world.

Years ago we struggled to understand the world around us. Wondering why 
nothing made sense. Why people were so stupid. Then we found one another.Two 
opinionated assholes, sharing the same opinion.

We were married on August 8, 2009, in the front-yard of our home.

We don't always see eye-to-eye on the details, but the overall functioning of 
the world suddenly makes sense. People have flat out stated to  The Wife that her 
success is due to marrying Kavius; and people have flat out stated to Kavius that 
his success is due to marrying  The Wife. Really we had both figured some of life 
out, were both independently successful, and were catapulted forward using the 
other as an anchour.

 The Wife brings a daughter, to the relationship, whom we both love and are 
trying to teach a little of what we have learned of the world. She is a fine 
young woman, and we are proud that she has decided to make a go of it on her 
own. Also note, she is a rotten child and Kavius hates her 😉

In our effort to succeed in the world, we have decided to move to Nova Scotia, 
buy a small-hold and make a go of running a farm. This is a terrifying 
prospect but one filled with optimism. This site is an effort to document our 
successes and failures, garner some good advice from others who have been down 
this path before, and failing that to offer our triumphs and failures to those 
who may follow.
