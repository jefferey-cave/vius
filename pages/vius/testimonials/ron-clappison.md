---
id: 12
title: Ron Clappison (Realtor)
date: 2010-05-11T01:10:12+00:00
author: Kavius
layout: page
guid: http://www.vius.ca/?page_id=12
---
<http://www.ronclappison.com>
  
Calgary, Alberta
  
403.278.6041

Ron Clappison has set a new standard in quality of service. Having dealt with many realtors in the past, we had begun to question what value they really offered to the selling process. Ron has demonstrated what realtors should be doing.

Ron's communication was fast and effective. From the first moment he stepped into our home, he walked us through his basic selling process. We never once had to ask a question or prompt him for information, he not only covered everything we felt we needed to know, he covered information and aspects we had never even considered.

Due to the lack of service in the past, we had done most of our own marketing, and had come to believe we knew how to sell a house (we had shown our own realtors a thing or two in the past). Ron set us straight. He not only covered all of the aspects of selling we felt were important, he also raised new issues we had never considered or come across. He was able to market our home in a more effective way than we thought possible.

As part of the marketing effort, we were very impressed with Rosemary Clappison's photographs and staging advice. These elements combined to effectively emphasize the strong aspects of our home. We were impressed with the end effect of all of the staging advice, especially as it used items we already had. Ron and Rosemary Clappison are an effective house selling team.

In the end, our house sold quickly (7 days), and for the price initially estimated. The negotiations went smoothly, we believe in part due to Ron's experience and advice. I can confidently recommend Ron Clappison to anyone selling their home.