---
id: 484
title: 'Student Loans: Pumpkin, I told you so'
date: 2010-07-16T01:57:55+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=484
permalink: /2010/07/16/student-loans-pumpkin-i-told-you-so/
categories:
  - Opinions
---

Our daughter has finished her schooling, and now has a trade she can ply to
earn a living. She has done this without a single scrap of student loans. This
was not easy. During the entirety of her schooling she kept coming to us to
get us to sign papers for student loans, but we continuously refused, and
encouraged her to do her schooling without. A couple of days ago, she told us
she was starting to save money to make a down payment on her first house, and
today  The Wife and I are pleased that she can do that without ever looking
back on a debt load from her education. She is free to move forward with her
life as she needs to without worry.

The Wife and I feel that all the fights about student loans are worth it
after reading some of the stupid comments on a
[Cancel Student Loans](http://www.thepetitionsite.com/1/cancelstudentdebts)
petition.

# Crazy Amounts

| Petition Signature | Amount |
| --- | --- |
| 304 | $156,000 |
| 294 | $190,000 |
| 287 | $100,000 |
| 268 | $170,000 |

What the f@#$ school costs $156,000 USD to get a four year bachelors degree!
That's $40,000 per year! That was the cost of my entire tuition for my
bachelor's degree, and most of my academically inclined friends feel I
over-paid. On top of that, I have earned less than that per year for most of
my working life (in Canadian Dollars). My advice: you are a student, live in
cheap housing, and drink less beer (not that I did, but I can guarantee I
could have dropped one of the two jobs I had through school).

# Crazy Comments

In particular, I really need to comment on person #268.

> For the next 20 years, I will have to continue to spend a minimum of
> $1450.00 monthly on outstanding student loan debt. The interest rates
> contribute to the amount of debt, so that it is even more difficult to pay
> down my balances.

This person overpaid for their education. It is very simple to tell that they
have as they feel it is necessary to describe that interest is extra money
that needs to be paid back. If you have paid $170,000 to get an education, but
are surprised <q>interest &hellip; contributes to the amount of debt</q>, your
education is not serving you well, and you have not received good value for
your money. This is not a slight on the educational institution, but instead a
slight on the student: my daughter got that much by the age of 17.

> Furthermore, the private banks overlent money to me, for additional living
> expenses, but allowing a young adult with a $12,000/yr salary to be given
> $170,000 in funding for education is irresponsible on the part of the bank.

Another concern I have is that this individual is confusing _Student Loans_,
with _loans received while a student_ (<q>the private banks overlent money to
me, for additional living expenses</q>). The petition is asking that student
loans be forgiven, but the money this individual is talking about was for
<q>additional living expenses</q>. My sympathy runs thin. This individual is
expecting the lending public to feel sorry for her because she lived a higher
lifestyle than she could afford. Both  The Wife and I, while working, were
able to save and live well on $30,000 a year (The Wife, as a single mother).
This is equivalent to asking us to feel sorry for this person because she went
on a holiday with her credit card and now can’t afford to pay the card back.
Sorry, but those of us that do not do things we cannot afford do not feel
sorry for you.

On top of this, I can easily understand why the individual can’t find the
money to pay back this loan: no employer in their right mind would hire
someone unable to take responsibility for their own actions (<q>irresponsible
on the part of the bank</q>). It’s easy to blame the banks for over-lending,
but let us rephrase her statement: <q>The banks gave me too much stuff, I got
greedy and took it, and now I don’t want to pay for the things I have
received</q>. At this point we are left to question whether this individual
ever had any intention of repaying this loan. Blaming the bank for giving the
money indicates to me that this person was spending a lot of time (and money)
on leisure activities, without spending time earning the money to pay for
those activities.

> Thus far, I have only been paying interest, and I have calculated that I
> have paid $50,000 in interest and no principal.

Further evidence that this person was looking for an easy ride can be found in
the fact that this person has never once even attempted to pay the original
money borrowed (<q>only been paying interest</q>). Aside from the surprise
this person has felt in realizing what interest is, it is horrifying to me
that this person has never attempted to pay back any of the original loan.
This is again an inability to take responsibility for her actions: she
received multiple services and wants for someone else to take the
responsibility for funding it. By never making an attempt to get out from
under this debt (by making only minimum payments), this person is hoping to
put the problem off until someone else will deal with the problem.

> That money, and all future monies could instead be used to drive the economy
> by fueling the housing, tourist, automotive, and retail industries

**[Ignoratio elenchi](http://en.wikipedia.org/wiki/Ignoratio_elenchi)!**<a href="#f-1" title="Yes, I looked it up" rel="footnote">1</a>
This is nothing more than a red herring meant to divert attention from the
real issue. This is really an an appeal to emotion; she is stating that if she
had this money she could spend it, and therefore create jobs. The emotion she
is appealing to is the need for the unemployed to want work. If those holding
the loans would just forgive them, there would be lots of money to spend.
There are many bits of wrong headedness with this statement.

Whether she would be able to use that money for other purposes is really
irrelevant since she has already chosen how she would spend that money. She
had the money, and did inject it into the economy <q>fueling the housing,
tourist, automotive, and retail industries</q>. She did that the first time
she spent the money. She had her chance, now it is up to someone else to try.

Her statement implies that the money is lost. That she has spent the money,
does not mean that money is not available to continue fuelling the economy.
Those funds went to pay land-lords for building and maintaining housing;
professors for professing; the clerks that registered her for classes; the
janitors that cleaned her classrooms and repaired her desks; and the waitress
that brought her drinks and who worked their way through school to keep her
debt load at a reasonable level<a href="#f-2" title="This is what our daughter did, and why I am proud of her" rel="footnote">2</a>.
All of these people will continue to need automotive repair, groceries,
clothes and all the other things people need to produce and buy, thereby
continuing the great chain of buying and selling we call an economy.

Not only has she had her chance to spend the money, but it wasn't her money to
begin with. She borrowed money from people who expected repayment. They
expected repayment because (surprise) they would like to spend the money on
something. If she does not repay the loans she is keeping her lenders from
spending money and driving the economy. Not only that, but the entire industry
built up around managing the lending of money would be hurt as well. All the
bank clerks, janitors at banks, IT infrastructure people, would be out of work
if nobody paid back their loans.

Not only is she missing the fact that the money is still in the system, and
now it is somebody else's turn to spend that money; she is making an effort
to divert attention from the core of the problem: this is primarily an ethical
question, not a pragmatic one. That money belonged to somebody else, they lent
it to her with the expectation of repayment, she doesn't want to give it back.
When I was a kid, we called this theft.

> Please allow for student loans to be eligible for dissolution

I hope that this individual's plea to have her loans dissolved is laughed off
by all serious individuals, because this is the critical point of the whole
discussion. All of the rest of the discussion is distraction. As an owner of
Sally Mae (<a href="http://www.google.com/finance?q=slm">SLM</a>), and a
depositor in private banks, it is me (as an individual) that she has borrowed
that money from, and continues to owe that money too. Her expectation that
someone else should pay for her high living, is an expectation that **\*I\***
will pay for her high living, at the expense of my daughter’s education, and
at the expense of my retirement. Her expectation is that she will live beyond
her means, and I will pay for it through lower interest rates from the bank,
and lower dividend payments on Sally Mae. This translates into me having to
work harder and retire later, and means I don’t get to live my life of
self-sufficiency and independence.

# Conclusion

> Not coming out of a Higher Education, either College or University with
> Debts is a wise decision
>
> ~ [_Cancel Student Loan Debts_, Online Petition](http://www.thepetitionsite.com/1/cancelstudentdebts)

This is a wise statement, but in the end a personal choice, not a societal
responsibility. Many of people have worked very hard to avoid indebtedness in
the first place, a very <q>wise decision</q>. To punish all of that hard work
by forcing those people to absorb the cost of not only their education, but
the education of others is unfair, and unwise.

Both my daughter, and I, have a trade we can ply and no debt tying us down.
This was not easy. After reading the stories from this petition, I feel
vindicated and glad over every yelling match I had with her. Rather than
being burdened with wondering how she is going to pay back $150,000 for
something in her past, she can look forward to her $150,000 home in the
future. I am pleased to think that I had a little part in her ability to be
financially self-reliant, rather than a burden on others.

Pumpkin, I told you so.

<aside>
 <ol>
  <li id="f-1">
   <p>
Yes, I looked it up
<a href="#rf1-484" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  <li id="f-2">
   <p>
This is what our daughter did, and why I am proud of her
<a href="#rf2-484" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
   </p>
  </li>
 </ol>
</aside>
