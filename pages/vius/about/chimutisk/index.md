---
id: 730
title: Chimutisk
date: 2011-01-13T20:23:36+00:00
author: Kavius
---
December, 2004 &mdash; June, 2011

![Chimutisk](/media/2011/08/Chimutisk.jpg "Chimutisk")

Chimutisk is Cree for <q>Thief</q>; and that is exactly what our pet ferret is. He earned his name when I introduced him to my Grandmother, and he instantly grabbed her keys off the coffee table and made a dash for it.

Chimutisk is the gentlest animal on the face of the planet. When he was young, I wasn't playing with him enough and he bit me to get my attention. After that we worked really hard to teach him not to bite , and it paid off (I wish we had worked as hard on his litter training). His teeth grazed  The Wife once (he thought it was me) and he felt so bad about it that, after licking the finger to make sure it was alright, he wouldn't look at her for two days. He has been a great ferret for introducing to people and has been handled by children through to the elderly without me ever having to worry about his behaviour.

Chimutisk  is absolutely terrified of <a title="Rabbit Stew" href="http://www.vius.ca/about/rabbit-stew/" target="_self">Stew</a>, and will travel well out of his way to avoid being pounced on by the <a title="The Black Rabbit of Inle" href="http://www.deviantart.com/art/The-Black-Rabbit-of-Inle-315960479" target="_blank">Black Bunny</a>. If Stew does pounce on him, he cries and we have to rush in to rescue him&hellip; that is until we figured out he was picking fights to get attention. We have watched (on several occasions) Chimutisk sneak up behind Stew, and lay-down to wait for Stew to notice him and hit him. Sure enough, as soon as Stew turns around, he gives Chimutisk a smack, Chimutisk cries, and then looks at us to rescue him and give the bunny heck. We don't smack the rabbit's bottom anymore, we just pull the stupid ferret out of harms way.

He is older now (almost six) and just before we moved, was diagnosed with a terminal illness. During the trip across Canada, he has lost a lot of the bounce in his step. He may be a little slower, but he is still bright eyed and bushy tailed, and as friendly as ever.

# Update

On June 15th, 2011, at 2am, Chimutisk breathed his last breath, while curled 
up in my arms.

He is missed.