---
author: Jeff
title: Is Harper a Bad for Canada?
slug: is-harper-a-bad-for-canada
id: 1933
date: '2012-05-28 19:10:15'
layout: draft
categories:
  - Uncategorized
---

<pre>This is a bit of a follow up to my [Bill C-10](http://vius.ca/2012/02/response-to-bill-c-10-video/ "Response to Bill C-10 Video") article</pre>

Is Harper bad for Canada? After careful research and consideration, I don't know. But my reasons for not knowing may be a little surprising. Recently, I have noticed a major trend (starting maybe a month ago) for Stephen Harper to be labelled "evil", or dangerous. There has been a lot of trash talk going around about Harper. Not being one who pays a lot ofattention to national politics (I just assume that all polititians are corrupt, therefore watching them serves no purpose) so I became curious as to what he had done recently to get such a violent backlash. The next time I saw a attack ad against Harper, I askedwhat he had done that was so offensive. "What do you mean? Isn't it obvious? Do a google search and you'll see" Not terribly informative, but I decided not to push the issue. Instead I asked someone else who had posted a different attack ad on a different website. "What do you mean? Isn't it obvious? Do a google search and you'll see" So I have been. Since late February, I have been doing all kinds of internet searches and have been coming up with ... well ... not a whole lot.   Anti-Harper Propaganda

# Informational Website

I found one source of issue-by-issue information at [http://UnseatHarper.ca](http://unseatharper.ca/harper-quotes.php). They claim to be a non-partisan group that wishes to remove Harper from power. They feel that Harper is so bad, even conservatives should want him out of office. In other words, this isn't about politics, its about Harper. I started looking through their site's information on naughty things the Conservative   Party has done since they came into power. After an hour of reading I was pretty much convinced that they were up to less good than politicians are usually up to. Just out of interest I turned to their Harper Quotes page, and started reading them. On this page are several quotes that are presented to make Harper look bad, but one quote in particular caught my eye:

> That's why the federal government should scrap its ridiculous pay equity law.

I'm assuming this quote is presented to show that Harper is working against equal pay for men and women, but I see something different in the quote. UnseatHarper.ca left out the part of the quote that gives Harper's reasoning. That sets alarm bells off in my head. Why did Harper say this? Did he have a reason that I may agree with? For all I know, Harper doesn't like the idea of ending pay inequity by killing the babies of women. You may think this is far fetched, but as recently as the late 70's a bunch of dope smoking hippies had just got their Ph.D. s from the University of Paris, managed to get their party elected to power, and were implementing just such a policy (Democratic Party of Kampuchea). Is that the law Harper was against? I don't know, they won't tell me. If they won't tell me, I assume its because they are hiding something. So I followed up on their source.

# Attack Campaigns

# Conclusions

In fact, I think we are asking the wrong question, the better question would be: "who is behind the media attack on Harper?"