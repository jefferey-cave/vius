---
id: 102
title: Found a Place
date: 2010-05-29T23:13:39+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=102
categories:
  - Opinions

---
We have found a place! We are now subletting a little one bedroom apartment
for six months. This is good news, we were starting to think that I may have
to call up the job and tell them I couldn't do it. As it stands, we are still
pressing forward. The rent is even a reasonable rate.

While we were looking, I came across this video on a
[landlord's website](http://www.killamproperties.com/killam-tenant)

A couple of days ago, I was asking
[why is rent so high](/2010/05/25/renting-in-halifax.html), and why won't
people lease for less than a year. Well, I was looking for reasons, and have
found both. This video explains why rent is so high, and (The Wife believes)
the [Nova Scotia Tenancy Act](http://www.gov.ns.ca/legislature/legc/statutes/resident.htm)
explains why a landlord won't rent for less than a year.

If a landlord signs a lease for less than a year, it will be considered
month-to-month. With that, the government has ensured that any landlord would
be insane to accept a tenancy contract for less than a year. If it isn't for a
year, the landlord only needs to receive one months notice, but if he can hold
out for a year, it goes to 3 months.

I think it is a combination of the high taxes and the tenancy act. Between the
two of them, a land lord can get in trouble without a one-year lease, and
there isn't enough competition to get a lone landlord to break rank. Because
the taxes are so high, landlords don't build new buildings often, and
therefore there aren't enough to compete heavily. Basically the high taxes
keeps supply low.

I would like to thank the Nova Scotia Government for increasing rent and
making it extraordinarily difficult to immigrate to Nova Scotia.
