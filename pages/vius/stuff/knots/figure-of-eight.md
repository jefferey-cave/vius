---
id: 1918
title: Figure of Eight
date: 2012-05-17T14:19:28+00:00
author: Kavius
layout: page
---

[Figure of 8 Knot](https://www.animatedknots.com/fig8join/)


This is the single most important knot you will ever learn. While the basic 
pattern is relatively simple, once learned it can be used in several different 
ways.

Knots that don't come undone are annoying. Part of what makes this knot so 
useful is that it rarely gets so tight that it binds. While under load, it has 
good gripping power; but once the load is taken off, this knot is easier to 
work loose.

All knots represent a weak point in the rope, but test done in "On Rope" 
found that the Figure of Eight is the least weak. Figure of Eights are 
aproximately 80% the strength of the original line. When you are dangling 
from a cliff, or hoisting an chainsaw, this matters.

These properties make this knot particularly useful for the individual who 
does not want to become a knot master. Learning this one knot allows the user
to have a large set of problems that can be solved with the minimum of fuss.

# Uses

* Anchoring
* Stoppers
* Joining

*Mostly* this knot get's used anywhere you need a loop that won't move. You need 
a loop in a lot of places; mostly anchors. Anchors are the knot you use to 
anchor a rope to a standing object. This may be tying your boat to a tree, 
setting a clothes line at the beach, hoisting tools to your roof, or stringing 
a dummy cord between you and your fishing rod.

# Bowline vs Figure of Eight

Inevitably, when discussing knots, and in particular, the Figure of Eight, 
someone will bring up the Bowline.

The Bowline is the most common anchor knot used in North America, and is a 
very good knot to know. However, I would argue, tha that the Figure of Eight 
is a better knot to know and study. 

| | Bowline | Figure of Eight |
| --- | :---: | :---: |
| Speed | (/) | (x) |
| Versatility | (x) | (/) |
| Dressing | (x) | (/) |
| Stability | (x) | (/) |

## Speed

The bowline is has one property that makes 
it a better knot in specialized circumstances: it is faster to tie. This sole 
advantage is a very narrow specialization: only people that need to tie a lot 
of knots fast need to know this. This puts you in a specialized field and you 
should study the knots associated with you r field.

## Versatility

Bowlines are good for one thing: anchors. 

If an individual is going to invest the time in leanring a knot, they should 
get the most out of it; the figure of eight offers that. It can be used as a
stopper, for joining two ropes together, or for anchoring. This makes it a far
more useful knot for general use.

Especially in the case of an individual who is not wanting to be a master knot 
tier, and is just learning a general purpose skill. In this case, they need to 
maximize their learning effort, and they do that by covering as many problems
as possible with as few knots as possible. 

The Figure of Eight offers a good generic knot that covers a lot of scenarios.

## Dressing

Dressing refers to the "tidiness" of a knot, and most people believe this is 
just a bit of snobery among knot tiers. However, dressing serves a very 
important purpose: the tidier a knot, the easier it is to tell it has been 
tied correctly.

For a novice, the figure of eight dresses in such a way that the knot can be 
*easily* inspected for correctness. Regardless of which knot is considered 
easier to learn under the hands of a master, the Figure of Eight can be assessed for 
correctness more easily. This leads to self correcting while learning.

This self-correcting lends itself to use in the field. For a person that does 
not tie knots regularily, but finds themselves in a situation where a stable 
knot is required, a knot that they can *easily* assess for correctness is 
important. After years without practice, a person familiar with a Figure of 
Eight can quickly see that "something" is wrong allowing them to investigate, 
and start over if necessary.

## Stability

I don't have much to say: the bowline comes undone more easily than the figure 
of eight. It's a really stable knot while under load, but when not under load 
it has a knack for coming undone.

That makes for a bad day (even if it is just your clothes ending up in the mud).
