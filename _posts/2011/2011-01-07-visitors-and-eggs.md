---
id: 723
title: Visitors and Eggs
date: 2011-01-07T00:00:49+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=723
permalink: /2011/01/07/visitors-and-eggs/
categories:
  - Living
---
We had visitors this past weekend. How exciting!

Before we made the move to Nova Scotia, we did a lot of research about how people felt about living in Nova Scotia. One source of information came from the guys at <a title="DownShiftMe.com" href="http://downshiftme.com" target="_blank">DownShiftMe.com</a>. When we arrived in Nova Scotia, and needed a Real Estate Agent, we decided to send them an email and see if they could recommend one. In the end, they couldn't (their's had moved) but we met with them for coffee.

This past weekend Matt and Adrian came over to our house for a visit. We have been to their house before, and they have been here, but this was the first time we were really set up for entertaining. We sat around all afternoon (and a little into the evening) talking about this and that and generally just having a good time. Being able to host some guests,  really made the place feel like home.<!--more-->

Both Matt and Adrian really liked our pets. [Stew](http://www.vius.ca/about/rabbit-stew/ "The Rabbit") decided he wasn't sure about them, gave them a couple of stamps, and ran away flicking his feet. We had to wake [Chimutisk](http://www.vius.ca/chimutisk/ "Chimutisk the Ferret") up, but once he was aware we had visitors, he spent the rest of the evening sleeping at their feet and even asked to be picked up twice. After they left, Stew started searching all of the areas they had been, I think he was ready to finally meet them.

When they arrived, Matt and Adrian brought with them eggs from the hens they keep. It was interesting to see home produced food stuff. It was nice to see something that was produced by someone living in the area, keeping a personal flock. It was wild to see all of the colours of eggs: brown, speckled, and blue (yes, blue eggs). They gave us the eggs; all two-dozen of them.That leaves us with 48 large eggs (we had just bought two dozen at the store).

The eggs are great. Chimutisk has been loving getting an egg every day (he gets the store-bought ones). We have had egg dishes every day. Delectable eggs, done in every way imaginable;  The Wife even looked up a few ways to prepare them that we hadn't imagined. Eggs for breakfast. Eggs for lunch. Eggs for dinner. Eggs, eggs, eggs&hellip;

I hate you Mat. I hate you Adrian.