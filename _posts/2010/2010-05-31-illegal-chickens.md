---
id: 112
title: Illegal Chickens
date: 2010-05-31T04:11:26+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=112
permalink: /2010/05/31/illegal-chickens/
categories:
  - Homesteading

---
One of the reasons  The Wife and I are leaving Calgary is due to the inability
to grow our own food.

When we had purchased our first house together, we put a lot of effort
choosing one that would allow us to grow a certain amount of our own food.
After moving in we began the redesign of the backyard. The landscaping focused
on two objectives: high intensity gardening, and a parking pad. The parking
pad was a necessity due to living on a cul-de-sac, but we always had our eye
on putting a driveway out front of the north facing house.

The reason we wanted move the cars eventually, was to turn the space into a
paddoc for goats. We didn't know much about goats, but they seemed like a good
animal to keep us in milk and that would not eat us out of house and home.
When we finished the driveway, I began looking into two things: what is the
right animal for an urban setting, and what is the legality of those animals?

## Goats

Not the right animal. Chickens or rabbits would be a better choice (but given
we have a pet rabbit&hellip;)

# Legality

It's not.

Calgary is a place where the Government **really** likes to govern. It's
really a city of busybody neighbours, backed by a busybody government.
Everyone is so concerned about the resale value of their home, that they
ignore the living value of their home.

The City of Calgary has banned any animal of an agricultural nature. This has
come to include miniature goats and pot belly pigs (two obviously <q>pet</q>
breeds). They explicitly mention rabbits and chickens in the by-law.

This wasn't the first time I was annoyed with the City, but that settled it,
it was time to leave and find somewhere where people will let you live your
life as you see fit.

# CLUCK

Well after we had decided to leave Calgary for Nova Scotia, we first heard of
<dfn>Canadian Liberated Urban Chicken Klub</dfn>
(<abbr title='Canadian Liberated Urban Chicken Klub'>CLUCK</abbr>).

> CLUCK wants changes to the city bylaw to allow people to keep six hens or
> fewer on their property.
>
> ~ [CLUCK](http://cluckurbanchickens.blogspot.com/2009/09/calgary-anti-urban-ag-bylaw-27-calgary.html)

I wish CLUCK all the best and am glad that someone is fighting the city on
this matter. The only thing that bugs me is that they have to come up with
silly reasons why they should be allowed chickens. At the end of the day, if
my neighbor wants to keep an obnoxious dog, I leave it be, if I want to keep a
chicken or two&hellip; remember the bit about busy body neighbors?

Glad to hear CLUCK is making [headway](http://calgaryfoodpolicy.blogspot.com/2010/03/chicken-charges-dropped-by-city-of.html)
