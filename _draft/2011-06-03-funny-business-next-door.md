---
author: Jeff
title: Funny Business Next Door
slug: funny-business-next-door
id: 1207
date: '2011-06-03 18:53:05'
layout: draft
categories:
  - Uncategorized
---

Individuals in power are always faced with the tempation of abusing that power for personal gain, at the expense of others. There are two forms this can take: those in power can create and modify regulations to give themselves a competittive edge in the market, or they can steal stuff. It appears that the municipal government, in the nearby town of Bridgetown, chose the latter. After numerous cases of financial discrepancies were identified, the Government of Nova Scotia, has [ordered a Forensic Audit](http://www.gov.ns.ca/news/details.asp?id=20110603001 "Province Announces Forensic Audit for Bridgetown") to determine what is going on. Bridgetown suffered the pull out of a potential employer. The two combined will be devastating. I do my banking in bridgetown My pharmacy is in bridgetown. Take very good care of Chimutisk. We know people in bridgetown "Power corrupts", as the old saying goes. While we do not live in Bridgetown, this is a small area, we all know someone, either friend, family, or business associate, and I'm certain reprocusions will be felt throughout Annapolis County.