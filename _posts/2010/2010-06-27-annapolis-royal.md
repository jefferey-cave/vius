---
id: 458
title: Annapolis Royal
date: 2010-06-27T00:59:04+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/2010/06/annapolis-royal/
permalink: /2010/06/27/annapolis-royal/
categories:
  - Moving
---
We arrived in Annapolis Royal yesterday, today we cruised around the area with a list of properties we may be interested in. The goal was to identify what the communities, that these houses are in, feel like.

We looked at places around Annapolis Royal, Bear River, Digby, and Centreville<sup id="rf1-458"><a href="#fn1-458" title="There are at least three Centrevilles, this is the least  central one" rel="footnote">1</a></sup>. None of them really appealed to  The Wife or I. We both felt it, but couldn't put words to what we were feeling. There were several feelings we expressed, but it was very hard to put our fingure on it. We keep coming around to comparing it to the North Shore: we like the North Shore better, but we can't tell why. We are concerned this is not an objective assessment.

While we were on the North Shore, we met a really nice couple who took us under their wing and introduced us to several people within the community, and took us house shopping. We felt so welcomed by this one couple, and felt so pulled into the community that we introduced to that to come to this new community feels cold. Basically, we felt like friends, now we feel like visitors. This is a problem.

There is nothing wrong with this region: the land is absolutely beautiful; the farm land is fertile; the houses are beautiful; the climate is gorgeous; and yet we are looking for excuses to not buy here. All of the reasons we have been giving are valid, if true. However, I am concerned that we are only responding on an emotional level; if we find a way to work ourselves into this community, we would love it just as much as the North Shore (maybe even more).

Fortunately, we have 6 months to make this decision. We can spend the time investigating and visiting until we are sure we are making the right choice. As it stands, I know I need to work extra hard to like this region purely because Chris and Christine gave us such a warm welcome to the North Shore.

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-458">
    <p>
      There are at least three Centrevilles, this is the least central one&nbsp;<a href="#rf1-458" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
</ol>