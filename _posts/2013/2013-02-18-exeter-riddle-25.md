---
id: 2093
title: Exeter Riddle 25
date: 2013-02-18T13:04:00+00:00
author: Kavius
layout: post
guid: http://vius.plaidsheep.ca/?p=2093
permalink: /2013/02/18/exeter-riddle-25/
categories:
  - Musings
---

Years ago I tried to teach myself Old English. I came across this translation I did of a riddle from Exeter.

I have since found <a title="Exeter Riddle 25" href="http://www.swarthmore.edu/Humanities/english/oldenglish/25.html" target="_blank">better translations</a>, offering far clearer clues, and basically making me look bad. Oh well&hellip;

> I am widely found, by worthy men, <br />
> And brought from grove, mountain, dale, and glen.
> 
> By day I'm carried upon airy feather,<br />
> And wrought by skill under roofed shelter.
> 
> Later, I am both warrior and goon<br />
> Whom men, young and old, wrestles down.
> 
> Shortly after I am found,<br />
> a man is trapped in struggling round;
> 
> His back will have to seek the field,<br />
> who lacks the wisdom when to yield.
> 
> Strength of speech, strength be stole,<br />
> hands, feet, spirit; lose control.
> 
> What on earth could I be,<br />
> that binds battered, foolish men, when first light seen?
> 
> <footer><cite>Exeter, Riddle 25, Translation: Kavius Cave 2007</cite></footer>

<details>
 <summary>The answer is</summary> <q>mead</q>
</details>

