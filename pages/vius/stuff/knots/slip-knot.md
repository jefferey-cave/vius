---
id: null
title: Slip Knot
date: 2018-10-30T16:21:02+00:00
author: Kavius
layout: page
---

Slip knots are the overlooked workhorse of the world. The ability to create a 
knot and throw it over the thing to be bound is extremely useful.

Slip knots are any knot where the knot "slips" along the the rope. This is a 
pretty generic definition, and there are a few knots that meet this 
definition, but there is one knot in particular that has extreme utility due 
to its simplicity: the simple one-handed slip knot.

Because these knots have the ability to get tighter when pulled, they *all* 
come with a warning: never put them around something living. Injury and Death 
have been the result of people screwing around with these knots.

# Basic Slip Knot

The Basic slip knot is useful in that it can be tied with one hand wiht 
mininmal movement, and also gets tighter when pulled from the standing end, 
and looser when pulled from the tag end. This knot is not remarkabely strong, 
which is infact one of its benefits. There is a low risk of this knot 
becoming permantely afixed to something.

1. Form a loop
2. Reach through the loop, grab the standing end
3. Pull a bight through

With a bit of practice, this action can be acheived single handedly. It is 
often useful to have some tension on the standing end when attempting to do 
this one handed (I will often stand on the rope if necessary).

## Uses

This knot gets used everywhere I need a line to be *temporarily* anchored, 
and I don't want to think about it.

* weaving
* untangling rope
* tying gear to trees on windy days

When spooling fishing line, I've used this knot. The act of winding line 
will cause the line to become firmly anchored, but the that initial start 
just needs to be loosely held.

# Other Slip Knots

There are a few slip knots that are remarkable for various properties:

* Noose (the forbidden knot)
* Trucker's Hitch
* Prussic

These knots are all remarkable for their ability to move *only one way* along 
the rope. The Prussic, and Trucker's Hitch are particularlily interesting as 
they can be used for moving along a standing line. The Prussic can then be 
used as an ascender, while the Truckers hitch is used as a compression strap. 

The Noose, is a dangerous knot since it's loop will slide tight, but once 
tight, cannot be loosened. This is useful when creating a permanent anchor, 
but also makes it the most dangerous of the slip knots.

