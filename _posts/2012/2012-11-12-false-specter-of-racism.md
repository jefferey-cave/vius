---
id: 2028
title: False Specter of Racism
date: 2012-11-12T19:18:36+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=2028
permalink: /2012/11/12/false-specter-of-racism/
image: /files/2012/11/RacistTweets.png
categories:
  - Opinions
---
[<img class="alignright size-full wp-image-2029" title="RacistTweets" src="/media/2012/11/RacistTweets.png" alt="" width="813" height="452" />](http://gawker.com/5959425/which-states-sent-the-most-racist-tweets-after-the-election)

<a title="Which States Sent the Most Racist Tweets After the Election" href="http://gawker.com/5959425/which-states-sent-the-most-racist-tweets-after-the-election" target="_blank">Which States Sent the Most Racist Tweets After the Election</a>

The article concludes that the Southern States are the most racist. However, if you _actually look at the map_, you will notice that the distribution seems pretty even. There are a lot of dark green spots on the along the Canadian border, a lot darker than say&hellip; Texas.

The clearest evidence that the author is trying to raise a false specter comes from this comment:

> Southern states have the highest proportion of African-Americans in the country. A good number of the Tweets containing the keywords came from them, and were sent in solidarity rather than with racist intentions.

So the author states that there is a benign explanation, only to follow it up with this:

> Choose your own explanation! Each one is super-depressing

How is a benign explanation that does away with the racism <q>super depressing</q>? That explanation is surely what we would hope happened, and not have to worry about racism.

Then we come across this statement that explains everything:

> And again: Minnesota, Oregon, what the hell, guys?

Ahh&hellip; the author acknowledges that the Northern states have as much racism, but also that he doesn't want it to be true. It fits with the authors prejudices that the South is full of racists and therefore any evidence to support that will be highlighted, any evidence to the contrary (even when its his own) will be downplayed, and any evidence that doesn't fit his nice little world-view (like Minnesota and North Dakota) will be treated as outliers.

My guess: the author wants there to be racism to justify his own sense of self-righteousness.

Choose your own explanation for this authors motives, everyone I've come up with is super depressing.

&nbsp;