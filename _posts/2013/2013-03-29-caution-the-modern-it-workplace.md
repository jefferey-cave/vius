---
id: 2101
title: 'Caution: The Modern IT Workplace'
date: 2013-03-29T18:45:03+00:00
author: Kavius
layout: post
guid: http://vius.plaidsheep.ca/?p=2101
permalink: /2013/03/29/caution-the-modern-it-workplace/
categories:
  - Musings
---
Over the years, I have worked for good companies and bad, and one thing I have become suspicious of is companies that try to _sell_ a _happy_ work environment. Let's take <a title="Khan Academy - Careers" href="https://www.khanacademy.org/careers" target="_blank">Khan Academy</a> for example:

* Flexible schedules and required time off to ensure you are well-rested

Time off needs to be scheduled? I generally think that <q>time-on</q> is what is scheduled and my own hours are just that. I have never been a clock-watcher, generally my wife is dragging me from my work, but when a company indicates that it has to schedule time-off for employees so they are <q>well-rested</q>, that indicates to me that they were burning staff out. Having to set official guidelines is a sign that something is wrong. This should go without saying, why do they feel the need to say it.

* Delicious catered lunches daily

When a company feeds you it is because they expect you to never leave the office. They are supplying food, because at some point they are going to -forbid- strongly discourage you from leaving the office.

* Fun team events and board game nights!

So I'm expected to be in the office at night, after hours, rather than home with my wife and children. Now the truth about that <q>required time off</q> comes to light. Board game nights are something you do with your family&hellip; oh, wait&hellip; how very <q>The Firm</q>

&nbsp;

[Careers &#8211; Khan Academy](/media/2013/03/Careers-Khan-Academy.pdf) [PDF]

&nbsp;

&nbsp;