---
id: 18
title: DRM Small Haul
date: 2010-05-11T15:43:57+00:00
author: Kavius
layout: page
guid: http://www.vius.ca/?page_id=18
---
<http://drmsmallhaul.com/>
  
Calgary, Alberta
  
403.540.0925
  
Loam, Gravel, Mulch delivery

We were rebuilding our yard and required some topsoil to back fill a retaining wall and fill miscellaneous raised beds we had constructed. After taking all of the measurements to determine how many cubic yards to fill, we contacted DRM Small Haul. 

I do not have anyone to compare DRM Small Haul to. We have never needed someone to bring in dirt for us. Mitch had the best price, so we called him. We are very glad we did. DRM Small Haul was very helpful, their pricing was the best we could find, and, most impressively, prompt. Really, the best I can say about DRM Small Haul is that I don't have much to say, in the end dealing with DRM has been so painless that I'm don't know what I could say about their service except <q>it just works</q>.

DRM Small Haul advertises a single rate for 0-4 cubic yards of screened loam. Most other companies were charging a per yard fee, plus delivery. The delivery fee for other companies was often very close to DRM's flat fee. Due to our lack of knowledge, DRM's fee was convenient for knowing what the cost was going to be.

I was surprised by the promptness of service, I was expecting to have to schedule weeks into the future. I was pleasantly surprised when Mitch at DRM stated that the next morning at 09:00 would be most convenient for him. I was even more surprised to hear a large engine behind my house at 08:48 the next morning. A peak out my window confirmed it was DRM Small Haul with a load of loam.

The <q>small haul</q> in DRM Small Haul was very convenient. DRM Small Haul drives a **small** truck (about the size of a large pickup). The truck backed easily into my driveway, and left plenty of room for discussing exactly where I wanted it. DRM Small Haul did a great job of placing the load right on the wood I laid over the gravel driveway.

The only problem I had was after the load was dumped; I had taken extra care to make sure all my measurements were correct so I would get enough. I needed 3.5 yard of topsoil; I ordered 4 (just to be on the safe side); DRM delivered 4.5. We had a good chuckle over it, DRM tends tries to give too much rather than too little. As far as problems go, that is the kind of problem I like to have. My only warning would be to tell them which side of caution to err on (which is better: too much or too little). 

A couple of weeks later, we hired DRM Small Haul to bring a fresh load of gravel for our driveway. It was scheduled for two days later at 10:00, and sure enough at 09:57 I could hear a truck behind my house (he was already backed in).

I spoke a little with Mitch (owner/operator) after the second delivery. DRM Small Haul is a one man operation. Mitch decided that running multiple trucks and dealing with angry customers and crappy staff wasn't worth his effort any more. Instead he has decided to run a single truck himself: his customers are happier, and he is happier.