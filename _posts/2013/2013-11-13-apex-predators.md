---
id: 2143
title: Apex Predators
date: 2013-11-13T12:41:06+00:00
author: Kavius
layout: post
guid: http://vius.plaidsheep.ca/?p=2143
permalink: /2013/11/13/apex-predators/
categories:
  - Musings
---


It's funny how science fiction universes so often treat humans as a boring, default everyman species, or even the weakest and dumbest.

I want to see a sci-fi universe where we're actually considered one of the more hideous and terrifying species.

How do we know our saliva and skin oils wouldn't be ultra-corrosive to most other sapient races? What if we actually have the strongest vocal chords and can paralyse or kill the inhabitants of other worlds just by screaming at them? What if most sentient life in the universe turns out to be vegetable-line and lives in fear of us rare <q>animal</q> races who can move so quickly and chew shit up with our teeth?

Like that old story <q>they're made of meat</q>, only we're scarier.

&#8212;
  
More seriously, humans do have a number of advantages even among Terrestrial life. Our endurance, shock resistance, and ability to recover from injury is absurdly high compared to almost any other animal. We often use the phrase <q>healthy as a horse</q> to connote heartiness &#8211; but compared to a human a horse is as fragile as spun glass. There's mounting evidence that our primitive ancestors would hunt large prey simply by following it at a walking pace, without sleep or rest, until it died of exhaustion; it's called pursuit predation. Basically, we're the Terminator.

(The only other animal that can sort of keep up with us? Dogs. That's why we use them for hunting. And even then, it's only <q>sort of</q>.)

Now extrapolate that to a galaxy in which most sapient life did *not* evolve from hyper-specialised pursuit predators:

  * Our strength and speed is nothing to write home about, but we don't need to overpower or outrun you. We just need to outlast you &#8211; and by any other species' standards, we just plain don't get tired.
  * Where a simple broken leg will cause most species to go into schock and die, we can recover from virtually any injury that's not immediately fatal. Even traumatic dismemberment isn't necessarily a career-ending injury for a human.
  * We heal from injuries with extreme rapidity, recovering in weeks from wounds that would take others months or years to heal. The results aren't pretty &#8211; humans have hyperactive scar tissue, amount our other survival-oriented traits &#8211; but they're highly functional.
  * Speaking of scarring, look at our medical science. We developed surgery centuries before developing even themost rudimentary anesthetics or life support. In extermis, humans have been known to perform surgery \*on themselves\* &#8211; and survive. Thanks to our exterme heartiness, we regard as routine medical procedures what most other species would regard as inventive forms of murder. We even perform radical surgery on ourselves for purely cosmetic reasons.
  * and by god, we will eat _anything_

In essence, we'd be Space Orcs.

It's one ting to face down a cheetah, which will slam into you at 60 mph and break your neck.

It's another thing to run very quickly to get away from a thing, only to have it just kind of

_show up_

to have it be intelligent enough to figure out where you are going to be, just by the fur and feather you've left behind, your footprints and piss and shit, and then you think you've lost it, you bed down for the night, and

_there it is_

_waiting_

so you split! but it keeps following you, always in the corner of your eye, until you just can't go on, until you just can't go on any more, and give up, and

_die_

**We are scary motherfuckers**

[Originally from](http://imgur.com/gallery/ASyumRs)
