---
id: 474
title: Still Here
date: 2010-07-11T21:45:30+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=474
permalink: /2010/07/11/still-here/
categories:
  - Moving
---
We aren't dead yet.

My (Kavius's) cell phone number has changed; as of Monday, I have a job; and as of Thursday, we have phone and internet in the house. Still no furniture, but at least we are connected to the world again. For those of you who need to update phone numbers, feel free to <a title="Contact Form" href="http://www.vius.ca/contact-us/" target="_self">contact us</a>.