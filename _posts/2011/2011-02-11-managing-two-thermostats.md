---
id: 981
title: Managing Two Thermostats
date: 2011-02-11T00:00:20+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=981
permalink: /2011/02/11/managing-two-thermostats/
categories:
  - Living
---

Having a wood/oil combination furnace is a bit of a juggling act.

The idea is to burn wood: it's cheaper. Unfortunately, at some point during the night, when I am not actively tending it, it runs out of fuel and stops producing heat. This is where the Oil comes in. When the wood furnace has run out of fuel and is no longer producing heat, I want the Oil to kick in and heat the place instead. Oil is expensive, so I only want it to kick in if absolutely necessary.

And I want it comfortably warm in here.

Traditionally this has been handled by setting up two thermostats and setting the oil to a lower temperature than the wood. Basically, the oil stays off while the wood is doing its job. Once the wood burns out, the temperature drops low enough to trip the Oil. Simple. Problem solved.

Sometimes I over-think things.<a href="#f-1">1</a> As a Business Analyst, one
of the my key duties was to identify inefficiencies in systems. I'm really
good at that job.<a href="#f-2">2</a>

The way wood furnaces work is to burn and produce heat. The heat builds up in
a Heat Exchanger which, once it reaches a critical level, forces the air
through the ducts, into the house. You cannot control the heat being forced
into the house. Once the heat reaches that pre-set level, it needs to be
released from the Heat Exchanger, otherwise you risk a fire starting around
the heat exchanger. Since while the wood is burning it is producing heat, the
only way to control the heat is to prevent the wood from burning. This is done
by starving the fire of oxygen until you want the heat.

Generally, once you get the fire burning in a wood furnace, you close the door
and the fire consumes most of the oxygen in the combustion chamber, causing
the fire to go out (or at least down). When the temperature drops to the
critical level for <q>turning the furnace on</q>, all it does is activate a
blower. The blower operates like the bellows at a black smith's, forcing air
into the fire and causing the fire to burn hotter. Hot enough to heat up the
heat exchanger and cause it to force air hot air through the house.

What started niggling at me was the fact that the blower requires electricity
to operate. If the Oil is set to a lower temperature than the wood, the house
will never get warm enough to turn off the Wood Thermostat. This means that
all night, after while the furnace has run out of wood, and the oil has
started to heat the house, the little blower is blowing its little brains out
trying to get a non-existent fire burning. Why am I wasting electricity on
that crap.

Enter digital thermostats.<a href="#f-3">3</a>

Theoretically, I should be able to set the controls to indicate that the
blower should give up at some point during the night. Basically this would be
achieved by setting the wood furnace temperature to lower than the oil. Since
the oil would keep the house warmer than the activation level for the wood,
the stupid little blower would not come on.  I would only want this inversion
to take place during a time when I am reasonably sure that there will not be
wood burning (night time).

So, what is the schedule?<a href="#f-4">4</a>

<div id='line'></div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dc/3.0.8/dc.css" integrity="sha256-ACDRNg4XMj44gU4axHnGqrSOjcSnoARDUboXDFX3FiY=" crossorigin="anonymous" />
<script src="//cdnjs.cloudflare.com/ajax/libs/d3/5.7.0/d3.min.js" integrity="sha256-va1Vhe+all/yVFhzgwmwBgWMVfLHjXlNJfvsrjUBRgk=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/crossfilter/1.3.12/crossfilter.js" integrity="sha256-cIcPwh2GcosI6bCl+ZAZt2j0Sk1QfgQ1ejEpjYOfa8g=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dc/3.0.8/dc.min.js" integrity="sha256-UCCx92vLKa5ydYRIKTHnetT0AzeahxCadYRR66vWFW8=" crossorigin="anonymous"></script>
<script>
let data = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23].map(d=>{return {'time':d};});
[16,16,14,14,14,14,14,18,18,18,18,18,18,18,18,18,20,20,20,20,20,16,16,16].forEach((d,i)=>{data[i].wood = d;});
[15,16,16,16,16,16,18,18,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15].forEach((d,i)=>{data[i].oil  = d;});
[18,17,16,17,16,17,18,19,18,19,19,18,19,18,19,18,19,20,21,20,21,20,19,19].forEach((d,i)=>{data[i].house = d;});


let chart = dc.compositeChart('#line');

let mycrossfilter = crossfilter(data);
let dimTime = mycrossfilter.dimension(d=>{
    return d.time;
});
let groupWood = dimTime.group().reduceSum((d)=>{return d.wood});
let groupOil = dimTime.group().reduceSum((d)=>{return d.oil});
let groupHouse = dimTime.group().reduceSum((d)=>{return d.house});
chart
    .width(400)
    .height(300)
    .x(d3.scaleLinear().domain([0,23]))
    .y(d3.scaleLinear().domain([10,23]))
    .brushOn(false)
    .transitionDuration(500)
    .elasticY(true)
    .title('Thermostat settings')
    .yAxisLabel("Temperature")
    .xAxisLabel("Time")
    .dimension(dimTime)
    .compose([
        dc.lineChart(chart).group(groupWood),
        dc.lineChart(chart).group(groupOil),
        dc.lineChart(chart).group(groupHouse),
    ])
    ;
chart.render();
</script>

# Footnotes

<aside>
 <ol>
  <li>
   <p>
As I type this,  The Wife and I discussing removing extraneous spaces from
documents. She was giving me that look that says, <q>really? You are really
bothering with that minutia again?</q> because I started to calculate the
number of times you would have to replace double spaces with single spaces to
result in no single spaces depending on the largest number of sequential
spaces&hellip; Seriously, these are the things I ponder in my quiet time to
myself.
   </p>
  </li>
  <li>
   <p>
I'm also good and fixing the inefficiencies, but that part is easy and not
worth discussing. The solution is, once you have defined what the problem is,
<strong>JUST STOP DOING IT THAT WAY!</strong>
   </p>
  </li>
  <li>
   <p>
When you think about it, electrical thermostats are a huge waste of
electricity as well, Mercury thermostats used no electricity until they
switched on, and never broke down (requiring a replacement)
   </p>
  </li>
  <li>
   <p>
The Wife just saw the graph&hellip; she's rolling her eyes again.
   </p>
  </li>
 </ol>
</aside>
