---
id: 2158
title: Vital Statistics
date: 2014-04-10T00:04:55+00:00
author: Kavius
layout: page
guid: http://vius.plaidsheep.ca/?page_id=2158
---
Stupid random crap about me&hellip; Can you believe people care about this stuff.

This started as keeping track of stupid things like Horoscopes. Then I realized I liked taking online personality tests, and needed a place to track my results.

There are a few things on this page:

  * Horoscope
  * Myers-Briggs
  * Multiple Intelligence
  * Political Spectrum
  * IQ
  * Moral Foundations

<span id="Horoscope:_Sagicorn">

<h1>
  Horoscope: Sagicorn
</h1></span> 

<a title="Sagittarius" href="http://www.astrology.com/sagittarius-sun-sign-zodiac-signs/2-d-d-66948" target="_blank" rel="noopener"><img class="alignnone size-full wp-image-2164" src="/media/2014/04/Signs_101_sagittarius.jpg" alt="Signs_101_sagittarius" width="250" height="300" /></a> <a title="Capricorn" href="http://www.astrology.com/capricorn-sun-sign-zodiac-signs/2-d-d-66920" target="_blank" rel="noopener"><img class="alignnone size-full wp-image-2165" src="/media/2014/04/Signs_101_capricorn.jpg" alt="Signs_101_capricorn" width="250" height="300" /></a>

<h1 id="Myers-Briggs:_INTJ">Myers-Briggs: INTJ</h1>

![INTJ](/media/2014/04/INTJ-future.jpg)

(http://www.humanmetrics.com/personality/intj "INTJ")

<span id="Gardner8217s_Multiple_Intelligence">

<h1>Gardner's Multiple Intelligence</h1>

<img src="https://docs.google.com/spreadsheets/d/1EaIuV7VISXjirvN_vrY4V09MXmXqVp1I2N3Gl4J0X6w/pubchart?oid=1743063188&format=image" />

https://docs.google.com/spreadsheets/d/1EaIuV7VISXjirvN_vrY4V09MXmXqVp1I2N3Gl4J0X6w/edit?usp=sharing

<h1 id="Political_Spectrum">Political Spectrum</h1>

  * 1.88 -- Economically Liberal
  * 5.28 -- Socially Liberal

Basically, do whatever you want, just don't bug me with it!! This 
makes it very difficult for me to vote in elections since there is no 
party that even remotely represents my interests.

<img src="https://www.politicalcompass.org/charts/crowdchart?TheWife=2.88,-5.28&Me=1.88,-5.28&Pol+Pot=-10,10&Ron+Paul=9,-1&Obama=6,6&Hitler=4,10&Sanders=-4,-1&Trudeau=2,4" />

https://www.politicalcompass.org/yourpoliticalcompass?ec=1.88&soc=-5.28

<h1>IQ: +2.4 σ</h1>

Turns out I don't qualify for the [Triple Nine Society](http://www.triplenine.org/), 
though I am vain enough to test myself.

<table>
  <tr>
    <td>
      Results for:
    </td>
    
    <td>
      KaviusCave
    </td>
  </tr>
  
  <tr>
    <td>
      Questions Answered Correctly:
    </td>
    
    <td>
      24
    </td>
  </tr>
  
  <tr>
    <td>
      Total Test Questions:
    </td>
    
    <td>
      36
    </td>
  </tr>
  
  <tr>
    <td>
      Questions Not Answered:
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      Test Standard Deviation:
    </td>
    
    <td>
      15 IQ points
    </td>
  </tr>
  
  <tr>
    <td>
      IQ Score:
    </td>
    
    <td>
      136
    </td>
  </tr>
  
  <tr>
    <td>
      IQ Percentile:
    </td>
    
    <td>
      99.2
    </td>
  </tr>
</table>

[IQ Comparison Site](http://www.iqcomparisonsite.com/Results.aspx?UserID=13a38db6-44c5-4baa-bcca-a9f39ee7f95b)

<table>
  <tr>
    <td>
      <a title="True or False IQ I" href="http://alliqtests.com/tests/test_score.php?TT_ID=570949">All IQ Tests &#8211; True or False IQ Tests</a>
    </td>
    
    <td>
      15 points
    </td>
    
    <td>
      142 IQ
    </td>
  </tr>
  
  <tr>
    <td>
    </td>
    
    <td>
    </td>
  </tr>
</table>

So adjusting for the fact some of this is an online testing, we can say I probably have an IQ somewhere between 70 and 130.

<h1 id="Big5">Big Five Personality</h1>

<a href="http://www.outofservice.com/bigfive/results/?o=81,100,100&c=56,69,69&e=31,81,31&a=50,75,50&n=63,19,50&y=1970&g=m">Big Five Results</a>

<table>
 <tr>
  <td>Open-Mindeness</td>
  <td>94 percentile</td>
 </tr>
 <tr>
  <td>Conscientiousness</td>
  <td>66 percentile</td>
 </tr>
 <tr>
  <td>Extraversion</td>
  <td>34 percentile</td>
 </tr>
 <tr>
  <td>Agreeableness</td>
  <td>39 percentile</td>
 </tr>
 <tr>
  <td>Negative Emotionality</td>
  <td>46 percentile</td>
 </tr>
</table>


<h1 id="Moral_Foundations">Moral Foundations</h1>

The Moral Foundations test was developed by Haidt (I think). It assesses people's moral framework on 5 dimensions.

<https://www.yourmorals.org/>

![Moral Foundations Score](/media/2018/05/MoralFoundations.png)

<table>
  <tr>
    <td>
    </td>
    
    <td>
      Liberals
    </td>
    
    <td>
      Me
    </td>
    
    <td>
      Conservatives
    </td>
  </tr>
  
  <tr>
    <td>
      Harm
    </td>
    
    <td>
      3.7
    </td>
    
    <td>
      3.0
    </td>
    
    <td>
      3.1
    </td>
  </tr>
  
  <tr>
    <td>
      Fairness
    </td>
    
    <td>
      3.7
    </td>
    
    <td>
      3.5
    </td>
    
    <td>
      3.1
    </td>
  </tr>
  
  <tr>
    <td>
      Loyalty
    </td>
    
    <td>
      2.2
    </td>
    
    <td>
      1.0
    </td>
    
    <td>
      3.1
    </td>
  </tr>
  
  <tr>
    <td>
      Authority
    </td>
    
    <td>
      2.1
    </td>
    
    <td>
      0.5
    </td>
    
    <td>
      3.3
    </td>
  </tr>
  
  <tr>
    <td>
      Purity
    </td>
    
    <td>
      1.4
    </td>
    
    <td>
      0.8
    </td>
    
    <td>
      3.0
    </td>
  </tr>
</table>

For the record, conservatives think I'm a liberal, and liberals think I'm a conservative. This may explain why.

Nothing like not fitting into your rotten molds

<span id="Conclusion">

<h1>
  Conclusion
</h1></span> 

I'm an arrogant twat. I'm so glad we got that crap out of the way.