---
id: 1105
title: Evicting Squatters
date: 2011-02-09T17:14:50+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1105
permalink: /2011/02/09/evicting-squatters/
categories:
  - Living
---

I'm a terrible neighbour. That's why I bought a huge property, it puts a little bit of a buffer between me and my neighbours. So having some squatter come along and start building a home right next to my home really makes me angry. I've invested a lot in ensuring there is a buffer between me an my neighbours, and having some ne'er-do-well plunk himself down like he owns the joint just isn't right.

Yesterday, while gathering wood from the woodshed, I discovered a squatter. This jerk had put together a little shanty hut from my firewood, not only does he use my firewood to build his house, but he had the gall to start telling me off for tearing down his home.

<figure>
 <img src="/media/2011/02/AmericanRedSquirrel.jpg" alt="American Red Squirrel" width="300" height="199" />
 <figcaption>American Red Squirrel</figcaption>
</figure>

Your home? I bought the wood, I bought the property, and you're telling **me**
off?

I figured a bop on the nose would straighten out his attitude, but he was
faster than me and ran off. Being a quick little bastard, he always stayed
just out of reach, yelling at me the whole time.

I should have expected it mind you. I have a long history with his ilk.

One of the first times The Wife and I went camping together, we were standing
next to our stove, making breakfast when a squirrel started to walk into the
camp-site. It showed absolutely no fear, so I assumed he hadn't seen us. I
told The Wife to stay perfectly still as we watched the little guy come over
for the bacon.

He got within arms reach and started to nose at the plate of bacon. Being cute
is one thing, but getting close to the bacon is totally another: I moved and
started talking with the intent of scaring him off. He didn't move. Well, he
didn't move _away_ from the bacon. Instead he looked annoyed that I would be
disturbing his breakfast with my conversation and instead turned his back to
me.

I shouted at him with the intent of scaring him off. He gave me a brief
glance, but continued to ignore me.

<q>You cheeky little devil!</q>

Wild animals should not be that comfortable around humans, so I decided to give him an education. I picked up a stick, intent on shooing him away and giving him a good healthy wariness of humans. With a mighty battle cry sallied forth to scare the hell out of him. I slashed to the left, I jabbed to the right. The battle raged on over the picnic table and  under the picnic table; from one side, to the other. Finally, I had him right where I wanted him&hellip; up a tree.

I slowly circled the tree trying to ensure he had fled the scene. I circled the tree twice looking for him. I had seen him go behind the tree, but I hadn't seen him go up the tree. Where did he go? Then out of the corner of my eye I saw him, not two inches from my face&hellip; staring at me from around the trunk.

I quickly spun around to catch him off guard, but he had vanished. I circled to the left&hellip;. circled to the right&hellip;. he was nowhere to be seen.

At this point, The Wife had a huge smirk on her face and was holding back gales of laughter. Which ever way I turned he was always just on the other side of the tree.

<q>Vile creature! Make a fool of me, will you?</q>

Another few minutes of circling and whacking of the tree only netted me brief glimpses of his tail as he stayed just one half turn around the tree from me. Finally, with sweat pouring from my brow, my stick fell from my exhausted arm&hellip; I stood defeated and shamed.

Having proved his point, the squirrel climbed the rest of the way into the tree, and began mocking me and calling out taunts in their horrid language. I ate my breakfast in shame.

Oh yes&hellip;. I'm familiar with their ilk. Even now, the squirrel that has decided to take my home from me, sits outside my office window staring at me. Plotting his next move.

Beware the squirrel squatter, their kind has no respect for law or order, and will take your home from you  if you give them the chance. Don't let their size deceive you: these pilfering fiends are fast, feisty, and formidable.
