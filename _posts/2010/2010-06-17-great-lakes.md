---
id: 334
title: Great Lakes
date: 2010-06-17T01:46:50+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/2010/06/334/
permalink: /2010/06/17/great-lakes/
categories:
  - Uncategorized
---

The motel we stayed at was a terrible one, I would not recommend to anyone;
especially after identifying the smell in the rug as urine. Stay away from the
Westwood Motel in Ignace.

We passed a sign stating we had crossed the national watershed line around
11:00 this morning. All rivers now flow the other way: towards the Great
Lakes. Today I saw the Great Lakes for the first time. Very impressive. I
swear I was looking at the ocean, but to think all that water is fresh
water&hellip; crazy.

Tonight we are staying at the Peninsula Inn, Marathon, Ontario. Very nice
place, the owner seems nice enough too.

Not going to spend a lot of time writing tonight, instead, I'm going to work
on uploading all the backdated pictures.

<figure>
 <img width="150" height="113" src="/media/2010/06/LakeSuperior2.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/LakeSuperior4.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/LakeSuperior5.jpg" />
</figure>
<figure>
 <img width="150" height="113" src="/media/2010/06/LakeSuperiorLookout.jpg" />
</figure>
