---
id: 1289
title: 'Reports of Layton's Beatification are Premature'
date: 2011-09-02T08:59:26+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1289
permalink: /2011/09/02/laytons-beatification-is-premature/
categories:
  - "1984"
---
Canada is in mourning this week as Jack Layton was laid to rest in a state funeral. This is the first time in Canadian history that this honour has been bestowed on a Member of Parliament (that is not a Prime Minister). Canadians across the country have banded together to express their sorrow at having lost such a great man.

How quickly we forget.

Layton's party has not been popular, and Layton himself has not been a saint. Like most politicians, Layton's political career has not been without its share of <q>funny</q> business. Before we go painting a rosey picture, and running to sign-up to vote for — and donate our money to — the NDP simply because this man was a saint, let's take a moment to put some things in perspective.

<span id="Subsidized_Housing">

<h2>
  Subsidized Housing
</h2></span> 

In 1985 it was discovered that Jack Layton was living in subsidized housing. It did not take long for accusations to start flying. That a federal politician, with a substantial salary, would be living in housing designated for the poor was too good to pass up.

Denials of wrong doing were abundant, and in the end apologists wrote it off as a [smear campaign](http://www.echoweekly.com/view-story.php?id=3014 "Co-op Housing Smear Has No Merit: Layton's Opponents Sink Low with Repeated Errors") with no basis, while his opponents could point out his wrong doings but not find anything he didn't have an answer for.

Apologists state that the housing was a Co-operative, not subsidized housing. Housing costs were to be based on the income of the registered tenants of the house. Layton infact payed the higher premium associated with being a higher wage earner.

These apologists fail to account for several anomalies:

  * The housing was a co-operative, but also received subsidies to offset the costs associated with those that did not pay the higher income fees. So, in fact, the housing complex **was** a subsidized housing project.
  * Housing costs were based on the income of the registered tenant: Layton's mother-in-law. Ms. Chow is a retired Chinese immigrant, and definitely did not qualify for paying the larger fees.
  * According to the [apologists](http://www.echoweekly.com/view-story.php?id=3014), <q>Jack Layton paid full market rent when he lived in the co-op</q>. Which is technically true. Layton did pay the fees in full, for the last three months he lived there.

After five years of living in the co-operative, under his mother-in-law's name, he starts paying in full, only few months before a damning article by an investigative journalist is published? While nobody can definitively say Layton has done anything wrong, the timing is suspicious at best.

<span id="One_House_8211_Two_Ridings">

<h1>
  One House &#8211; Two Ridings
</h1></span> 

The key to the Parliamentary system in Canada is that we have one individual from our geographic location, represent our viewpoint for the country as a whole. Part of the key to this is that the individual sent needs to reside in that geographic region, otherwise they are unable to represent that region's view.<figure id="attachment_1411" style="max-width: 370px" class="wp-caption alignright">

[<img class="wp-image-1411 size-full" title="Layton Chow Ridings" src="/media/2011/09/LaytonChowRidings.png" alt="Jack Layton's and Olivia Chow's Ridings" width="370" height="352" />](/media/2011/09/LaytonChowRidings.png)<figcaption class="wp-caption-text">Layton's riding in orange and Chow's riding in red</figcaption></figure> 

Given this principle, it is very confusing to understand how Olivia Chow, and Jack Layton, could live under the same roof, but still run in different ridings. Without doing extensive research, that would likely violate Olivia Chow's right to a certain amount of privacy, I don't know which riding they lived in — Trinity-Spadina (Chow's Riding) or Toronto-Danforth (Layton's Riding).

With respect to Chow's privacy, I would like to know: did Chow live in Layton's Riding; or did Layton live in Chow's riding. Either way, there is some funny business going on. Chow and Layton claimed that their ridings overlapped, so everything was alright.

The ridings do overlap, but only in the vaguest sense (there is a whole riding between the two). Even though the two overlap, they still can't have lived in both. Legal? Maybe. Ethical?

<span id="Double_Billing_Housing_Expenses">

<h1>
  Double Billing Housing Expenses
</h1></span> 

Sending Members of Parliament from their homes to represent us in Ottawa means that they must maintain two residences: their home residence and their Ottawa residence. Given their service<sup id="rf1-1289"><a href="#fn1-1289" title="I mean &ldquo;service&rdquo; in every sarcastic way possible" rel="footnote">1</a></sup>, it is only fair that we allow MPs maintaining this second residence on behalf of their work for Canada to claim the expense on their taxes.

Jack Layton claimed the expense of his rental home in Ottawa on his taxes; as did his wife, Olivia Chow.

Both Layton and Chow were Members of Parliament, and both claimed the housing expense on their taxes. They paid the bill once, and then submitted two claims to Revenue Canada. They paid once, and claimed twice. When challenged, on the matter, Olivia Chow was **very quick** to point out that tax law does allow for it; they were not doing anything illegal.

It is true, that Parliament had never considered that a Husband and Wife pair could both be MPs simultaneously. Given the way the parlimentary system is supposed to work this was a fair, but erroneious, assumption. The tax law does allow for both of them to pay once and claim twice.

While nobody can point to anything technically illegal, the act is blatantly underhanded, and certainly unethical.

<span id="Private_Clinic">

<h1>
  Private Clinic
</h1></span> 

In the mid 90's, Jack Layton suffered from a hernia. A terrible and debilitating condition. Fortunately, he was able to quickly get himself booked in for surgery to have the hernia repaired.

Quickly? In Canada?

As a member of the NDP (the most socialist party in Canada) Layton has been cited as stating he would never have himself or his family use a private clinic. Yet Layton did in fact have his hernia repaired at a private clinic. Having the work done at a private clinic allowed Layton to receive treatment in a timely fashion, well before those in the public system would have received similar treatment.

While visiting a private clinic is not illegal, and in my mind actually helpful to the system, it is interesting that a strong advocate for socialized healthcare turns to the private system when its his own hernia on the line.<sup id="rf2-1289"><a href="#fn2-1289" title="http://www.cbc.ca/story/canadavotes2006/national/2006/01/12/layton-surgery060112.html" rel="footnote">2</a></sup>

Layton claims that he didn't know it was a private clinic, that he simply used a private clinic but paid with his public insurance. Again, it is not possible to refute Layton: I don't know what went on in his head, and I am not privy to his doctor's books. However, it has been my experience that the quality and comfort of private care is significantly better than public healthcare, it is also more expensive and not covered by your public health plan.

I find it suspicious that Mr. Layton was unable to tell the difference, had he ever used public health care?

<span id="Conclusion">

<h1>
  Conclusion
</h1></span> 

While my heart goes out to his family and friends during their time of loss (death is a tragedy when it touches our lives) but a little perspective is necessary: I did not know the man, and in all probability, neither did you.

There is no need to vilify Layton and this is not an attempt at such. This is an attempt to put his life in perspective in light of the recent emotional response to his death. The sudden glorification of his name is dishonest: the man was a career politician, he was corrupt, he was dishonest, and he was unpopular. That he died should not change any of these things.

How quickly we forget.

<span id="Update:_2012-08-30">

<h1>
  Update: 2012-08-30
</h1></span> 

[<u>Layton state funeral cost taxpayers $368K</u>: Cost of NDP leader Jack Layton's final farewell last summer was more than total cost of two former governors general](http://www.cbc.ca/news/politics/story/2012/08/30/layton-funeral-costs.html)

Legal? Maybe.

Ethical? &hellip;

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1289">
    <p>
      I mean <q>service</q> in every sarcastic way possible&nbsp;<a href="#rf1-1289" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-1289">
    <p>
      <a href="http://www.cbc.ca/story/canadavotes2006/national/2006/01/12/layton-surgery060112.html">http://www.cbc.ca/story/canadavotes2006/national/2006/01/12/layton-surgery060112.html</a>&nbsp;<a href="#rf2-1289" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
</ol>