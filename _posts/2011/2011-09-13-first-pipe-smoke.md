---
id: 1459
title: First Pipe Smoke
date: 2011-09-13T19:09:51+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1459
permalink: /2011/09/13/first-pipe-smoke/
categories:
  - Uncategorized
---
I just got my shipment of tobaccos, and lit my first pipe. I got a sampler of tobaccos from an online supplier, it came with 5 x 1oz packages of various types: Prairie Wind, Alamosa, Bakerstreet, Imagine, and Weysbridge.

I gathered up my $5 corncob pipe, some Blackberry juice, and the Praire Wind. I packed using the <a title="Frank Method of Pipe Packing" href="http://members.bellatlantic.net/~vze43wza/frank.html" target="_blank">Frank Method</a>, and lit up. It didn't go so well. I kept having to relight, but that is to be expected given I have never really done this before. After I got about half way down, I packed it a little tighter and sure enough it just started burning.

At this point, I was able to just sit back and watch the sun set.

There was a lot of smoking going on — in the future, I think I will only half-fill the bowl — but all in all, I really enjoyed the experience and am looking forward to smoking more in the future.