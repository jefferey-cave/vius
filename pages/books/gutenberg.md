---
title: GutenSearch
date: 2018-10-30T21:57:37+00:00
author: Kavius
layout: default

js:
  - "js/gutensearch.js"
---

<section>
 <button class='showhide'>&#9776;</button>
 <form is='gut-search-box' for='lib-list'>
  <label>Author</label>
  <input name='creators' type='text' />
  <label>Subject</label>
  <input name='subjects' type='text' />
 </form>
 <page-ctl for='lib-list'>
  <option>3</option>
  <option>4</option>
  <option>6</option>
  <option>12</option>
  <option>24</option>
 </page-ctl>
</section>



<main>
 <lib-list db='gutindex'>
  <h1><output name='name' /></h1>
  <ul name='creators'>
   <li><output name='name' /></li>
  </ul>
  <ul name='subjects'>
   <li><output name='.' /></li>
  </ul>
  <ul name='isAllowedForFree' title='Known Allowed Countries'>
   <li><img src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Flag_of_the_United_States.svg" alt="United States of America" width="22" height="14" /></li>
  </ul>
 </lib-list>
</main>



<section>
 <p>Available books <output id='pages'>0</output></p>
 <!--button id='reset'>Reset</button-->
 <button id='reload'>Reload</button>
</section>
