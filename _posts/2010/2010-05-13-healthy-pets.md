---
id: 37
title: Healthy Pets
date: 2010-05-13T00:00:17+00:00
author: Kavius
layout: post
categories:
  - Uncategorized
---

The Wife and I have two pets: a rabbit (Stewie) and a ferret (Chimutisk).
Today was a trip to the vet for both of them.

For the most part we do not take our pets to the vet. They get treated like
the humans for the most part and do not get to see the veterinarian unless
there is something specifically wrong. In this case I was eager to take both
in, Chimutisk for a follow up and Stew because we a check-up prior to travel
seemed like a good idea.

Overall, both passed with flying colours. The vet has commented that both are
in incredibly healthy condition for animals their age.

It makes me really happy to know that I have been able to take good care of
these animals and give them a good life. I'm proud that they are both in good
shape and healthy. I pay a lot of attention to their diet, and mental
stimulation, and I'm glad to see it has paid off.

# Stewie (The Rabbit)

The rabbit, Stew, is a 7 year old flop ear bunny.He loves running around the
house doing sprints up and down our hallway and running laps around the coffee
table.According the the vet he is in really good condition. His teeth and coat
are in excellent condition, he is strong and active, and is not a cowering
animal.

For seven, he's in awfully good shape.

# Chimutisk (The Ferret)

Unfortunately, Chimutisk was diagnosed with insulinoma two weeks ago. This is
very unfortunate, but not totally unexpected for a ferret of his age (5 years
old). He is on medication to treat his condition (which is the real reason we
went to the vet). Other than his condition, the vet is very impressed with
him: clear, inquisitive eyes; nice coat, clean teeth&hellip; a healthy
geriatric ferret in all ways (except the insulinoma)

Chimutisk's condition means he needs to have medication given twice a day,
which is a &frac12; hour process each time (he needs one drug, wait 20 minutes,
then the other). I also have a policy of feeding him a <q>snack</q> (generally
some egg) 4 times a day, just to make sure he doesn't let his blood sugar
drop. When I explained this to the vet, she was pleasantly surprised that I
was doing all of that.

For five, he's in awfully good shape.

# Keeping Contrary Pets

Yes, we have considered the problems associated with keeping both a ferret and
a rabbit, however it was somewhat unavoidable. Prior to meeting each other,
The Wife and I purchased pets for each of our households. When we began dating
we needed to assess whether or not our pets were compatible. We spent three
months introducing them, with the knowledge that if it didn't work out, she
and I would not be able to continue forward together.

Everyday, I would bring Chimutisk over to her apartment and leave his kennel
next to the couch, allowing Stew time to adjust to the smell and sniff noses
with the guy inside. Finally, we took Chimutisk out of his kennel and put him
on a leash to see how they would react to one another without bars between
them. As they touched noses, I braced to yank Chimutisk away the minute he
attacked. Suddenly, Stew jumped up, spun around and kicked Chimutisk in the
head, knocking him senseless. That was it, the ferret never wanted anything to
do with the rabbit ever again.

We continued to introduce them slowly for the next 2 months, always observed,
but the tone was set. The ferret spent the rest of his life avoiding the bunny
and the bunny has spent the rest of his life hunting the ferret. It's not how
things like this are supposed to work, but I'll take it.

One of the sweetest moments was after I had moved into  The Wife's apartment,
just prior to us buying a house together. I came home from work, and there was
Stew pressed up against the bars of Chimutisk's cage, sound asleep. Since
moving into our new house, both animals have free range of the house (while we
are home), and we think its hilarious to watch the ferret take extremely
round about routes to avoid the rabbit, or to ask to be picked up when the
rabbit is blocking the hallway.

<blockquote style="background-color: pink;border: black solid 1px">
<p>
<strong>WARNING:</strong> This is a dumb idea. Don't think you can do it.
These two species are natural enemies. Ferrets chew the heads of rabbits. Yes
this is graphic, but I don't want you thinking that this will be a cute
combination to bring into your house. It has been hard and constant work for 3
years, and that's with the help of some very good luck.
</p>
</blockquote>
