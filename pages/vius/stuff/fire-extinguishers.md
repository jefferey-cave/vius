---
id: 1701
title: Fire Extinguishers
date: 2011-12-19T11:39:42+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1701
---
The local fire department is operated by a dedicated group of volunteers; they don't get paid, and they take time out of their day to assist in case of a fire. They are hard working, they are dedicated, and they are a long way away. When I phone for assistance, the fire fighters have to leave their jobs, drive to the fire hall, load up, then drive to my property. This means I don't want to have to depend on them in the case of a fire on my property.

Living in the country means you have to be prepared to deal with the problems yourself.

We maintain fire extinguishers at several locations throughout our property: the home, the shop, and the barns. Fire extinguishers need to be located in a convenient location, and as with any emergency tools, located on the way from the emergency (not toward the emergency).

There are four types of extinguisher: A &#8211; solids, B &#8211; Liquids, C &#8211; Electrical, D &#8211; Metals. All fire extinguishers on our property are ABC type; while this is more expensive, this saves us from having to determine if we have the correct extinguisher for the job in the event of a fire. Type D extinguishers are not expected to be needed as we do not do any metal work, so there is little to no risk of igniting metals.

<span id="Inspections">

<h1>
  Inspections
</h1></span> 

It is important to ensure that Fire Extinguishers are operational **before** you have a problem. For me, this means a monthly inspection of all fire extinguishers on the property. Every month we check:

  * that the pressure gauge reads <q>full</q>
  * there are no cracks or rust on the extinguisher
  * the date stamped on the cylinder is less than 12 years ago
  * We also flip the cylinder over, and give it a shake to ensure the chemicals haven't settled in the bottom

&nbsp;