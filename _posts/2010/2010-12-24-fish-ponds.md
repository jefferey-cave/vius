---
id: 679
title: Fish Ponds
date: 2010-12-24T00:00:42+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=679
permalink: /2010/12/24/fish-ponds/
categories:
  - Living
---
Quite a few years ago, I went out with some friends hunting for Crayfish in a river near their home. After we cooked up a large number of them, I took five home, and put them in a fish tank. For the next two years, I engaged in a battle to maintain their environment by adding other fish (to feed them), snails and clams (to clean the algae), heaters to maintain temperature, filters to clean up after them, and bubblers to help them breath. It was a lot of hard work, but maintaining the environmental balance is exactly the kind of problem my mind likes to work on.

<!--more-->After about two years, I was presented with a problem. Crayfish are amphibious, territorial, and aggressive by nature: one had escaped

<sup id="rf1-679"><a href="#fn1-679" title="Actually two escaped, but my roommate found one in his bedroom. When we moved out of the house, we found the dried up husk of the other one three stories down, in the basement under the stairs among some old bags." rel="footnote">1</a></sup>, and infighting had resulted in the deaths of two others<sup id="rf2-679"><a href="#fn2-679" title="It is&nbsp;really easy to clean up&nbsp;Crayfish that die in fights: they are scavengers and will eat almost anything" rel="footnote">2</a></sup>. I had had enough, I decided right then and there that I was not going to put all of this effort into maintaining their habitat only for them to eat one another. If anyone was going to get something to eat out of this, it was going to be me. I promptly renamed the remaining Crayfish <q>Lemon</q> and <q>Garlic</q>.

They fried up very nicely.

Fish tanks are a great metaphor for what  The Wife and I are trying to achieve. They are self-sustaining ecosystems that require an effective balance to be achieved between all life-forms in the tank. Certainly, there are inputs and outputs: feeding adds food, heaters add energy, cleaning removes waste; but a really well managed fish tank reduces these interventions to a minimum, allowing the ecosystem to be self-sustaining while humans simply offer a guiding hand periodically. This is what we are trying to achieve on our property, with one significant difference: we aren’t just managing the inhabitants; we are the inhabitants.

One of the resources we have on the property is several ponds, the largest of which is right outside the house. There is a great potential for this pond to play a significant role in our ecosystem: it can offer water for livestock or irrigation; boating, swimming, skating are great recreation activities; and they encourage wild animals to stop on the property to live or just get a drink. While all of these are great reasons to maintain the pond, there is one that really grabs my attention: it is potential food producing resource. Just like an overgrown field can be turned into pasture for animals, or fields of wheat, or gardens for vegetables; a pond can be used as the equivalent of pasture for fish. This is where the metaphor of fish tanks becomes reality. Rather than maintaining a small glass fish environment in the living room, I will be maintaining a large fish environment in the yard.

There is a lot of work to do with the pond: the trees need to be cut back, the weeds need to be cleared, and some sort of aeration system needs to be put in place. Having said all of that, I'm really interested in maintaining a fish pond that can provide us with a simple meat supply with relatively little care. Hopefully some people can offer a little advice along the way (Sue, I'm specifically referring to you).

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-679">
    <p>
      Actually two escaped, but my roommate found one in his bedroom. When we moved out of the house, we found the dried up husk of the other one three stories down, in the basement under the stairs among some old bags.&nbsp;<a href="#rf1-679" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-679">
    <p>
      It is really easy to clean up Crayfish that die in fights: they are scavengers and will eat almost anything&nbsp;<a href="#rf2-679" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
</ol>