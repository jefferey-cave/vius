---
id: 1296
title: Expensive to Eat
date: 2011-08-29T08:18:25+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1296
permalink: /2011/08/29/expensive-to-eat/
categories:
  - Living
---
The other day, The Wife and I were in the grocery store. For the most part, I don't pay attention to prices, but the Wife is pretty value concious. So when she pointed out that chicken was ridiculously expensive, my first reaction was to simply shrug&hellip; I don't know a good price from a bad price. I did happen to glance at the sticker.

$14 for two chicken breasts!

I still don't have a good idea as to whether that is a good or bad price for meat in the store, but I do know I can get chicken for a lot cheaper than that.

About a month ago, we expanded our chicken flock by 7 birds; we paid about $7 per bird from an advertisement on Kijiji.

Let's break that down a little: 2 breasts @ $14 , or a whole chicken for $7. For $14 I get 2 breasts; for $7 I get two breasts, two legs, two wings. and a soup carcass.

Now, I imagine that the used layers I get off kijiji aren't going to weigh quite as much, and aren't going to be of the same plump variety, but I have to ask about a price disparity like that. How can people afford to eat?

I think I'll be cruising Kijiji for some more <q>laying hens</q> over the next week or so.