---
id: 1631
title: This Thing We Call Science
date: 2011-10-22T15:19:19+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1631
permalink: /2011/10/22/this-thing-we-call-science/
categories:
  - Opinions
---
I recently read the article <q>Independent, skeptic climate change study finds global warming is real</q> from <a title="Independent, skeptic climate change study finds global warming is real" href="http://www.zmescience.com/ecology/global-warming-study-22102011/trackback/" target="_blank">ZME Science</a>.

> So even this open source study &hellip; finds that global warming is a major issue, you would expect people to just back off and admit they were wrong, right? I mean, if I root for something, and my own people show me I’m wrong, I admit I’m wrong and that’s that. Nope, this is not the case. You can still expect people to deny climate change in an almost pathological fashion, you can still expect people to ignore the evidence and we still have a lot to wait for real, global measures against global warming to take place. This is the wonderful world we live in; hopefully, by the time people are convinced, it won’t be too late.

This is some fairly charged rhetoric for a <q>science</q> &#8216;zine! Language such as <q>pathological fashion</q> are clearly ad hominum attacks on skeptics.

<!--more-->

<span id="Research_Problems">

<h1>
  Research Problems
</h1></span> 

The most interesting part of the cited research is that it was done in the open, something that is most often lacking in modern research. Modern researchers have a habit of hiding their studies, and data, stating that laymen are unqualified to understand the underlying processes. Allow me to translate: you are to stupid, we will think for you. I applaud this research not for its findings, but instead for its publishing techniques.

While the research studied cited is an interesting one, it is not for the definitiveness of their study. Sceptics have long been questioning the validity of studying short periods of time relative to the lifespan of the earth, and declaring trends. It is important to recognize that the indication of a short-term warming trend is not indicative of a longer pattern. Effectively, this is a blip on the readouts when looking at long term trends. This does not mean that there isn't a problem, or not a problem, it just becomes data that is suggestive.

<span style="color: #000000;font-size: 31px;line-height: 46px">Ad Hominum Attack</span>

I think linking to Al Gore's<sup id="rf1-1631"><a href="#fn1-1631" title="Personally, the minute I see a reference to anything Gore has said, it is a clear sign that the author is striving for drama, rather than clear thinking" rel="footnote">1</a></sup> <q>racism</q> statements may be the most blatant example of this considering his statements well predate this paper being published.

I don't think anyone is denying that Global Warming<sup id="rf2-1631"><a href="#fn2-1631" title="Are we calling it &ldquo;Global Warming&rdquo;, or are we still unsure which way the temperature is trending, and calling it &ldquo;Climate Change&rdquo;?" rel="footnote">2</a></sup> is occuring, I think the real question is whether it is Anthropogenic, or a Natural process. The sceptics that I am aware of are not questioning that it is happening, but instead <q>why it is happening</q>, and <q>what to do about it</q>. The first question regards fundamental cause: Al Gore's political campaigns aside, there is still a great deal of debate as to the cause of the warming trends. Is this cause by humans? Does this correspond to the natural rythms of the earth? Is this relate to solar activity? Has God just decided to turn up the heat?

As a friend is often quick to remind me: correlation is not causation.

<span id="Why_it_Matters">

<h1>
  Why it Matters
</h1></span> 

Without clear thinking on the subject, and basing our analysis on political agendas and fear mongering, we will end up with absurd conclusions. For example:

  1. Climate Change is bad;
  2. CO<sub>2</sub> emissions cause Climate Change, therefore they are bad;
  3. Wind Power doesn't release CO<sub>2</sub>, therefore it is good;
  4. Therefore, Climate Change caused by Wind Power is potentially good.<sup id="rf3-1631"><a href="#fn3-1631" title="MIT researchers concluded that the climate change created by Wind Turbines was good climate change" rel="footnote">3</a></sup>

The sweetest irony of this ludicrous reasoning is that it has traditionally been the argument used by many skeptics (myself included): Global Warming is happening, and thank goodness, this will allow us to turn Siberia and Northern Canada (the largest land masses) into food producing regions, thereby ending world hunger.

In all potential causes of Global Warming, only the _Man-Made Global Warming_ calls for _man-made action_, all other cases beg the question of whether or not we should be attempting to initiate <q>Man-Made Climate Change</q>? In the end, until we get rid of the politics, and focus on the science, we are far more likely to do more harm than good by meddling. The author, as a traditional fear monger, would have us _do something_ to end Global Warming; I would hope we don't do _something_, but instead do the _correct thing_.

Here ends my RSS subscription to ZME Science.

<span id="Update_2011-10-23:_On_Sceptics">

<h2>
  Update 2011-10-23: On Sceptics
</h2></span> 

In need to clear something up: I am not an expert on Climate Change, nor do I ever expect to be. Instead, I am a defender of sceptics.

In hindsight, it occurs to me that _science_ and _scepticism_ go hand in hand: you can not have science without scepticism; scepticism is the very definition of science. The scientific process is really just defined as someone coming up with a zany idea, and people keep trying to poke holes in the idea. These sceptics that critisize the ideas and try to tear them apart are engaged in science. Science is formal scepticism.

In the end, we begin to accept scientific ideas after they have resisted the evidence presented by sceptics. The more the idea is able to resist criticism, the more acceptable the idea becomes.<sup id="rf4-1631"><a href="#fn4-1631" title="This has its roots in Socrates/Aristotle (can&rsquo;t remember which) and the Dialectics, but has come a long way since." rel="footnote">4</a></sup> In the end, nothing can be proved, only disproved (or not as the case may be).

Proponents of the scientific method _must never_ criticize scepticism: **when scientists stop being sceptical, they are no longer engaging in science**.

&nbsp;

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1631">
    <p>
      Personally, the minute I see a reference to anything Gore has said, it is a clear sign that the author is striving for drama, rather than clear thinking&nbsp;<a href="#rf1-1631" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-1631">
    <p>
      Are we calling it <q>Global Warming</q>, or are we still unsure which way the temperature is trending, and calling it <q>Climate Change</q>?&nbsp;<a href="#rf2-1631" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn3-1631">
    <p>
      <a href="//insciences.org/article.php?article_id=8523)" target="_blank">MIT researchers</a> concluded that the climate change created by Wind Turbines was good climate change&nbsp;<a href="#rf3-1631" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn4-1631">
    <p>
      This has its roots in Socrates/Aristotle (can't remember which) and the Dialectics, but has come a long way since.&nbsp;<a href="#rf4-1631" class="backlink" title="Jump back to footnote 4 in the text.">&#8617;</a>
    </p>
  </li>
</ol>