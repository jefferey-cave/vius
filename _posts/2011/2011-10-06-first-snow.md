---
id: 1572
title: First Snow
date: 2011-10-06T18:19:57+00:00
author: Kavius
layout: post
guid: http://vius.ca/?p=1572
permalink: /2011/10/06/first-snow/
categories:
  - Uncategorized
---
A friend came over for a visit today and as we were all sitting around having coffee and cookies, my jaw dropped. The ladies looked at me and asked what I was staring at: snow. There was a fine mist of hail coming down. 

The air had been extra frosty all day, when I went out for my lunch smoke, I couldn't tell if I was smoking or just had steamy breath. Sure enough it started to snow around 13:00. Made for a fine visiting with company fire, and a good excuse to get the new wood stove fired up.

Snowing outside, good company inside, everybody gathered around a roaring fire; you would think it was Christmas.