---
id: 1925
title: My Email to Peggy Nash
date: 2012-05-17T19:06:48+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1925
permalink: /2012/05/17/email-to-peggy-nash/
categories:
  - Opinions
---
I just sent the following email

`To:   Peggy Nash<br />
CC:   Jim Flaherty<br />
Subject: Statements to Flaherty`

I recently read an article on CBC regarding a debate between yourself and Flaherty, in which you stated:

> If you are a computer software developer, will you be working at Tim Hortons? If you are an unemployed teacher or nurse, will you be working in the agricultural sector picking fruit?

Ironically, my personal life has taken on many elements of the examples used in this debate. I am an experienced Software Developer that has moved across the country to find work. , I am currently employed as a farm labourer, because Tim Hortons would not return my calls after my second interview.

For 10 years I worked as a system developer for various industries, finally culminating as a business analyst increasing efficiency for several companies. At this time, I am currently employed as a Farm Labourer (an apple farm) in a small Nova Scotia town. I had applied for a job at Tim Horton's, but they won't hire me. I have applied at almost every company in town, and the only one that would accept me was the local farmer, looking for someone to pick apples. Let's face it, if people don't need Software Developers or teachers, then they need to go find employment elsewhere; sometimes that will mean picking apples, sometimes that will mean serving donuts.

All you have indicated to me, is that you view those of us picking apples as worth less as people than those who don't. An odd statement for an NDP minister to make.

Interestingly, the job at Tim Horton's went to a woman who had just immigrated from England and the job I had been applying for at Home Hardware went to her husband. It isn't about having to take a job that pays less, or one outside your field: its about having a job at all.

`<br />
################################<br />
### Redacted Signature block ###<br />
################################<br />
`

PS. I don't know what the answer is, but I will tell you this: I have closed down my home-based company, after 3 years of offering web development and hosting. I no find it worthwhile to pay the fees associated with keeping a business name. Granted, this comes about because of a move to Nova Scotia where you are charged annually just for having a business (It was a, nominal, one time fee in Alberta).

The whole situation is a farce to me: the Government is debating \*my\* life; mean while a Government agency (granted it is provincial, not federal, in this case) has made it difficult for me to earn a little income on the side; followed by a government official denigrating those who choose to do the work I am using to keep myself off EI.

&#8212;

<span id="Response:_2012-05-30">

<h1>
  Response: 2012-05-30
</h1></span>

`<br />
From: Peggy.Nash@parl.gc.ca<br />
Sent: Wednesday, May 30, 2012 1:55 PM<br />
To:   #######@##########.###<br />
Subject: RE: Statements to Flaherty<br />
`

Dear Mr. #######,

Thank you for taking the time to write. Please allow me to clarify what I am trying to emphasize.

There is certainly nothing wrong with performing manual labour, whether it be roofing, carpentry, agriculture, or anything else. And it certainly does not matter if someone works at Tim Horton’s. These hard-working Canadians are the very people that fuel our economy.

Indeed, the NDP has fought long and hard to increase the minimum wage so that people who work in these industries can have a wage that properly reflects the difficulty of their jobs and so that they are properly compensated for their work.

The point that I am trying to convey is that we need our nurses in our hospitals, and we need our welders to weld, we don’t need them to be forced to work jobs that are well outside what they are specially trained to do. People who are highly trained and have specialized skills should be working in their field, especially when we have shortages in these fields. In your particular case, while i emphasize that there is no shame in the good work that you are doing, it is important that someone with your advanced skill set cannot work in your specialized field.

Forcing people to take any job, as the Conservatives seem to be doing, will only make it more difficult for employers to find the skilled employees that they need, and it will make it more difficult for employees to find a job in their field. Also, in the case of teachers and nurses, decisions like this will have the potential to impact public services that Canadians rely on.

Thank you for taking the time to write to me with your concerns.

Sincerely,

Peggy Nash

Member of Parliament | Parkdale &#8211; High Park

&#8212;

<span id="My_Response:_2012-06-11">

<h1>
  My Response: 2012-06-11
</h1></span>

I apologize for the delayed response, I had family visiting from overseas and out of province.

You seem to have missed some of the more important points in my original statement.

> the NDP has fought long and hard to increase the minimum wage

Unfortunately, I believe this is why I cannot find work. I do not require $10.15/hour (minimum wage in Nova Scotia), but I do require something. Most employers will not hire me because I lack experience and they can get experienced workers for minimum wage. I have actually run the numbers on this, if I could find a steady job at ~$6/hour (excluding taxes), my wife and I would be alright. Instead, I have farm labour work, when/if I can find it.

As of a few weeks ago, I am unemployed again. There was not enough work to go around, and I am the least experienced.

Minimum wage is part of the reason I am finding it difficult (impossible?) to find employment.

> it is important that someone with your advanced skill set cannot work in your specialized field

I should emphasize that it is by choice that I no longer work in my field. In my region, Business Analysts get paid minimum wage (that was for the local government ESL program). Until they value BAs higher in this region, I would rather not have the responsibilities, overtime, and stress, associated with that type of work (I informed them that $80K-$100K is standard, I would do it for $40K, I didn’t hear back from them). I would rather pick apples and leave my work at home at the end of the day.

Both your’s and my comments are filled with irony:

  * the government funds programs to teach ESL, but that program will not pay a fair wage for services
  * its a couple of immigrants that got the minimum wage jobs I wanted (nice couple, I don’t fault them)
  * I would be willing to do those jobs for less than minimum
  * government agencies are giving grants to get employment in my region
  * various government agencies have made it impossible to be self-employed (taxes and fees)

Something is wrong, I don’t have the answers, and based on your response, I am confident you don’t have the answers either. Please, stop helping me.
