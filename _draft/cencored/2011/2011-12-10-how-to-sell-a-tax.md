---
id: 1683
title: How to sell a tax
date: 2011-12-10T00:00:41+00:00
author: Jeff
layout: post
guid: http://vius.ca/?p=1683
permalink: /2011/12/10/how-to-sell-a-tax/
categories:
  - Uncategorized
---
Nova Scotia is run pretty much on the Black Market. While the province is considered to be poor because little economic activity occurs, it is difficult to say how true that is since most of the economic activity occurs off the books. With the high regulatory and tax burden, much of the economic activity that does take place is never recorded in the hopes that the government will not learn of it. Its hard to trust the government numbers when most people here try to keep the government from finding out the truth.

<!--more-->

In light of pondering the economic reality in Nova Scota, I came across an article discussing an effort to <a title="Charge For Cash Transactions Proposed" href="http://jp.dk/uknews/article2629547.ece" target="_blank">tax cash transactions in Denmark</a>.

The banks are proposing it as a means to encourage individuals to use electronic transactions rather than cash transactions. Their belief is that on net, it is more expensive for society to use cash than it is to use digital currencies. This is possibly true, but ignores the individual's preference for cash: the banks charge a fee for electronic transactions, cash is free to the consumer.<sup id="rf1-1683"><a href="#fn1-1683" title="There are also silly arguments like the health benefits of people not touching the germs left on the money. Also, dealing in cash for small purchases, or for transactions among individuals (like borrowing $20 from a friend) is much easier in cash." rel="footnote">1</a></sup> This is a classic big business maneuver, rather than making your product (in this case, the ease and security of electronic transactions) more competitive, get the government to hurt the competition (in this case tax cash transactions).

Unfortunately for the individual, it is likely that the government of Denmark will be easily sold on this idea. While the banks are calling it a <q>fee</q>,  the reality of such a system would dictate it being a <q>tax</q>. Since the banks do not have control over transactions outside their electronic systems, it would be up to the government and tax bureau to manage such a fee system. Let me sum up the arguements for you: Government of Denmark, for the greater good of the people of Denmark, take money from the people and put it in your pocket.

I have a feeling the tax will be implemented. Especially in light of the black market activity I see around me every day.

I am fairly confident that the Government of Nova Scotia would love an excuse to push cash out of the system. An excuse like the one presented to the Government of Denmark is just what they would need to make cash illegal (or at least more restricted). Transactions that are not recorded are usually handled with cash. Use credit, debit, or cheque, and the banks will be forced to tell the government, cash is necessary for keeping transactions <q>off the books</q>.<sup id="rf2-1683"><a href="#fn2-1683" title="Banks are required to share all transactions with the Government" rel="footnote">2</a></sup>

Many people will see good in this (everyone should be paying their share), most people in my area will be horrified by the idea (they'll do anything to hold on to the precious dollars they have). Either way I suppose it doesn't matter, what is important is that if people are going to discuss making cash less legal, can we please be open about why we are doing it. It is not about protecting people from the common cold, its about increasing the profits of banks, and (rightly or wrongly) ensuring people cannot keep their money from the government.

<span id="Post-Analysis">

<h1>
  Post-Analysis
</h1></span> 

The Wife and I were discussing this article over breakfast, and concluded that if cash were difficult to come by, the local economy would shift to a barter system.<sup id="rf3-1683"><a href="#fn3-1683" title="It&rsquo;s almost there already" rel="footnote">3</a></sup> This led us to the conclusion that some commodity based money would evolve out of that barter. For a time we speculated what medium people would use for exchange (now would be the time to corner that commodity) and toyed with the idea of a local trade currency.

We feel that private non-commodity currencies (AKA: fiat currency) would fail due to a few scenarios. The currency could not be centrally managed digitally because this would leave a paper trail, exactly what the people using it are trying to avoid. Alternately, hyper-inflasion caused by one of a few scenarios: either the makers of the currency would produce too much of the currency (to line their own pockets with goods), or counter-feighters would copy the currency (and since both groups are engaging in illegal activity, there is no legal recourse for dispute resolution).

Basically, there are many problems with fiat currencies, and private fiat currencies suffer from all the same problems as government fiat currencies.

I came home to an article entitled <a title="Greece's Emerging Barter Economy" href="http://www.mises.ca/posts/articles/greeces-emerging-barter-economy/" target="_blank">Greece's Emerging Barter Economy</a> which states that they have a Barter Market which will use a fiat currency called a TEM that will be worth one Euro, but its not money&hellip; its barter.

Palm, meet Forehead.

&nbsp;

<hr class="footnotes" />

<ol class="footnotes">
  <li id="fn1-1683">
    <p>
      There are also silly arguments like the health benefits of people not touching the germs left on the money. Also, dealing in cash for small purchases, or for transactions among individuals (like borrowing $20 from a friend) is much easier in cash.&nbsp;<a href="#rf1-1683" class="backlink" title="Jump back to footnote 1 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn2-1683">
    <p>
      Banks are required to share all transactions with the Government&nbsp;<a href="#rf2-1683" class="backlink" title="Jump back to footnote 2 in the text.">&#8617;</a>
    </p>
  </li>
  
  <li id="fn3-1683">
    <p>
      It's almost there already&nbsp;<a href="#rf3-1683" class="backlink" title="Jump back to footnote 3 in the text.">&#8617;</a>
    </p>
  </li>
</ol>