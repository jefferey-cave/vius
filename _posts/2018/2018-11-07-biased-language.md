---
id: 
title: Biasing Language
date: 2018-11-07
author: Kavius
layout: post
categories:
tags:
  - "1984"
  - "opinion"

---

<style>
    details {
        display:inline;
        width:50%;
        float:right;
        border:1px solid black;
    }
</style>

Yesterday, the US Midterm elections occured.

Skimming the headlines of my RSS feed, I came across this statement:

> The energy and outrage of the Democratic resistance faced off against the 
> brute strength of U.S. President Donald Trump's GOP<a href='#f-1'>1</a>

Currently, I am living by a news reading rule: `I do not read news that 
mentions a name in the title`; so I have not read this article. However, I was 
struck by the leading nature of the language.

# Grade 6: Ethics of Language

<p>
One of the traumatic asignments of my school years took place in Grade 6. We 
were asked to write a headline for a local event: it was a win by a local 
sports team.<a href='#f-1'>1</a> Part of the assignment was to create a 
headline that enticed the reader to continue on: we were to use Pathos to 
inspire the reader to carry on.
</p>


As part of the assignment, we were told that we were using a sports event 
because they are held to a lower standard than other articles: in most news 
articles you are to remain impartial, and ensure you are not unduely 
influencing your reader. In our sports article, we were attempting to create 
*exciting* language that would generate interest, however we were not to state 
which team was the prefered team, that is to be left to the reader. Some 
readers will be disapointed by the Flames having won, and some will be 
excited, and that's OK.

Recently, I attempted to pass this same understanding on to a class I was 
teaching in "Data User Experience". The idea of the class is that collecting 
and collating data is one thing, but communicating it out ot audiences is more 
challenging. As one of the modules of the class, we discussed the proper use 
of Rhetoric as a means to help people to understand the information being 
presented, as well as the "weaponization" of Rhetoric in which it can be used 
to *make people think what you want them to*.

This is an excellent example of using rhetoric to lead your audience.

# Charged Language

This headline is a fascinating example of Pathos at work in a headline. In 
just the first 20 words the author has densly packed a remarkable amount of 
activity.

<style>
:root{
    --rHigh: darkred;
    --rLow: salmon;
    --rContrast: white;
    --dHigh: blue;
    --dLow: lightblue;
    --dContrast: white;
}
.repub{
    background:var(--rLow);
    color:var(--rContrast);
    border-radius:0.3em;
    padding:0.1em;
}
.dem{
    background:var(--dLow);
    color:var(--dContrast);
    border-radius:0.3em;
    padding:0.1em;
}
main meter{
	width:80%;
	display:block;
}
</style>

The sentence describes a conflict between two parties, however, rather than 
simply identifying the groups involved, a lot of the words used are placed to 
tell the reader which group they are to like. 

The emotive bias being attached can be demonstrated by rephrasing the sentence 
to a purely factual phrasing, using a single named identifier for each group:

<blockquote id='compare'>
<style>
#compare{
	overflow:ellipse;
}
#compare span{
	position:absolute;
	display:inline;
	letter-spacing:0;
	/* white-space: nowrap; */
	opacity:1;
	transition:opacity 1s;
}
#compare span.hid{
	position:absolute;
	display:inline;
	opacity:0.1;
	transition:opacity 1s;
}
</style>
<meter value='50' min='0' max='100'></meter>
the <br />
<span class='dem hid'>energy and outrage of the Democratic resistance</span><span class='dem'>Democrats</span> <br />
faced off against the <br />
<span class='repub hid'>brute strength of U.S. President Donald Trump's GOP</span><span class='repub'>Republicans</span><br />

</blockquote>
<script>
setInterval(()=>{
	let compArea = document.querySelector('#compare');
	let meter = compArea.querySelector('meter');
	let value = 0;
	let exclude = 0;
	Array.from(compArea.querySelectorAll('span'))
		.forEach((elem)=>{
			let hidden = Array.from(elem.classList).some(c=>{return c === 'hid';});
			if(hidden){
				elem.classList.remove('hid');
				value += elem.innerText.split(' ').length;
			}
			else{
				elem.classList.add('hid');
			}
		});
	meter.value = value;
	meter.max = compArea.innerText.split(' ').length;
},3000);
</script>

By maintaining impartiality, we reduce the statement from 20 words to 7; a 65% reduction in text. 65% of the text was dedicated to emotive influence.

<meter value='20' max='20'></meter>
<meter value='7' max='20'></meter>


# Breakdown

The chosen phrases are emotive in nature and have either positive or negative in connotation. Further, the elements are reinforced via repitition: once for the Democrats, and once for the Republicans. We can see that the emotive elements are paired off to ensure that the correct emotive attitude is demontrated for each group.

> The 
> <span class='dem' data-elem='1'>energy and outrage</span> 
> of the 
> <span class='dem' data-elem='2'>Democratic</span> 
> <span class='dem' data-elem='3'>resistance</span> 
> faced off against the 
> <span class='repub' data-elem='1'>brute strength</span> 
> of 
> <span class='repub' data-elem='3'>U.S. President</span> 
> <span class='repub' data-elem='2'>Donald Trump</span>'s GOP

| Democrat | Republican | Connotation |
| :--- | :--- | :--- |
| energy and outrage |  brute strength | working vs force |
| Democratic | Donald Trump | will of the people vs single individual |
| resistance | U.S. President | standing up to powerful people |

<script>
(function(){
	let table = document.querySelector("table:last-of-type > tbody");
	
	function selectElement(tr){
		tr = tr.currentTarget;
		let index = tr.rowIndex || tr.dataset.elem;
		
		tr = table.rows[index-1];
		tr.addEventListener('mouseout',deselectElement);
		tr.removeEventListener('mouseover',selectElement);
		tr.style.fontWeight = 'bold';
		document.querySelectorAll("span[data-elem='"+index+"']").forEach((item)=>{
			item.style.opacity = 1;
			item.addEventListener('mouseout',deselectElement);
			item.removeEventListener('mouseover',selectElement);
		});
		
	}
	function deselectElement(tr){
		tr = tr.currentTarget;
		let index = tr.rowIndex || tr.dataset.elem;
		tr = table.rows[index-1];
		tr.removeEventListener('mouseout',deselectElement);
		tr.addEventListener('mouseover',selectElement);
		let item = document.querySelector("span[data-elem='"+index+"']");
		tr.style.fontWeight = 'normal';
		document.querySelectorAll("span[data-elem='"+index+"']").forEach((item)=>{
			item.style.opacity = 0.5;
			item.removeEventListener('mouseout',deselectElement);
			item.addEventListener('mouseover',selectElement);
		});
	}
	
	Array.from(table.querySelectorAll('tr')).forEach((tr)=>{
		deselectElement({currentTarget:tr});
	});
})();
</script>

Each of these mirrored elements creates contrasting emotive responses which are 
then ascribed to the targetted group.

## Work Ethic

The words "energy" and "outrage" both carry positive connotations. *Energy* 
speaks to activity, some who is energetic in their endevours, while *outrage* 
at an unjust action is socially expected.

This can be contrasted with "brute" and "force": *brute* expresses one who 
acts without care, and *force* implies an action that is done without the will 
of those it is being imposed on. Together, the term "brute force" implies a 
forced action that is performed without consent, and is done without any 
subtley or thought to concequences.

## Demos

Democratic is a word that comes from the greek "demos" meaning "people". This implies that this is the will of "The People", which would include the reader. Something which is willed by many is (by implication) something that must be supperior to the will of the individual "Donald Trump".

## Resistance

People love an underdog, and "resistance" fighters are therefore idealized by 
in history, mythos, and literature. In order for us to have a hero, the hero 
first needs a weekness to overcome.<a href='#f-3'>3</a> 

Whether that is admiring the rebels in Star Wars, the Taliban resisting the 
Soviet Union, or students standing against a teacher, resisting figures of 
authority is always welcomed.

While "US President" does not normally carry negative conotations in the US, 
every "resistance" needs an authority figure: the Emperor in Star Wars, the 
Communist Party in Afghanistan, and Teachers. In the world, there is no 
gereater authority figure than The President, and therefore is the perfect 
authority figure for any resistance.

# Conclusion

Students of rhetoric would do well to study this headline. The introduced 
phrasing to create an emotive response, guides the reader to the desired 
conclusion. Further the use of repition is subtley used so as to re-inforce, 
without being obvious.

---

# Footnotes

1. The article itself has has since been altered to reflect the actual 
   results, but the original RSS entry was: 
   
   > [**Democrats leading in some key House races, early midterm results show**](https://www.cbc.ca/news/world/midterms-trump-results-1.4894601?cmp=rss)
   > 
   > CBC - Top Stories RSS, 2018-11-06 21:36
   > 
   > The energy and outrage of the Democratic resistance faced off against the 
   > brute strength of U.S. President Donald Trump's GOP in a fight for 
   > control of Congress and statehouses across the nation
   
2. I will never forget the assignment because the Calgary Flames, had beaten the Edmonton Oilers, and I had written a headline that played on the chemical nature of oil and its combustable nature. My teacher informed me that oil is not combustible.
3. Vladmir Propp?