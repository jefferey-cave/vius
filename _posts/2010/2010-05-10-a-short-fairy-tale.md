---
id: 49
title: A (short) Fairy Tale
date: 2010-05-10T01:36:04+00:00
author: The Wife
layout: post
categories:
  - HomeSteading

---

Once upon a time there was a princess who lived with her prince in a nice
little home in the big city. They worked hard together; making their house a
home and building a small garden. After a while the prince & princess yearned
for their own kingdom; someplace they could feel free and fend for themselves.
The prince had dreams of chickens and goats; the princess of creating
something out of nothing.

As time plodded on, their enemy (of the insidious kind), watched and waited.
The prince and princess were getting more worried by the day so they set off
in search of a kingdom close by, somewhere to call their own. Unfortunately
everywhere they looked it was either too cold, too dry, too barren or just
plain too expensive.

One day the princess said to her prince <q>Why don’t we try Nova Scotia?</q>
<q>It might just be the perfect place for people like you and me</q>.

So here and now they start their journey. Their house has been sold,and soon
they will set out in their carriage for a magical place in the country where
the chickens lay golden eggs and the goats spin flax into….wait a minute
that’s another story..

~The Wife
