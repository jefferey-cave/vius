---
author: Jeff
title: Wind Power
slug: wind-power
id: 1400
date: '2011-09-08 20:32:31'
layout: draft
categories:
  - Opinions
---

Yesterday, The Wife and I went to a meeting involving the Annapolis County Wind Power Advisory Committee. Over the last two years, the Advisory Committee has been drafting a by-law to deal with proposed construction of Wind Turbines in the county. These Turbines would be operated by a company that sold the power to Nova Scotia Power (a public utility company), and the land that the towers are to be built on would be leased from private land-owners. Now that the by-law is nearly complete, and is nearly ready for public review, a single contentious issue has become key. The question revolves around gaurantees that the wind turbines will not be abandonded. The company constructing the turbines estimates that each turbine will have a life span of 25 years, councile and members of the public are concerned that in 25 years, there won't bew anyone around to dismantle the turbine. Who bears ultimate responsibility, what is the risk, and how do we ensure it does not become a public problem? The current proposed solution, is to have the builder of the turbine post a bond. This bond would be held against the dismantling of the turbine, and in the event that the turbine was not dismantled by its owners, the bond would off-set the costs of removal. While I had intended to speak, it quickly became obvious that what was being discussed had little to do with the advertised subject of the meeting, something else had happened. There were snide remarks flying back and forth between various members of the public, and various council members. I have never invested in Wind Power. I believe Wind Power to be a very poor means of generating power with no hope of acheiving its objectives. Wind Power Projects will fail. It is not a question of if they will fail, but when they will fail. For these reasons, I have personally made every attempt to ensure that my personal dollars never go near a wind-turbine.

# Climate Change

# Net Energy Consumers

# Not Profitable

# Government Financing

# Liability

# Conclusion

I personally am opposed to wind-power, it is neither feasible, nor ethical. For this reason, I have made every attempt to ensure I do not invest in its development. I certainly do not want to find myself paying for the clean-up required in the aftermath of such a poorly thought out project. While bonds offer some protection against Annapolis County residents investing in a project against their will, it does not come without its own risks. Personally, I am opposed to putting barriers to entry for businesses. As a business owner, I have found enough of those in this community already. While the chain of liability seems clear to me, I am not a lawyer, and if there has been some oversight that allows the shifting of responsibility, then some measures will be required to ensure that the liability is not left to the community as a whole, but instead stays with those who wish to make these investments in the first place. Really, the issue of whether for force a bond or not is contentious because it is a band-aid solution. It is contentios because it does not address the underlying issue that it is mearly  covering over: the fact that the chain of liability can be broken. If the posting of bonds is the only way to protect the residents of Annapolis County, it is indicative of a serious fundamental problem underlying our judicial system. If this is the only way to protect residents, then the issue of bonds is a straw man arguement distracting us from the real issue: is the real liability, facing Nova Scotians, The Government of Nova Scotia Inc. PS: The meeting left me in a bit of a quandary: while I agree with his stance on the issue, Councilman McWade needs to be removed from office at the earliest possible moment.