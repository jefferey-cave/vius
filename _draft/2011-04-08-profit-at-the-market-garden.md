---
author: Jeff
title: Profit at the Market (Garden)
slug: profit-at-the-market-garden
id: 1202
date: '2011-04-08 07:28:18'
layout: draft
categories:
  - Uncategorized
---

[http://blog.mises.org/16401/capitalism-at-the-farm-stand/comment-page-1/#comment-770927](http://blog.mises.org/16401/capitalism-at-the-farm-stand/comment-page-1/#comment-770927) I have been discussing these issues with several people who are looking to operate market gardens. The more I speak with this group, the more I’ve come to realize that they aren’t opposed to “capitalism”, but are opposed to “crony-ism”. They like the idea of _small_ entrepreneurs, but object to _giant corporations_; the biggest differentiation being the use of unfair competitive practices (government lobbying and the resulting legislation). Unfortunately, I have yet to meet anyone who understands where they stand on the matter; as the article states, they are “muddled” in their thinking. In their mind, crony-ism is synonymous with capitalism. This confusion leads to all kinds of excuses and exceptions and in the end continued confused thinking; all of the exceptions means that people don’t know where they actually stand, where to actually draw the line. What I find most frustrating is that the word “profit” is forbidden. Every time one of them starts to talk about the fact that they are going to sell food, they justify it with the statement that they are are going to “make just enough”; not a “real profit”. Sometimes I think that if I could just break that nut, I could get some consistent thought from people around here. Then again, I may be dreaming. Every one of them is working hard to get government subsidy, or is at least vocal that we need more of it. I’d settle for less “help” from the local governments… they seem to be helping me to death.