'use strict';

/*
global fetch
global pako
*/

importScripts('/assetts/js/libs/pako.js');
importScripts('/assetts/js/vius/untar.js');


onmessage = function(e) {
	switch(e.data.cmd.toLowerCase()){
		case "start":
			start(e.data.config);
			break;
		case "stop":
			Terminate();
			break;
		case "pause":
			Pause();
			break;
		case "continue":
			Continue(true);
			break;
		case "status":
			postMessage(state.status);
			break;
		default:
			postMessage({type:'error',value:e.data.cmd,message:'unrecognized message command'});
			break;
	}
};

let state= {};
const stm = {
	//Zip : new pako.Inflate({to:'string'}),
	Zip : new pako.Inflate(),
	File : new FileReader(),
	Tar : new untar(),
	Blob: {
		BSIZE: 10240,//*1024,
		Z_SYNC_FLUSH: 2,
		pos: 0,
		timer:null,
	}
};



Initialize();

function Initialize(){
	state = {
		stm:stm,
		buffer: {
			chunks:[],
			elements:[],
		},
		status: {
			phase: 'init',
			progress:{
				value:0,
				max:0
			},
			records:{
				reads:0,
				saves:0,
			},
		},
		awaits:[]
	};
}

function Terminate(){
	post('complete');
	close();
}


function start(config){
	Initialize();
	state.config = config;
	syncCatalogue();
}


function Pause(){
	if(!stm.Blob.timer) return;
	state.status.phase = 'paused';
	clearTimeout(stm.Blob.timer);
	stm.Blob.timer = false;
}


function taskAdd(qty = 1){
	state.status.progress.max += qty;
	post('taskadd',qty);
}

function taskDone(qty = 1){
	state.status.progress.value += qty;
	post('taskdone',qty);
}

function post(type,value=null){
	let msg = {
		type:type,
		value:value,
		status:state.status,
	};
	postMessage(msg);
}

function Continue(force = false){
	if(stm.Blob.timer === false && !force){
		return;
	}
	let result = stm.File.result;
	let bsize = stm.Blob.BSIZE;
	let pos = stm.Blob.pos;

	if(stm.Blob.pos >= result.byteLength){
		return false;
	}
	state.status.phase = 'processing';
	if(pos+bsize > result.byteLength){
		bsize = result.byteLength - pos;
		stm.Blob.Z_SYNC_FLUSH = true;
	}
	let chunk = new Uint8Array(result, pos, bsize);
	stm.Blob.pos += chunk.length;
	stm.Zip.push(chunk, stm.Blob.Z_SYNC_FLUSH);

	if(stm.Blob.timer !== false || force){
		stm.Blob.timer = setTimeout(Continue,15);
	}

	return true;
}


async function syncCatalogue(){
	let url = state.config.catalogue.location;

	console.log('Fetching:' + url);
	state.status.phase = 'download';
	taskAdd();
	let response = await fetch(url);
	taskDone();
	if(response.status !== 200){
		post('error','Invalid File:'+response.status);
		Terminate();
		return;
	}
	taskAdd();
	let blob = await response.blob();
	taskDone();
	stm.File.readAsArrayBuffer(blob);
}


stm.Zip.onData = (chunk)=>{
	state.stm.Tar.push(chunk);
	// Progress report
	taskDone(state.stm.Zip.strm.next_in - stm.Zip.strm.last_in);
	stm.Zip.strm.last_in = state.stm.Zip.strm.next_in;
};


stm.Zip.onEnd = (status)=>{
	state.buffer.chunks.shift();
	state.status.phase = 'save';
};


stm.File.addEventListener('loadend',stmFileOnLoadEnd);
function stmFileOnLoadEnd(){
	state.status.phase = 'read';
	taskAdd(stm.File.result.byteLength);
	stm.Zip.strm.last_in = 0;
	Continue();
}


stm.Tar.addEventListener('file',stmTarOnLoadEnd);
function stmTarOnLoadEnd(e){
	if(e.detail.header.type.type !== 0){
		return;
	}
	console.log('File: ' + e.detail.header.name);
	let reader = new FileReaderSync();
	let result = reader.readAsText(e.detail.content);
	post("xml",{type:'xml',xml:result});
}

stm.Tar.addEventListener('done',stmTarOnDone);
function stmTarOnDone(e){
	Terminate();
}
