---
id: 971
title: My Wife Doesn't Want Me Around Anymore
date: 2011-02-01T00:00:08+00:00
author: Kavius
layout: post
categories:
  - Musings
---

I don't think  The Wife loves me anymore.She doesn't seem to want me around.

Every-time she's in the kitchen [baking sweat, succulent, tasty smelling bread](http://quiltingthefarm.vius.ca/2011/01/bagels/ " The Wife's Bagels. Mmmm...."), she yells at me and tells me to get out of the kitchen.

&#128519;

I'm just trying to be helpful &hellip;

Really.

That's all.

Honest.

![Whistling Smiley](/media/2011/01/WhistlingSmiley.gif)