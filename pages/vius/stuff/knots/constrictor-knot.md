---
id: 1531
title: Constrictor Knot
date: 2011-09-28T18:56:08+00:00
author: Kavius
layout: page
---

The first time I ever collected apples for cider pressing resulted in two 
problems: how do I pick them, and what do I put them in. I ended up noticing I 
had a bunch of chicken feed sacks laying about and decided they would make 
good bags. The only problem was I had no way to close the tops. A bit of 
research turned up the constrictor knot, a knot that gets really tight, but 
doesn't loosen up, exactly what is needed to close the top of a filled sack.

# Instructions

1. Hold the standing end between thumb and palm, with the palm facing you.
2. Wrap the line over the hand and behind the top two fingers and bring it back through the middle.
3. Wrap the line over the bottom two fingers and back behind the two fingers, and back through the middle. Pass the line back to your wrist. It is now the standing line.
4. Take the old standing end, and pass it between the middle fingers.
5. Take the loop over the top fingers, and pull it down over the bottom fingers.

Should be done. Now you can pull the loop over the top of the sack and pull 
the ends tight. This will hold the sack shut.


## Slip Constrictor

Unfortunately, the constrictor knot may become so tight that you have to cut it 
off (Gordian?<a href="#f-1">1</a>). There is a slip knot variation that allows 
you to get it undone if necessary.

To make the slip part, just pass a bight through your fingers rather than the 
end of the line. You should just be able to pull the loop out when it comes 
time to undo the lot.


<aside>
<h1>Footnotes</h1>
<ol>
  <li id="f-1">
    <p>
I know a lot of research has gone into potential Gordian knots, but could it 
be this simple? Though why a farmer would use a constrictor knot to tie his ox 
while at market is beyond me.
    </p>
  </li>
</ol>
</aside>
