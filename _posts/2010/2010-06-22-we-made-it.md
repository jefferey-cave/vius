---
id: 396
title: We Made It!
date: 2010-06-22T21:18:54+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=396
permalink: /2010/06/22/we-made-it/
categories:
  - Moving
---
After a long and exciting trip, we arrived in our new home province of Nova Scotia yesterday evening.