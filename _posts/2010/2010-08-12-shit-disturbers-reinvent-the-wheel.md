---
id: 544
title: Shit Disturbers Reinvent the Wheel
date: 2010-08-12T07:44:58+00:00
author: Kavius
layout: post
guid: http://www.vius.ca/?p=544
permalink: /2010/08/12/shit-disturbers-reinvent-the-wheel/
categories:
  - Opinions
---

Automated testing of systems is my pet peeve: I think every computer system
should have a series of tests that get run by another computer that test every
problem ever thought of. My current employer has asked me to start developing
a system just like this for their record keeping and delivery system.

Currently my company has purchased a third-party tool for automating control
of the software, unfortunately, the tool sucks. It is very difficult for
non-programmers to understand its round about logic (it's using screen scraper
triggered events), and has no mechanism for managing large numbers of scripts
(each one is managed in and of itself). When building testing systems, the
tests themselves tend to be easy to create, managing and tracking large
numbers of tests becomes the problem. Being a diligent employee (alright, a
diligent problem solver), I suggested it was possible to build our own tool
that was better able to be understood by non-programmers, and would allow us
to easier manage large numbers of tests.

# Reinventing the Wheel

That's when I heard it: <q>There's no point reinventing the wheel</q>.

I take exception this, I am encouraged to solve problems in the office and
invention is the key to doing this. I recognize that all problems have been
solved; we already have wheels. The only problem that ever exists is the need
to refine the general solution to the particular instance of the problem.

To get back to the analogy, if we had never reinvented the wheel we would
still be driving around on Wagon Wheels. I myself like having soft rubber
tires on my car.

Reinventions of the Wheel:

* [Wagon Wheels](http://en.wikipedia.org/wiki/File:Australian_cart.jpg)
* [Tweel](http://en.wikipedia.org/wiki/Tweel)
* [Locomotive Wheel](http://en.wikipedia.org/wiki/File:Steam_locomotive_driving_wheel.jpg)
* [Automotive Tire](http://en.wikipedia.org/wiki/File:Topazwheel.jpg)

In the end, we reinvent the wheel on a regular basis, not every wheel is
perfect for every vehicle. Similarily, when solving problems at work while
designing systems, it is sometimes necessary to build a custom component that
suites the needs of the problem. While not a total reinvention, they are a
design better suited to the problem at hand. To work around the foibles of the
existing technology, just because the technology already exists, is the kind
of short sightedness that leads to problems being ignored.

# Shit Disturber

Naturally, the moment I suggest all of this, I am accused of being a Shit
Disturber. When someone accuses me of being a Shit Disturber, I know I'm on
the right track. Let's break that term down; <q>shit</q> and <q>disturber</q>;
or a disturber of shit. In order for this to be true, there must be
<q>shit</q> to disturb. That I am being accused of being a Shit Disturber
forces my audience to acknowledge that there is in fact shit present. If there
is shit present that has been ignored and avoided; it may be more important to
ask questions like, <q>when does somebody intend to do something about the
shit, rather than ignore it?</q> This is usually the hardest part of
convincing people to change: getting them to acknowledge that there is a
problem which requires fixing.

Being a <q>Disturber of Shit</q> is not a bad thing. Just because you are
disturbing the shit, does not mean you put it there. If the shit is in the
middle of the road, we can either ignore the shit or do something about it.
Naturally, this causes some discomfort: people have got used to their path
around the shit; while it is being moved, the shit tends to stink; people have
a hard enough time cleaning their own shit; and the person that put the shit
there probably feels like shit for not cleaning it up in the first place. The
<dfn>Disturber</dfn> is just the person willing to do something about the
problem. The fact of the matter is, we can ignore problems for a long time, or
put up with the temporary discomfort of fixing them.

# Conclusion

Shit stinks and The Wheel turns; these are two truths of the world. Ignoring
them does not make them go away. In life we need to identify problems (shit),
find solutions (reinvent the wheel), and make the changes to enact those
solutions (disturb the shit). In the past, I have been both punished and
praised for taking drastic action to solve drastic problems (often regarding
the same problem and by the same person). While we may find change
uncomfortable, we should never turn away from those solutions.

So a tip of the hat to all those Shit Disturbers out there; may you always
keep finding ways to reinvent the wheel.
