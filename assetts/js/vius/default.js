'use strict';


import SiteMapNav from '/assetts/js/vius/widgets/sitemapNav.js';

import '/assetts/js/vius/widgets/WordChart.js';
import '/assetts/js/vius/widgets/pageTOC.js';

import '/assetts/js/vius/widgets/psProgress.js';


window.addEventListener('load',()=>{
	const siteMap = new SiteMapNav();
	siteMap.getChildren().then(function(children){
		let toc = Array.from(document.querySelectorAll('nav.childpages > ul'));
		toc.forEach((childpages)=>{
			children = children.map(function(loc){
				return '<li><a href="' + loc.url + '">'+loc.title+'</a></li>';
			});
			if(children.length === 0){
				children.push('<li class="dummy">&nbsp;</li>');
			}
			childpages.innerHTML = children.join('\n');
		});
	});
	siteMap.getPath().then(function(path){
		let pathLinks = document.querySelector('nav#breadcrumb > ol');
		path.unshift({title:'Home',url:'/'});
		pathLinks.innerHTML = path.map(function(loc){
			return '<li><a href="' + loc.url + '">'+loc.title+'</a></li>';
		}).join('\n');
	});



	let title = document.querySelector('body > header > h1');
	if(title){
		title.innerText = document.querySelector('head > title').innerText;
		title.innerHTML = '&#5809; ' + title.innerText;
	}

	// set the favicon
	let favicon = document.querySelector('head > link[rel="shortcut icon"] ');
	if(!favicon){
		favicon = document.createElement('link');
		document.head.append(favicon);
		favicon.rel = 'shortcut icon';
	}
	favicon.href="https://s.gravatar.com/avatar/b0fbe9582d6e6d4e815554789f434c81?s=32";

	// make the menu button do something
	let menus = Array.from(document.querySelectorAll('button.showhide'));
	menus.forEach((menu)=>{
		menu.dataset.visibleState = JSON.stringify(true);
		menu.addEventListener('click',(e)=>{
			let isVisible = !JSON.parse(e.target.dataset.visibleState);
			e.target.dataset.visibleState = JSON.stringify(isVisible);
			Array.from(e.target.parentElement.childNodes).forEach((d)=>{
				if(d === e.target) return;
				if(! d.style) return;

				if(isVisible){
					d.style.display = d.dataset.oldDisplay;
				}
				else{
					d.dataset.oldDisplay = d.style.display;
					d.style.display = 'none';
				}
			});
		});
		let click = new MouseEvent('click', {
				view: window,
				bubbles: true,
				cancelable: true
			});
		menu.dispatchEvent(click);
		menu.dispatchEvent(click);
	});
});


/**
 * wait for the page to load before starting everything
 */
window.addEventListener('load',function(){
	let title = document.querySelector('body > header > h1');
	if(title){
		title.innerText = document.querySelector('head > title').innerText;
		title.innerHTML = '&#5809; ' + title.innerText;
	}

	// set the favicon
	let favicon = document.querySelector('head > link[rel="shortcut icon"] ');
	if(!favicon){
		favicon = document.createElement('link');
		document.head.append(favicon);
		favicon.rel = 'shortcut icon';
	}
	favicon.href="https://s.gravatar.com/avatar/b0fbe9582d6e6d4e815554789f434c81?s=32";

	// make the menu button do something
	let menus = Array.from(document.querySelectorAll('button.showhide'));
	menus.forEach((menu)=>{
		menu.dataset.visibleState = JSON.stringify(true);
		menu.addEventListener('click',(e)=>{
			let isVisible = !JSON.parse(e.target.dataset.visibleState);
			e.target.dataset.visibleState = JSON.stringify(isVisible);
			Array.from(e.target.parentElement.childNodes).forEach((d)=>{
				if(d === e.target) return;
				if(! d.style) return;

				if(isVisible){
					d.style.display = d.dataset.oldDisplay;
				}
				else{
					d.dataset.oldDisplay = d.style.display;
					d.style.display = 'none';
				}
			});
		});
		let click = new MouseEvent('click', {
				view: window,
				bubbles: true,
				cancelable: true
			});
		menu.dispatchEvent(click);
		menu.dispatchEvent(click);
	});
});
