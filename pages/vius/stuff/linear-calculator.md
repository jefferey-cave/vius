---
id: 1973
title: Linear Calculator
date: 2012-07-18T14:33:25+00:00
author: Kavius
layout: page
guid: http://vius.ca/?page_id=1973
javascript:
  - |
    <div id="linearea">
    <div id="form">
    <table>
    <tr><th>Equation</th><td> <input id="equation" type="text" placeholder="Linear Equation" required value="Math.log(x)/-2.3+3.15" /></td></tr>
    <tr><th>Start</th><td> <input id="min" type="number" placeholder="Min to Graph X" required value="40" /></td></tr>
    <tr><th>End</th><td> <input id="max" type="number" placeholder="Max to Graph X" required value="300"/></td></tr>
    <tr><th>Step</th><td> <input id="step" type="number" placeholder="Calculation Step By" required value="1"/></td></tr>
    <tr><td> <button id="calc" type="button" disabled="disabled" onclick="Render();"/>Calculate</button></td></tr>
    </table>
    </div>
    <canvas id="chart" width="500" height="400"></canvas>
    <table id="table">
    <tr><th>X</th><th>Y</th></tr>
    </table>
    <script type="text/javascript">
    var inEq = document.getElementById("equation");
    var inMin = document.getElementById("min");
    var inMax =document.getElementById("max");
    var inStep = document.getElementById("step");
    var inCalc =document.getElementById("calc");
    var outChart =  document.getElementById("chart");
    var outTable =  document.getElementById("table");
    var context= outChart.getContext('2d');
    
    var min;
    var max;
    var step;
    var eq;
    
    function Render()
    {
    LoadParams();
    Validate();
    var rs = CreateRecordset();
    ///TODO: need to get it sorting
    //rs.sort(function(a,b){a.x>b.x?-1:1;});
    RenderTable(rs);
    RenderChart(rs);
    }
    
    function AddTableRow(rec)
    {
    var row = outTable.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.innerHTML = rec.x;
    var cell = row.insertCell(-1);
    cell.innerHTML = rec.y;
    }
    
    function RenderTable(rs)
    {
    while(outTable.rows.length > 1){
    outTable.rows.deleteRow(-1);
    }
    rs.forEach(AddTableRow);
    }
    
    function RenderChart(rs)
    {
    
    DrawYAxis(rs);
    DrawXAxis(rs);
    DrawLine(rs);
    }
    
    function DrawLine(rs)
    {
    var height = outChart.height-40;
    var width = outChart.width-40;
    var maxX=0;
    var maxY=0;
    
    maxX = rs[rs.length-1].x;
    rs.forEach(function(row){
    if(row.y > maxY){
    maxY = row.y;
    }
    });
    
    var yRatio = Math.floor(height/maxY);
    var xRatio = Math.floor(height/maxX);
    
    context.strokeStyle = '#000';
    context.lineWidth   = 1;
    
    context.moveTo(rs[0].x*xRatio+20,height-(rs[0].y*yRatio));
    rs.forEach(function(row){
    context.lineTo(row.x*xRatio+20,height-(row.y*yRatio));
    });
    context.stroke();
    }
    
    function DrawYAxis(rs)
    {
    var height;
    var tick;
    
    var maxY=0;
    
    rs.forEach(function(row){
    if(row.y > maxY){
    maxY = row.y;
    }
    });
    
    height = outChart.height-40;
    
    context.strokeStyle = '#000000'; // red
    context.lineWidth   = 1;
    
    context.moveTo(20,20);
    context.lineTo(20,height+20);
    context.stroke();
    
    var tickDist = Math.floor(height/maxY);
    if(tickDist < 10) tickDist = 10;
    for(var tick = height+20; tick>20; tick-=tickDist){
    context.moveTo(15,tick);
    context.lineTo(20,tick);
    context.stroke();
    }
    }
    
    function DrawXAxis(rs)
    {
    var height;
    var tick;
    
    var maxX=0;
    maxX = rs[rs.length-1].x;
    
    height = outChart.height-40;
    width = outChart.width-40;
    
    context.strokeStyle = '#000';
    context.lineWidth   = 1;
    
    context.moveTo(20,height+20);
    context.lineTo(width-20,height+20);
    context.stroke();
    
    var tickDist = Math.floor(width/maxX);
    if(tickDist < 10) tickDist = 10;
    for(var tick = width-20; tick>20; tick-=tickDist){
    context.moveTo(tick,height+25);
    context.lineTo(tick,height+20);
    context.stroke();
    }
    }
    
    function LoadParams()
    {
    min = parseInt(inMin.value);
    max = parseInt(inMax.value);
    step = parseInt(inStep.value);
    eq = inEq.value;
    }
    
    function Validate()
    {
    if(min > max){
    var swap = min;
    min = max;
    max = swap;
    }
    }
    
    function CreateRecordset()
    {
    Validate();
    var rs = new Array();
    var y;
    for(var x=min; x<=max; x+=step){
    y = eval(eq);
    rs.push({"x":x,"y":y});
    }
    return(rs);
    }
    
    inCalc.disabled = false;
    //Render();
    
    </script>
    </div>
    
  - |
    <div id="linearea">
    <div id="form">
    <table>
    <tr><th>Equation</th><td> <input id="equation" type="text" placeholder="Linear Equation" required value="Math.log(x)/-2.3+3.15" /></td></tr>
    <tr><th>Start</th><td> <input id="min" type="number" placeholder="Min to Graph X" required value="40" /></td></tr>
    <tr><th>End</th><td> <input id="max" type="number" placeholder="Max to Graph X" required value="300"/></td></tr>
    <tr><th>Step</th><td> <input id="step" type="number" placeholder="Calculation Step By" required value="1"/></td></tr>
    <tr><td> <button id="calc" type="button" disabled="disabled" onclick="Render();"/>Calculate</button></td></tr>
    </table>
    </div>
    <canvas id="chart" width="500" height="400"></canvas>
    <table id="table">
    <tr><th>X</th><th>Y</th></tr>
    </table>
    <script type="text/javascript">
    var inEq = document.getElementById("equation");
    var inMin = document.getElementById("min");
    var inMax =document.getElementById("max");
    var inStep = document.getElementById("step");
    var inCalc =document.getElementById("calc");
    var outChart =  document.getElementById("chart");
    var outTable =  document.getElementById("table");
    var context= outChart.getContext('2d');
    
    var min;
    var max;
    var step;
    var eq;
    
    function Render()
    {
    LoadParams();
    Validate();
    var rs = CreateRecordset();
    ///TODO: need to get it sorting
    //rs.sort(function(a,b){a.x>b.x?-1:1;});
    RenderTable(rs);
    RenderChart(rs);
    }
    
    function AddTableRow(rec)
    {
    var row = outTable.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.innerHTML = rec.x;
    var cell = row.insertCell(-1);
    cell.innerHTML = rec.y;
    }
    
    function RenderTable(rs)
    {
    while(outTable.rows.length > 1){
    outTable.rows.deleteRow(-1);
    }
    rs.forEach(AddTableRow);
    }
    
    function RenderChart(rs)
    {
    
    DrawYAxis(rs);
    DrawXAxis(rs);
    DrawLine(rs);
    }
    
    function DrawLine(rs)
    {
    var height = outChart.height-40;
    var width = outChart.width-40;
    var maxX=0;
    var maxY=0;
    
    maxX = rs[rs.length-1].x;
    rs.forEach(function(row){
    if(row.y > maxY){
    maxY = row.y;
    }
    });
    
    var yRatio = Math.floor(height/maxY);
    var xRatio = Math.floor(height/maxX);
    
    context.strokeStyle = '#000';
    context.lineWidth   = 1;
    
    context.moveTo(rs[0].x*xRatio+20,height-(rs[0].y*yRatio));
    rs.forEach(function(row){
    context.lineTo(row.x*xRatio+20,height-(row.y*yRatio));
    });
    context.stroke();
    }
    
    function DrawYAxis(rs)
    {
    var height;
    var tick;
    
    var maxY=0;
    
    rs.forEach(function(row){
    if(row.y > maxY){
    maxY = row.y;
    }
    });
    
    height = outChart.height-40;
    
    context.strokeStyle = '#000000'; // red
    context.lineWidth   = 1;
    
    context.moveTo(20,20);
    context.lineTo(20,height+20);
    context.stroke();
    
    var tickDist = Math.floor(height/maxY);
    if(tickDist < 10) tickDist = 10;
    for(var tick = height+20; tick>20; tick-=tickDist){
    context.moveTo(15,tick);
    context.lineTo(20,tick);
    context.stroke();
    }
    }
    
    function DrawXAxis(rs)
    {
    var height;
    var tick;
    
    var maxX=0;
    maxX = rs[rs.length-1].x;
    
    height = outChart.height-40;
    width = outChart.width-40;
    
    context.strokeStyle = '#000';
    context.lineWidth   = 1;
    
    context.moveTo(20,height+20);
    context.lineTo(width-20,height+20);
    context.stroke();
    
    var tickDist = Math.floor(width/maxX);
    if(tickDist < 10) tickDist = 10;
    for(var tick = width-20; tick>20; tick-=tickDist){
    context.moveTo(tick,height+25);
    context.lineTo(tick,height+20);
    context.stroke();
    }
    }
    
    function LoadParams()
    {
    min = parseInt(inMin.value);
    max = parseInt(inMax.value);
    step = parseInt(inStep.value);
    eq = inEq.value;
    }
    
    function Validate()
    {
    if(min > max){
    var swap = min;
    min = max;
    max = swap;
    }
    }
    
    function CreateRecordset()
    {
    Validate();
    var rs = new Array();
    var y;
    for(var x=min; x<=max; x+=step){
    y = eval(eq);
    rs.push({"x":x,"y":y});
    }
    return(rs);
    }
    
    inCalc.disabled = false;
    //Render();
    
    </script>
    </div>
    
---
[cf-shortcode plugin=</q>generic</q> field=</q>javascript</q>]