
class ByteHelper{
	static stringUTF8ToBytes(stringToConvert)
	{
		let bytes = [];

		for (let i = 0; i < stringToConvert.length; i++)
		{
			let byte = stringToConvert.charCodeAt(i);
			bytes.push(byte);
		}

		return bytes;
	}

	static bytesToStringUTF8(bytesToConvert)
	{
		let returnValue = "";

		for (let i = 0; i < bytesToConvert.length; i++)
		{
			let byte = bytesToConvert[i];
			let byteAsChar = String.fromCharCode(byte);
			returnValue += byteAsChar;
		}

		return returnValue;
	}
}
